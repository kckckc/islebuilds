### base
FROM node:20-slim AS base

ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable
RUN corepack prepare pnpm@latest-9 --activate

### build-deps
FROM base AS build-deps
WORKDIR /app

# copy lockfile and `pnpm fetch`
COPY pnpm-lock.yaml .
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm fetch --frozen-lockfile

# copy everything and `pnpm install`
COPY . .
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --offline

### build-server
FROM build-deps AS build-server
WORKDIR /app

# build server and `pnpm deploy` prod dependencies to /app/pruned
RUN pnpm --filter islebuilds-server run build
RUN pnpm --filter islebuilds-server deploy --prod /pruned

### server
FROM base AS server
WORKDIR /app

COPY --from=build-server /pruned .

ENV NODE_ENV production

EXPOSE 4000
CMD ["pnpm", "start"]

### build-web
FROM build-deps AS build-web
WORKDIR /app/web

ENV NEXT_TELEMETRY_DISABLED 1
RUN pnpm run build

### web
FROM base AS web
WORKDIR /app

ENV NODE_ENV production
ENV NEXT_TELEMETRY_DISABLED 1

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs

COPY --from=build-web --chown=nextjs:nodejs /app/web/.next/standalone ./
COPY --from=build-web --chown=nextjs:nodejs /app/web/.next/static ./web/.next/static
COPY --from=build-web --chown=nextjs:nodejs /app/web/public ./web/public

USER nextjs

EXPOSE 3000
ENV PORT 3000

WORKDIR /app/web

CMD HOSTNAME="0.0.0.0" node server.js
