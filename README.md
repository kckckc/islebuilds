# Islebuilds

Islebuilds is a WIP companion site for Isleward.

## Development

Server listens on :4000, web app listens on :3000 and forwards /api requests to :4000.

- Clone
- `cd server`, `yarn watch` and `yarn dev` (need both)
- `cd web && yarn dev`
- Run GraphQL codegen: `cd web && yarn gen`

## Notes

Misc

### Migrating
Generating a migration:
```
export POSTGRES_PORT=5002
export POSTGRES_HOST=XXX.XXX.XXX.XXX
npx typeorm migration:generate MigrationName -d dist/dataSource.js
```
Remember to move the generated file afterwards.
If we rename the file while the dev watcher is going, there will be old junk migrations in `dist/`.
MAKE SURE TO CLEAR DIST BEFORE ATTEMPTING TO APPLY MIGRATIONS.

To apply migrations:
- CLEAN DIST (maybe even just delete the whole thing)
- `yarn watch` to rebuild everything
- export the port and host like before to point to prod, then
```
npx typeorm migration:run -d dist/dataSource.js
```

### Roles

Permissions:

- MODERATION
- ADMIN (can add/remove moderators)
- * (can add/remove admins)

Use @Authenticated to require auth (throws error -> redirects to login; I don't think the redirect is implemented yet)

Use @Authorized for nullable fields or to check roles

Authorized requires all listed roles, * grants all, admin grants moderation

### Moderation model
- Incidents
- Can be associated with a user profile, sprite, character, etc
	- Incidents on a sub-object should also report the parent user?
- Can include title, description
- Includes user who reported
- Open/closed

### Env

Provide in `stack.env` for `docker-compose`, or `.env.local` in web and `.env` in server

```
# Server
SESSION_SECRET=secret for creating sessions in express
GOOGLE_CLIENT_ID=oauth client id
GOOGLE_CLIENT_SECRET=oauth secret
REDIS_HOST='islebuilds_redis' in docker-compose or localhost
POSTGRES_HOST='islebuilds_postgres' in docker-compose or localhost
POSTGRES_PORT=5432
CORS_ORIGIN=web url
DISCORD_WEBHOOK_REPORTS=discord webhook url

# Both?
APP_WEB_URL=redirect here after login
APP_API_URL=graphql/login endpoints
APP_API_URL_SERVER=graphql endpoint for SSR (http://islebuilds_api:4000 for docker-compose)

# Client
APP_MEASUREMENT_ID=GA measurement id
```
