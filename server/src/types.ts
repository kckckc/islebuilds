import type { Request, Response } from 'express';
import type { Redis } from 'ioredis';
// import { createUserLoader } from './utils/createUserLoader';

export type MyContext = {
	req: Request;
	res: Response;
	redis: Redis;
	myId: string | undefined;
	// userLoader: ReturnType<typeof createUserLoader>;
};

export type JsonValue =
	| string
	| number
	| boolean
	| null
	| JsonValue[]
	| { [key: string]: JsonValue };

declare module 'express-session' {
	export interface SessionData {
		returnTo?: string;
		passport: {
			user: string;
		};
	}
}

declare global {
	namespace Express {
		interface User {
			id?: string | undefined;
		}
	}
}

export enum CharacterSpirit {
	OWL = 'owl',
	BEAR = 'bear',
	LYNX = 'lynx',
}
