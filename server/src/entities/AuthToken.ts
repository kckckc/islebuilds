import { Field, ObjectType } from 'type-graphql';
import {
	BaseEntity,
	Column,
	CreateDateColumn,
	Entity,
	ManyToOne,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import { User } from './User';

@ObjectType()
@Entity()
export class AuthToken extends BaseEntity {
	@Field()
	@PrimaryGeneratedColumn('uuid')
	id!: string;

	@Field()
	@Column()
	label!: string;

	@Field()
	@Column({ unique: true })
	token!: string;

	@Field()
	@Column()
	userId!: string;

	@Field(() => User)
	@ManyToOne(() => User, (user) => user.authTokens)
	user!: Promise<User>; //lazy

	@Field(() => String)
	@CreateDateColumn()
	createdAt: Date;

	@Field(() => String)
	@UpdateDateColumn()
	updatedAt: Date;
}
