import { Field, ObjectType } from 'type-graphql';
import {
	BaseEntity,
	Column,
	CreateDateColumn,
	Entity,
	Index,
	ManyToOne,
	OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import { Incident } from './Incident';
import { User } from './User';

@ObjectType()
@Entity()
@Index('IDX_SPRITE_TRGM', { synchronize: false })
export class Sprite extends BaseEntity {
	@Field()
	@PrimaryGeneratedColumn('uuid')
	id!: string;

	@Field()
	@Column()
	name!: string;

	@Field()
	@Column()
	description!: string;

	@Field()
	@Column({ default: false })
	visible: boolean;

	@Field()
	@Column({ default: false })
	deleted: boolean;

	@Field()
	@Column()
	size: number;

	@Field()
	@Column()
	pixels: string;

	@Field()
	@Column()
	userId!: string;

	@Field(() => User)
	@ManyToOne(() => User, (user) => user.sprites)
	user!: Promise<User>;

	@OneToMany(() => Incident, (incident) => incident.reportedSprite)
	incidents: Incident[];

	@Field(() => String)
	@CreateDateColumn()
	createdAt!: Date;

	@Field(() => String)
	@UpdateDateColumn()
	updatedAt!: Date;
}
