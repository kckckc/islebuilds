import { Field, ObjectType } from 'type-graphql';
import {
	BaseEntity,
	Column,
	CreateDateColumn,
	Entity,
	ManyToOne,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import { User } from './User';

@ObjectType()
@Entity()
export class Inventory extends BaseEntity {
	@Field()
	@PrimaryGeneratedColumn('uuid')
	id!: string;

	@Field()
	@Column()
	name!: string;

	@Field()
	@Column()
	stash!: boolean;

	@Field()
	@Column({ default: false })
	visible!: boolean;

	@Column({ type: 'jsonb' })
	items!: string;

	@Field()
	@Column()
	userId!: string;

	@Field(() => User)
	@ManyToOne(() => User, (user) => user.characters)
	user!: Promise<User>;

	@Field(() => String)
	@Column()
	uploadedAt!: Date;

	@Field(() => String)
	@CreateDateColumn()
	createdAt!: Date;

	@Field(() => String)
	@UpdateDateColumn()
	updatedAt!: Date;
}
