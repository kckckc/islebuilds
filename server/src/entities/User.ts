import { Authorized, Field, ObjectType } from 'type-graphql';
import {
	BaseEntity,
	Column,
	CreateDateColumn,
	Entity,
	OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import { Character } from './Character';
import { FederatedCredential } from './FederatedCredential';
import { AuthToken } from './AuthToken';
import { Sprite } from './Sprite';
import { Incident } from './Incident';

@ObjectType()
@Entity()
export class User extends BaseEntity {
	@Field()
	@PrimaryGeneratedColumn('uuid')
	id!: string;

	@Field()
	@Column({ unique: true })
	displayName!: string;

	@Column({ default: false })
	displayNameChanged!: boolean;

	@Authorized('MODERATION')
	@Field(() => [String], { nullable: true })
	@Column('text', { array: true, default: '{}' })
	roles: string[];

	@OneToMany(() => Character, (character) => character.user)
	characters: Character[];

	@OneToMany(() => FederatedCredential, (cred) => cred.user)
	federatedCredentials: FederatedCredential[];

	@OneToMany(() => AuthToken, (token) => token.user)
	authTokens: AuthToken[];

	@OneToMany(() => Sprite, (sprite) => sprite.user)
	sprites: Sprite[];

	@OneToMany(() => Incident, (incident) => incident.reportedBy)
	reportedIncidents: Incident[];

	@OneToMany(() => Incident, (incident) => incident.reportedUser)
	incidents: Incident[];

	@Field(() => String)
	@CreateDateColumn()
	createdAt: Date;

	@Field(() => String)
	@UpdateDateColumn()
	updatedAt: Date;
}
