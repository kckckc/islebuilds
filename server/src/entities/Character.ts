import { Field, Int, ObjectType } from 'type-graphql';
import {
	BaseEntity,
	Column,
	CreateDateColumn,
	Entity,
	Index,
	ManyToOne,
	OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import { CharacterSpirit } from '../types';
import { Incident } from './Incident';
import { User } from './User';

@ObjectType()
@Entity()
@Index('IDX_CHARACTER_NAME_LOWER', { synchronize: false })
@Index('IDX_CHARACTER_NAME_TRGM', { synchronize: false })
export class Character extends BaseEntity {
	@Field()
	@PrimaryGeneratedColumn('uuid')
	id!: string;

	@Field()
	@Column({ unique: true })
	name!: string;

	@Field()
	@Column()
	league!: string;

	// Stats
	// xp?
	// gold?
	@Field()
	@Column()
	level!: number;

	@Field(() => String)
	@Column({
		type: 'enum',
		enum: CharacterSpirit,
	})
	spirit!: CharacterSpirit;

	@Field(() => [Int])
	@Column('int', { array: true })
	passives: number[];

	// Equipment
	@Column({ nullable: true, type: 'jsonb' })
	eq_head?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_neck?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_chest?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_hands?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_finger1?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_finger2?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_waist?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_legs?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_feet?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_trinket?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_oneHanded?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_offHand?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_tool?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_quickslot?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_rune1?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_rune2?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_rune3?: string;
	@Column({ nullable: true, type: 'jsonb' })
	eq_rune4?: string;

	// Visual
	@Field()
	@Column()
	portraitX: number;
	@Field()
	@Column()
	portraitY: number;
	@Field()
	@Column()
	skinId: string; // skinId is always set
	@Field({ nullable: true })
	@Column({ nullable: true })
	skinSheetName?: string; // not sent when mounted, so this is optional
	@Field({ nullable: true })
	@Column({ nullable: true })
	skinCell?: number; // also not sent when mounted

	// Prophecies
	@Field()
	@Column({ default: false })
	prophecy_austere!: boolean;
	@Field()
	@Column({ default: false })
	prophecy_butcher!: boolean;
	@Field()
	@Column({ default: false })
	prophecy_hardcore!: boolean;
	@Field()
	@Column({ default: false })
	prophecy_crushable!: boolean;

	// Reputation
	@Field({ nullable: true })
	@Column({ nullable: true })
	rep_gaekatla: number;
	@Field({ nullable: true })
	@Column({ nullable: true })
	rep_fjolgard: number;
	@Field({ nullable: true })
	@Column({ nullable: true })
	rep_akarei: number;
	@Field({ nullable: true })
	@Column({ nullable: true })
	rep_peopleOfTheSun: number;
	@Field({ nullable: true })
	@Column({ nullable: true })
	rep_pumpkinSailor: number;
	@Field({ nullable: true })
	@Column({ nullable: true })
	rep_theWinterMan: number;

	// @Field()
	// @Column({ default: false })
	// hideUser: boolean;

	@Field()
	@Column()
	userId!: string;

	@Field(() => User)
	@ManyToOne(() => User, (user) => user.characters)
	user!: Promise<User>;

	@OneToMany(() => Incident, (incident) => incident.reportedCharacter)
	incidents: Incident[];

	@Field(() => String)
	@Column()
	uploadedAt!: Date;

	@Field(() => String)
	@CreateDateColumn()
	createdAt!: Date;

	@Field(() => String)
	@UpdateDateColumn()
	updatedAt!: Date;
}
