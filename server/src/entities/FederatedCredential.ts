import {
	BaseEntity,
	Column,
	CreateDateColumn,
	Entity,
	ManyToOne,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import { User } from './User';

@Entity()
export class FederatedCredential extends BaseEntity {
	@PrimaryGeneratedColumn()
	id!: number;

	@Column({ nullable: true })
	displayName?: string;

	@Column({ nullable: true })
	email?: string;

	@Column()
	provider!: string;

	@Column()
	subject!: string;

	@Column({ nullable: true })
	accessToken?: string;

	@Column({ nullable: true })
	refreshToken?: string;

	@Column({ default: false })
	visible: boolean;

	@Column()
	userId!: string;

	@ManyToOne(() => User, (user) => user.federatedCredentials)
	user!: User;

	@CreateDateColumn()
	createdAt: Date;

	@UpdateDateColumn()
	updatedAt: Date;
}
