import { Field, ObjectType } from 'type-graphql';
import {
	BaseEntity,
	Column,
	CreateDateColumn,
	Entity,
	ManyToOne,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import { Character } from './Character';
import { Sprite } from './Sprite';
import { User } from './User';

@ObjectType()
@Entity()
export class Incident extends BaseEntity {
	@Field()
	@PrimaryGeneratedColumn('uuid')
	id!: string;

	@Field()
	@Column({ default: false })
	resolved: boolean;

	@Field({ nullable: true })
	@Column({ nullable: true })
	title?: string;

	@Field({ nullable: true })
	@Column({ nullable: true })
	description?: string;

	@Field({ nullable: true })
	@Column({ nullable: true })
	reportedById?: string;

	@Field(() => User, { nullable: true })
	@ManyToOne(() => User, (user) => user.reportedIncidents, { nullable: true })
	reportedBy?: Promise<User>; //lazy

	@Field({ nullable: true })
	@Column({ nullable: true })
	reportedUserId?: string;

	@Field(() => User, { nullable: true })
	@ManyToOne(() => User, (user) => user.incidents, { nullable: true })
	reportedUser?: Promise<User>; //lazy

	@Field()
	@Column({ default: false })
	reportedProfile!: boolean;

	@Field({ nullable: true })
	@Column({ nullable: true })
	reportedCharacterId?: string;

	@Field(() => Character, { nullable: true })
	@ManyToOne(() => Character, (character) => character.incidents, {
		nullable: true,
	})
	reportedCharacter?: Promise<Character>; //lazy

	@Field({ nullable: true })
	@Column({ nullable: true })
	reportedSpriteId?: string;

	@Field(() => Sprite, { nullable: true })
	@ManyToOne(() => Sprite, (sprite) => sprite.incidents, { nullable: true })
	reportedSprite?: Promise<Sprite>; //lazy

	@Field(() => String)
	@CreateDateColumn()
	createdAt: Date;

	@Field(() => String)
	@UpdateDateColumn()
	updatedAt: Date;
}
