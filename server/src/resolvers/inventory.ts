import {
	Arg,
	Ctx,
	FieldResolver,
	Mutation,
	Query,
	Resolver,
	Root,
} from 'type-graphql';
import { Character } from '../entities/Character';
import { Inventory } from '../entities/Inventory';
import { Authenticated } from '../middlewares/authenticated';
import { MyContext } from '../types';

@Resolver(Inventory)
export class InventoryResolver {
	@Query(() => [Inventory])
	myInventories(@Ctx() { myId }: MyContext) {
		if (!myId) return [];

		return Inventory.findBy({ userId: myId });
	}

	@FieldResolver(() => String, { nullable: true })
	items(@Root() inventory: Inventory): string | null {
		return JSON.stringify(inventory.items);
	}

	@FieldResolver(() => Character, { nullable: true })
	async character(@Root() inventory: Inventory): Promise<Character | null> {
		if (inventory.stash) {
			return null;
		}

		const character = await Character.findOneBy({ name: inventory.name });

		return character;
	}

	@Authenticated()
	@Mutation(() => Boolean)
	async setInventoryVisible(
		@Arg('inventory') inventoryId: string,
		@Arg('visible') visible: boolean,
		@Ctx() { myId }: MyContext
	): Promise<boolean> {
		const inventory = await Inventory.findOne({
			where: {
				userId: myId,
				id: inventoryId,
			},
		});

		if (!inventory) return false;

		await Inventory.update({ id: inventory.id }, { visible });

		return true;
	}
}
