import { Arg, Ctx, Mutation, Query, Resolver } from 'type-graphql';
import { AuthToken } from '../entities/AuthToken';
import { MyContext } from '../types';
import { nanoid } from 'nanoid';
import { Authenticated } from '../middlewares/authenticated';

@Resolver(AuthToken)
export class AuthTokenResolver {
	@Authenticated()
	@Mutation(() => AuthToken)
	async createAuthToken(
		@Arg('label') label: string,
		@Ctx() { myId }: MyContext
	): Promise<AuthToken> {
		if (label === '') {
			label = 'Token';
		}

		return AuthToken.create({
			label,
			token: nanoid(),
			userId: myId,
		}).save();
	}

	@Authenticated()
	@Mutation(() => Boolean)
	async revokeAuthToken(
		@Arg('id') id: string,
		@Ctx() { myId }: MyContext
	): Promise<Boolean> {
		const token = await AuthToken.findOneBy({
			id,
			userId: myId,
		});
		if (!token) return false;

		await token.remove();

		return true;
	}

	@Authenticated()
	@Query(() => [AuthToken])
	async authTokens(@Ctx() { myId }: MyContext): Promise<AuthToken[]> {
		return AuthToken.findBy({ userId: myId });
	}
}
