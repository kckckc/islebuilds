import { Field, ObjectType, Query, Resolver } from 'type-graphql';
import { Character } from '../entities/Character';
import { QueryCache } from '../QueryCache';

const RUNE_LOADOUTS_MAX_AGE = 1000 * 60 * 5; // 5 minutes
let cache: QueryCache<RuneLoadout[]> = new QueryCache(RUNE_LOADOUTS_MAX_AGE);

@ObjectType()
class RuneLoadout {
	@Field(() => [String])
	runes: string[];

	@Field()
	count: number;
}

@ObjectType()
class RuneLoadoutsResult {
	@Field(() => [RuneLoadout])
	loadouts: RuneLoadout[];

	@Field()
	lastUpdated: number;
}

@Resolver()
export class RuneLoadoutsResolver {
	@Query(() => RuneLoadoutsResult)
	async runeLoadouts(): Promise<RuneLoadoutsResult> {
		const cached = cache.fetch();
		if (cached) {
			return {
				loadouts: cached,
				lastUpdated: cache.lastUpdated,
			};
		}

		const data = (
			await Character.createQueryBuilder()
				.select('COUNT(1)', 'count')
				.addSelect(
					`ARRAY(SELECT UNNEST(ARRAY["eq_rune1"->>'name', "eq_rune2"->>'name', "eq_rune3"->>'name', "eq_rune4"->>'name']) ORDER BY 1)`,
					'runes'
				)
				.where('level=20')
				.groupBy('runes')
				.orderBy('count', 'DESC')
				.getRawMany()
		)
			.map(
				(raw): RuneLoadout => ({
					runes: raw.runes.filter((s: string | null) => s !== null),
					count: parseInt(raw.count),
				})
			)
			.filter((loadout) => loadout.runes.length === 4);

		cache.store(data);

		return {
			loadouts: data,
			lastUpdated: cache.lastUpdated,
		};
	}
}
