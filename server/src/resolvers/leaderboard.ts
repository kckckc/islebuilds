import { Resolver, Query, Arg, Field, ObjectType } from 'type-graphql';
import { Not, IsNull } from 'typeorm';
import { Character } from '../entities/Character';
import { QueryCache } from '../QueryCache';

const FACTION_LEADERBOARD_MAX_AGE = 1000 * 60 * 5;
export const ALLOWED_FACTIONS = [
	'gaekatla',
	'fjolgard',
	'akarei',
	'peopleOfTheSun',
	'pumpkinSailor',
	'theWinterMan',
];
const factionLeaderboardCaches: Record<string, QueryCache<Character[]>> = {};

export const getLeaderboards = () => {
	return factionLeaderboardCaches;
};

export const preloadLeaderboards = () => {
	ALLOWED_FACTIONS.forEach((f) => {
		fetchFactionLeaderboard(f);
	});
};

const fetchFactionLeaderboard = async (
	faction: string
): Promise<Character[]> => {
	const prop = `rep_${faction}`;

	const characters = await Character.find({
		order: {
			[prop]: 'DESC',
		},
		where: {
			[prop]: Not(IsNull()),
		},
		take: 100,
	});

	let cache = factionLeaderboardCaches[faction];
	if (!cache) {
		cache = new QueryCache(FACTION_LEADERBOARD_MAX_AGE);
		factionLeaderboardCaches[faction] = cache;
	}
	cache.store(characters);

	return characters;
};

@ObjectType()
class LeaderboardResult {
	@Field(() => [Character])
	characters: Character[];

	@Field()
	lastUpdated: number;

	@Field({ nullable: true })
	error?: boolean;
}

@Resolver()
export class LeaderboardResolver {
	@Query(() => LeaderboardResult)
	async factionLeaderboard(
		@Arg('faction') faction: string
	): Promise<LeaderboardResult> {
		// Validate faction name
		if (!ALLOWED_FACTIONS.includes(faction)) {
			return {
				error: true,
				characters: [],
				lastUpdated: 0,
			};
		}

		// Check cache
		let cache = factionLeaderboardCaches[faction];
		if (cache) {
			const cachedValue = cache.fetch();
			if (cachedValue) {
				return {
					characters: cachedValue,
					lastUpdated: cache.lastUpdated,
				};
			}
		}

		const characters = await fetchFactionLeaderboard(faction);

		return {
			characters,
			lastUpdated: cache.lastUpdated,
		};
	}
}
