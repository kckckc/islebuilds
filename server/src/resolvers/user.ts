import { nanoid } from 'nanoid';
import {
	Arg,
	Ctx,
	Field,
	FieldResolver,
	Mutation,
	ObjectType,
	Query,
	Resolver,
	Root,
} from 'type-graphql';
import { COOKIE_NAME, MERGE_PREFIX, OTP_PREFIX } from '../constants';
import { dataSource } from '../dataSource';
import { AuthToken } from '../entities/AuthToken';
import { Character } from '../entities/Character';
import { FederatedCredential } from '../entities/FederatedCredential';
import { User } from '../entities/User';
import { MyContext } from '../types';
import { FieldError } from '../util/FieldError';
import { validateDisplayName } from '../util/validateDisplayName';

@ObjectType()
class ChangeDisplayNameResponse {
	@Field(() => [FieldError], { nullable: true })
	errors?: FieldError[];

	@Field(() => Boolean, { nullable: true })
	success?: true;
}

@ObjectType()
class UserConnection {
	@Field()
	provider: string;

	@Field()
	visible: boolean;

	@Field({ nullable: true })
	displayName?: string;

	@Field({ nullable: true })
	email?: string;
}

@ObjectType()
class UserMergeInfo {
	@Field()
	current: User;

	@Field()
	other: User;
}

@Resolver(User)
export class UserResolver {
	@Query(() => User, { nullable: true })
	me(@Ctx() { myId }: MyContext) {
		// findOneBy({ id: undefined }) finds the first of any
		if (!myId) return null;

		return User.findOneBy({ id: myId });
	}

	@Query(() => User, { nullable: true })
	user(@Arg('id', () => String) id: string) {
		return User.findOneBy({ id });
	}

	@FieldResolver(() => Character, { nullable: true })
	async lastCharacter(@Root() user: User) {
		const char = await Character.findOne({
			where: {
				userId: user.id,
			},
			order: {
				uploadedAt: 'DESC',
			},
		});
		return char;
	}

	@FieldResolver(() => [UserConnection], { nullable: true })
	async connections(
		@Ctx() { myId }: MyContext,
		@Root() user: User
	): Promise<UserConnection[] | null> {
		const isSelf = myId === user.id;

		const creds = await FederatedCredential.find({
			where: { userId: user.id },
		});

		return creds
			.filter((c) => isSelf || c.visible)
			.map((c) => {
				return {
					provider: c.provider,
					visible: c.visible,
					displayName: c.displayName,
					email: isSelf ? c.email : undefined,
				};
			});
	}

	@Mutation(() => Boolean)
	async setConnectionVisible(
		@Arg('provider') provider: string,
		@Arg('visible') visible: boolean,
		@Ctx() { myId }: MyContext
	): Promise<boolean> {
		const cred = await FederatedCredential.findOne({
			where: {
				userId: myId,
				provider,
			},
		});

		if (!cred) return false;

		await FederatedCredential.update(
			{ id: cred.id },
			{
				visible,
			}
		);

		return true;
	}

	@Mutation(() => ChangeDisplayNameResponse)
	async changeDisplayName(
		@Arg('displayName') displayName: string,
		@Ctx() { myId }: MyContext
	): Promise<ChangeDisplayNameResponse> {
		const error = validateDisplayName(displayName);

		if (error) {
			return {
				errors: [
					{
						field: 'displayName',
						message: error,
					},
				],
			};
		}

		try {
			await User.update(
				{
					id: myId,
				},
				{
					displayName,
					displayNameChanged: true,
				}
			);
		} catch (e) {
			if (e?.code === '23505') {
				return {
					errors: [
						{
							field: 'displayName',
							message: 'name is already taken',
						},
					],
				};
			} else {
				return {
					errors: [
						{
							field: 'displayName',
							message: 'something went wrong',
						},
					],
				};
			}
		}

		return {
			success: true,
		};
	}

	@Query(() => UserMergeInfo)
	async getMergeInfo(
		@Arg('mergeId') mergeId: string,
		@Ctx() { myId, redis }: MyContext
	): Promise<UserMergeInfo | null> {
		const mergeInfo = await redis.get(MERGE_PREFIX + mergeId);

		if (!mergeInfo) {
			return null;
		}

		const parts = mergeInfo.split(':');
		const currentId = parts[0];
		const otherId = parts[1];

		if (myId !== currentId) {
			return null;
		}

		const current = await this.user(currentId);
		const other = await this.user(otherId);

		if (!current || !other) return null;

		return {
			current,
			other,
		};
	}

	@Mutation(() => Boolean)
	async performMerge(
		@Arg('mergeId') mergeId: string,
		@Arg('primaryId') primaryId: string,
		@Ctx() { myId, redis, req, res }: MyContext
	): Promise<Boolean> {
		const mergeInfo = await redis.get(MERGE_PREFIX + mergeId);

		if (!mergeInfo) {
			return false;
		}

		const parts = mergeInfo.split(':');
		const currentId = parts[0];
		const otherId = parts[1];

		if (myId !== currentId) {
			return false;
		}

		if (primaryId !== currentId && primaryId !== otherId) return false;

		const current = await this.user(currentId);
		const other = await this.user(otherId);

		if (!current || !other) return false;

		let primary, secondary: User;
		if (primaryId === currentId) {
			primary = current;
			secondary = other;
		} else {
			primary = other;
			secondary = current;
		}

		// Perform merge
		await redis.del(MERGE_PREFIX + mergeId);

		await dataSource
			.createQueryBuilder()
			.update(AuthToken)
			.set({
				userId: primary.id,
			})
			.where('userId = :id', { id: secondary.id })
			.execute();

		await dataSource
			.createQueryBuilder()
			.update(FederatedCredential)
			.set({
				userId: primary.id,
			})
			.where('userId = :id', { id: secondary.id })
			.execute();

		await dataSource
			.createQueryBuilder()
			.update(Character)
			.set({
				userId: primary.id,
			})
			.where('userId = :id', { id: secondary.id })
			.execute();

		await User.delete({
			id: secondary.id,
		});

		await new Promise<void>((resolve) => {
			req.session.destroy((err) => {
				res.clearCookie(COOKIE_NAME);

				if (err) {
					console.error(err);
					resolve();
					return;
				}

				resolve();
			});
		});

		return true;
	}

	@Mutation(() => String, { nullable: true })
	async generateOTP(
		@Arg('authTokenId') authTokenId: string,
		@Ctx() { myId, redis }: MyContext
	) {
		const authToken = await AuthToken.findOneBy({
			id: authTokenId,
			userId: myId,
		});

		if (!authToken) {
			return null;
		}

		const otp = nanoid();

		// 5 minutes
		redis.set(OTP_PREFIX + otp, authToken.token, 'EX', 60 * 5);

		return otp;
	}

	@Mutation(() => Boolean, { nullable: true })
	async logout(@Ctx() { req, res }: MyContext): Promise<Boolean> {
		return new Promise((resolve) => {
			req.session.destroy((err) => {
				res.clearCookie(COOKIE_NAME);

				if (err) {
					console.error(err);
					resolve(false);
					return;
				}

				resolve(true);
			});
		});
	}
}
