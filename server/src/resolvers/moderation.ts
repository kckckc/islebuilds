import { Arg, Ctx, Mutation } from 'type-graphql';
import { User } from '../entities/User';
import { checkRoles } from '../middlewares/authChecker';
import { MyContext } from '../types';

export class ModerationResolver {
	@Mutation(() => Boolean)
	async setUserRole(
		@Arg('user') userId: string,
		@Arg('role') role: string,
		@Arg('value') value: boolean,
		@Ctx() { myId }: MyContext
	): Promise<boolean> {
		const me = await User.findOne({ where: { id: myId } });
		if (!me) return false;

		// Validate role type
		if (role !== 'ADMIN' && role !== 'MODERATION') return false;

		// Managing admins requires *
		if (role === 'ADMIN' && !checkRoles(me?.roles ?? [], ['*']))
			return false;
		// Managing moderators requires admin
		if (role === 'MODERATION' && !checkRoles(me?.roles ?? [], ['ADMIN']))
			return false;

		if (value) {
			// Add role
			await User.createQueryBuilder('user')
				.update()
				.set({
					roles: () => `roles || '${role}'::text`,
				})
				.where('id = :userId AND NOT (roles @> ARRAY[:role]::text[])', {
					userId,
					role,
				})
				.execute();
		} else {
			// Remove role
			await User.createQueryBuilder('user')
				.update()
				.set({
					roles: () => `array_remove(roles, '${role}'::text)`,
				})
				.where('id = :userId', { userId })
				.execute();
		}

		return true;
	}
}
