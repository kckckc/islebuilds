import {
	Arg,
	Authorized,
	Ctx,
	Field,
	InputType,
	Mutation,
	Query,
	Resolver,
} from 'type-graphql';
import { FindManyOptions } from 'typeorm';
import { Character } from '../entities/Character';
import { Incident } from '../entities/Incident';
import { Sprite } from '../entities/Sprite';
import { Authenticated } from '../middlewares/authenticated';
import { MyContext } from '../types';
import { postReportWebhook } from '../util/postReportWebhook';

@InputType()
class SearchIncidentsInput {
	@Field()
	page: number;

	@Field()
	showResolved: boolean;

	@Field()
	showUnresolved: boolean;
}

@InputType()
class ReportDetailsInput {
	@Field()
	title?: string;

	@Field()
	description?: string;
}

@Resolver(Incident)
export class IncidentResolver {
	@Authorized('MODERATION')
	@Mutation(() => Boolean)
	async setIncidentResolved(
		@Arg('incident') incidentId: string,
		@Arg('resolved') resolved: boolean
	): Promise<boolean> {
		const incident = await Incident.findOne({
			where: {
				id: incidentId,
			},
		});

		if (!incident) return false;

		await Incident.update(
			{ id: incident.id },
			{
				resolved,
			}
		);

		return true;
	}

	@Authorized('MODERATION')
	@Query(() => Incident, { nullable: true })
	async incident(@Arg('id') id: string): Promise<Incident | null> {
		return await Incident.findOneBy({ id });
	}

	@Authorized('MODERATION')
	@Query(() => [Incident])
	async searchIncidents(
		@Arg('filter') filter: SearchIncidentsInput
	): Promise<Incident[]> {
		const options: FindManyOptions<Incident> = {
			order: {
				createdAt: 'DESC',
			},
			take: 25,
			skip: filter.page * 25,
		};

		if (!filter.showResolved && !filter.showUnresolved) {
			// Nothing selected
			return [];
		} else if (filter.showResolved && !filter.showUnresolved) {
			options.where = {
				resolved: true,
			};
		} else if (!filter.showResolved && filter.showUnresolved) {
			options.where = {
				resolved: false,
			};
		} else {
			// Don't filter
		}

		const incidents = await Incident.find(options);

		return incidents;
	}

	@Authenticated()
	@Mutation(() => Boolean)
	async reportUser(
		@Arg('target') targetId: string,
		@Arg('details') details: ReportDetailsInput,
		@Ctx() { myId }: MyContext
	): Promise<boolean> {
		if (!myId) return false;

		try {
			const result = await Incident.createQueryBuilder()
				.insert()
				.values({
					title: details.title,
					description: details.description,
					reportedById: myId,
					reportedUserId: targetId,
					reportedProfile: true,
				})
				.returning('*')
				.execute();

			if (result?.raw?.length) {
				const incident = Incident.create(result.raw[0]);
				await postReportWebhook(incident, 'Profile');

				return true;
			} else {
				return false;
			}
		} catch (e) {
			// Failed due to foreign key violation of target?

			console.log('Failed to submit report', e);

			return false;
		}
	}

	@Authenticated()
	@Mutation(() => Boolean)
	async reportCharacter(
		@Arg('target') targetId: string,
		@Arg('details') details: ReportDetailsInput,
		@Ctx() { myId }: MyContext
	): Promise<boolean> {
		if (!myId) return false;

		try {
			const character = await Character.findOne({
				where: {
					id: targetId,
				},
			});
			if (!character) return false;

			const result = await Incident.createQueryBuilder()
				.insert()
				.values({
					title: details.title,
					description: details.description,
					reportedById: myId,
					reportedUserId: character.userId,
					reportedCharacterId: targetId,
				})
				.returning('*')
				.execute();

			if (result?.raw?.length) {
				const incident = Incident.create(result.raw[0]);
				await postReportWebhook(incident, 'Character');

				return true;
			} else {
				return false;
			}
		} catch (e) {
			// Failed due to foreign key violation of target?

			console.log('Failed to submit report', e);

			return false;
		}
	}

	@Authenticated()
	@Mutation(() => Boolean)
	async reportSprite(
		@Arg('target') targetId: string,
		@Arg('details') details: ReportDetailsInput,
		@Ctx() { myId }: MyContext
	): Promise<boolean> {
		if (!myId) return false;

		try {
			const sprite = await Sprite.findOne({
				where: {
					id: targetId,
				},
			});
			if (!sprite) return false;

			const result = await Incident.createQueryBuilder()
				.insert()
				.values({
					title: details.title,
					description: details.description,
					reportedById: myId,
					reportedUserId: sprite.userId,
					reportedSpriteId: targetId,
				})
				.returning('*')
				.execute();

			if (result?.raw?.length) {
				const incident = Incident.create(result.raw[0]);
				await postReportWebhook(incident, 'Sprite');

				return true;
			} else {
				return false;
			}
		} catch (e) {
			// Failed due to foreign key violation of target?

			console.log('Failed to submit report', e);

			return false;
		}
	}
}
