import {
	Arg,
	Ctx,
	Field,
	InputType,
	Int,
	Mutation,
	ObjectType,
	Query,
	Resolver,
} from 'type-graphql';
import { dataSource } from '../dataSource';
import { Sprite } from '../entities/Sprite';
import { User } from '../entities/User';
import { checkRoles } from '../middlewares/authChecker';
import { Authenticated } from '../middlewares/authenticated';
import { MyContext } from '../types';
import { validateSprite } from '../util/validateSprite';

const MAX_PER_REQUEST = 40;

@InputType()
class SpritesFilters {
	@Field({ nullable: true })
	userId?: string;

	// @Field(() => CharactersSortBy, { nullable: true })
	// sortBy?: CharactersSortBy;
}

@InputType()
class CursorPaginatedSpritesInput {
	@Field(() => Int, { nullable: true })
	limit?: number;

	@Field(() => String, { nullable: true })
	cursor?: string;

	@Field(() => SpritesFilters, { nullable: true })
	filter?: SpritesFilters;
}

@ObjectType()
class CursorPaginatedSprites {
	@Field(() => [Sprite])
	sprites: Sprite[];

	@Field({ nullable: true })
	cursor?: string;

	@Field()
	hasMore: boolean;
}

@InputType()
class OffsetPaginatedSpritesInput {
	@Field(() => Int, { nullable: true })
	limit?: number;

	@Field(() => Int, { nullable: true })
	offset?: number;

	@Field(() => SpritesFilters, { nullable: true })
	filter?: SpritesFilters;
}

@ObjectType()
class OffsetPaginatedSprites {
	@Field(() => [Sprite])
	sprites: Sprite[];

	// Call this cursor because if they use it as an input it should continue
	@Field({ nullable: true })
	cursor?: number;

	@Field()
	hasMore: boolean;
}

@InputType()
class PostSpriteInput {
	@Field()
	name: string;

	@Field()
	description: string;

	@Field()
	visible: boolean;

	@Field()
	size: number;

	@Field()
	pixels: string;
}

@Resolver(Sprite)
export class SpriteResolver {
	@Query(() => Sprite, { nullable: true })
	async spriteById(
		@Arg('id') id: string,
		@Ctx() { myId }: MyContext
	): Promise<Sprite | null> {
		let canBypassVisibility = false;
		if (myId) {
			const me = await User.findOneBy({ id: myId });

			if (me && checkRoles(me.roles, ['MODERATION'])) {
				canBypassVisibility = true;
			}
		}

		const query = Sprite.createQueryBuilder('sprite');

		if (!canBypassVisibility) {
			query.where(
				'(sprite.id = :id) AND ((sprite.visible) OR (sprite.userId = :myId)) AND (NOT sprite.deleted)',
				{ id, myId }
			);
		} else {
			query.where('sprite.id = :id', { id, myId });
		}

		return query.getOne();
	}

	@Query(() => CursorPaginatedSprites)
	async recentSprites(
		@Arg('input', () => CursorPaginatedSpritesInput)
		input: CursorPaginatedSpritesInput,
		@Ctx() { myId }: MyContext
	) {
		const { limit, cursor, filter } = input;

		// Page calculations
		const perPage = Math.max(Math.min(MAX_PER_REQUEST, limit ?? 5), 0);
		const numFetched = perPage + 1;

		const query = Sprite.createQueryBuilder('sprite');

		// Cursor pagination
		if (cursor) {
			query.where('sprite.updatedAt < :cursor', {
				cursor: new Date(parseInt(cursor)),
			});
		}

		// Visibility
		query.andWhere(
			'((sprite.visible) OR (sprite.userId = :id)) AND (NOT sprite.deleted)',
			{
				id: myId,
			}
		);

		// Filters
		if (filter?.userId) {
			query.andWhere('sprite.userId = :id', { id: filter.userId });
		}

		// Order
		query.orderBy('sprite.createdAt', 'DESC');

		// Query
		query.limit(numFetched);
		const sprites = await query.getMany();

		// Check for more results and prepare returned cursor
		const lastSprite = sprites[sprites.length - 2];
		const hasMore = sprites.length === numFetched;

		return {
			cursor: hasMore ? lastSprite?.updatedAt : undefined,
			hasMore,
			sprites: sprites.slice(0, perPage),
		};
	}

	@Query(() => OffsetPaginatedSprites)
	async sprites(
		@Arg('input', () => OffsetPaginatedSpritesInput)
		input: OffsetPaginatedSpritesInput,
		@Ctx() { myId }: MyContext
	): Promise<OffsetPaginatedSprites> {
		const { limit, offset, filter } = input;

		// Page calculations
		const perPage = Math.max(Math.min(MAX_PER_REQUEST, limit ?? 5), 0);
		const numFetched = perPage + 1;

		const query = Sprite.createQueryBuilder('sprite');

		// Visibility
		query.where(
			'((sprite.visible) OR (sprite.userId = :myId)) AND (NOT sprite.deleted)',
			{
				myId,
			}
		);

		// Filters
		if (filter?.userId) {
			query.andWhere('sprite.userId = :id', { id: filter.userId });
		}

		// Order by
		// const order: FindOptionsOrder<Sprite> = {};
		// const sortBy = filter?.sortBy ?? CharactersSortBy.Name;
		// if (sortBy === CharactersSortBy.Level) {
		// 	order.level = 'DESC';
		// } else {
		// 	// Includes CharactersSortBy.Name
		// 	order.name = 'ASC';
		// }

		// Query
		query.limit(numFetched);
		query.offset(offset ?? 0);
		const sprites = await query.getMany();

		// Check for more results and prepare returned cursor
		const hasMore = sprites.length === numFetched;

		return {
			cursor: (offset ?? 0) + perPage,
			hasMore,
			sprites: sprites.slice(0, perPage),
		};
	}

	@Query(() => [Sprite])
	async searchSprites(
		@Arg('search') search: string,
		@Ctx() { myId }: MyContext
	): Promise<Sprite[]> {
		const res = await dataSource
			.createQueryBuilder()
			.select('sprite')
			.from(Sprite, 'sprite')
			.where(
				'((sprite.name % :search) OR (sprite.description % :search2)) AND (sprite.visible OR sprite.userId = :myId) AND (NOT sprite.deleted)',
				{ search: search, search2: search, myId }
			)
			.addOrderBy(
				'(SELECT MAX(v) FROM (VALUES (similarity(sprite.name, :search)), (similarity(sprite.description, :search))) AS value(v))',
				'DESC'
			)
			.addOrderBy('sprite.name')
			.limit(25)
			.getMany();

		return res;
	}

	@Authenticated()
	@Mutation(() => Boolean)
	async postSprite(
		@Arg('input') input: PostSpriteInput,
		@Ctx() { myId }: MyContext
	): Promise<boolean> {
		if (input.size !== 8 && input.size !== 16 && input.size !== 24) {
			return false;
		}

		if (input.name.length > 50 || input.description.length > 300) {
			return false;
		}

		try {
			validateSprite(input.size, input.pixels);
		} catch (e) {
			return false;
		}

		await Sprite.insert({
			name: input.name,
			description: input.description,
			visible: input.visible,
			size: input.size,
			pixels: input.pixels,
			userId: myId,
		});

		return true;
	}

	@Authenticated()
	@Mutation(() => Boolean)
	async setSpriteVisible(
		@Arg('sprite') spriteId: string,
		@Arg('visible') visible: boolean,
		@Ctx() { myId }: MyContext
	): Promise<boolean> {
		const sprite = await Sprite.findOne({
			where: {
				userId: myId,
				id: spriteId,
				deleted: false,
			},
		});

		if (!sprite) return false;

		await Sprite.update(
			{ id: sprite.id },
			{
				visible,
			}
		);

		return true;
	}

	@Authenticated()
	@Mutation(() => Boolean)
	async setSpriteDeleted(
		@Arg('sprite') spriteId: string,
		@Arg('deleted') deleted: boolean,
		@Ctx() { myId }: MyContext
	): Promise<boolean> {
		if (!myId) return false;

		let isModerator = false;
		const me = await User.findOneBy({ id: myId });
		if (me && checkRoles(me.roles, ['MODERATION'])) {
			isModerator = true;
		}

		// Only moderators can undelete
		if (!deleted && !isModerator) {
			return false;
		}

		const where = isModerator
			? { id: spriteId }
			: { userId: myId, id: spriteId };

		await Sprite.update(where, {
			deleted,
		});

		return true;
	}
}
