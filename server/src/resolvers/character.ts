import {
	Arg,
	Ctx,
	Field,
	FieldResolver,
	InputType,
	Int,
	Mutation,
	ObjectType,
	Query,
	registerEnumType,
	Resolver,
	Root,
} from 'type-graphql';
import { FindOptionsOrder, FindOptionsWhere, LessThan } from 'typeorm';
import { dataSource } from '../dataSource';
import { Character } from '../entities/Character';
import { Inventory } from '../entities/Inventory';
import { Authenticated } from '../middlewares/authenticated';
import { MyContext } from '../types';
import { ALLOWED_FACTIONS, getLeaderboards } from './leaderboard';

const PAGE_MAX_CHARACTERS = 100;

enum CharactersSortBy {
	Name,
	Level,
}
registerEnumType(CharactersSortBy, {
	name: 'CharactersSortBy',
	description: 'Sorting orders for paginated character lists.',
});

@InputType()
class CharactersFilters {
	@Field({ nullable: true })
	userId?: string;

	@Field(() => CharactersSortBy, { nullable: true })
	sortBy?: CharactersSortBy;

	@Field(() => [String], { nullable: true })
	runes?: string[];

	@Field({ nullable: true })
	level?: number;
}

@InputType()
class CursorPaginatedCharactersInput {
	@Field(() => Int, { nullable: true })
	limit?: number;

	@Field(() => String, { nullable: true })
	cursor?: string;

	@Field(() => CharactersFilters, { nullable: true })
	filter?: CharactersFilters;
}

@ObjectType()
class CursorPaginatedCharacters {
	@Field(() => [Character])
	characters: Character[];

	@Field({ nullable: true })
	cursor?: string;

	@Field()
	hasMore: boolean;
}

@InputType()
class OffsetPaginatedCharactersInput {
	@Field(() => Int, { nullable: true })
	limit?: number;

	@Field(() => Int, { nullable: true })
	offset?: number;

	@Field(() => CharactersFilters, { nullable: true })
	filter?: CharactersFilters;
}

@ObjectType()
class OffsetPaginatedCharacters {
	@Field(() => [Character])
	characters: Character[];

	// Call this cursor because if they use it as an input it should continue
	@Field({ nullable: true })
	cursor?: number;

	@Field()
	hasMore: boolean;
}

@ObjectType()
class CharacterRanking {
	@Field()
	leaderboard: string;

	@Field()
	rank: number;
}

@Resolver(Character)
export class CharacterResolver {
	@Query(() => Character, { nullable: true })
	characterById(@Arg('id') id: string): Promise<Character | null> {
		return Character.findOneBy({ id });
	}

	@Query(() => Character, { nullable: true })
	character(@Arg('name') name: string): Promise<Character | null> {
		return Character.createQueryBuilder()
			.where('LOWER(name) = LOWER(:search)', { search: name })
			.getOne();
	}

	@Authenticated()
	@Mutation(() => Boolean)
	async deleteCharacter(
		@Arg('character') characterId: string,
		@Ctx() { myId }: MyContext
	): Promise<Boolean> {
		const character = await Character.findOneBy({
			id: characterId,
			userId: myId,
		});
		if (!character) return false;

		await character.remove();

		return true;
	}

	@Query(() => CursorPaginatedCharacters)
	async recentCharacters(
		@Arg('input', () => CursorPaginatedCharactersInput)
		input: CursorPaginatedCharactersInput
	) {
		const { limit, cursor, filter } = input;

		// Page calculations
		const perPage = Math.max(Math.min(PAGE_MAX_CHARACTERS, limit ?? 10), 0);
		const numFetched = perPage + 1;

		const where: FindOptionsWhere<Character> = {};
		// Cursor pagination
		if (cursor) {
			where.updatedAt = LessThan(new Date(parseInt(cursor)));
		}

		// Filters
		// TODO: not all filters implemented here
		if (filter?.userId) {
			where.userId = filter.userId;
		}

		// Order
		const order: FindOptionsOrder<Character> = {};
		order.uploadedAt = 'DESC';

		// Query
		const characters = await Character.find({
			where,
			order,
			take: numFetched,
		});

		// Check for more results and prepare returned cursor
		const lastCharacter = characters[characters.length - 2];
		const hasMore = characters.length === numFetched;

		return {
			cursor: hasMore ? lastCharacter?.updatedAt : undefined,
			hasMore,
			characters: characters.slice(0, perPage),
		};
	}

	@Query(() => OffsetPaginatedCharacters)
	async characters(
		@Arg('input', () => OffsetPaginatedCharactersInput)
		input: OffsetPaginatedCharactersInput
	): Promise<OffsetPaginatedCharacters> {
		const { limit, offset, filter } = input;

		const query = Character.createQueryBuilder().select();

		// Page calculations
		const perPage = Math.max(Math.min(PAGE_MAX_CHARACTERS, limit ?? 10), 0);
		const numFetched = perPage + 1;

		query.take(numFetched);
		query.skip(offset ?? 0);

		// Filters
		if (filter?.userId) {
			query.andWhere('"userId" = :userId', { userId: filter.userId });
		}
		if (filter?.runes) {
			const filterRunes = filter.runes.slice(0);
			if (filter.runes.some((r) => !r.match(/^[A-Za-z ]+$/))) {
				throw new Error('invalid filter');
			}
			filterRunes.sort();

			const uniqueRunes = filterRunes.filter(
				(v, i, a) => a.indexOf(v) === i
			);

			query.andWhere(
				`("eq_rune1"->>'name' = ANY(:uniqueRunes)) AND ("eq_rune2"->>'name' = ANY(:uniqueRunes)) AND ("eq_rune3"->>'name' = ANY(:uniqueRunes)) AND ("eq_rune4"->>'name' = ANY(:uniqueRunes))`,
				{ uniqueRunes }
			);
			query.andWhere(
				`ARRAY(SELECT UNNEST(ARRAY["eq_rune1"->>'name', "eq_rune2"->>'name', "eq_rune3"->>'name', "eq_rune4"->>'name']) ORDER BY 1) = :filterRunes`,
				{ filterRunes }
			);
		}
		if (typeof filter?.level === 'number') {
			query.andWhere('"level" = :level', { level: filter.level });
		}

		// Order by
		const sortBy = filter?.sortBy ?? CharactersSortBy.Name;
		if (sortBy === CharactersSortBy.Level) {
			query.addOrderBy('level', 'DESC');
			query.addOrderBy('name', 'ASC'); // Secondary order since levels can be equal
		} else {
			query.addOrderBy('name', 'ASC');
		}

		// Query
		const characters = await query.getMany();

		// Check for more results and prepare returned cursor
		const hasMore = characters.length === numFetched;

		return {
			cursor: (offset ?? 0) + perPage,
			hasMore,
			characters: characters.slice(0, perPage),
		};
	}

	@Query(() => [Character])
	async searchCharacters(@Arg('name') name: string): Promise<Character[]> {
		const res = await dataSource
			.createQueryBuilder()
			.select('character')
			.from(Character, 'character')
			.where('character.name % :search', { search: name })
			.addOrderBy('similarity(character.name, :search)', 'DESC')
			.addOrderBy('character.name')
			.limit(25)
			.getMany();

		return res;
	}

	// Get character's places on leaderboard
	@FieldResolver(() => [CharacterRanking])
	rankings(@Root() character: Character): CharacterRanking[] {
		const rankings: CharacterRanking[] = [];

		ALLOWED_FACTIONS.forEach((factionId) => {
			const data = getLeaderboards()[factionId];

			const rank = data.data?.findIndex((c) => c.id === character.id);

			if (typeof rank === 'number' && rank > -1) {
				rankings.push({
					leaderboard: factionId,
					rank: rank + 1,
				});
			}
		});

		return rankings;
	}

	// Check if character is owned by us
	@FieldResolver(() => Boolean)
	isMine(@Ctx() { myId }: MyContext, @Root() character: Character): boolean {
		return !!myId && myId === character.userId;
	}

	// Get inventory for character
	@FieldResolver(() => Inventory, { nullable: true })
	async inventory(
		@Ctx() { myId }: MyContext,
		@Root() character: Character
	): Promise<Inventory | null> {
		const inventory = await Inventory.findOne({
			where: {
				name: character.name,
				stash: false,
			},
		});

		// Must be the uploader of the character or the inventory must be visible
		const isOwner = myId && myId === character.userId;
		if (isOwner || inventory?.visible) {
			return inventory;
		} else {
			return null;
		}
	}

	// Stringify JSONB columns for client

	@FieldResolver(() => String, { nullable: true })
	eq_head(@Root() character: Character): string | null {
		return character?.eq_head ? JSON.stringify(character.eq_head) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_neck(@Root() character: Character): string | null {
		return character?.eq_neck ? JSON.stringify(character.eq_neck) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_chest(@Root() character: Character): string | null {
		return character?.eq_chest ? JSON.stringify(character.eq_chest) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_hands(@Root() character: Character): string | null {
		return character?.eq_hands ? JSON.stringify(character.eq_hands) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_finger1(@Root() character: Character): string | null {
		return character?.eq_finger1
			? JSON.stringify(character.eq_finger1)
			: null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_finger2(@Root() character: Character): string | null {
		return character?.eq_finger2
			? JSON.stringify(character.eq_finger2)
			: null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_waist(@Root() character: Character): string | null {
		return character?.eq_waist ? JSON.stringify(character.eq_waist) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_legs(@Root() character: Character): string | null {
		return character?.eq_legs ? JSON.stringify(character.eq_legs) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_feet(@Root() character: Character): string | null {
		return character?.eq_feet ? JSON.stringify(character.eq_feet) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_trinket(@Root() character: Character): string | null {
		return character?.eq_trinket
			? JSON.stringify(character.eq_trinket)
			: null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_oneHanded(@Root() character: Character): string | null {
		return character?.eq_oneHanded
			? JSON.stringify(character.eq_oneHanded)
			: null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_offHand(@Root() character: Character): string | null {
		return character?.eq_offHand
			? JSON.stringify(character.eq_offHand)
			: null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_tool(@Root() character: Character): string | null {
		return character?.eq_tool ? JSON.stringify(character.eq_tool) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_quickslot(@Root() character: Character): string | null {
		return character?.eq_quickslot
			? JSON.stringify(character.eq_quickslot)
			: null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_rune1(@Root() character: Character): string | null {
		return character?.eq_rune1 ? JSON.stringify(character.eq_rune1) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_rune2(@Root() character: Character): string | null {
		return character?.eq_rune2 ? JSON.stringify(character.eq_rune2) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_rune3(@Root() character: Character): string | null {
		return character?.eq_rune3 ? JSON.stringify(character.eq_rune3) : null;
	}
	@FieldResolver(() => String, { nullable: true })
	eq_rune4(@Root() character: Character): string | null {
		return character?.eq_rune4 ? JSON.stringify(character.eq_rune4) : null;
	}
}
