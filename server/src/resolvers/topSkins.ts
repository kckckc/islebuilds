import { Field, ObjectType, Query, Resolver } from 'type-graphql';
import { Character } from '../entities/Character';
import { QueryCache } from '../QueryCache';

const TOP_SKINS_MAX_AGE = 1000 * 60 * 5;
let cache: QueryCache<SkinRanking[]> | null = null;

@ObjectType()
class SkinRanking {
	@Field()
	id: string;

	@Field({ nullable: true })
	sheetName?: string;

	@Field({ nullable: true })
	cell?: number;

	@Field()
	count: number;
}

@ObjectType()
class TopSkinsResult {
	@Field(() => [SkinRanking])
	skins: SkinRanking[];

	@Field()
	lastUpdated: number;
}

@Resolver()
export class TopSkinsResolver {
	@Query(() => TopSkinsResult)
	async topSkins(): Promise<TopSkinsResult> {
		if (cache) {
			const data = cache.fetch();

			if (data) {
				return {
					skins: data,
					lastUpdated: cache.lastUpdated,
				};
			}
		}

		// Sometimes, the sheetname/cell will be different (because of mounts)
		// On the client end, group by skinId and take the most common sheetname/cell combo as the sprite

		const newData: SkinRanking[] = (
			await Character.createQueryBuilder()
				.select('"skinId", "skinSheetName", "skinCell", COUNT(*)')
				// .where('"skinSheetName" NOT ILIKE \'%mount%\'')
				.addGroupBy('"skinId", "skinSheetName", "skinCell"')
				.getRawMany()
		).map(
			(raw): SkinRanking => ({
				id: raw.skinId,
				sheetName: raw.skinSheetName,
				cell: raw.skinCell,
				count: parseInt(raw.count),
			})
		);

		if (!cache) {
			cache = new QueryCache<SkinRanking[]>(TOP_SKINS_MAX_AGE);
		}
		cache.store(newData);

		return {
			skins: newData,
			lastUpdated: cache.lastUpdated,
		};
	}
}
