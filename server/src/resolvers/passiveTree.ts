import {
	Arg,
	Field,
	InputType,
	Int,
	ObjectType,
	Query,
	Resolver,
} from 'type-graphql';
import { dataSource } from '../dataSource';
import { Character } from '../entities/Character';
import { QueryCache } from '../QueryCache';

// Config
const CACHE_MAX_AGE_MS = 1000 * 60 * 5;
const CACHE_LIMIT_MAX = 25;

@InputType()
class PaginatedPassiveTreesInput {
	@Field(() => Int, { nullable: true })
	limit?: number;

	@Field(() => Int, { nullable: true })
	offset?: number;

	@Field(() => String, { nullable: true })
	spirit?: string;
}

@ObjectType()
class PaginatedPassiveTrees {
	@Field(() => [PassiveTreeWithCount], { nullable: true })
	passiveTrees?: PassiveTreeWithCount[];

	@Field({ nullable: true })
	hasMore?: boolean;

	@Field({ nullable: true })
	lastUpdated?: number;

	@Field({ nullable: true })
	error?: boolean;
}

@ObjectType()
class PassiveTreeWithCount {
	@Field(() => [Int])
	passives: number[];

	@Field(() => Int)
	count: number;
}

type SortedPassivesData = {
	sorted_passives: number[];
	count: string;
}[];

const passivesCache: QueryCache<SortedPassivesData> = new QueryCache(
	CACHE_MAX_AGE_MS
);
const filteredCache: {
	owl: SortedPassivesData;
	bear: SortedPassivesData;
	lynx: SortedPassivesData;
} = {
	owl: [],
	bear: [],
	lynx: [],
};

const fetchSortedPassives = async () => {
	// Query
	const res: SortedPassivesData = await dataSource
		.createQueryBuilder()
		.select(
			'array(SELECT unnest(character.passives) ORDER BY 1) as sorted_passives, COUNT(*)'
		)
		.from(Character, 'character')
		.where('array_length(character.passives, 1) = 21')
		.groupBy('sorted_passives')
		.orderBy('count', 'DESC')
		.getRawMany();

	// Store in cache w/ age
	passivesCache.store(res);

	// Update filtered versions
	const owl = [];
	const bear = [];
	const lynx = [];
	for (let i = 0; i < res.length; i++) {
		const tree = res[i];
		if (tree.sorted_passives.includes(80)) {
			owl.push(tree);
		}
		if (tree.sorted_passives.includes(104)) {
			bear.push(tree);
		}
		if (tree.sorted_passives.includes(105)) {
			lynx.push(tree);
		}
	}

	filteredCache.owl = owl;
	filteredCache.bear = bear;
	filteredCache.lynx = lynx;

	return res;
};

@Resolver()
export class PassiveTreeResolver {
	@Query(() => PaginatedPassiveTrees)
	async passiveTrees(
		@Arg('input', () => PaginatedPassiveTreesInput)
		input: PaginatedPassiveTreesInput
	): Promise<PaginatedPassiveTrees> {
		const { limit, spirit } = input;
		const offset = input.offset || 0;

		// Validate spirit
		if (spirit && !['owl', 'bear', 'lynx'].includes(spirit)) {
			return {
				error: true,
			};
		}

		// Check cache
		let data = passivesCache.fetch();
		if (!data) {
			data = await fetchSortedPassives();
		}

		// Use filtered data if spirit is specified
		if (spirit) {
			data = filteredCache[spirit as keyof typeof filteredCache] ?? [];
		}

		// Hard limit on limit
		const perPage = Math.max(Math.min(CACHE_LIMIT_MAX, limit ?? 5), 0);

		// Read from cache
		const results = data.slice(offset, offset + perPage).map((raw) => ({
			passives: raw.sorted_passives,
			count: parseInt(raw.count),
		}));

		// Calculate hasMore
		const hasMore = data.length > offset + perPage;

		return {
			passiveTrees: results,
			hasMore,
			lastUpdated: passivesCache.lastUpdated,
		};
	}
}
