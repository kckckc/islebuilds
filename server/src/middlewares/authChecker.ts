import { AuthChecker } from 'type-graphql';
import { User } from '../entities/User';
import { MyContext } from '../types';

export type RoleString = 'MODERATION' | 'ADMIN' | '*';

export const checkRoles = (roles: string[], required: RoleString[]) => {
	const roleList = roles.slice(0);

	// * grants all
	if (roleList.includes('*')) {
		return true;
	}

	// Inherit mod from admin
	if (roleList.includes('ADMIN') && !roleList.includes('MODERATION')) {
		roleList.push('MODERATION');
	}

	// Check if they are missing any of the required roles
	if (required.some((r) => !roleList.includes(r))) {
		return false;
	}

	// All roles matched, success
	return true;
};

export const authChecker: AuthChecker<MyContext> = async (
	{ context: { myId } },
	requiredRoles
) => {
	// If just @Authorized, must be logged in
	if (!requiredRoles.length) {
		console.warn(
			'Use @Authenticated for checking logins and throwing, use @Authorized for nullable fields or roles'
		);

		return !!myId;
	}

	// If we require roles and they're not authenticated, fail
	if (!myId) return false;

	// Fetch user
	// TODO: check if this adds an extra query?
	const user = await User.findOne({ where: { id: myId }, select: ['roles'] });
	if (!user) return false;

	return checkRoles(user.roles, requiredRoles as RoleString[]);
};
