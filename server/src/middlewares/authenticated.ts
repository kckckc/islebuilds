import { MiddlewareFn, UseMiddleware } from 'type-graphql';
import { MyContext } from '../types';

export const isAuth: MiddlewareFn<MyContext> = ({ context }, next) => {
	if (!context.myId) {
		throw new Error('not authenticated');
	}

	return next();
};

export const Authenticated = () => UseMiddleware(isAuth);
