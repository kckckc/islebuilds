export class QueryCache<T> {
	data: T | null;
	maxAge: number;
	lastUpdated: number;

	constructor(maxAge: number) {
		this.data = null;
		this.maxAge = maxAge; // ms
		this.lastUpdated = 0;
	}

	fetch(): T | null {
		if (Date.now() - this.lastUpdated > this.maxAge) {
			return null;
		}

		return this.data;
	}

	store(value: T) {
		this.data = value;
		this.lastUpdated = Date.now();
	}
}
