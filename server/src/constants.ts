export const __prod__ = process.env.NODE_ENV === 'production';
export const COOKIE_NAME = 'qid';
export const OTP_PREFIX = 'otp:';
export const MERGE_PREFIX = 'merge:';
