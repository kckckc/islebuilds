import 'reflect-metadata';
import 'dotenv/config';
import { COOKIE_NAME, __prod__ } from './constants';
import express from 'express';
import cors from 'cors';
import session from 'express-session';
import RedisStore from 'connect-redis';
import { redis } from './redis';
import { dataSource } from './dataSource';

// Routes
import authRouter from './routes/auth';
import uploadCharacterRouter from './routes/uploadCharacter';
import uploadInventoryRouter from './routes/uploadInventory';
import otpRouter from './routes/otp';
import nicknamesRouter from './routes/nicknames';
import { applyGraphql } from './routes/graphql';
import { preloadLeaderboards } from './resolvers/leaderboard';
import passport from 'passport';

const main = async () => {
	await dataSource.initialize();

	// Use synchronize in dev
	if (__prod__) {
		await dataSource.runMigrations();
	}

	// Preload leaderboards
	// To show character rank on leaderboard on their profile
	preloadLeaderboards();

	const redisStore = new RedisStore({
		client: redis,
		disableTouch: true,
	});

	const app = express();

	// Trust nginx
	app.set('trust proxy', 1);

	app.use(
		cors({
			origin: [
				process.env.CORS_ORIGIN as string | undefined,
				'https://play.isleward.com',
			].filter((x): x is string => !!x),
			credentials: true,
		})
	);

	// For POSTs
	app.use(express.urlencoded({ extended: false }));
	app.use(express.json());

	app.use(
		session({
			name: COOKIE_NAME,
			store: redisStore,
			cookie: {
				domain: __prod__ ? '.islebuilds.com' : undefined,
				maxAge: 1000 * 60 * 60 * 24 * 365 * 10, // 10 years
				httpOnly: true,
				sameSite: 'lax', // csrf
				secure: __prod__, // cookie only works in https
			},
			saveUninitialized: false,
			secret: process.env.SESSION_SECRET!,
			resave: false,
		})
	);
	app.use(passport.initialize());
	app.use(passport.session());

	app.use('/api', authRouter);
	app.use('/api', uploadCharacterRouter);
	app.use('/api', uploadInventoryRouter);
	app.use('/api', otpRouter);
	app.use('/api', nicknamesRouter);

	await applyGraphql(app, redis);

	app.listen(4000, () => {
		console.log('Server started on localhost:4000');
	});
};

main().catch((e) => console.error(e));
