import path from 'path';
import { DataSource } from 'typeorm';
import { __prod__ } from './constants';
import { AuthToken } from './entities/AuthToken';
import { Character } from './entities/Character';
import { FederatedCredential } from './entities/FederatedCredential';
import { Incident } from './entities/Incident';
import { Inventory } from './entities/Inventory';
import { Sprite } from './entities/Sprite';
import { User } from './entities/User';

export const dataSource = new DataSource({
	host: process.env.POSTGRES_HOST,
	database: 'islebuilds',
	username: 'postgres',
	password: 'postgres',
	port: parseInt(process.env.POSTGRES_PORT!),
	type: 'postgres',
	logging: !__prod__,
	synchronize: !__prod__,
	migrations: [path.join(__dirname, './migrations/*')],
	entities: [
		User,
		Character,
		FederatedCredential,
		AuthToken,
		Sprite,
		Incident,
		Inventory,
	],
});
