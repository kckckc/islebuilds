import { MigrationInterface, QueryRunner } from 'typeorm';

export class Leagues1723337400569 implements MigrationInterface {
	name = 'Leagues1723337400569';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE "character" ADD COLUMN "league" character varying NOT NULL DEFAULT 'standard'`
		);
		await queryRunner.query(
			`UPDATE "character" SET "league" = 'hardcore' WHERE prophecy_hardcore = true`
		);
		await queryRunner.query(
			`ALTER TABLE "character" ALTER COLUMN "league" DROP DEFAULT`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "character" DROP COLUMN "league"`);
	}
}
