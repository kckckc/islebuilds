import { MigrationInterface, QueryRunner } from "typeorm";

export class ProphecyDefaults1725325866242 implements MigrationInterface {
    name = 'ProphecyDefaults1725325866242'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "character" ALTER COLUMN "prophecy_austere" SET DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "character" ALTER COLUMN "prophecy_butcher" SET DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "character" ALTER COLUMN "prophecy_hardcore" SET DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "character" ALTER COLUMN "prophecy_crushable" SET DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "character" ALTER COLUMN "prophecy_crushable" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "character" ALTER COLUMN "prophecy_hardcore" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "character" ALTER COLUMN "prophecy_butcher" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "character" ALTER COLUMN "prophecy_austere" DROP DEFAULT`);
    }

}
