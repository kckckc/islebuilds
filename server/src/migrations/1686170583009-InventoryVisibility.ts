import { MigrationInterface, QueryRunner } from "typeorm";

export class InventoryVisibility1686170583009 implements MigrationInterface {
    name = 'InventoryVisibility1686170583009'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "inventory" ADD "visible" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "inventory" DROP COLUMN "visible"`);
    }

}
