import { MigrationInterface, QueryRunner } from 'typeorm';

export class Sprites1662508194490 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`CREATE TABLE "sprite" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "description" character varying NOT NULL, "visible" boolean NOT NULL DEFAULT false, "deleted" boolean NOT NULL DEFAULT false, "size" integer NOT NULL, "pixels" character varying NOT NULL, "userId" uuid NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_0c2ab4b884c704710daa4810b71" PRIMARY KEY ("id"))`
		);
		await queryRunner.query(
			`ALTER TABLE "sprite" ADD CONSTRAINT "FK_c9026d5714861cc7537325d37fb" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE "sprite" DROP CONSTRAINT "FK_c9026d5714861cc7537325d37fb"`
		);
		await queryRunner.query(`DROP TABLE "sprite"`);
	}
}
