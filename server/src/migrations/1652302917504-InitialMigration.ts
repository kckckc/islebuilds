import { MigrationInterface, QueryRunner } from "typeorm";

export class InitialMigration1652302917504 implements MigrationInterface {
    name = 'InitialMigration1652302917504'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."character_spirit_enum" AS ENUM('owl', 'bear', 'lynx')`);
        await queryRunner.query(`CREATE TABLE "character" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "level" integer NOT NULL, "spirit" "public"."character_spirit_enum" NOT NULL, "passives" integer array NOT NULL, "eq_head" jsonb, "eq_neck" jsonb, "eq_chest" jsonb, "eq_hands" jsonb, "eq_finger1" jsonb, "eq_finger2" jsonb, "eq_waist" jsonb, "eq_legs" jsonb, "eq_feet" jsonb, "eq_trinket" jsonb, "eq_oneHanded" jsonb, "eq_offHand" jsonb, "eq_tool" jsonb, "eq_quickslot" jsonb, "eq_rune1" jsonb, "eq_rune2" jsonb, "eq_rune3" jsonb, "eq_rune4" jsonb, "portraitX" integer NOT NULL, "portraitY" integer NOT NULL, "skinSheetName" character varying NOT NULL, "skinId" character varying NOT NULL, "skinCell" integer NOT NULL, "prophecy_austere" boolean NOT NULL, "prophecy_butcher" boolean NOT NULL, "prophecy_hardcore" boolean NOT NULL, "prophecy_crushable" boolean NOT NULL, "rep_gaekatla" integer, "rep_fjolgard" integer, "rep_akarei" integer, "rep_peopleOfTheSun" integer, "rep_pumpkinSailor" integer, "rep_theWinterMan" integer, "userId" uuid NOT NULL, "uploadedAt" TIMESTAMP NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_d80158dde1461b74ed8499e7d89" UNIQUE ("name"), CONSTRAINT "PK_6c4aec48c564968be15078b8ae5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "federated_credential" ("id" SERIAL NOT NULL, "provider" character varying NOT NULL, "subject" character varying NOT NULL, "userId" uuid NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_6259d0fa4dbae6b54cd502c2035" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "displayName" character varying NOT NULL, "roles" character varying array NOT NULL DEFAULT '{}', "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_059e69c318702e93998f26d1528" UNIQUE ("displayName"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "auth_token" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "label" character varying NOT NULL, "token" character varying NOT NULL, "userId" uuid NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_7a74281711d1a581b4dcc4d59ff" UNIQUE ("token"), CONSTRAINT "PK_4572ff5d1264c4a523f01aa86a0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "character" ADD CONSTRAINT "FK_04c2fb52adfa5265763de8c4464" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "federated_credential" ADD CONSTRAINT "FK_8daedced078f01472fc06d1fb3b" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "auth_token" ADD CONSTRAINT "FK_5a326267f11b44c0d62526bc718" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "auth_token" DROP CONSTRAINT "FK_5a326267f11b44c0d62526bc718"`);
        await queryRunner.query(`ALTER TABLE "federated_credential" DROP CONSTRAINT "FK_8daedced078f01472fc06d1fb3b"`);
        await queryRunner.query(`ALTER TABLE "character" DROP CONSTRAINT "FK_04c2fb52adfa5265763de8c4464"`);
        await queryRunner.query(`DROP TABLE "auth_token"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "federated_credential"`);
        await queryRunner.query(`DROP TABLE "character"`);
        await queryRunner.query(`DROP TYPE "public"."character_spirit_enum"`);
    }

}
