import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateInventoryTable1667782050007 implements MigrationInterface {
	name = 'CreateInventoryTable1667782050007';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`CREATE TABLE "inventory" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "stash" boolean NOT NULL, "items" jsonb NOT NULL, "userId" uuid NOT NULL, "uploadedAt" TIMESTAMP NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_82aa5da437c5bbfb80703b08309" PRIMARY KEY ("id"))`
		);
		await queryRunner.query(
			`ALTER TABLE "inventory" ADD CONSTRAINT "FK_fe4917e809e078929fe517ab762" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE "inventory" DROP CONSTRAINT "FK_fe4917e809e078929fe517ab762"`
		);
		await queryRunner.query(`DROP TABLE "inventory"`);
	}
}
