import { MigrationInterface, QueryRunner } from 'typeorm';

export class SpriteSearch1662508194491 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`CREATE EXTENSION IF NOT EXISTS "pg_trgm"`);
		await queryRunner.query(`CREATE EXTENSION IF NOT EXISTS "btree_gin"`);

		await queryRunner.query(
			`CREATE INDEX "IDX_SPRITE_TRGM" ON "sprite" USING GIN(name, description gin_trgm_ops)`
		);
	}

	public async down(): Promise<void> {}
}
