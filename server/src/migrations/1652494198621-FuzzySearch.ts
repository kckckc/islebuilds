import { MigrationInterface, QueryRunner } from 'typeorm';

export class FuzzySearch1652494198621 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`CREATE EXTENSION IF NOT EXISTS "pg_trgm"`);
		await queryRunner.query(
			`CREATE INDEX "IDX_CHARACTER_NAME_LOWER" ON "character" (lower("name"))`
		);
		await queryRunner.query(
			`CREATE INDEX "IDX_CHARACTER_NAME_TRGM" ON "character" USING GIN(name gin_trgm_ops)`
		);
	}

	public async down(_queryRunner: QueryRunner): Promise<void> {}
}
