import { MigrationInterface, QueryRunner } from 'typeorm';

export class SkinOptional1716588846555 implements MigrationInterface {
	name = 'SkinOptional1716588846555';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE "character" ALTER COLUMN "skinSheetName" DROP NOT NULL`
		);
		await queryRunner.query(
			`ALTER TABLE "character" ALTER COLUMN "skinCell" DROP NOT NULL`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE "character" ALTER COLUMN "skinCell" SET NOT NULL`
		);
		await queryRunner.query(
			`ALTER TABLE "character" ALTER COLUMN "skinSheetName" SET NOT NULL`
		);
	}
}
