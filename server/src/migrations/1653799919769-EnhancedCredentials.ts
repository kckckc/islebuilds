import { MigrationInterface, QueryRunner } from "typeorm";

export class EnhancedCredentials1653799919769 implements MigrationInterface {
    name = 'EnhancedCredentials1653799919769'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "federated_credential" ADD "displayName" character varying`);
        await queryRunner.query(`ALTER TABLE "federated_credential" ADD "email" character varying`);
        await queryRunner.query(`ALTER TABLE "federated_credential" ADD "accessToken" character varying`);
        await queryRunner.query(`ALTER TABLE "federated_credential" ADD "refreshToken" character varying`);
        await queryRunner.query(`ALTER TABLE "federated_credential" ADD "visible" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "user" ADD "displayNameChanged" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "displayNameChanged"`);
        await queryRunner.query(`ALTER TABLE "federated_credential" DROP COLUMN "visible"`);
        await queryRunner.query(`ALTER TABLE "federated_credential" DROP COLUMN "refreshToken"`);
        await queryRunner.query(`ALTER TABLE "federated_credential" DROP COLUMN "accessToken"`);
        await queryRunner.query(`ALTER TABLE "federated_credential" DROP COLUMN "email"`);
        await queryRunner.query(`ALTER TABLE "federated_credential" DROP COLUMN "displayName"`);
    }

}
