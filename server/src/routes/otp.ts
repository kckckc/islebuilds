import { Router } from 'express';
import { OTP_PREFIX } from '../constants';
import { redis } from '../redis';

const router = Router();

router.get('/otp', async (req, res) => {
	const authorization = req.headers.authorization;
	const parts = authorization?.split(' ');
	if (!parts || parts[0] !== 'Bearer') {
		res.status(400);
		res.send('expired');
		return;
	}

	const otpString = parts[1];

	const token = await redis.get(OTP_PREFIX + otpString);
	if (!token) {
		res.status(400);
		res.send('expired');
		return;
	}

	await redis.del(OTP_PREFIX + otpString);

	res.send(token);
});

export default router;
