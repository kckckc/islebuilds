import {
	Profile as DiscordProfile,
	Scope as DiscordScope,
	Strategy as DiscordStrategy,
} from '@oauth-everything/passport-discord';
import { NextFunction, Request, Router } from 'express';
import { nanoid } from 'nanoid';
import passport, { Profile } from 'passport';
import {
	Profile as GoogleProfile,
	Strategy as GoogleStrategy,
	VerifyCallback,
} from 'passport-google-oauth20';
import { MERGE_PREFIX } from '../constants';
import { FederatedCredential } from '../entities/FederatedCredential';
import { User } from '../entities/User';
import { redis } from '../redis';

const apiUrl = process.env.APP_API_URL;
const webUrl = process.env.APP_WEB_URL;

const generateDisplayName = () => {
	return `User ${Math.floor(Math.random() * 100000)}`;
};

type VerifyFn = (
	req: Request,
	accessToken: string,
	refreshToken: string,
	profile: Profile,
	done: VerifyCallback
) => void;

interface NormalizedProfile {
	id: string;
	displayName?: string;
	email?: string;
}

const genericVerify = <T extends Profile | DiscordProfile>(
	provider: string,
	transform: (from: T) => NormalizedProfile
): VerifyFn => {
	return async (req, accessToken, refreshToken, originalProfile: any, cb) => {
		const profile = transform(originalProfile);

		try {
			const cred = await FederatedCredential.findOne({
				where: {
					provider,
					subject: profile.id,
				},
			});

			// User has previously logged in
			if (cred) {
				// Update some fields
				await FederatedCredential.update(cred.id, {
					displayName: profile?.displayName,
					email: profile?.email,
					accessToken: accessToken,
					refreshToken: refreshToken,
				});

				// If the credential exists and we are already logged in
				if (req.user) {
					// If the credential is linked to a different account, merge
					if (req.user.id !== cred.userId) {
						cb(null, req.user, {
							merge: true,
							current: req.user.id,
							with: cred.userId,
						});

						return;
					}
				}

				const user = await User.findOne({
					where: {
						id: cred.userId,
					},
				});

				if (!user) {
					//?
					cb(null, {});
				} else {
					cb(null, user);
				}

				return;
			}

			// Check if already logged in to an account
			let user = req.user;
			if (!user) {
				// User has not logged in before
				// Create a new user for them
				user = await User.create({
					// Don't use display name
					displayName: generateDisplayName(),
					displayNameChanged: false,
				}).save();
			}

			// Create new credential, either with the new user or existing user
			await FederatedCredential.create({
				userId: user.id,
				provider,
				subject: profile.id,
				displayName: profile?.displayName,
				email: profile?.email,
				accessToken: accessToken,
				refreshToken: refreshToken,
			}).save();

			cb(null, user);
		} catch (err) {
			cb(err);
		}
	};
};

passport.use(
	new GoogleStrategy(
		{
			clientID: process.env.GOOGLE_CLIENT_ID!,
			clientSecret: process.env.GOOGLE_CLIENT_SECRET!,
			callbackURL: apiUrl + '/api/login/redirect/google',
			scope: ['email', 'profile'],
			state: true,
			passReqToCallback: true,
		},
		genericVerify(
			'https://accounts.google.com',
			(profile: GoogleProfile) => {
				return {
					id: profile.id,
					displayName: profile?.displayName,
					email: profile?.emails?.[0]?.value,
				};
			}
		)
	)
);

// Discord

passport.use(
	new DiscordStrategy(
		{
			clientID: process.env.DISCORD_CLIENT_ID!,
			clientSecret: process.env.DISCORD_CLIENT_SECRET!,
			callbackURL: apiUrl + '/api/login/redirect/discord',
			scope: [DiscordScope.IDENTIFY, DiscordScope.EMAIL],
			passReqToCallback: true,
		},
		genericVerify('https://discord.com', (profile: DiscordProfile) => {
			return {
				id: profile.id,
				displayName: profile?.displayName,
				email: profile?.emails?.[0]?.value,
			};
		}) as any
	)
);

// Passport stuff

passport.serializeUser((user, done) => {
	done(null, user.id);
});

passport.deserializeUser(async (id: string, done) => {
	const user = await User.findOneBy({ id });
	if (user) {
		done(null, user);
	} else {
		//?
		done(null, {});
	}
});

// Express routing

const makeRedirectHandler = (strategy: string) => {
	return (req: Express.Request, res: any, next: NextFunction) => {
		// Retrieve redirect url
		const returnTo = req.session.returnTo ?? '/';
		// Clear redirect url
		req.session.returnTo = undefined;

		// TODO: types
		passport.authenticate(strategy, (err: any, user: any, info: any) => {
			if (err) {
				return next(err);
			}

			// Failure redirect
			if (!user) {
				return res.redirect(webUrl + '/login');
			}

			console.log('\n\n\nUSER', user, info);

			// Create login session manually
			req.logIn(user, (err) => {
				if (err) {
					return next(err);
				}

				if (info?.merge) {
					const token = nanoid();

					// 30 minutes
					redis.set(
						MERGE_PREFIX + token,
						info.current + ':' + info.with,
						'EX',
						60 * 30
					);

					res.redirect(webUrl + '/account/merge/' + token);
				}

				// Success redirect
				res.redirect(webUrl + returnTo);
			});
		})(req, res, next);
	};
};

const router = Router();

router.get('/login/federated/google', (req, res, next) => {
	// Store redirect url
	req.session.returnTo = req.query.next as string | undefined;

	passport.authenticate('google', {
		accessType: 'offline',
		prompt: 'consent',
	})(req, res, next);
});

router.get('/login/redirect/google', makeRedirectHandler('google'));

router.get('/login/federated/discord', (req, res, next) => {
	// Store redirect url
	req.session.returnTo = req.query.next as string | undefined;

	passport.authenticate('discord')(req, res, next);
});

router.get('/login/redirect/discord', makeRedirectHandler('discord'));

export default router;
