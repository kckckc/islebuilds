import { Router } from 'express';
import { DeepPartial } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { AuthToken } from '../entities/AuthToken';
import { Inventory } from '../entities/Inventory';
import { JsonValue } from '../types';
import { ValidationError } from '../util/errors';

const router = Router();

const sanitizeInventory = (
	data: JsonValue
): QueryDeepPartialEntity<Inventory> => {
	// Top-level
	if (
		typeof data === 'string' ||
		typeof data === 'number' ||
		typeof data === 'boolean' ||
		data === null ||
		typeof data === 'undefined' ||
		Array.isArray(data)
	) {
		throw new ValidationError('Data is invalid');
	}

	// Build object for db update
	const result: DeepPartial<Inventory> = {};

	// Name
	if (typeof data.name !== 'string') {
		throw new ValidationError('Name is invalid');
	}
	result.name = data.name;

	// Stash
	if (typeof data.stash !== 'boolean') {
		throw new ValidationError('Stash is invalid');
	}
	result.stash = data.stash;

	// Items
	if (typeof data.items !== 'string') {
		throw new ValidationError('Items is invalid');
	}

	try {
		// Assert because the typeorm entity object wants strings but we need to pass objects
		// to properly insert jsonb
		result.items = JSON.parse(data.items) as string;
	} catch (e) {
		throw new ValidationError('Items is invalid');
	}

	return result;
};

// POST /api/inventory
// Include header:
//   Authorization: Bearer XYZ
router.post('/inventory', async (req, res) => {
	const authorization = req.headers.authorization;
	const parts = authorization?.split(' ');
	if (!parts || parts[0] !== 'Bearer') {
		res.status(400);
		res.send('token invalid');
		return;
	}

	const tokenString = parts[1];

	const authToken = await AuthToken.findOneBy({
		token: tokenString,
	});

	if (!authToken) {
		res.status(401);
		res.send('token expired');
		return;
	}

	try {
		const data = sanitizeInventory(req.body);

		// Add userId for relation
		data.userId = authToken.userId;

		// Save uploaded time separate from updated time
		data.uploadedAt = () => 'NOW()';

		// Manual upsert
		const old = await Inventory.findOne({
			select: { id: true, name: true, stash: true },
			where: { name: data.name as string, stash: data.stash as boolean },
		});
		if (old) {
			// Update
			await Inventory.update(
				{ name: data.name as string, stash: data.stash as boolean },
				data
			);
		} else {
			// Insert
			await Inventory.insert(data);
		}

		res.send('OK');
	} catch (e) {
		console.error('Failed to upload inventory:', e);

		res.status(400).send('invalid');
	}
});

export default router;
