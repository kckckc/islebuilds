import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import { ApolloServerPluginLandingPageDisabled } from '@apollo/server/plugin/disabled';
import { ApolloServerPluginLandingPageLocalDefault } from '@apollo/server/plugin/landingPage/default';
import express from 'express';
import Redis from 'ioredis';
import { buildSchema, NonEmptyArray } from 'type-graphql';
import { __prod__ } from '../constants';
import { authChecker } from '../middlewares/authChecker';
import { MyContext } from '../types';

// Loaders
// import { createUserLoader } from './utils/createUserLoader';

// Resolvers
import { UserResolver } from '../resolvers/user';
import { AuthTokenResolver } from '../resolvers/authToken';
import { CharacterResolver } from '../resolvers/character';
import { PassiveTreeResolver } from '../resolvers/passiveTree';
import { LeaderboardResolver } from '../resolvers/leaderboard';
import { TopSkinsResolver } from '../resolvers/topSkins';
import { SpriteResolver } from '../resolvers/sprite';
import { ModerationResolver } from '../resolvers/moderation';
import { IncidentResolver } from '../resolvers/incident';
import { InventoryResolver } from '../resolvers/inventory';
import { RuneLoadoutsResolver } from '../resolvers/runeLoadouts';

const resolvers: NonEmptyArray<Function> = [
	UserResolver,
	AuthTokenResolver,
	CharacterResolver,
	PassiveTreeResolver,
	LeaderboardResolver,
	TopSkinsResolver,
	SpriteResolver,
	ModerationResolver,
	IncidentResolver,
	InventoryResolver,
	RuneLoadoutsResolver,
];

const applyGraphql = async (app: express.Application, redis: Redis) => {
	const apolloPlugins = [];
	if (!__prod__) {
		apolloPlugins.push(ApolloServerPluginLandingPageLocalDefault());
	} else {
		apolloPlugins.push(ApolloServerPluginLandingPageDisabled());
	}

	const apolloServer = new ApolloServer<MyContext>({
		plugins: apolloPlugins,
		schema: await buildSchema({
			resolvers,
			validate: false,
			authChecker,
			authMode: 'null',
		}),
	});

	await apolloServer.start();

	app.use(
		'/api/graphql',
		expressMiddleware(apolloServer, {
			context: async ({ req, res }) => {
				const user = req?.session?.passport?.user;
				return {
					req,
					res,
					redis,
					myId: user,
					// TODO
					// userLoader: createUserLoader()
				} satisfies MyContext;
			},
		})
	);
};

export { applyGraphql };
