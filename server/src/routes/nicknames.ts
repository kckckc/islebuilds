import { Router } from 'express';
import { Character } from '../entities/Character';
import { isValidName } from '../util/validateName';

const router = Router();

router.post('/nicknames', async (req, res) => {
	const rawNameList: string[] = req.body.characters.filter(
		(c: any): c is string => typeof c === 'string'
	);

	const names = rawNameList
		.map((c) => c.trim())
		.filter((c) => isValidName(c));

	const data = await Character.createQueryBuilder()
		.select([])
		.addSelect('Character.name')
		.addSelect('user.displayName')
		.innerJoin('Character.user', 'user', 'user.displayNameChanged = true')
		.where('Character.name IN(:...names)', { names })
		.getRawMany();

	const out: Record<string, string> = {};
	data.forEach((r) => {
		out[r.Character_name] = r.user_displayName;
	});

	res.send(out);
});

export default router;
