import { Router } from 'express';
import { DeepPartial } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { AuthToken } from '../entities/AuthToken';
import { Character } from '../entities/Character';
import { CharacterSpirit, JsonValue } from '../types';
import { ValidationError } from '../util/errors';
import { cleanName } from '../util/validateName';
import { validatePassives } from '../util/validatePassives';

const router = Router();

// Sanitizers

const sanitizeItem = (raw: JsonValue): string | null => {
	if (typeof raw !== 'string') {
		return null;
	}

	try {
		const item = JSON.parse(raw);

		// Delete properties that are only useful to the original character
		delete item.id;
		delete item.pos;
		delete item.eq;
		delete item.runeSlot;
		delete item.cd; // keep cdMax
		delete item.isNew;

		return item;
	} catch (e) {
		return null;
	}
};

const sanitizeCharacter = (
	data: JsonValue
): QueryDeepPartialEntity<Character> => {
	// Top-level
	if (
		typeof data === 'string' ||
		typeof data === 'number' ||
		typeof data === 'boolean' ||
		data === null ||
		typeof data === 'undefined' ||
		Array.isArray(data)
	) {
		throw new ValidationError('Data is invalid');
	}

	// Build object for db update
	const result: DeepPartial<Character> = {};

	// Name
	if (typeof data.name !== 'string') {
		throw new ValidationError('Name is invalid');
	}
	const cleanedName = cleanName(data.name);
	result.name = cleanedName;

	// Level
	// Don't check the max?
	if (typeof data.level !== 'number' || data.level < 1) {
		throw new ValidationError('Level is invalid');
	}
	result.level = data.level;

	// Spirit
	if (
		typeof data.spirit !== 'string' ||
		!Object.values<string>(CharacterSpirit).includes(data.spirit)
	) {
		throw new ValidationError('Spirit is invalid');
	}
	result.spirit = data.spirit as CharacterSpirit;

	if (typeof data.league !== 'string') {
		throw new ValidationError('League is invalid');
	}
	result.league = data.league;

	// Passives
	if (!validatePassives(data.passives)) {
		throw new ValidationError('Passives is invalid');
	}
	result.passives = data.passives as number[];

	// Visual (just check strings, no specific validity checks)
	if (
		typeof data.portrait !== 'object' ||
		Array.isArray(data.portrait) ||
		data.portrait === null
	) {
		throw new ValidationError(`Portrait is invalid`);
	}
	if (typeof data.portrait.x !== 'number') {
		throw new ValidationError(`Portrait X is invalid`);
	}
	result.portraitX = data.portrait.x;
	if (typeof data.portrait.y !== 'number') {
		throw new ValidationError(`Portrait Y is invalid`);
	}
	result.portraitY = data.portrait.y;
	if (typeof data.skinId !== 'string') {
		throw new ValidationError(`SkinId is invalid`);
	}
	result.skinId = data.skinId;

	// optional (skipped when mounted)
	if (data.skinSheetName) {
		if (typeof data.skinSheetName !== 'string') {
			throw new ValidationError(`SkinSheetName is invalid`);
		}
		result.skinSheetName = data.skinSheetName;
	}
	if (data.skinCell) {
		if (typeof data.skinCell !== 'number') {
			throw new ValidationError(`SkinCell is invalid`);
		}
		result.skinCell = data.skinCell;
	}

	// Rep
	if (typeof data.rep_akarei === 'number') {
		result.rep_akarei = data.rep_akarei;
	}
	if (typeof data.rep_gaekatla === 'number') {
		result.rep_gaekatla = data.rep_gaekatla;
	}
	if (typeof data.rep_fjolgard === 'number') {
		result.rep_fjolgard = data.rep_fjolgard;
	}
	if (typeof data.rep_peopleOfTheSun === 'number') {
		result.rep_peopleOfTheSun = data.rep_peopleOfTheSun;
	}
	if (typeof data.rep_pumpkinSailor === 'number') {
		result.rep_pumpkinSailor = data.rep_pumpkinSailor;
	}
	if (typeof data.rep_theWinterMan === 'number') {
		result.rep_theWinterMan = data.rep_theWinterMan;
	}

	// Equipment
	(
		[
			'eq_head',
			'eq_neck',
			'eq_chest',
			'eq_hands',
			'eq_finger1',
			'eq_finger2',
			'eq_waist',
			'eq_legs',
			'eq_feet',
			'eq_trinket',
			'eq_oneHanded',
			'eq_offHand',
			'eq_tool',
			'eq_quickslot',
			'eq_rune1',
			'eq_rune2',
			'eq_rune3',
			'eq_rune4',
		] as const
	).forEach((prop) => {
		const item = data[prop];
		// Assert because the typeorm entity object wants strings but we need to pass objects
		// to properly insert jsonb
		result[prop] = sanitizeItem(item) as string;
	});

	return result;
};

// POST /api/character/upload
// Old route: /api/character/waddon/:name
// Include header:
//   Authorization: Bearer XYZ
router.post('/character/upload', async (req, res) => {
	// console.log('HEADERS', req.headers);

	const authorization = req.headers.authorization;
	const parts = authorization?.split(' ');
	if (!parts || parts[0] !== 'Bearer') {
		res.status(400);
		res.send('token invalid');
		return;
	}

	const tokenString = parts[1];

	const authToken = await AuthToken.findOneBy({
		token: tokenString,
	});

	if (!authToken) {
		res.status(401);
		res.send('token expired');
		return;
	}

	try {
		// console.log('TOKEN', authToken);

		// console.log('BODY', req.body);

		const data = sanitizeCharacter(req.body);

		// console.log('SANE', data);

		// Add userId for relation
		data.userId = authToken.userId;

		// Save uploaded time separate from updated time
		data.uploadedAt = () => 'NOW()';

		// Manual upsert
		const old = await Character.findOne({
			select: { id: true, name: true },
			where: { name: data.name as string },
		});
		if (old) {
			// Update
			await Character.update({ name: data.name as string }, data);
		} else {
			// Insert
			await Character.insert(data);
		}

		res.send('OK');
	} catch (e) {
		console.error('Failed to upload character:', e);

		res.status(400).send('invalid');
	}
});

export default router;
