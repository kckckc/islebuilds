import { ValidationError } from './errors';

export const isValidName = (name: string): boolean => {
	try {
		return validateName(name);
	} catch (e) {
		return false;
	}
};

export const cleanName = (name: string) => {
	// split at newline and take the first line
	// also trim whitespace
	const trimmed = name.split('\n')[0].trim();

	validateName(trimmed);

	return trimmed;
};

export const validateName = (name: string): boolean => {
	if (name.length < 3) {
		throw new ValidationError('Name must be at least 3 characters');
	}
	if (name.length > 12) {
		throw new ValidationError('Name must be at most 12 characters');
	}

	for (let i = 0; i < name.length; i++) {
		let char = name[i].toLowerCase();
		let valid = [
			'a',
			'b',
			'c',
			'd',
			'e',
			'f',
			'g',
			'h',
			'i',
			'j',
			'k',
			'l',
			'm',
			'n',
			'o',
			'p',
			'q',
			'r',
			's',
			't',
			'u',
			'v',
			'w',
			'x',
			'y',
			'z',
		];

		if (!valid.includes(char)) {
			throw new ValidationError(
				`Name contains invalid character at position ${i}: '${name[i]}' (name was ${name})`
			);
		}
	}

	return true;
};
