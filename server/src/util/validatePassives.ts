import { JsonValue } from '../types';
import { ValidationError } from './errors';

// Naive passive validation
export const validatePassives = (passives: JsonValue): boolean => {
	if (!Array.isArray(passives)) {
		throw new ValidationError('Passives must be array');
	}

	passives.forEach((p) => {
		if (typeof p !== 'number') {
			throw new ValidationError('Passives must be array of numbers');
		}
	});

	// TODO: Check starting nodes, connectivity, max points, etc

	return true;
};
