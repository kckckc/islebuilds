import { Incident } from '../entities/Incident';
import axios from 'axios';

const webhookUrl = process.env.DISCORD_WEBHOOK_REPORTS;
if (!webhookUrl) {
	throw new Error('missing DISCORD_WEBHOOK_REPORTS url for reports');
}

const blockQuote = (str: string) => '```\n' + str + '\n```';

const markdownUrl = (label: string, url: string) => `[${label}](${url})`;

const islebuildsUrl = (path: string) => 'https://islebuilds.com' + path;

export const postReportWebhook = async (
	incident: Incident,
	typeLabel?: string
) => {
	let reportedByName = '?';
	if (incident.reportedById) {
		const reportedByEntity = await incident.reportedBy;
		reportedByName = reportedByEntity?.displayName ?? '?';
	}

	let reportedUserName = '?';
	if (incident.reportedUserId) {
		const reportedUserEntity = await incident.reportedUser;
		reportedUserName = reportedUserEntity?.displayName ?? '?';
	}

	let reportedCharacterName = '?';
	if (incident.reportedCharacterId) {
		const reportedCharacterEntity = await incident.reportedCharacter;
		reportedCharacterName = reportedCharacterEntity?.name ?? '?';
	}

	let reportedSpriteName = '?';
	let reportedSpriteDescription = '?';
	if (incident.reportedSpriteId) {
		const reportedSpriteEntity = await incident.reportedSprite;
		reportedSpriteName = reportedSpriteEntity?.name ?? '?';
		reportedSpriteDescription = reportedSpriteEntity?.description ?? '?';
	}

	await axios.post(webhookUrl, {
		embeds: [
			{
				color: 0xa24eff,
				title: `New ${typeLabel}${typeLabel ? ' ' : ''}Report`,
				url: islebuildsUrl('/incident/' + incident.id),
				description: [
					blockQuote(
						incident.title && incident.title.trim().length > 0
							? incident.title
							: '(No title)'
					),
					blockQuote(
						incident.description &&
							incident.description.trim().length > 0
							? incident.description
							: '(No description)'
					),
				].join(''),
				fields: [
					incident.reportedById && {
						name: 'Reported by',
						value: markdownUrl(
							reportedByName,
							islebuildsUrl('/user/' + incident.reportedById)
						),
						inline: true,
					},
					incident.reportedUserId && {
						name: 'Entity owner',
						value: markdownUrl(
							reportedUserName,
							islebuildsUrl('/user/' + incident.reportedUserId)
						),
						inline: true,
					},
					incident.reportedCharacterId && {
						name: 'Reported character',
						value: markdownUrl(
							reportedCharacterName,
							islebuildsUrl('/character/' + reportedCharacterName)
						),
					},
					incident.reportedSpriteId && {
						name: 'Reported sprite',
						value: markdownUrl(
							reportedSpriteName.trim().length > 0
								? reportedSpriteName
								: '(No name)',
							islebuildsUrl(
								'/sprite-editor/' + incident.reportedSpriteId
							)
						),
					},
					incident.reportedSpriteId && {
						name: 'Reported sprite description',
						value:
							reportedSpriteDescription.trim().length > 0
								? blockQuote(reportedSpriteDescription)
								: '(No description)',
					},
				].filter((f) => !!f),
				timestamp: new Date().toISOString(),
			},
		],
	});
};
