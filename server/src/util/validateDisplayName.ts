const allowed = /^[A-Za-z0-9]+$/;
const minLength = 3;
const maxLength = 20;

export const validateDisplayName = (
	displayName: string
): string | undefined => {
	if (displayName.length < minLength) {
		return `name must be ${minLength} or more characters`;
	}
	if (displayName.length > maxLength) {
		return `name must be ${maxLength} or fewer characters`;
	}

	if (!displayName.match(allowed)) {
		return 'name can only contain letters and numbers';
	}

	return;
};
