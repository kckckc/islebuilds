class BaseError extends Error {
	constructor(message: string) {
		super(message);

		// Set to class name
		this.name = this.constructor.name;
	}
}

class ValidationError extends BaseError {
	constructor(message: string) {
		super(message);
	}
}

class PropertyRequiredError extends ValidationError {
	property: string;

	constructor(property: string) {
		super('Missing property: ' + property);
		this.property = property;
	}
}

export { ValidationError, PropertyRequiredError };
