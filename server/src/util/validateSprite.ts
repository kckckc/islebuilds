import { JsonValue } from '../types';
import { ValidationError } from './errors';

export const validateSprite = (size: number, raw: string): boolean => {
	let data: JsonValue;
	try {
		data = JSON.parse(raw);
	} catch (e) {
		throw new ValidationError('Invalid pixel data');
	}

	if (!Array.isArray(data)) {
		throw new ValidationError('Invalid pixel data');
	}

	if (data.length !== size) {
		throw new ValidationError('Invalid pixel data');
	}

	for (let i = 0; i < data.length; i++) {
		const row = data[i];

		if (!row || !Array.isArray(row) || row.length !== size) {
			throw new ValidationError('Invalid pixel data');
		}

		for (let j = 0; j < row.length; j++) {
			const cell = row[j];

			if (typeof cell !== 'number' || cell < 0 || cell > 44) {
				throw new ValidationError('Invalid pixel data');
			}
		}
	}

	return true;
};
