import { Link, ListItem, Text, UnorderedList } from '@chakra-ui/react';
import NextLink from 'next/link';
import { ReactNode } from 'react';
import { PlainLink } from '../components/atoms/PlainLink';
import { REPORT_ISSUE_URL } from '../components/molecules/HelpButton';

export const newsData: {
	title: string;
	at: number;
	content: ReactNode[];
}[] = [
	{
		title: 'Update 2023-03-22',
		at: 1679522400000,
		content: [
			<Text key="blurb">
				The <PlainLink href="/inventory">Inventory Viewer</PlainLink> is
				now available to display, sort, and filter items from all your
				characters and stashes (with more features coming in the
				future). You will need to install Waddon and enable &quot;sync
				inventory&quot; under the Islebuilds section of the Waddon menu.
				Continue reading for a list of other improvements and plans for
				Islebuilds.
			</Text>,
			'heading:Other changes',
			`
			There have been a lot of other unannounced updates in the past 10 months since the new Islebuilds released.
			Here are the highlights:
			`,
			<UnorderedList ml={8} key="changelog">
				<ListItem>Sprite editor and gallery</ListItem>
				<ListItem>Boss/event timers</ListItem>
				<ListItem>Character fuzzy search</ListItem>
				<ListItem>Top passive trees lists by spirit</ListItem>
				<ListItem>Top skins list</ListItem>
				<ListItem>More calculators and random tools</ListItem>
				<ListItem>Moderation tools</ListItem>
				<ListItem>Other fixes and improvements</ListItem>
			</UnorderedList>,
			'heading:Future plans',
			`
			The Inventory Viewer is the last major feature that was missing after the Islebuilds rewrite.
			There's still improvements I want to make to it though (especially to the filtering, which is far less powerful than I want it to be).
			Other than that, there's a few things I want to add eventually, but no concrete plans:
			`,
			<UnorderedList ml={8} key="plans-list">
				<ListItem>Item showcase/linking?</ListItem>
				<ListItem>DPT calculations?</ListItem>
				<ListItem>Build editor?</ListItem>
				<ListItem>More calculators/reference pages?</ListItem>
			</UnorderedList>,
			`heading:Thank you`,
			<Text key="thank-you-section">
				As usual, please report any issues{' '}
				<Link
					as="a"
					href={REPORT_ISSUE_URL}
					target="_blank"
					color="blue.500"
				>
					here, on GitLab
				</Link>
				. Thanks for reading!
			</Text>,
		],
	},
	{
		title: 'Welcome!',
		at: 1652338800000,
		content: [
			`
			Islebuilds has been completely rewritten!
			Continue reading to learn what's changed
			and how to upload characters on the new site.
			`,
			'heading:Breaking changes',
			`
			While rebuilding the site, the upload process changed.
			Waddon will have an accompanying update to enable uploading to the new site.
			If you are still using an old version of Waddon, uploading will not work
			until you update to a more recent version (>3.0.0).
			`,
			`
			Additionally, uploading to Islebuilds now requires an account.
			This is mostly a preemptive moderation measure, but it also allows for
			cool new features in the future (like the now-removed Item Manager).
			`,
			<Text key="how-to-upload">
				To upload characters, you will need{' '}
				<NextLink href="/waddon" passHref legacyBehavior>
					<Link as="a" color="blue.500">
						Waddon
					</Link>
				</NextLink>{' '}
				installed. Then, log in to Islebuilds with Google, and navigate
				to{' '}
				<NextLink href="/account/keys" passHref legacyBehavior>
					<Link as="a" color="blue.500">
						API Key settings
					</Link>
				</NextLink>{' '}
				and generate a key. You can copy/paste this key into Waddon, or
				use the one-click Use button, which will open an Isleward tab
				and set the key for you.
			</Text>,
			`
			Unfortunately, there will be no data migration from the old Islebuilds.
			Character data will be gone until it's uploaded again.
			However, we can use this as a chance to keep PTR data off the site this time.
			Snapshots are also being removed for now, since they were mostly unused.
			`,
			'heading:Features',
			<UnorderedList ml={8} key="feature-list">
				<ListItem>
					Accounts &mdash; Use a single name across Islebuilds; see
					related characters
				</ListItem>
				<ListItem>Character lookup</ListItem>
				<ListItem>Faction reputation leaderboards</ListItem>
				<ListItem>Light/dark mode toggle</ListItem>
				<ListItem>Some misc tools and calculators</ListItem>
			</UnorderedList>,
			`heading:What's next?`,
			`
			Here's some things I'm planning to work on next:
			`,
			<UnorderedList ml={8} key="plans-list">
				<ListItem>DPT calculations</ListItem>
				<ListItem>Boss/event timers</ListItem>
				<ListItem>
					Better search &mdash; Case insensitive and fuzzy character
					search
				</ListItem>
				<ListItem>
					Item finder &mdash; Search across multiple inventories and
					stashes
				</ListItem>
				<ListItem>More calculators</ListItem>
				<ListItem>More leaderboards?</ListItem>
				<ListItem>Sprite editor?</ListItem>
				<ListItem>Build editor?</ListItem>
				<ListItem>In-game nickname API w/ Waddon?</ListItem>
			</UnorderedList>,
			`heading:Thank you`,
			<Text key="thank-you-section">
				Please report any issues{' '}
				<Link
					as="a"
					href={REPORT_ISSUE_URL}
					target="_blank"
					color="blue.500"
				>
					here, on GitLab
				</Link>
				. Thanks for reading!
			</Text>,
		],
	},
];
