import { MoonIcon, SunIcon } from '@chakra-ui/icons';
import { useColorMode, IconButton } from '@chakra-ui/react';
import React from 'react';

interface ColorModeButtonProps {}

export const ColorModeButton: React.FC<ColorModeButtonProps> = ({}) => {
	const { colorMode, toggleColorMode } = useColorMode();

	return (
		<IconButton
			aria-label={'Toggle Theme'}
			onClick={toggleColorMode}
			icon={colorMode === 'light' ? <MoonIcon /> : <SunIcon />}
		/>
	);
};
