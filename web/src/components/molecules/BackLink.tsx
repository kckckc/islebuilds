import { ChevronLeftIcon } from '@chakra-ui/icons';
import { Link } from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';

interface BackLinkProps {
	href: string;
	children?: React.ReactNode;
}

export const BackLink: React.FC<BackLinkProps> = ({ href, children }) => {
	return (
		<NextLink href={href} passHref legacyBehavior>
			<Link>
				<ChevronLeftIcon mx={'2px'} mb="2px" />
				{children}
			</Link>
		</NextLink>
	);
};
