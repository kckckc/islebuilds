import { Button, Center, Text } from '@chakra-ui/react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { SiDiscord } from 'react-icons/si';

interface DiscordButtonProps {
	next?: string;
}

export const DiscordButton: React.FC<DiscordButtonProps> = ({ next }) => {
	const router = useRouter();

	const query = next ? { next: next } : { ...router.query };

	// @ts-ignore
	const apiUrl = globalThis.apiUrl ?? '';

	return (
		<NextLink
			href={{
				pathname: `${apiUrl}/api/login/federated/discord`,
				query,
			}}
			passHref
			legacyBehavior
		>
			<Button
				w={'full'}
				maxW={'md'}
				variant={'outline'}
				leftIcon={<SiDiscord />}
				_hover={{
					bg: '#717cf3',
				}}
				bg="#5865F2"
				textColor="whiteAlpha.900"
			>
				<Center>
					<Text>Log in with Discord</Text>
				</Center>
			</Button>
		</NextLink>
	);
};
