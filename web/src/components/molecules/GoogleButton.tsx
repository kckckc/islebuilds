import { Button, Center, Text } from '@chakra-ui/react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { FcGoogle } from 'react-icons/fc';

interface GoogleButtonProps {
	next?: string;
}

export const GoogleButton: React.FC<GoogleButtonProps> = ({ next }) => {
	const router = useRouter();

	const query = next ? { next: next } : { ...router.query };

	// @ts-ignore
	const apiUrl = globalThis.apiUrl ?? '';

	return (
		<NextLink
			href={{
				pathname: `${apiUrl}/api/login/federated/google`,
				query,
			}}
			passHref
			legacyBehavior
		>
			<Button
				w={'full'}
				maxW={'md'}
				variant={'outline'}
				leftIcon={<FcGoogle />}
			>
				<Center>
					<Text>Log in with Google</Text>
				</Center>
			</Button>
		</NextLink>
	);
};
