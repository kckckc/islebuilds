import { Box, Flex } from '@chakra-ui/react';
import React from 'react';
import { Character } from '../../generated/graphql';
import { SectionHeading } from '../templates/SectionHeading';
import { LevelTag } from '../atoms/LevelTag';
import { SkinDisplay } from '../atoms/SkinDisplay';
import { SpiritTag } from '../atoms/SpiritTag';

interface CharacterListItemProps {
	character: Partial<Character>;
}

export const CharacterListItem: React.FC<CharacterListItemProps> = ({
	character,
}) => {
	const right = (
		<Flex
			flexDirection="row"
			columnGap={1}
			p={2}
			display={{ base: 'none', md: 'flex' }}
		>
			{character.level && <LevelTag level={character.level} />}
			{character.spirit && <SpiritTag spirit={character.spirit} />}
		</Flex>
	);

	return (
		<Flex
			key={character.id}
			p={2}
			shadow="md"
			borderWidth="1px"
			rounded="md"
		>
			<Box flex={1}>
				<Flex flexDirection={'row'} alignItems="center">
					<Box mr={2}>
						<SkinDisplay
							skinCell={character.skinCell}
							skinSheetName={character.skinSheetName}
							size="sm"
						/>
					</Box>
					<SectionHeading href={`/character/${character.name}`}>
						{character.name}
					</SectionHeading>
					<Box flex={1} />
					{right}
				</Flex>
			</Box>
		</Flex>
	);
};
