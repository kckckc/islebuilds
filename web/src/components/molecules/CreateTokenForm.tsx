import React from 'react';
import { Formik, Form } from 'formik';
import { InputField } from '../atoms/InputField';
import { Box, Button } from '@chakra-ui/react';
import {
	AuthTokensDocument,
	AuthTokensQuery,
	useCreateAuthTokenMutation,
} from '../../generated/graphql';

interface CreateTokenFormProps {}

export const CreateTokenForm: React.FC<CreateTokenFormProps> = ({}) => {
	const [createAuthToken] = useCreateAuthTokenMutation();

	return (
		<Box maxW="400px" w="100%">
			<Formik
				initialValues={{ label: '' }}
				onSubmit={async (values) => {
					await createAuthToken({
						variables: values,
						update: (cache, mutationResult) => {
							const newToken =
								mutationResult.data?.createAuthToken;
							let data = cache.readQuery<AuthTokensQuery>({
								query: AuthTokensDocument,
							});
							if (!data) {
								data = { authTokens: [] };
							}
							cache.writeQuery({
								query: AuthTokensDocument,
								data: {
									authTokens: [...data.authTokens, newToken],
								},
							});
						},
					});
				}}
			>
				{({ isSubmitting }) => (
					<Form>
						<Box>
							<InputField
								name="label"
								placeholder="name"
								label="Key name"
							/>
						</Box>
						<Button
							mt={4}
							isLoading={isSubmitting}
							type="submit"
							colorScheme="teal"
						>
							Create API Key
						</Button>
					</Form>
				)}
			</Formik>
		</Box>
	);
};
