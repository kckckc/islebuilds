import {
	Box,
	Button,
	DeepPartial,
	FormControl,
	Select,
} from '@chakra-ui/react';
import React, { useState } from 'react';
import { Sprite } from '../../generated/graphql';
import { downloadPixels } from '../../util/downloadPixels';

interface SpriteDownloadProps {
	sprite: DeepPartial<Sprite>;
}

export const SpriteDownload: React.FC<SpriteDownloadProps> = ({ sprite }) => {
	const [resize, setResize] = useState(1);

	return (
		<>
			<FormControl
				display="flex"
				flexDirection="row"
				alignItems="center"
				gap={2}
			>
				<Box>Resize by</Box>
				<Select
					id="selectResize"
					maxW="100px"
					value={resize}
					onChange={(v) => {
						setResize(parseInt(v.currentTarget.value));
					}}
				>
					<option value="1">1x</option>
					<option value="2">2x</option>
					<option value="4">4x</option>
					<option value="8">8x</option>
				</Select>
				<Button
					onClick={() => {
						if (!sprite.pixels || !sprite.size) return;

						downloadPixels(
							JSON.parse(sprite.pixels),
							sprite.size,
							resize
						);
					}}
				>
					Download
				</Button>
			</FormControl>
		</>
	);
};
