import React from 'react';
import { Formik, Form } from 'formik';
import { InputField } from '../atoms/InputField';
import { Box, Button } from '@chakra-ui/react';
import {
	AuthTokensDocument,
	AuthTokensQuery,
	MeDocument,
	MeQuery,
	useChangeDisplayNameMutation,
	useCreateAuthTokenMutation,
} from '../../generated/graphql';
import { toErrorMap } from '../../util/toErrorMap';

interface ChangeDisplayNameFormProps {}

export const ChangeDisplayNameForm: React.FC<
	ChangeDisplayNameFormProps
> = ({}) => {
	const [changeDisplayName] = useChangeDisplayNameMutation();

	return (
		<Box maxW="400px" w="100%">
			<Formik
				initialValues={{ displayName: '' }}
				onSubmit={async (values, { setErrors }) => {
					const res = await changeDisplayName({
						variables: values,
						update: (cache, mutationResult) => {
							if (
								!mutationResult?.data?.changeDisplayName.success
							)
								return;

							const data = cache.readQuery<MeQuery>({
								query: MeDocument,
							});
							cache.writeQuery({
								query: MeDocument,
								data: {
									me: {
										...data?.me,
										displayName: values.displayName,
									},
								},
							});
						},
					});

					const errors = res.data?.changeDisplayName.errors;
					if (errors && errors.length) {
						setErrors(toErrorMap(errors));
					}
				}}
			>
				{({ isSubmitting }) => (
					<Form>
						<Box>
							<InputField
								name="displayName"
								placeholder="name"
								label="Display Name"
							/>
						</Box>
						<Button
							mt={4}
							isLoading={isSubmitting}
							type="submit"
							colorScheme="teal"
						>
							Update profile
						</Button>
					</Form>
				)}
			</Formik>
		</Box>
	);
};
