import { FormControl, FormLabel, Select } from '@chakra-ui/react';
import { Field, FieldProps } from 'formik';
import React, { ReactNode } from 'react';

type SelectFieldProps = {
	label: string;
	name: string;
	children?: ReactNode;
};

export const SelectField: React.FC<SelectFieldProps> = ({
	children,
	label,
	name,
}) => (
	<Field name={name} id={name}>
		{({ field, form }: FieldProps) => (
			<FormControl>
				<FormLabel htmlFor={field.name}>{label}</FormLabel>
				<Select
					name={name}
					id={name}
					value={field.value}
					onChange={field.onChange}
				>
					{children}
				</Select>
			</FormControl>
		)}
	</Field>
);
