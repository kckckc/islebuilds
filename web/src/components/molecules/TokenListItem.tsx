import {
	AddIcon,
	CopyIcon,
	DeleteIcon,
	PlusSquareIcon,
} from '@chakra-ui/icons';
import {
	chakra,
	Box,
	Flex,
	Heading,
	IconButton,
	Text,
	Code,
	Button,
	useClipboard,
} from '@chakra-ui/react';
import React from 'react';
import {
	AuthToken,
	AuthTokensDocument,
	AuthTokensQuery,
	useGenerateOtpMutation,
	useRevokeAuthTokenMutation,
} from '../../generated/graphql';
import NextLink from 'next/link';

interface TokenListItemProps {
	token: Partial<AuthToken>;
}

export const TokenListItem: React.FC<TokenListItemProps> = ({ token }) => {
	const [revokeAuthToken, { loading: revokeLoading }] =
		useRevokeAuthTokenMutation({
			variables: {
				id: token.id!,
			},
		});

	const [generateOTP, { loading: otpLoading }] = useGenerateOtpMutation();

	const { hasCopied, onCopy } = useClipboard(token.token!);

	let createdEl = null;
	if (token.createdAt) {
		const date = new Date(parseInt(token.createdAt));
		createdEl = (
			<Text>
				created{' '}
				{date.toLocaleString(undefined, {
					year: 'numeric',
					month: 'short',
					day: 'numeric',
					hour: 'numeric',
					minute: 'numeric',
					timeZoneName: 'short',
				})}
			</Text>
		);
	}

	return (
		<Flex key={token.id} p={5} shadow="md" borderWidth="1px">
			<Box flex={1}>
				<Flex flexDirection={'row'} justifyContent="space-between">
					<Heading fontSize="xl">{token.label}</Heading>
					{createdEl}
				</Flex>

				<Flex align="center" mt={4}>
					<Text flex={1}>
						Token (hover to reveal):{' '}
						<Code variant="subtle">
							<chakra.span
								sx={{
									'&:not(:hover)': { opacity: 0 },
									transition: 'all 0.2s ease',
								}}
							>
								{token.token}
							</chakra.span>
						</Code>
					</Text>

					<Flex columnGap={2}>
						<NextLink href="#" legacyBehavior>
							<Button
								aria-label="Use token"
								size="sm"
								colorScheme="teal"
								leftIcon={<AddIcon />}
								onClick={async () => {
									const result = await generateOTP({
										variables: {
											authTokenId: token.id!,
										},
									});
									const otp = result.data?.generateOTP;
									if (!otp) return;

									window.open(
										`https://play.isleward.com/?islebuilds=${otp}`,
										'_blank'
									);
								}}
								isLoading={otpLoading}
							>
								Use
							</Button>
						</NextLink>
						<Button
							aria-label="Copy token"
							size="sm"
							onClick={onCopy}
							leftIcon={<CopyIcon />}
						>
							{hasCopied ? 'Copied' : 'Copy'}
						</Button>
						<Button
							aria-label="Revoke token"
							size="sm"
							colorScheme="red"
							isLoading={revokeLoading}
							onClick={async () => {
								revokeAuthToken({
									update: (cache) => {
										const data =
											cache.readQuery<AuthTokensQuery>({
												query: AuthTokensDocument,
											});
										if (!data?.authTokens.length) return;

										cache.writeQuery({
											query: AuthTokensDocument,
											data: {
												authTokens:
													data.authTokens.filter(
														(t) => t.id !== token.id
													),
											},
										});
									},
								});
							}}
							leftIcon={<DeleteIcon />}
						>
							Revoke
						</Button>
					</Flex>
				</Flex>
			</Box>
		</Flex>
	);
};
