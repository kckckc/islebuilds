import { QuestionOutlineIcon } from '@chakra-ui/icons';
import {
	IconButton,
	Menu,
	MenuButton,
	MenuDivider,
	MenuItem,
	MenuList,
} from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';

interface HelpButtonProps {}

export const REPORT_ISSUE_URL =
	'https://gitlab.com/hazel_meow/islebuilds/-/issues';

export const HelpButton: React.FC<HelpButtonProps> = ({}) => {
	return (
		<Menu>
			<MenuButton as={IconButton} icon={<QuestionOutlineIcon />} />
			<MenuList>
				<NextLink href="/about" passHref legacyBehavior>
					<MenuItem as="a" cursor="pointer">
						Help
					</MenuItem>
				</NextLink>
				<NextLink href="/news" passHref legacyBehavior>
					<MenuItem as="a" cursor="pointer">
						News
					</MenuItem>
				</NextLink>
				<NextLink href="/about" passHref legacyBehavior>
					<MenuItem as="a" cursor="pointer">
						About Islebuilds
					</MenuItem>
				</NextLink>

				<MenuDivider />

				<MenuItem
					as="a"
					href={REPORT_ISSUE_URL}
					target="_blank"
					cursor="pointer"
				>
					Report an issue
				</MenuItem>
			</MenuList>
		</Menu>
	);
};
