import { Box } from '@chakra-ui/react';
import React from 'react';
import { PageHeading } from '../atoms/PageHeading';
import { BackLink } from './BackLink';

interface SpriteNotFoundProps {}

export const SpriteNotFound: React.FC<SpriteNotFoundProps> = ({}) => {
	return (
		<>
			<PageHeading>Sprite not found</PageHeading>
			<Box mt={2}>
				<BackLink href="/sprite-editor/gallery">
					Sprite Gallery
				</BackLink>
			</Box>

			<Box mt={4}>
				This sprite doesn&apos;t exist, is deleted, or is private.
			</Box>
		</>
	);
};
