import { Button } from '@chakra-ui/react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';

interface LogInButtonProps {}

export const LogInButton: React.FC<LogInButtonProps> = ({}) => {
	const router = useRouter();

	return (
		<NextLink href={'/login?next=' + router.pathname} legacyBehavior>
			<Button colorScheme="teal">Log in</Button>
		</NextLink>
	);
};
