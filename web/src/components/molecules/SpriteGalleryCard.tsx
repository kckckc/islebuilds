import {
	Box,
	chakra,
	DeepPartial,
	Flex,
	Heading,
	Text,
} from '@chakra-ui/react';
import NextLink from 'next/link';
import React, { useRef } from 'react';
import { Sprite } from '../../generated/graphql';
import { useDrawSprite } from '../../hooks/useDrawSprite';
import { HiddenIndicator } from '../atoms/HiddenIndicator';
import { UserLink } from '../atoms/UserLink';

interface SpriteGalleryCardProps {
	sprite: DeepPartial<Sprite>;
	hideName?: boolean;
}

export const SpriteGalleryCard: React.FC<SpriteGalleryCardProps> = ({
	sprite,
	hideName = false,
}) => {
	const canvasRef = useRef<HTMLCanvasElement | null>(null);
	useDrawSprite(canvasRef, sprite);

	const userId = sprite.user?.id;
	const userName = sprite.user?.displayName;

	return (
		<Flex
			direction="column"
			shadow="md"
			borderWidth="1px"
			rounded="md"
			justifyContent="space-between"
		>
			<NextLink
				href={'/sprite-editor/' + sprite.id}
				passHref
				legacyBehavior
			>
				<chakra.a w="100%" h="100%">
					<chakra.canvas
						w="100%"
						bg="#2d2136"
						p={4}
						roundedTop="md"
						style={{
							imageRendering: 'pixelated',
						}}
						ref={canvasRef}
					></chakra.canvas>
				</chakra.a>
			</NextLink>
			<Box p={2}>
				<NextLink
					href={'/sprite-editor/' + sprite.id}
					passHref
					legacyBehavior
				>
					<chakra.a
						display="flex"
						flexDirection="row"
						justifyContent="space-between"
					>
						<Heading
							textOverflow="ellipsis"
							whiteSpace="nowrap"
							overflow="hidden"
							size="md"
						>
							{sprite.name}
						</Heading>
						{!sprite.visible && hideName && (
							<chakra.span ml={2}>
								<HiddenIndicator />
							</chakra.span>
						)}
					</chakra.a>
				</NextLink>
				{userId && userName && !hideName && (
					<Flex direction="row" justifyContent="space-between">
						<Text>
							Posted by{' '}
							<UserLink id={userId} displayName={userName} />
						</Text>
						{!sprite.visible && (
							<chakra.span ml={2}>
								<HiddenIndicator />
							</chakra.span>
						)}
					</Flex>
				)}
			</Box>
		</Flex>
	);
};
