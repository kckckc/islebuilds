import { chakra, Flex, Heading, Text, Tooltip } from '@chakra-ui/react';
import React from 'react';
import { useCurrentTime } from '../../hooks/useCurrentTime';
import {
	toDateTime,
	humanizeAgo,
	humanizeUntil,
	hhmmUTC,
} from '../../util/humanize';
import humanizeDuration from 'humanize-duration';
import parser from 'cron-parser';

const shortEnHumanizer = humanizeDuration.humanizer({
	language: 'shortEn',
	languages: {
		shortEn: {
			y: () => 'y',
			mo: () => 'mo',
			w: () => 'w',
			d: () => 'd',
			h: () => 'hr',
			m: () => 'min',
			s: () => 's',
			ms: () => 'ms',
		},
	},
});

interface TimerProps {
	name: string;
	cron: string;
}

export const Timer: React.FC<TimerProps> = ({ name, cron }) => {
	const { timestamp } = useCurrentTime(1000);

	const job = parser.parseExpression(cron, { utc: true });
	const nextDate = job.next();
	const next = nextDate.getTime();
	const until = next - timestamp;

	return (
		<Flex direction="row" m={2}>
			{/* <Box>
			icon
		</Box> */}
			<Flex direction="column">
				<Heading size="md">{name}</Heading>
				<Text>at {hhmmUTC(nextDate)} server time,</Text>
				<Text>
					in{' '}
					<Tooltip label={toDateTime(next)}>
						<chakra.span
							textDecoration="dotted underline"
							textUnderlineOffset={2}
							suppressHydrationWarning={true}
						>
							{shortEnHumanizer(until, {
								round: true,
								largest: 2,
							})}
						</chakra.span>
					</Tooltip>
				</Text>
			</Flex>
		</Flex>
	);
};
