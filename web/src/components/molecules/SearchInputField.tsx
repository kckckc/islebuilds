import React from 'react';
import { useField, FieldHookConfig, useFormikContext, Formik } from 'formik';
import {
	Box,
	Button,
	FormControl,
	FormErrorMessage,
	FormLabel,
	IconButton,
	Input,
	InputGroup,
	InputRightElement,
} from '@chakra-ui/react';
import { Search2Icon } from '@chakra-ui/icons';

type SearchInputFieldProps = FieldHookConfig<any> & {
	placeholder: string;
	smallIcon?: boolean;
};

export const SearchInputField: React.FC<SearchInputFieldProps> = (props) => {
	const [field, { error }] = useField(props);
	const { dirty, isSubmitting } = useFormikContext();

	return (
		<FormControl isInvalid={!!error}>
			<InputGroup size="md">
				<Input {...field} placeholder={props.placeholder} />
				<InputRightElement w="fit-content" mr={1}>
					<IconButton
						type="submit"
						aria-label="Search"
						isLoading={isSubmitting}
						disabled={!dirty}
						colorScheme="teal"
						icon={<Search2Icon />}
						size="sm"
						display={props.smallIcon ? 'block' : { md: 'none' }}
					></IconButton>

					<Button
						type="submit"
						isLoading={isSubmitting}
						disabled={!dirty}
						colorScheme="teal"
						leftIcon={<Search2Icon />}
						size="sm"
						display={
							props.smallIcon
								? 'none'
								: { base: 'none', md: 'block' }
						}
					>
						Search
					</Button>
				</InputRightElement>
			</InputGroup>
			{error ? <FormErrorMessage>{error}</FormErrorMessage> : null}
		</FormControl>
	);
};
