import { Center, Stack, Text } from '@chakra-ui/react';
import React from 'react';
import { useAuthTokensQuery } from '../../generated/graphql';
import { TokenListItem } from './TokenListItem';

interface TokenListProps {}

export const TokenList: React.FC<TokenListProps> = ({}) => {
	const { loading, data, error } = useAuthTokensQuery();

	// TODO
	if (loading) return null;

	const items = data?.authTokens ?? [];

	if (!items.length) {
		return (
			<Center>
				<Text>No keys created yet.</Text>
			</Center>
		);
	}

	return (
		<Stack spacing={4}>
			{items.map((i) => (
				<TokenListItem key={i.id} token={i} />
			))}
		</Stack>
	);
};
