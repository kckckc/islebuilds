import { Box, Flex, Heading, Text } from '@chakra-ui/react';
import React, { ReactNode } from 'react';

interface LabeledBoxProps {
	label?: string;
	description?: string;
	children?: ReactNode;
}

export const LabeledBox: React.FC<LabeledBoxProps> = ({
	label,
	description,
	children,
}) => (
	<Box borderWidth="1px">
		{(label || description) && (
			<Flex flexDir="row" alignItems="baseline">
				{label && (
					<Heading size="sm" m={2}>
						{label}
					</Heading>
				)}
				{description && <Text color="gray.500">{description}</Text>}
			</Flex>
		)}
		<Box p={2}>{children}</Box>
	</Box>
);
