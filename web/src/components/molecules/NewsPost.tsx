import { Heading, Link, Text } from '@chakra-ui/react';
import React, { ReactNode } from 'react';
import { Section } from '../templates/Section';
import { SectionHeading } from '../templates/SectionHeading';
import NextLink from 'next/link';
import { humanize, toDateTimeShort } from '../../util/humanize';

interface NewsPostProps {
	post: { title: string; content: ReactNode[]; at: number };
	isPreview?: boolean;
}

export const NewsPost: React.FC<NewsPostProps> = ({
	post,
	isPreview = false,
}) => {
	return (
		<Section>
			<SectionHeading
				noColor
				href={isPreview ? '/news' : undefined}
				size={isPreview ? 'md' : 'lg'}
			>
				{post.title}
			</SectionHeading>

			<Text fontStyle="italic" suppressHydrationWarning={true}>
				Posted{' '}
				{isPreview ? toDateTimeShort(post.at) : humanize(post.at)}
			</Text>

			{isPreview ? (
				<>
					{typeof post.content[0] === 'string' ? (
						<Text>{post.content[0]}</Text>
					) : (
						post.content[0]
					)}

					<Text>
						<NextLink href="/news" passHref legacyBehavior>
							<Link color="blue.500">read more...</Link>
						</NextLink>
					</Text>
				</>
			) : (
				React.Children.map(post.content, (el) => {
					if (typeof el === 'string') {
						if (el.startsWith('heading:')) {
							el = el.replace('heading:', '');
							return <Heading size="md">{el}</Heading>;
						} else {
							return <Text>{el}</Text>;
						}
					}

					return el;
				})
			)}
		</Section>
	);
};
