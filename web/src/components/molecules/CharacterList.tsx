import { Flex, Spinner } from '@chakra-ui/react';
import React from 'react';
import { useInView } from 'react-intersection-observer';
import { Character } from '../../generated/graphql';
import { useDebouncedEffect } from '../../hooks/useDebouncedEffect';
import { CharacterListItem } from './CharacterListItem';

interface CharacterListProps {
	data?: Partial<Character>[];
	hasMore?: boolean;
	onFetchMore?: () => void;
	showFetchMore?: boolean;
	loading?: boolean;
}

export const CharacterList: React.FC<CharacterListProps> = ({
	data = [],
	hasMore = false,
	onFetchMore,
	showFetchMore = false,
	loading = false,
}) => {
	const { ref, inView } = useInView({});

	useDebouncedEffect(
		() => {
			if (inView && hasMore && onFetchMore && !loading) {
				onFetchMore();
			}
		},
		[inView, hasMore, onFetchMore, loading],
		100
	);

	return (
		<Flex flexDirection="column" rowGap={2}>
			{data.map((c) => (
				<CharacterListItem key={c.id} character={c} />
			))}

			{showFetchMore && hasMore ? (
				<Flex ref={ref} mt={4} flexDir="row" justifyContent="center">
					<Spinner></Spinner>
				</Flex>
			) : null}
		</Flex>
	);
};
