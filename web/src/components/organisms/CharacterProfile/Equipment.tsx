import { Box, Flex } from '@chakra-ui/react';
import React, { ReactNode } from 'react';
import { ThemedItemSlot } from '../../atoms/ThemedItemSlot';

interface EquipmentProps {
	eq_head?: string | null;
	eq_neck?: string | null;
	eq_chest?: string | null;
	eq_hands?: string | null;
	eq_finger1?: string | null;
	eq_finger2?: string | null;
	eq_waist?: string | null;
	eq_legs?: string | null;
	eq_feet?: string | null;
	eq_trinket?: string | null;
	eq_oneHanded?: string | null;
	eq_offHand?: string | null;
	eq_tool?: string | null;
	eq_quickslot?: string | null;
	eq_rune1?: string | null;
	eq_rune2?: string | null;
	eq_rune3?: string | null;
	eq_rune4?: string | null;

	children?: ReactNode;
}

export const Equipment: React.FC<EquipmentProps> = ({
	eq_head,
	eq_neck,
	eq_chest,
	eq_hands,
	eq_finger1,
	eq_finger2,
	eq_waist,
	eq_legs,
	eq_feet,
	eq_trinket,
	eq_oneHanded,
	eq_offHand,
	eq_tool,
	eq_quickslot,
	eq_rune1,
	eq_rune2,
	eq_rune3,
	eq_rune4,
	children,
}) => {
	return (
		<Flex flexDirection="column">
			<Flex
				columnGap={1}
				flexDirection="row"
				justifyContent={{ base: 'space-around' }}
			>
				<Flex rowGap={1} flexDirection="column">
					<ThemedItemSlot itemJson={eq_head} slot="head" />
					<ThemedItemSlot itemJson={eq_neck} slot="neck" />
					<ThemedItemSlot itemJson={eq_chest} slot="chest" />
					<ThemedItemSlot itemJson={eq_hands} slot="hands" />
					<ThemedItemSlot itemJson={eq_finger1} slot="finger-1" />
					<ThemedItemSlot itemJson={eq_finger2} slot="finger-2" />
					<ThemedItemSlot itemJson={eq_quickslot} slot="quick" />
					<Flex
						display={{ base: 'flex', md: 'none' }}
						flexDirection="column"
						rowGap={1}
					>
						<ThemedItemSlot itemJson={eq_rune1} slot="rune" />
						<ThemedItemSlot itemJson={eq_rune2} slot="rune" />
					</Flex>
				</Flex>
				<Flex
					flex={1}
					flexDirection="column"
					// alignItems="center"
					display={{ base: 'none', md: 'flex' }}
				>
					<Box flex={1}>{children}</Box>
					<Flex
						gap={1}
						flexDirection="row"
						flexWrap="wrap"
						justifyContent="center"
					>
						<ThemedItemSlot itemJson={eq_rune1} slot="rune" />
						<ThemedItemSlot itemJson={eq_rune2} slot="rune" />
						<ThemedItemSlot itemJson={eq_rune3} slot="rune" />
						<ThemedItemSlot itemJson={eq_rune4} slot="rune" />
					</Flex>
				</Flex>
				<Flex rowGap={1} flexDirection="column">
					<ThemedItemSlot itemJson={eq_waist} slot="waist" />
					<ThemedItemSlot itemJson={eq_legs} slot="legs" />
					<ThemedItemSlot itemJson={eq_feet} slot="feet" />
					<ThemedItemSlot itemJson={eq_trinket} slot="trinket" />
					<ThemedItemSlot itemJson={eq_oneHanded} slot="oneHanded" />
					<ThemedItemSlot itemJson={eq_offHand} slot="offHand" />
					<ThemedItemSlot itemJson={eq_tool} slot="tool" />
					<Flex
						display={{ base: 'flex', md: 'none' }}
						flexDirection="column"
						rowGap={1}
					>
						<ThemedItemSlot itemJson={eq_rune3} slot="rune" />
						<ThemedItemSlot itemJson={eq_rune4} slot="rune" />
					</Flex>
				</Flex>
			</Flex>

			<Box flex={1} mt={4} display={{ base: 'block', md: 'none' }}>
				{children}
			</Box>
		</Flex>
	);
};
