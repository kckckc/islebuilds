import {
	Button,
	IconButton,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Tooltip,
	useDisclosure,
} from '@chakra-ui/react';
import { Trash } from 'lucide-react';
import React from 'react';
import { useDeleteCharacterMutation } from '../../../generated/graphql';

interface DeleteCharacterButtonProps {
	characterId: string;
}

export const DeleteCharacterButton: React.FC<DeleteCharacterButtonProps> = ({
	characterId,
}) => {
	const [deleteCharacter] = useDeleteCharacterMutation();

	const { isOpen, onOpen, onClose } = useDisclosure();

	const handleDelete = async () => {
		onClose();

		await deleteCharacter({
			variables: {
				character: characterId,
			},
			refetchQueries: ['Character'],
		});
	};

	return (
		<>
			<Tooltip label="Delete character">
				<IconButton
					aria-label="Delete character"
					icon={<Trash size={20} />}
					colorScheme="red"
					onClick={onOpen}
				/>
			</Tooltip>

			<Modal
				isOpen={isOpen}
				onClose={onClose}
				preserveScrollBarGap
				isCentered
			>
				<ModalOverlay />
				<ModalContent>
					<ModalHeader>Are you sure?</ModalHeader>
					<ModalCloseButton />
					<ModalBody>
						Deleting a character cannot be undone, but you can still
						upload this character again in the future using Waddon.
					</ModalBody>

					<ModalFooter>
						<Button colorScheme="red" mr={2} onClick={handleDelete}>
							Delete
						</Button>
						<Button onClick={onClose}>Cancel</Button>
					</ModalFooter>
				</ModalContent>
			</Modal>
		</>
	);
};
