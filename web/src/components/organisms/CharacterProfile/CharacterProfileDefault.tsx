import { Box, Center, Divider, useColorMode } from '@chakra-ui/react';
import React from 'react';
import {
	CharacterRanking,
	FullCharacterFragment,
	RegularUserFragment,
} from '../../../generated/graphql';
import { NothingText } from '../../atoms/NothingText';
import { PassiveTree } from '../PassiveTree';
import { PassiveStatsList } from '../PassiveTree/PassiveStatsList';
import { CharacterInventory } from './CharacterInventory';
import { CharacterReputation } from './CharacterReputation';
import { CharacterStats } from './CharacterStats';
import { Equipment } from './Equipment';

interface CharacterProfileDefaultProps {
	character: FullCharacterFragment & {
		user: RegularUserFragment;
		rankings: CharacterRanking[];
	};
}

export const CharacterProfileDefault: React.FC<
	CharacterProfileDefaultProps
> = ({ character }) => {
	const { colorMode } = useColorMode();

	return (
		<>
			{/* <Divider my={8} /> */}

			{/* <Heading size="md">Equipment</Heading> */}

			<Box mt={8}>
				<Equipment {...character}>
					<CharacterStats character={character} />
				</Equipment>
			</Box>

			{(character.isMine || character?.inventory?.visible) && (
				<>
					<Divider my={8} />
					<CharacterInventory
						isStoreView={false}
						isMine={character.isMine}
						inventory={character.inventory}
					/>
				</>
			)}

			<Divider my={8} />

			{/* <Heading size="md">Reputation</Heading> */}

			<Box>
				<CharacterReputation character={character} />
			</Box>

			<Divider my={8} />

			{(character.passives?.length ?? 0) > 1 ? (
				<>
					<PassiveTree
						selected={character.passives}
						divBackground={colorMode === 'light'}
						drawBackground={false}
					/>

					<Box mt={4}>
						<PassiveStatsList
							level={character.level}
							selected={character.passives}
						/>
					</Box>
				</>
			) : (
				<Center>
					<NothingText>No passives selected</NothingText>
				</Center>
			)}
		</>
	);
};
