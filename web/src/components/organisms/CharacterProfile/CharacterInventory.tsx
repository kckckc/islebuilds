import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import { Box, Center, Flex, IconButton, Tooltip } from '@chakra-ui/react';
import { IWDItem } from '@kckckc/isleward-util/isleward';
import React from 'react';
import {
	FullCharacterFragment,
	useSetInventoryVisibleMutation,
} from '../../../generated/graphql';
import { useIsMobile } from '../../../hooks/useIsMobile';
import { NothingText } from '../../atoms/NothingText';
import { PlainLink } from '../../atoms/PlainLink';
import { TimeAgo } from '../../atoms/TimeAgo';
import { InventoryItemList } from '../../pages/InventoryPage/InventoryItemList';
import {
	InventoryViewerContext,
	mockInventoryViewerContext,
} from '../../pages/InventoryPage/InventoryViewerContext';
import { SectionHeading } from '../../templates/SectionHeading';

interface CharacterInventoryProps {
	isStoreView: boolean;
	isMine: boolean;
	inventory: FullCharacterFragment['inventory'];
}

export const CharacterInventory: React.FC<CharacterInventoryProps> = ({
	isStoreView,
	isMine,
	inventory,
}) => {
	const showCompact = useIsMobile();

	const [setVisible, { loading: setVisibleLoading }] =
		useSetInventoryVisibleMutation();

	if (!inventory) {
		return (
			<Center>
				<NothingText>
					{isStoreView ? (
						<>Store view is not available for this character.</>
					) : (
						<>
							Inventory not uploaded yet.{' '}
							<PlainLink href="/inventory">More info</PlainLink>
						</>
					)}
				</NothingText>
			</Center>
		);
	}

	const uploadedAt = new Date(parseInt(inventory.uploadedAt));

	let items: IWDItem[] = [];
	try {
		items = JSON.parse(inventory.items ?? '[]');
	} catch (e) {
		console.error('Failed to parse inventory items:', inventory.items);
		items = [];
	}

	return (
		<Flex direction="column" gap={4} alignItems="center">
			<Flex direction="row" gap={4} alignItems="baseline" width="100%">
				<SectionHeading size="md">Inventory</SectionHeading>
				<Box>
					<TimeAgo date={uploadedAt} />
				</Box>
				<Box flex={1}></Box>
				{isMine && (
					<Box>
						<Tooltip
							label={
								inventory.visible
									? 'Anyone can see this'
									: 'Only you can see this'
							}
						>
							<IconButton
								aria-label="Toggle visibility"
								icon={
									inventory.visible ? (
										<ViewIcon />
									) : (
										<ViewOffIcon />
									)
								}
								isLoading={setVisibleLoading}
								onClick={() => {
									setVisible({
										variables: {
											inventory: inventory.id,
											visible: !inventory.visible,
										},
										refetchQueries: ['Character'],
									});
								}}
							/>
						</Tooltip>
					</Box>
				)}
			</Flex>

			<InventoryViewerContext.Provider
				value={{
					...mockInventoryViewerContext(),
					compact: showCompact,
					limitWidth: false,
					hideEquipped: true,
					smallItems: true,
				}}
			>
				<InventoryItemList items={items} isStash={false} />
			</InventoryViewerContext.Provider>
		</Flex>
	);
};
