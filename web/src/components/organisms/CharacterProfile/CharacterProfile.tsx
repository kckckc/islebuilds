import { Box, Center, Divider, useColorMode } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import { useRouter } from 'next/router';
import React from 'react';
import {
	CharacterRanking,
	FullCharacterFragment,
	RegularUserFragment,
} from '../../../generated/graphql';
import { capitalize } from '../../../util/capitalize';
import { NothingText } from '../../atoms/NothingText';
import { PassiveTree } from '../PassiveTree';
import { PassiveStatsList } from '../PassiveTree/PassiveStatsList';
import { WithReportCharacterModal } from '../ReportModal/WithReportCharacterModal';
import { CharacterDetails } from './CharacterDetails';
import { CharacterInventory } from './CharacterInventory';
import { CharacterProfileDefault } from './CharacterProfileDefault';
import { CharacterProfileStore } from './CharacterProfileStore';
import { CharacterReputation } from './CharacterReputation';
import { CharacterStats } from './CharacterStats';
import { Equipment } from './Equipment';

interface CharacterProfileProps {
	character: FullCharacterFragment & {
		user: RegularUserFragment;
		rankings: CharacterRanking[];
	};
}

export const CharacterProfile: React.FC<CharacterProfileProps> = ({
	character,
}) => {
	const router = useRouter();

	const seoProphs = [
		character.prophecy_austere && 'Austere',
		character.prophecy_butcher && 'Butcher',
		character.prophecy_crushable && 'Crushable',
		character.prophecy_hardcore && 'Hardcore',
	]
		.filter((x) => !!x)
		.join(', ');
	const seoDesc = `${character.name} - Level ${character.level} ${capitalize(
		character.spirit
	)}${seoProphs !== '' ? ' - ' + seoProphs : ''}`;

	const { view } = router.query;
	const LayoutComponent =
		view === 'store' ? CharacterProfileStore : CharacterProfileDefault;

	return (
		<WithReportCharacterModal characterId={character.id}>
			<NextSeo
				title={character.name + ' | Character'}
				description={seoDesc}
			/>

			<CharacterDetails character={character} />

			<LayoutComponent character={character} />
		</WithReportCharacterModal>
	);
};
