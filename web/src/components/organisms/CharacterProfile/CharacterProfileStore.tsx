import { Box } from '@chakra-ui/react';
import React from 'react';
import {
	CharacterRanking,
	FullCharacterFragment,
	RegularUserFragment,
} from '../../../generated/graphql';
import { CharacterInventory } from './CharacterInventory';

interface CharacterProfileStoreProps {
	character: FullCharacterFragment & {
		user: RegularUserFragment;
		rankings: CharacterRanking[];
	};
}

export const CharacterProfileStore: React.FC<CharacterProfileStoreProps> = ({
	character,
}) => {
	return (
		<Box mt={8}>
			<CharacterInventory
				isStoreView={true}
				isMine={character.isMine}
				inventory={character.inventory}
			/>
		</Box>
	);
};
