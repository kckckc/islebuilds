import { Tab, TabList, TabPanel, TabPanels, Tabs } from '@chakra-ui/react';
import {
	applyItemStats,
	applyPassiveNodes,
	Stats,
} from '@kckckc/isleward-util';
import { IWDItem } from '@kckckc/isleward-util/isleward';
import React from 'react';
import {
	FullCharacterFragment,
	RegularUserFragment,
} from '../../../generated/graphql';
import { useIsMobile } from '../../../hooks/useIsMobile';
import { StatsTab } from './StatsTab';

const categories = {
	basic: [
		'hpMax',
		'manaMax',
		'regenHp',
		'regenMana',
		'',
		'str',
		'int',
		'dex',
		'vit',
		'',
		'magicFind',
		'itemQuantity',
		'',
		'sprintChance',
		'',
		'xpIncrease',
	],
	offense: [
		'critChance',
		'critMultiplier',
		'attackCritChance',
		'attackCritMultiplier',
		'spellCritChance',
		'spellCritMultiplier',
		'',
		'elementArcanePercent',
		'elementFirePercent',
		'elementFrostPercent',
		'elementHolyPercent',
		'elementPoisonPercent',
		'physicalPercent',
		'spellPercent',
		'',
		'attackSpeed',
		'castSpeed',
	],
	defense: [
		'armor',
		'',
		'blockAttackChance',
		'blockSpellChance',
		'',
		'dodgeAttackChance',
		'dodgeSpellChance',
		'',
		'elementArcaneResist',
		'elementFireResist',
		'elementFrostResist',
		'elementHolyResist',
		'elementPoisonResist',
		'',
		'elementAllResist',
		'',
		'lifeOnHit',
	],
	// fishing: [
	// 	'catchChance',
	// 	'catchSpeed',
	// 	'fishRarity',
	// 	'fishWeight',
	// 	'fishItems',
	// ],
};

interface CharacterStatsProps {
	character: FullCharacterFragment & {
		user: RegularUserFragment;
	};
}

export const CharacterStats: React.FC<CharacterStatsProps> = ({
	character,
}) => {
	const isMobile = useIsMobile();
	const tabsVariant = isMobile ? 'solid-rounded' : 'line';
	const tabsBorder = isMobile ? '1px' : undefined;

	// Calculate character stats
	const stats = new Stats(character.level, true, character.spirit);
	applyPassiveNodes(stats, character.passives ?? []);
	const eqs: (keyof typeof character)[] = [
		'eq_head',
		'eq_neck',
		'eq_chest',
		'eq_hands',
		'eq_finger1',
		'eq_finger2',
		'eq_waist',
		'eq_legs',
		'eq_feet',
		'eq_trinket',
		'eq_oneHanded',
		'eq_offHand',
		'eq_tool',
	];
	eqs.map((prop) => character[prop]).forEach((raw) => {
		const parsedItem = JSON.parse(raw as string);
		if (parsedItem) {
			applyItemStats(stats, parsedItem as IWDItem, true);
		}
	});

	return (
		<Tabs
			mx={4}
			isFitted
			colorScheme="teal"
			variant={tabsVariant ?? 'line'}
		>
			<TabList flexDirection={{ base: 'column', md: 'row' }}>
				<Tab borderWidth={tabsBorder}>Basic</Tab>
				<Tab borderWidth={tabsBorder}>Offensive</Tab>
				<Tab borderWidth={tabsBorder}>Defensive</Tab>
			</TabList>

			<TabPanels>
				<TabPanel>
					<StatsTab statNames={categories.basic} stats={stats} />
				</TabPanel>
				<TabPanel>
					<StatsTab statNames={categories.offense} stats={stats} />
				</TabPanel>
				<TabPanel>
					<StatsTab statNames={categories.defense} stats={stats} />
				</TabPanel>
			</TabPanels>
		</Tabs>
	);
};
