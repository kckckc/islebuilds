import { IconButton, Tooltip } from '@chakra-ui/react';
import { WarehouseIcon } from 'lucide-react';
import { useRouter } from 'next/router';
import React from 'react';

interface CharacterLayoutButtonProps {}

export const CharacterLayoutButton: React.FC<
	CharacterLayoutButtonProps
> = ({}) => {
	const router = useRouter();

	const label =
		router.query.view === 'store' ? 'Show normal view' : 'Show store view';

	return (
		<Tooltip label={label}>
			<IconButton
				aria-label={label}
				icon={<WarehouseIcon size={20} />}
				onClick={() => {
					const query = { ...router.query };
					if (query.view === 'store') {
						delete query.view;
					} else {
						query.view = 'store';
					}
					router.push(
						{
							pathname: router.pathname,
							query: query,
						},
						undefined,
						{
							shallow: true,
						}
					);
				}}
			/>
		</Tooltip>
	);
};
