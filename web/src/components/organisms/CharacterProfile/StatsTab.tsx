import { Box, Divider, Flex } from '@chakra-ui/react';
import {
	Stats,
	translateStatName,
	translateStatValue,
} from '@kckckc/isleward-util';
import React from 'react';

interface StatsTabProps {
	statNames: string[];
	stats: Stats;
}

export const StatsTab: React.FC<StatsTabProps> = ({ statNames, stats }) => {
	return (
		<Flex flex={1} justifyContent="center">
			<Flex flexDirection="column" columnGap={undefined}>
				{statNames.map((stat, i) => {
					if (stat !== '') {
						let value: number =
							stats.values[stat as keyof typeof stats.values];

						// Add global crit chance to spell/attack crit chance
						if (stat.indexOf('CritChance') > 0) {
							value += stats.values.critChance;
						}
						if (stat.indexOf('CritMultiplier') > 0) {
							value += stats.values.critMultiplier;
						}

						// Show as 125% instead of 25%
						if (stat === 'castSpeed' || stat === 'attackSpeed') {
							value += 100;
						}

						return (
							<Box key={i}>
								{translateStatName(stat)}:{' '}
								{translateStatValue(stat, value, true)}
							</Box>
						);
					}

					// Blank string makes a spacer
					// return <Box key={i}>-</Box>;
					return <Divider key={i} my={2} />;
				})}
			</Flex>
		</Flex>
	);
};
