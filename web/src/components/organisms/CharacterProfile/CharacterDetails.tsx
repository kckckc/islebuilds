import { Box, Flex, Heading, Link, Text } from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';
import {
	FullCharacterFragment,
	RegularUserFragment,
	useMeQuery,
} from '../../../generated/graphql';
import { LeagueTag } from '../../atoms/LeagueTag';
import { LegacyProphecies } from '../../atoms/LegacyPropheciesTag';
import { LevelTag } from '../../atoms/LevelTag';
import { ReportCharacterButton } from '../../atoms/ReportButton/ReportCharacterButton';
import { SelfTag } from '../../atoms/SelfTag';
import { SkinDisplay } from '../../atoms/SkinDisplay';
import { SpiritTag } from '../../atoms/SpiritTag';
import { TimeAgo } from '../../atoms/TimeAgo';
import { CharacterLayoutButton } from './CharacterLayoutButton';
import { DeleteCharacterButton } from './DeleteCharacterButton';

interface CharacterDetailsProps {
	character: FullCharacterFragment & {
		user: RegularUserFragment;
	};
}

export const CharacterDetails: React.FC<CharacterDetailsProps> = ({
	character,
}) => {
	const { data: me } = useMeQuery();

	const uploadedAt = new Date(parseInt(character.uploadedAt));

	return (
		<Flex flexDirection="row">
			<SkinDisplay
				skinSheetName={character.skinSheetName}
				skinCell={character.skinCell}
				skinId={character.skinId}
			/>
			<Box ml={4}>
				<Heading size="lg">{character.name}</Heading>{' '}
				<Text>
					uploaded <TimeAgo date={uploadedAt} /> by{' '}
					<NextLink
						href={`/user/${character.user.id}`}
						passHref
						legacyBehavior
					>
						<Link color="teal.500">
							{character.user.displayName}
						</Link>
					</NextLink>
				</Text>
				<Flex flexDirection="row" columnGap={1} mt={2}>
					{/* <PortraitIcon {...character} /> */}
					<LeagueTag {...character} />
					<LevelTag {...character} />
					<SpiritTag {...character} />
					<LegacyProphecies {...character} />
					<SelfTag userId={character.user.id} />
				</Flex>
				<Flex
					flexDirection="row"
					gap={2}
					display={{ base: 'flex', md: 'none' }}
					mt={2}
				>
					<ReportCharacterButton />
					<CharacterLayoutButton />
					{me?.me?.id == character.user.id && (
						<DeleteCharacterButton characterId={character.id} />
					)}
				</Flex>
			</Box>

			<Box flex={1} />

			<Flex
				flexDirection="row"
				gap={2}
				display={{ base: 'none', md: 'flex' }}
			>
				{me?.me?.id == character.user.id && (
					<DeleteCharacterButton characterId={character.id} />
				)}
				<CharacterLayoutButton />
				<ReportCharacterButton />
			</Flex>
		</Flex>
	);
};
