import { Box, Flex } from '@chakra-ui/react';
import React from 'react';
import { FactionIcon } from '../../atoms/FactionIcon';
import { getFactionName } from '../../atoms/FactionName';

const TIERS = [
	{
		tier: 0,
		name: 'Hated',
		rep: -25000,
	},
	{
		tier: 1,
		name: 'Hostile',
		rep: -10000,
	},
	{
		tier: 2,
		name: 'Unfriendly',
		rep: -1000,
	},
	{
		tier: 3,
		name: 'Neutral',
		rep: 0,
	},
	{
		tier: 4,
		name: 'Friendly',
		rep: 1000,
	},
	{
		tier: 5,
		name: 'Honored',
		rep: 10000,
	},
	{
		tier: 6,
		name: 'Revered',
		rep: 25000,
	},
	{
		tier: 7,
		name: 'Exalted',
		rep: 50000,
	},
];

const findTier = (rep: number) => {
	let highestTier = TIERS[0];
	for (let tier of TIERS) {
		if (rep >= tier.rep) {
			highestTier = tier;
		}
	}
	return highestTier;
};

interface ReputationBarProps {
	factionId: string;
	rep: number;
	ranking?: number;
}

export const ReputationBar: React.FC<ReputationBarProps> = ({
	factionId,
	rep,
	ranking,
}) => {
	const currentTier = findTier(rep);

	let bar;

	let percentText;
	let leftText;
	let centerText;
	let rightText;

	if (currentTier.name !== 'Exalted') {
		const nextTier = TIERS[currentTier.tier + 1];

		const percent =
			((rep - currentTier.rep) / (nextTier.rep - currentTier.rep)) * 100;

		const roundedPercent = Math.round(percent * 10) / 10;

		percentText = `${roundedPercent}%`;
		centerText = `${currentTier.name} ${roundedPercent}%`;
		rightText = `(${rep}/${nextTier.rep} rep)`;
	} else {
		percentText = '100%';
		centerText = 'Exalted';
		rightText = `(${rep} rep)`;
	}

	if (ranking) {
		leftText = `Leaderboard #${ranking}`;
	}

	bar = (
		<Box
			position="relative"
			rounded="lg"
			width="100%"
			height="100%"
			backgroundColor="#533399"
		>
			<Box
				position="absolute"
				rounded="lg"
				width={percentText}
				height="100%"
				backgroundColor="#7a3ad3"
			/>
			<Flex
				zIndex={1}
				fontSize={{ base: 'sm', md: 'md' }}
				position="absolute"
				width="100%"
				height="100%"
				justifyContent="space-between"
				alignItems="center"
			>
				<Box
					flex={1}
					flexShrink={1}
					textAlign="left"
					color="q0"
					px={2}
					filter={`drop-shadow(0px -2px 0px #312136)
						drop-shadow(0px 2px 0px #312136)
						drop-shadow(2px 0px 0px #312136)
						drop-shadow(-2px 0px 0px #312136)`}
					display={{ base: 'none', md: 'block' }}
				>
					{leftText}
				</Box>

				<Box
					flex={1}
					textAlign={{ base: 'left', md: 'center' }}
					px={2}
					color="q0"
					filter={`drop-shadow(0px -2px 0px #312136)
						drop-shadow(0px 2px 0px #312136)
						drop-shadow(2px 0px 0px #312136)
						drop-shadow(-2px 0px 0px #312136)`}
				>
					{centerText}
				</Box>

				<Box
					flex={1}
					textAlign="right"
					color="q0"
					mr={2}
					filter={`drop-shadow(0px -2px 0px #312136)
						drop-shadow(0px 2px 0px #312136)
						drop-shadow(2px 0px 0px #312136)
						drop-shadow(-2px 0px 0px #312136)`}
				>
					{rightText}
				</Box>
			</Flex>
		</Box>
	);

	return (
		<Flex flexDirection="row">
			<Box mr={2}>
				<FactionIcon
					href={`/leaderboard/faction/${factionId}`}
					factionId={factionId}
				/>
			</Box>
			<Flex
				flex={1}
				flexDirection="column"
				justifyContent="space-between"
				rowGap={1}
			>
				<Box />
				<Box>{getFactionName(factionId)}</Box>
				<Box height="28px">{bar}</Box>
			</Flex>
		</Flex>
	);
};
