import { Center, Flex } from '@chakra-ui/react';
import React from 'react';
import {
	CharacterRanking,
	FullCharacterFragment,
	RegularUserFragment,
} from '../../../generated/graphql';
import { NothingText } from '../../atoms/NothingText';
import { ReputationBar } from './ReputationBar';

interface CharacterReputationProps {
	character: FullCharacterFragment & {
		user: RegularUserFragment;
		rankings: CharacterRanking[];
	};
}

export const CharacterReputation: React.FC<CharacterReputationProps> = ({
	character,
}) => {
	const hasReputation =
		character.rep_akarei ||
		character.rep_fjolgard ||
		character.rep_gaekatla ||
		character.rep_peopleOfTheSun ||
		character.rep_pumpkinSailor ||
		character.rep_theWinterMan;

	if (!hasReputation) {
		return (
			<Center>
				<NothingText>No factions discovered</NothingText>
			</Center>
		);
	}

	return (
		<Flex flexDirection="column" rowGap={2}>
			{character.rep_akarei && (
				<ReputationBar
					factionId="akarei"
					rep={character.rep_akarei}
					ranking={
						character?.rankings?.find(
							(r) => r.leaderboard === 'akarei'
						)?.rank
					}
				/>
			)}
			{character.rep_fjolgard && (
				<ReputationBar
					factionId="fjolgard"
					rep={character.rep_fjolgard}
					ranking={
						character?.rankings?.find(
							(r) => r.leaderboard === 'fjolgard'
						)?.rank
					}
				/>
			)}
			{character.rep_gaekatla && (
				<ReputationBar
					factionId="gaekatla"
					rep={character.rep_gaekatla}
					ranking={
						character?.rankings?.find(
							(r) => r.leaderboard === 'gaekatla'
						)?.rank
					}
				/>
			)}
			{character.rep_peopleOfTheSun && (
				<ReputationBar
					factionId="peopleOfTheSun"
					rep={character.rep_peopleOfTheSun}
					ranking={
						character?.rankings?.find(
							(r) => r.leaderboard === 'peopleOfTheSun'
						)?.rank
					}
				/>
			)}
			{character.rep_pumpkinSailor && (
				<ReputationBar
					factionId="pumpkinSailor"
					rep={character.rep_pumpkinSailor}
					ranking={
						character?.rankings?.find(
							(r) => r.leaderboard === 'pumpkinSailor'
						)?.rank
					}
				/>
			)}
			{character.rep_theWinterMan && (
				<ReputationBar
					factionId="theWinterMan"
					rep={character.rep_theWinterMan}
					ranking={
						character?.rankings?.find(
							(r) => r.leaderboard === 'theWinterMan'
						)?.rank
					}
				/>
			)}
		</Flex>
	);
};
