import { Center } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import {
	useCreateAuthTokenMutation,
	useGenerateOtpMutation,
} from '../../generated/graphql';

interface WaddonSetupWizardProps {
	active: boolean;
}

export const WaddonSetupWizard: React.FC<WaddonSetupWizardProps> = ({
	active,
}) => {
	const [createAuthToken] = useCreateAuthTokenMutation();
	const [generateOTP] = useGenerateOtpMutation();
	const [tokenCreated, setTokenCreated] = useState(false);
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(false);

	useEffect(() => {
		if (tokenCreated) return;
		if (!active) return;

		const work = async () => {
			const tokenResult = await createAuthToken({
				variables: {
					label: 'Waddon',
				},
			});
			const token = tokenResult.data?.createAuthToken;

			if (!token) {
				setLoading(false);
				setError(true);
				return;
			}

			const otpResult = await generateOTP({
				variables: {
					authTokenId: token.id!,
				},
			});
			const otp = otpResult.data?.generateOTP;
			if (!otp) {
				setLoading(false);
				setError(true);
				return;
			}

			window.open(
				`https://play.isleward.com/?islebuilds=${otp}`,
				'_blank'
			);

			setLoading(false);
		};

		work();
		setTokenCreated(true);
	}, [active, tokenCreated, createAuthToken, generateOTP]);

	if (!active) return null;

	if (error) return <Center>Something went wrong.</Center>;

	if (loading) return <Center>Generating key...</Center>;

	return <Center>Done!</Center>;
};
