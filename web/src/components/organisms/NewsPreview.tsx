import { Box, Link, Text } from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';
import { newsData } from '../../static/newsData';
import { NewsPost } from '../molecules/NewsPost';
import { SectionHeading } from '../templates/SectionHeading';

interface NewsPreviewProps {}

export const NewsPreview: React.FC<NewsPreviewProps> = ({}) => {
	return (
		<>
			<Box mb={2}>
				<SectionHeading href="/news" noColor>
					Recent News
				</SectionHeading>
			</Box>

			<NewsPost post={newsData[0]} isPreview />

			<Box mt={2}>
				<Text textAlign="right">
					<NextLink href="/news" passHref legacyBehavior>
						<Link color="blue.500">more news...</Link>
					</NextLink>
				</Text>
			</Box>
		</>
	);
};
