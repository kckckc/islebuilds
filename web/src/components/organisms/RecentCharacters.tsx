import { Box } from '@chakra-ui/react';
import React from 'react';
import { useRecentCharactersQuery } from '../../generated/graphql';
import { CenterLoading } from '../atoms/CenterLoading';
import { CharacterList } from '../molecules/CharacterList';

interface RecentCharactersProps {}

export const RecentCharacters: React.FC<RecentCharactersProps> = ({}) => {
	const { data, loading, error, fetchMore } = useRecentCharactersQuery({
		variables: {
			limit: 5,
		},
	});

	if (loading) {
		return <CenterLoading />;
	} else if (error) {
		return <Box>error</Box>;
	}

	if (!data?.recentCharacters) return null;

	return (
		<CharacterList
			data={data.recentCharacters.characters}
			hasMore={data.recentCharacters.hasMore}
			onFetchMore={() => {
				fetchMore({
					variables: {
						cursor: data.recentCharacters.cursor,
					},
				});
			}}
			showFetchMore={false}
			loading={loading}
		/>
	);
};
