import {
	Checkbox,
	Code,
	DeepPartial,
	Divider,
	Flex,
	Heading,
} from '@chakra-ui/react';
import React from 'react';
import { User, useSetUserRoleMutation } from '../../generated/graphql';
import { useRoleCheck } from '../../hooks/useRoleCheck';
import { NothingText } from '../atoms/NothingText';

interface ProfileRolesProps {
	user: DeepPartial<User>;
}

export const ProfileRoles: React.FC<ProfileRolesProps> = ({ user }) => {
	const isSuperadmin = useRoleCheck('*');
	const isAdmin = useRoleCheck('ADMIN');
	const isModerator = useRoleCheck('MODERATION');

	const [setModeration, { loading: moderationLoading }] =
		useSetUserRoleMutation({ refetchQueries: ['User', 'Me'] });
	const [setAdmin, { loading: adminLoading }] = useSetUserRoleMutation({
		refetchQueries: ['User', 'Me'],
	});

	if (!isModerator) return null;

	const roles = user?.roles ?? [];

	return (
		<>
			<Divider my={8} />

			<Heading size="md">Roles</Heading>

			<Flex mt={4} direction="column" gap={2}>
				<Checkbox
					isDisabled={moderationLoading}
					// isIndeterminate={moderationLoading}
					isChecked={roles.includes('MODERATION')}
					onChange={async () => {
						if (!isAdmin) return;

						if (!user.id) return;

						await setModeration({
							variables: {
								user: user.id,
								role: 'MODERATION',
								value: !roles.includes('MODERATION'),
							},
						});
					}}
				>
					<Code mr={2}>MODERATION</Code>
					{isAdmin && <NothingText>Can be modified</NothingText>}
				</Checkbox>
				<Checkbox
					isDisabled={adminLoading}
					// isIndeterminate={adminLoading}
					isChecked={roles.includes('ADMIN')}
					onChange={async () => {
						if (!isSuperadmin) return;

						if (!user.id) return;

						await setAdmin({
							variables: {
								user: user.id,
								role: 'ADMIN',
								value: !roles.includes('ADMIN'),
							},
						});
					}}
				>
					<Code mr={2}>ADMIN</Code>
					{isSuperadmin && <NothingText>Can be modified</NothingText>}
				</Checkbox>
				<Checkbox isChecked={roles.includes('*')}>
					<Code>*</Code>
				</Checkbox>
			</Flex>
		</>
	);
};
