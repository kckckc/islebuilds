import { Code, Text } from '@chakra-ui/react';
import React, { Fragment } from 'react';
import { useMeQuery } from '../../generated/graphql';
import { Section } from '../templates/Section';
import { SectionHeading } from '../templates/SectionHeading';

interface AccountRolesProps {}

export const AccountRoles: React.FC<AccountRolesProps> = ({}) => {
	const { data } = useMeQuery();

	if (!data?.me?.roles) return null;

	return (
		<Section>
			<SectionHeading>Roles</SectionHeading>
			<Text>
				You have been granted the following roles:{' '}
				{data.me.roles.map((r, i, a) => (
					<Fragment key={r}>
						<Code>{r}</Code>
						{i < a.length - 1 ? ', ' : null}
					</Fragment>
				))}
			</Text>
		</Section>
	);
};
