import { Center, Divider, Heading, Text } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import { useRouter } from 'next/router';
import React, { ReactNode } from 'react';
import { useSearchCharactersQuery } from '../../generated/graphql';
import { CharacterList } from '../molecules/CharacterList';
import { BigSearch } from './BigSearch';

interface CharacterNotFoundProps {}

export const CharacterNotFound: React.FC<CharacterNotFoundProps> = () => {
	const router = useRouter();
	const { name } = router.query;

	const { data } = useSearchCharactersQuery({
		variables: {
			name: (name as string) ?? '',
		},
	});

	let didYouMean: ReactNode = null;
	if (data?.searchCharacters && data.searchCharacters.length) {
		const chars = data.searchCharacters;

		didYouMean = (
			<>
				<Center mb={4}>
					Found {chars.length} character
					{chars.length === 1 ? '' : 's'} with{' '}
					{chars.length === 1 ? 'a ' : ''}similar name
					{chars.length === 1 ? '' : 's'}:
				</Center>

				<CharacterList data={chars} showFetchMore={false} />

				<Divider my={16} />
			</>
		);
	}

	return (
		<>
			<NextSeo title="Character not found" />
			<Heading size="md">Character not found: {name}</Heading>
			<Text mt={2} mb={8}>
				The character you&apos;re looking for might not be uploaded to
				Islebuilds.
			</Text>

			{didYouMean}

			<BigSearch />
		</>
	);
};
