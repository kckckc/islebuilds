import { Box, DeepPartial, Flex, Spinner } from '@chakra-ui/react';
import React from 'react';
import { useInView } from 'react-intersection-observer';
import { Sprite } from '../../generated/graphql';
import { useDebouncedEffect } from '../../hooks/useDebouncedEffect';
import { SpriteGalleryCard } from '../molecules/SpriteGalleryCard';

interface SpriteGalleryProps {
	data?: DeepPartial<Sprite>[];
	hasMore?: boolean;
	onFetchMore?: () => void;
	showFetchMore?: boolean;
	loading?: boolean;
	hideName?: boolean;
	cardMinW?: string;
}

export const SpriteGallery: React.FC<SpriteGalleryProps> = ({
	data = [],
	hasMore = false,
	onFetchMore,
	showFetchMore = false,
	loading = false,
	hideName = false,
	cardMinW = '200px',
}) => {
	const { ref, inView } = useInView({});

	useDebouncedEffect(
		() => {
			if (inView && hasMore && onFetchMore && !loading) {
				onFetchMore();
			}
		},
		[inView, hasMore, onFetchMore, loading],
		100
	);

	return (
		<Flex direction="column">
			<Box
				display="grid"
				gridTemplateColumns={`repeat(auto-fill, minmax(${cardMinW}, 1fr))`}
				gap={2}
			>
				{data.map((c) => (
					<SpriteGalleryCard
						key={c.id}
						sprite={c}
						hideName={hideName}
					/>
				))}
			</Box>

			{showFetchMore && hasMore ? (
				<Flex ref={ref} mt={4} flexDir="row" justifyContent="center">
					<Spinner></Spinner>
				</Flex>
			) : null}
		</Flex>
	);
};
