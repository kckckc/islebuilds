import {
	Box,
	Center,
	Flex,
	Heading,
	Stack,
	Text,
	useColorModeValue,
} from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import React from 'react';
import { DiscordButton } from '../molecules/DiscordButton';
import { GoogleButton } from '../molecules/GoogleButton';

interface LogInCardProps {}

export const LogInCard: React.FC<LogInCardProps> = ({}) => {
	return (
		<>
			<NextSeo title="Log in" />
			<Flex
				minH={'100vh'}
				align={'center'}
				justify={'center'}
				bg={useColorModeValue('gray.50', 'gray.800')}
			>
				<Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
					<Stack align={'center'}>
						<Heading fontSize={'4xl'}>
							Log in to your account
						</Heading>
						<Text fontSize={'lg'} color={'gray.600'}>
							to continue to Islebuilds
						</Text>
					</Stack>
					<Box
						rounded={'lg'}
						bg={useColorModeValue('white', 'gray.700')}
						boxShadow={'lg'}
						p={8}
					>
						<Stack spacing={4}>
							<Text mx="auto">Log in with an option below:</Text>

							<Center px={8} pt={8}>
								<GoogleButton />
							</Center>
							<Center px={8} pb={8}>
								<DiscordButton />
							</Center>
						</Stack>
					</Box>
				</Stack>
			</Flex>
		</>
	);
};
