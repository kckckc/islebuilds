import { Box, Flex, Heading, Grid, GridItem, Text } from '@chakra-ui/react';
import React, { ReactNode } from 'react';
import { LabeledBox } from '../../molecules/LabeledBox';

interface IndexedGridProps<T> {
	label?: string;
	description?: string;
	noBorder?: boolean;
	labelX?: string;
	labelY?: string;
	length?: number;
	getX?: (idx: number) => T;
	showX?: (x: T) => ReactNode;
	getY?: (x: T) => ReactNode;
}

export const IndexedGrid: React.FC<IndexedGridProps<any>> = <
	T extends ReactNode
>({
	label,
	description,
	noBorder = false,
	labelX,
	labelY,
	length = 1,
	getX,
	showX,
	getY,
}: IndexedGridProps<T>) => {
	const inner = (
		<Grid
			templateColumns={`repeat(${length + 1}, 1fr)`}
			alignItems="center"
		>
			<GridItem
				mx={1}
				px={2}
				textAlign="center"
				fontWeight="bold"
				width="100%"
				height="100%"
				borderBottom="1px"
			>
				{labelX ?? 'X'}
			</GridItem>
			{new Array(length).fill(1).map((_, i) => (
				<GridItem
					borderLeft="1px"
					borderBottom="1px"
					mx={1}
					width="100%"
					height="100%"
					textAlign="center"
					key={i}
				>
					{getX && (showX ? showX(getX(i)) : getX(i))}
				</GridItem>
			))}
			<GridItem
				mx={1}
				px={2}
				textAlign="center"
				fontWeight="bold"
				width="100%"
				height="100%"
			>
				{labelY ?? 'Y'}
			</GridItem>
			{new Array(length).fill(1).map((_, i) => (
				<GridItem
					borderLeft="1px"
					mx={1}
					width="100%"
					height="100%"
					textAlign="center"
					key={i}
				>
					{getX && getY && getY(getX(i))}
				</GridItem>
			))}
		</Grid>
	);

	if (!noBorder) {
		return (
			<LabeledBox label={label} description={description}>
				{inner}
			</LabeledBox>
		);
	} else {
		return inner;
	}
};
