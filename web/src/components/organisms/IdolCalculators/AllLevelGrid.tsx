import { Box, Flex, Grid, GridItem, Heading, Text } from '@chakra-ui/react';
import React from 'react';

interface AllLevelGridProps {
	label?: string;
	description?: string;
	rowLabel: string;
	calcValue: (level: number) => string;
}

export const AllLevelGrid: React.FC<AllLevelGridProps> = ({
	label,
	description,
	rowLabel,
	calcValue,
}) => (
	<Box borderWidth="1px">
		{(label || description) && (
			<Flex flexDir="row" alignItems="baseline">
				{label && (
					<Heading size="sm" m={2}>
						{label}
					</Heading>
				)}
				{description && <Text color="gray.500">{description}</Text>}
			</Flex>
		)}
		<Grid templateColumns="repeat(11, 1fr)" padding={2} alignItems="center">
			<GridItem
				mx={1}
				px={2}
				textAlign="center"
				fontWeight="bold"
				width="100%"
				height="100%"
				borderBottom="1px"
			>
				Level
			</GridItem>
			{new Array(10).fill(1).map((_, i) => (
				<GridItem
					borderLeft="1px"
					borderBottom="1px"
					mx={1}
					width="100%"
					height="100%"
					textAlign="center"
					key={i}
				>
					{i + 1}
				</GridItem>
			))}
			<GridItem
				mx={1}
				px={2}
				textAlign="center"
				fontWeight="bold"
				width="100%"
				height="100%"
			>
				{rowLabel}
			</GridItem>
			{new Array(10).fill(1).map((_, i) => (
				<GridItem
					borderLeft="1px"
					mx={1}
					width="100%"
					height="100%"
					textAlign="center"
					key={i}
				>
					{calcValue(i + 1)}
				</GridItem>
			))}
		</Grid>
		<Grid templateColumns="repeat(11, 1fr)" padding={2} alignItems="center">
			<GridItem
				mx={1}
				px={2}
				textAlign="center"
				fontWeight="bold"
				width="100%"
				height="100%"
				borderBottom="1px"
			>
				Level
			</GridItem>
			{new Array(10).fill(1).map((_, i) => (
				<GridItem
					borderLeft="1px"
					borderBottom="1px"
					mx={1}
					width="100%"
					height="100%"
					textAlign="center"
					key={i}
				>
					{i + 11}
				</GridItem>
			))}
			<GridItem
				mx={1}
				px={2}
				textAlign="center"
				fontWeight="bold"
				width="100%"
				height="100%"
			>
				{rowLabel}
			</GridItem>
			{new Array(10).fill(1).map((_, i) => (
				<GridItem
					borderLeft="1px"
					mx={1}
					width="100%"
					height="100%"
					textAlign="center"
					key={i}
				>
					{calcValue(i + 11)}
				</GridItem>
			))}
		</Grid>
	</Box>
);
