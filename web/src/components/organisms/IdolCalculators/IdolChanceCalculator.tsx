import { Box, Code, Flex, Grid, GridItem, Text } from '@chakra-ui/react';
import { Form, Formik } from 'formik';
import React from 'react';
import { InputField } from '../../atoms/InputField';
import { Span } from '../../atoms/Span';
import { TwoColumnLayout } from '../../templates/TwoColumnLayout';
import { AllLevelGrid } from './AllLevelGrid';
import { IndexedGrid } from './IndexedGrid';

interface IdolChanceCalculatorProps {}

const BASE_IDOL_CHANCE = 0.035;

const calcChance = (playerLevel: number, mobLevel: number) => {
	let idolChance = BASE_IDOL_CHANCE;

	if (mobLevel < 5) {
		idolChance = 0;
	} else if (mobLevel < 14) {
		idolChance *= (mobLevel - 4) / 11;
	}

	if (playerLevel && playerLevel - mobLevel > 4) {
		const levelDelta = playerLevel - mobLevel;
		idolChance /= Math.pow(levelDelta - 3, 2);
	}

	return idolChance;
};

export const IdolChanceCalculator: React.FC<
	IdolChanceCalculatorProps
> = ({}) => (
	<>
		<Text>
			Only the first item generated when an enemy dies has a chance to be
			an idol, so quantity and quality stats don&apos;t affect idol rates.
			Before level 5, enemies don&apos;t drop idols. From level 5 to 13,
			the idol chance slowly increases, and at level 14+, idols have a
			3.5% base chance to drop. If there is a level difference of 5 or
			more between the player and the enemy, the chance is greatly reduced
			(base chance is divided by <Code>(Level Difference - 3)^2</Code>).
		</Text>
		<Formik
			initialValues={{ playerLevel: 20, mobLevel: 20 }}
			onSubmit={() => {}}
		>
			{({ values }) => {
				const { playerLevel, mobLevel } = values;

				const idolChance = calcChance(playerLevel, mobLevel);

				return (
					<>
						<TwoColumnLayout
							left={
								<Form>
									<Flex flexDir="column" gap={2}>
										<InputField
											name="playerLevel"
											placeholder="level"
											label="Player Level"
										/>
										<InputField
											name="mobLevel"
											placeholder="level"
											label="Enemy Level"
										/>
									</Flex>
								</Form>
							}
							right={
								<Flex
									flex={1}
									flexDir="column"
									justifyContent="center"
									alignItems="center"
								>
									<Box
										borderWidth="1px"
										textAlign="center"
										p={2}
									>
										Idol Chance: <br />
										<Span fontWeight="bold">
											{Math.round(idolChance * 10000) /
												100}
											%
										</Span>
									</Box>
								</Flex>
							}
						/>
						<Box mt={4}>
							<AllLevelGrid
								label="Chance"
								description={`(at level ${playerLevel})`}
								rowLabel="Chance"
								calcValue={(level) =>
									Math.round(
										calcChance(playerLevel, level) * 10000
									) /
										100 +
									'%'
								}
							/>
						</Box>
					</>
				);
			}}
		</Formik>
		<Box mt={4}>
			<AllLevelGrid
				label="Base Chance"
				description="(at equal levels)"
				rowLabel="Chance"
				calcValue={(level) =>
					Math.round(calcChance(level, level) * 10000) / 100 + '%'
				}
			/>
		</Box>
		<Box mt={4}>
			<IndexedGrid
				label="Chance by Level Difference"
				labelX="Difference"
				labelY="Chance"
				length={11}
				getX={(i: number) => i}
				showX={(i) => <>&plusmn;{i}</>}
				getY={(level: number) =>
					Math.round(calcChance(20, 20 - level) * 10000) / 100 + '%'
				}
			/>
		</Box>
	</>
);
