import { Box, Code, Text } from '@chakra-ui/react';
import React from 'react';
import { IndexedGrid } from './IndexedGrid';

const IDOL_CONFIG = {
	Unstable: 37,
	Ascendant: 15,
	'Dragon-Glass': 5,
	Bone: 6,
	Smoldering: 1,
};

interface IdolTypeCalculatorProps {}

export const IdolTypeCalculator: React.FC<IdolTypeCalculatorProps> = ({}) => {
	const names = Object.keys(IDOL_CONFIG);
	const pulls = Object.values(IDOL_CONFIG);

	const totalPulls = pulls.reduce((a, b) => a + b, 0);

	return (
		<>
			<Text>
				Each type of idol has a weight which determines its chance. The
				chance for each type of idol is{' '}
				<Code>weight / sum of weights</Code>.
			</Text>
			<Box mt={4}>
				<IndexedGrid
					labelX="Idol Type"
					labelY="Chance"
					length={names.length}
					getX={(x) => x}
					showX={(i) => names[i]}
					getY={(i) => (
						<>
							{Math.round((pulls[i] / totalPulls) * 10000) / 100 +
								'%'}
							<br />({pulls[i]} / {totalPulls})
						</>
					)}
				/>
			</Box>
		</>
	);
};
