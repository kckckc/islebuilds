import { Box, Code, Text } from '@chakra-ui/react';
import React from 'react';
import { LabeledBox } from '../molecules/LabeledBox';
import { IndexedGrid } from './IdolCalculators/IndexedGrid';

interface SlotChancesProps {}

const SLOT_CONFIG = {
	head: 85,
	neck: 45,
	chest: 100,
	hands: 90,
	finger: 40,
	waist: 80,
	legs: 100,
	feet: 90,
	trinket: 35,
	oneHanded: 60,
	twoHanded: 60,
	offHand: 40,
};

export const SlotChances: React.FC<SlotChancesProps> = ({}) => {
	const names = Object.keys(SLOT_CONFIG);
	const pulls = Object.values(SLOT_CONFIG);

	const totalPulls = pulls.reduce((a, b) => a + b, 0);

	const halfLen = Math.ceil(names.length / 2);

	return (
		<>
			<Text>
				Each equipment slot has a weight which determines its chance.
				The chance for each slot is <Code>weight / sum of weights</Code>
				.
			</Text>
			<Box mt={4}>
				<LabeledBox>
					<Box mb={4}>
						<IndexedGrid
							labelX="Slot Type"
							labelY="Chance"
							noBorder
							length={halfLen}
							getX={(x) => x}
							showX={(i) => names[i]}
							getY={(i) => (
								<>
									{Math.round(
										(pulls[i] / totalPulls) * 10000
									) /
										100 +
										'%'}
									<br />({pulls[i]} / {totalPulls})
								</>
							)}
						/>
					</Box>

					<IndexedGrid
						labelX="Slot Type"
						labelY="Chance"
						noBorder
						length={names.length - halfLen}
						getX={(x) => x + halfLen}
						showX={(i) => names[i]}
						getY={(i) => (
							<>
								{Math.round((pulls[i] / totalPulls) * 10000) /
									100 +
									'%'}
								<br />({pulls[i]} / {totalPulls})
							</>
						)}
					/>
				</LabeledBox>
			</Box>
		</>
	);
};
