import { Button, Divider, Flex } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React, { useContext } from 'react';
import { useMeQuery } from '../../../generated/graphql';
import { SpriteEditorContext } from './SpriteEditorContext';

interface EditorToolbarProps {}

export const EditorToolbar: React.FC<EditorToolbarProps> = ({}) => {
	const { data: meData } = useMeQuery();
	const router = useRouter();

	const {
		selectedTool,
		setSelectedTool,
		setupDisclosure,
		shareDisclosure,
		downloadDisclosure,
		loginPromptDisclosure,
	} = useContext(SpriteEditorContext);

	const ToolButton = ({ type, label }: { type: string; label?: string }) => {
		return (
			<Button
				onClick={() => setSelectedTool(type)}
				color={selectedTool === type ? 'teal.500' : undefined}
				display="flex"
				flexDirection="row"
				justifyContent="space-between"
			>
				<span>{type}</span>
				{label ? <span>[{label}]</span> : null}
			</Button>
		);
	};

	return (
		<Flex direction="column" gap={2}>
			<ToolButton type="draw" label="d" />
			<ToolButton type="erase" label="e" />
			<ToolButton type="pick color" label="RMB" />
			<Button disabled>move</Button>
			<Divider />
			<Button onClick={setupDisclosure.onOpen}>change size</Button>
			<Divider />
			{/* autosaves? */}
			<Button
				onClick={() => {
					if (meData?.me?.id) {
						shareDisclosure.onOpen();
					} else {
						loginPromptDisclosure.onOpen();
					}
				}}
			>
				share
			</Button>
			<Button onClick={() => router.push('/sprite-editor/gallery')}>
				browse
			</Button>
			<Button onClick={downloadDisclosure.onOpen}>download .png</Button>
		</Flex>
	);
};
