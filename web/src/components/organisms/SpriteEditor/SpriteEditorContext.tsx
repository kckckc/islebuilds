import { useDisclosure } from '@chakra-ui/react';
import { createContext } from 'react';
import { mockDisclosure } from '../../../util/mockDisclosure';

interface SpriteEditorContextProps {
	size: number;
	setSize: React.Dispatch<React.SetStateAction<number>>;
	pixels: number[][];
	setPixels: React.Dispatch<React.SetStateAction<number[][]>>;
	selectedColor: number;
	setSelectedColor: React.Dispatch<React.SetStateAction<number>>;
	selectedTool: string;
	setSelectedTool: React.Dispatch<React.SetStateAction<string>>;

	setupDisclosure: ReturnType<typeof useDisclosure>;
	shareDisclosure: ReturnType<typeof useDisclosure>;
	downloadDisclosure: ReturnType<typeof useDisclosure>;
	loginPromptDisclosure: ReturnType<typeof useDisclosure>;

	attemptedRestore: boolean;
	setAttemptedRestore: React.Dispatch<React.SetStateAction<boolean>>;
	restoreDisclosure: ReturnType<typeof useDisclosure>;
}

export const SpriteEditorContext = createContext<SpriteEditorContextProps>({
	size: 8,
	setSize: () => {},
	pixels: [],
	setPixels: () => {},
	selectedColor: 37,
	setSelectedColor: () => {},
	selectedTool: 'draw',
	setSelectedTool: () => {},

	setupDisclosure: mockDisclosure(),
	shareDisclosure: mockDisclosure(),
	downloadDisclosure: mockDisclosure(),
	loginPromptDisclosure: mockDisclosure(),

	attemptedRestore: true,
	setAttemptedRestore: () => {},
	restoreDisclosure: mockDisclosure(),
});
