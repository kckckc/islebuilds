import { Box, Flex, useColorModeValue, useDisclosure } from '@chakra-ui/react';
import React, { useState } from 'react';
import { useIsMobile } from '../../../hooks/useIsMobile';
import { PageHeading } from '../../atoms/PageHeading';
import { BackLink } from '../../molecules/BackLink';
import { EditorCanvas } from './EditorCanvas';
import { EditorDownloadModal } from './EditorDownloadModal';
import { EditorLoginPromptModal } from './EditorLoginPromptModal';
import { EditorPalette } from './EditorPalette';
import { EditorRestoreModal } from './EditorRestoreModal';
import { EditorSetupModal } from './EditorSetupModal';
import { EditorShareModal } from './EditorShareModal';
import { EditorToolbar } from './EditorToolbar';
import { SpriteEditorContext } from './SpriteEditorContext';

export const LOCAL_SAVE_KEY = 'sprite-editor-save';

interface SpriteEditorProps {}

export const SpriteEditor: React.FC<SpriteEditorProps> = ({}) => {
	const [size, setSize] = useState(8);

	const [pixels, setPixels] = useState<number[][]>(() => {
		const p = [];
		for (let i = 0; i < size; i++) {
			const r = [];
			for (let j = 0; j < size; j++) {
				r.push(0);
			}
			p.push(r);
		}
		return p;
	});

	const [selectedColor, setSelectedColor] = useState(37);
	const [selectedTool, setSelectedTool] = useState('draw');

	const [attemptedRestore, setAttemptedRestore] = useState(false);
	const restoreDisclosure = useDisclosure();

	const setupDisclosure = useDisclosure();
	const shareDisclosure = useDisclosure();
	const downloadDisclosure = useDisclosure();
	const loginPromptDisclosure = useDisclosure();

	const isMobile = useIsMobile();

	const noticeColor = useColorModeValue('gray.800', 'gray.50');

	return (
		<SpriteEditorContext.Provider
			value={{
				size,
				setSize,
				pixels,
				setPixels,
				selectedColor,
				setSelectedColor,
				selectedTool,
				setSelectedTool,
				setupDisclosure,
				shareDisclosure,
				downloadDisclosure,
				loginPromptDisclosure,
				attemptedRestore,
				setAttemptedRestore,
				restoreDisclosure,
			}}
		>
			{isMobile ? (
				<Flex direction="column" gap={4}>
					<Box>
						<PageHeading nowrap>Sprite Editor</PageHeading>
						<Box my={2}>
							<BackLink href="/tools">Tools</BackLink>
						</Box>
						<Box
							borderWidth={1}
							borderColor={noticeColor}
							rounded="lg"
							p={2}
						>
							Currently doesn&apos;t work well on mobile or small
							screens.
						</Box>
					</Box>
					<Flex direction="row" justifyContent="space-between">
						<EditorToolbar />
						<EditorPalette />
					</Flex>

					<EditorCanvas />
				</Flex>
			) : (
				<Flex direction="row" gap={8} justifyContent="center">
					<Box>
						<PageHeading nowrap>Sprite Editor</PageHeading>
						<Box mt={2} mb={6}>
							<BackLink href="/tools">Tools</BackLink>
						</Box>
						<EditorToolbar />
					</Box>
					<Box w="70vh" style={{ aspectRatio: '1' }}>
						<EditorCanvas />
					</Box>
					<EditorPalette />
				</Flex>
			)}

			<EditorSetupModal />
			<EditorShareModal />
			<EditorDownloadModal />
			<EditorRestoreModal />
			<EditorLoginPromptModal />
		</SpriteEditorContext.Provider>
	);
};
