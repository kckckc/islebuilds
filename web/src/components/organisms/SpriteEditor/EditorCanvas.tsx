import React, {
	KeyboardEventHandler,
	MouseEvent,
	MouseEventHandler,
	useCallback,
	useContext,
	useEffect,
	useRef,
	useState,
} from 'react';
import { getPaletteColor, PALETTE } from '../../../util/getPaletteColor';
import { LOCAL_SAVE_KEY } from './SpriteEditor';
import { SpriteEditorContext } from './SpriteEditorContext';

const BACKGROUND_COLOR = '#2d2136';
const WHITE = PALETTE[36];

interface EditorCanvasProps {}

export const EditorCanvas: React.FC<EditorCanvasProps> = ({}) => {
	const {
		size,
		pixels,
		setPixels,
		selectedTool,
		setSelectedTool,
		selectedColor,
		setSelectedColor,
		attemptedRestore,
	} = useContext(SpriteEditorContext);

	const [madeChanges, setMadeChanges] = useState(false);

	const saveLocal = useCallback(() => {
		if (!attemptedRestore) return;

		// Don't save if nothing has been drawn yet
		if (!madeChanges) return;

		localStorage.setItem(
			LOCAL_SAVE_KEY,
			JSON.stringify({
				size,
				pixels,
			})
		);
	}, [madeChanges, attemptedRestore, size, pixels]);

	const setPixel = (x: number, y: number, value: number) => {
		if (x < 0 || y < 0 || x >= size || y >= size) return;

		setPixels((prev) => {
			(prev[y] ?? [])[x] = value;
			return prev;
		});

		setMadeChanges(true);
		saveLocal();
	};
	const getPixel = (x: number, y: number) => {
		if (x < 0 || y < 0 || x >= size || y >= size) return 0;

		return (pixels[y] ?? [])[x] ?? 0;
	};

	const canvasRef = useRef<HTMLCanvasElement>(null);

	const [mouseDown, setMouseDown] = useState(false);

	const [selected, setSelected] = useState([-1, -1]);

	const getEventPixelPosition = (event: MouseEvent) => {
		if (!canvasRef.current) return [0, 0];

		const bounds = canvasRef.current.getBoundingClientRect();
		const canvasX = event.clientX - bounds.left;
		const canvasY = event.clientY - bounds.top;

		const pixelSize = bounds.width / size;

		const x = Math.floor(canvasX / pixelSize);
		const y = Math.floor(canvasY / pixelSize);

		return [x, y];
	};

	// Draw
	const doDraw = useCallback(() => {
		const canvas = canvasRef.current;
		if (!canvas) return;

		const bounds = canvas.getBoundingClientRect();
		const pixelSize = Math.floor(bounds.width / size);

		canvas.width = size * pixelSize;
		canvas.height = size * pixelSize;

		const ctx = canvas.getContext('2d');
		if (!ctx) return;

		// Clear
		// Background is provided by css
		ctx.clearRect(0, 0, size * pixelSize, size * pixelSize);

		// Draw pixels
		for (let y = 0; y < size; y++) {
			for (let x = 0; x < size; x++) {
				const colorIndex = (pixels[y] ?? [])[x] ?? 0;
				const color = getPaletteColor(colorIndex);
				if (!color) {
					continue;
				}
				ctx.fillStyle = color;
				ctx.fillRect(
					x * pixelSize,
					y * pixelSize,
					pixelSize,
					pixelSize
				);
			}
		}

		// Draw tool indicators
		if (selectedTool == 'draw') {
			const color = getPaletteColor(selectedColor);
			if (color) {
				ctx.fillStyle = color;
				ctx.fillRect(
					selected[0] * pixelSize,
					selected[1] * pixelSize,
					pixelSize,
					pixelSize
				);
			}
		} else if (selectedTool == 'erase') {
			ctx.strokeStyle = WHITE;
			ctx.lineWidth = 1;
			ctx.setLineDash([4, 2]);
			ctx.strokeRect(
				selected[0] * pixelSize,
				selected[1] * pixelSize,
				pixelSize,
				pixelSize
			);
		}
	}, [size, canvasRef, pixels, selected, selectedTool, selectedColor]);

	useEffect(() => {
		requestAnimationFrame(doDraw);
	}, [doDraw, size, pixels, canvasRef]);

	// Handle canvas resizes
	useEffect(() => {
		if (pixels.length < size) {
			setPixels((prev) => {
				prev.forEach((r) => {
					while (r.length < size) {
						r.push(0);
					}
				});
				while (prev.length < size) {
					const r = [];
					for (let i = 0; i < size; i++) {
						r.push(0);
					}
					prev.push(r);
				}
				return prev;
			});
		} else if (pixels.length > size) {
			setPixels((prev) => {
				while (prev.length > size) {
					prev.pop();
				}
				prev.forEach((r) => {
					while (r.length > size) {
						r.pop();
					}
				});
				return prev;
			});
		}

		saveLocal();

		requestAnimationFrame(doDraw);
	}, [saveLocal, doDraw, size, pixels, setPixels]);

	const onMouseDown: MouseEventHandler<HTMLCanvasElement> = (event) => {
		if (!canvasRef.current) return;
		const [x, y] = getEventPixelPosition(event);

		setMouseDown(true);

		if (event.button == 2 || selectedTool === 'pick color') {
			// Pick color
			const color = getPixel(x, y);
			if (color) {
				setSelectedColor(color);
			}
		} else if (selectedTool === 'draw') {
			setPixel(x, y, selectedColor);
		} else if (selectedTool === 'erase') {
			setPixel(x, y, 0);
		}

		requestAnimationFrame(doDraw);
	};

	const onMouseUp: MouseEventHandler<HTMLCanvasElement> = (event) => {
		if (!canvasRef.current) return;

		setMouseDown(false);

		requestAnimationFrame(doDraw);
	};

	const onMouseMove: MouseEventHandler<HTMLCanvasElement> = (event) => {
		if (!canvasRef.current) return;
		const [x, y] = getEventPixelPosition(event);

		setSelected([x, y]);

		if (mouseDown) {
			if (selectedTool === 'draw') {
				setPixel(x, y, selectedColor);
			} else if (selectedTool === 'erase') {
				setPixel(x, y, 0);
			}
		}

		requestAnimationFrame(doDraw);
	};

	const onMouseLeave: MouseEventHandler<HTMLCanvasElement> = () => {
		setMouseDown(false);

		setSelected([-1, -1]);

		requestAnimationFrame(doDraw);
	};

	const onContextMenu: MouseEventHandler<HTMLCanvasElement> = (event) => {
		event.preventDefault();
	};

	const onKeyDown: KeyboardEventHandler<HTMLCanvasElement> = (event) => {
		if (event.key === 'd') {
			setSelectedTool('draw');
		} else if (event.key === 'e') {
			setSelectedTool('erase');
		}

		requestAnimationFrame(doDraw);
	};

	return (
		<canvas
			ref={canvasRef}
			style={{
				width: '100%',
				height: '100%',
				backgroundColor: BACKGROUND_COLOR,
				imageRendering: 'pixelated',
				outline: 'none', // because of tabindex
			}}
			onMouseDown={onMouseDown}
			onMouseUp={onMouseUp}
			onMouseMove={onMouseMove}
			onMouseLeave={onMouseLeave}
			onContextMenu={onContextMenu}
			onKeyDown={onKeyDown}
			tabIndex={0}
		></canvas>
	);
};
