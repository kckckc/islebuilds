import {
	Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalCloseButton,
	ModalBody,
	Box,
	FormControl,
	FormLabel,
	Select,
	ModalFooter,
	Button,
} from '@chakra-ui/react';
import React, { useCallback, useContext, useState } from 'react';
import { downloadPixels } from '../../../util/downloadPixels';
import { getPaletteColor, PALETTE } from '../../../util/getPaletteColor';
import { Span } from '../../atoms/Span';
import { SpriteEditorContext } from './SpriteEditorContext';

interface EditorDownloadModalProps {}

export const EditorDownloadModal: React.FC<EditorDownloadModalProps> = ({}) => {
	const { size, pixels, downloadDisclosure } =
		useContext(SpriteEditorContext);

	const { isOpen, onClose } = downloadDisclosure;

	const [resize, setResize] = useState(1);

	const handleDownload = useCallback(() => {
		downloadPixels(pixels, size, resize);
	}, [pixels, size, resize]);

	return (
		<Modal isOpen={isOpen} onClose={onClose} isCentered>
			<ModalOverlay />
			<ModalContent>
				<ModalHeader>Download</ModalHeader>
				<ModalCloseButton />
				<ModalBody>
					<FormControl>
						<FormLabel htmlFor="selectResize">Resize</FormLabel>
						<Select
							id="selectResize"
							value={resize}
							onChange={(v) => {
								setResize(parseInt(v.currentTarget.value));
							}}
						>
							<option value="1">1x</option>
							<option value="2">2x</option>
							<option value="4">4x</option>
							<option value="8">8x</option>
						</Select>
					</FormControl>
				</ModalBody>

				<ModalFooter>
					<Button colorScheme="blue" mr={3} onClick={handleDownload}>
						Download
					</Button>
					<Button onClick={onClose}>Cancel</Button>
				</ModalFooter>
			</ModalContent>
		</Modal>
	);
};
