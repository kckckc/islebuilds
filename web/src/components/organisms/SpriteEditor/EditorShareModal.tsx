import { useMutation } from '@apollo/client';
import {
	Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalCloseButton,
	ModalBody,
	Box,
	FormControl,
	FormLabel,
	Select,
	ModalFooter,
	Button,
	Input,
	useDisclosure,
	Checkbox,
	Textarea,
} from '@chakra-ui/react';
import React, { useCallback, useContext, useState } from 'react';
import { usePostSpriteMutation } from '../../../generated/graphql';
import { getPaletteColor, PALETTE } from '../../../util/getPaletteColor';
import { Span } from '../../atoms/Span';
import { SpriteEditorContext } from './SpriteEditorContext';

interface EditorShareModalProps {}

export const EditorShareModal: React.FC<EditorShareModalProps> = ({}) => {
	const [postSprite, { data, loading }] = usePostSpriteMutation();

	const { size, pixels, shareDisclosure } = useContext(SpriteEditorContext);

	const successDisclosure = useDisclosure();

	const [name, setName] = useState('');
	const [desc, setDesc] = useState('');
	const [visible, setVisible] = useState(true);

	const handleShare = useCallback(async () => {
		await postSprite({
			variables: {
				input: {
					name: name,
					description: desc,
					visible: visible,
					size: size,
					pixels: JSON.stringify(pixels),
				},
			},
		});

		setName('');
		setDesc('');
		setVisible(true);

		shareDisclosure.onClose();
		successDisclosure.onOpen();
	}, [
		name,
		desc,
		visible,
		size,
		pixels,
		postSprite,
		shareDisclosure,
		successDisclosure,
	]);

	return (
		<>
			<Modal
				size="lg"
				isOpen={shareDisclosure.isOpen}
				onClose={shareDisclosure.onClose}
				isCentered
			>
				<ModalOverlay />
				<ModalContent>
					<ModalHeader>Share</ModalHeader>
					<ModalCloseButton />
					<ModalBody>
						<FormControl>
							<FormLabel htmlFor="inputName">
								Sprite name
							</FormLabel>
							<Input
								id="inputName"
								value={name}
								onChange={(e) => {
									setName(e.target.value);
								}}
								maxLength={50}
								placeholder="Name"
							/>
							<FormLabel mt={4} htmlFor="inputDesc">
								Sprite description
							</FormLabel>
							<Textarea
								id="inputDesc"
								value={desc}
								onChange={(e) => {
									setDesc(e.target.value);
								}}
								maxLength={300}
								resize="none"
								placeholder="Description"
							/>
							<Checkbox
								size="lg"
								mt={4}
								isChecked={visible}
								onChange={() => setVisible((v) => !v)}
							>
								Visible to others
							</Checkbox>
						</FormControl>
					</ModalBody>

					<ModalFooter>
						<Button
							colorScheme="blue"
							mr={3}
							isDisabled={
								!name.length || Math.max(...pixels.flat()) === 0
							}
							isLoading={loading}
							onClick={handleShare}
						>
							Share
						</Button>
						<Button onClick={shareDisclosure.onClose}>
							Cancel
						</Button>
					</ModalFooter>
				</ModalContent>
			</Modal>
			<Modal
				size="sm"
				isOpen={successDisclosure.isOpen}
				onClose={successDisclosure.onClose}
				isCentered
			>
				<ModalOverlay />
				<ModalContent>
					<ModalHeader>
						{data?.postSprite ? 'Success' : 'Failed'}
					</ModalHeader>
					<ModalCloseButton />
					<ModalBody>
						{data?.postSprite
							? 'Your sprite has been posted.'
							: 'Failed to post sprite.'}
					</ModalBody>

					<ModalFooter>
						<Button onClick={successDisclosure.onClose}>
							Close
						</Button>
					</ModalFooter>
				</ModalContent>
			</Modal>
		</>
	);
};
