import {
	Box,
	Button,
	FormControl,
	FormLabel,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Select,
} from '@chakra-ui/react';
import React, { useContext } from 'react';
import { Span } from '../../atoms/Span';
import { SpriteEditorContext } from './SpriteEditorContext';

interface EditorSetupModalProps {}

export const EditorSetupModal: React.FC<EditorSetupModalProps> = ({}) => {
	const { setPixels, size, setSize, setupDisclosure } =
		useContext(SpriteEditorContext);

	const { isOpen, onClose } = setupDisclosure;

	return (
		<Modal isOpen={isOpen} onClose={onClose} isCentered>
			<ModalOverlay />
			<ModalContent>
				<ModalHeader>Change Size</ModalHeader>
				<ModalCloseButton />
				<ModalBody>
					<Box mb={4}>
						<Span fontWeight="bold">Note: </Span>When making the
						sprite smaller, the image will be cropped. Be careful!
					</Box>
					<FormControl>
						<FormLabel htmlFor="selectSize">Size</FormLabel>
						<Select
							id="selectSize"
							value={size}
							onChange={(v) => {
								setSize(parseInt(v.currentTarget.value));
							}}
						>
							<option value="8">
								8x8 (skins, enemies, other objects)
							</option>
							<option value="16">16x16 (items)</option>
							<option value="24">
								24x24 (bosses, other big objects)
							</option>
						</Select>
					</FormControl>
					<Button
						mt={4}
						colorScheme="red"
						onClick={() => {
							const p = [];
							for (let i = 0; i < size; i++) {
								const r = [];
								for (let j = 0; j < size; j++) {
									r.push(0);
								}
								p.push(r);
							}
							setPixels(p);
						}}
					>
						Clear canvas
					</Button>
				</ModalBody>

				<ModalFooter>
					<Button colorScheme="blue" onClick={onClose}>
						Done
					</Button>
				</ModalFooter>
			</ModalContent>
		</Modal>
	);
};
