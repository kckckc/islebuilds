import {
	Button,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React, { useContext } from 'react';
import { SpriteEditorContext } from './SpriteEditorContext';

interface EditorLoginPromptModalProps {}

export const EditorLoginPromptModal: React.FC<
	EditorLoginPromptModalProps
> = ({}) => {
	const router = useRouter();

	const { loginPromptDisclosure } = useContext(SpriteEditorContext);
	const { isOpen, onClose } = loginPromptDisclosure;

	return (
		<Modal isOpen={isOpen} onClose={onClose} isCentered>
			<ModalOverlay />
			<ModalContent>
				<ModalHeader>Log in</ModalHeader>
				<ModalCloseButton />
				<ModalBody>
					You must be logged in to share sprites. Your work will be
					saved.
				</ModalBody>

				<ModalFooter>
					<Button
						colorScheme="blue"
						mr={3}
						onClick={() => {
							router.push('/login?next=' + router.pathname);
						}}
					>
						Log in
					</Button>
					<Button onClick={onClose}>Cancel</Button>
				</ModalFooter>
			</ModalContent>
		</Modal>
	);
};
