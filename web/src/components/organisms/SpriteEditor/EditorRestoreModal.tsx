import {
	Button,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
} from '@chakra-ui/react';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { LOCAL_SAVE_KEY } from './SpriteEditor';
import { SpriteEditorContext } from './SpriteEditorContext';

interface SavedData {
	pixels: number[][];
	size: number;
}

interface EditorRestoreModalProps {}

export const EditorRestoreModal: React.FC<EditorRestoreModalProps> = ({}) => {
	const {
		setSize,
		setPixels,
		restoreDisclosure,
		attemptedRestore,
		setAttemptedRestore,
	} = useContext(SpriteEditorContext);
	const { isOpen, onClose } = restoreDisclosure;

	const [savedData, setSavedData] = useState<SavedData | null>(null);

	useEffect(() => {
		if (attemptedRestore) return;

		const raw = localStorage.getItem(LOCAL_SAVE_KEY);
		if (raw) {
			// Check if the canvas is empty
			const data = JSON.parse(raw);
			if (Math.max(...data.pixels.flat()) === 0) {
				setAttemptedRestore(true);
				return;
			}

			setSavedData(data);
			restoreDisclosure.onOpen();
		} else {
			setAttemptedRestore(true);
		}
	}, [attemptedRestore, setAttemptedRestore, restoreDisclosure]);

	const handleRestore = useCallback(() => {
		setAttemptedRestore(true);

		if (!savedData) return;

		setSize(savedData.size);
		setPixels(savedData.pixels);

		onClose();
	}, [setAttemptedRestore, setSize, setPixels, savedData, onClose]);

	const finishAndClose = useCallback(() => {
		setAttemptedRestore(true);
		onClose();
	}, [setAttemptedRestore, onClose]);

	const deleteAndClose = useCallback(() => {
		setAttemptedRestore(true);
		localStorage.removeItem(LOCAL_SAVE_KEY);
		onClose();
	}, [setAttemptedRestore, onClose]);

	return (
		<Modal
			isOpen={isOpen && !!savedData}
			onClose={finishAndClose}
			isCentered
		>
			<ModalOverlay />
			<ModalContent>
				<ModalHeader>Restore Work</ModalHeader>
				<ModalCloseButton />
				<ModalBody>
					A sprite you were previously editing was found.
				</ModalBody>

				<ModalFooter>
					<Button colorScheme="blue" mr={3} onClick={handleRestore}>
						Restore
					</Button>
					<Button colorScheme="red" onClick={deleteAndClose}>
						Delete
					</Button>
				</ModalFooter>
			</ModalContent>
		</Modal>
	);
};
