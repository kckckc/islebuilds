import { Box, Grid } from '@chakra-ui/react';
import React, { useContext, useState } from 'react';
import { PALETTE } from '../../../util/getPaletteColor';
import { SpriteEditorContext } from './SpriteEditorContext';

interface EditorPaletteProps {}

const ColorButton = ({ color }: { color: number } & EditorPaletteProps) => {
	const { selectedColor, setSelectedColor } = useContext(SpriteEditorContext);

	const [hovered, setHovered] = useState(false);
	const selected = color === selectedColor;

	return (
		<Box
			style={{ aspectRatio: '1' }}
			backgroundColor={PALETTE[color - 1]}
			onClick={() => setSelectedColor(color)}
			borderWidth={selected || hovered ? '4px' : '0xpx'}
			borderColor={selected ? 'white' : 'whiteAlpha.700'}
			onMouseEnter={() => setHovered(true)}
			onMouseLeave={() => setHovered(false)}
		/>
	);
};

export const EditorPalette: React.FC<EditorPaletteProps> = () => {
	return (
		<Grid
			width="128px"
			templateColumns="repeat(4, 1fr)"
			alignContent="flex-start"
		>
			{PALETTE.map((_, i) => (
				<ColorButton color={i + 1} key={i} />
			))}
		</Grid>
	);
};
