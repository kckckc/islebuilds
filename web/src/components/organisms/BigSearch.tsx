import { Box } from '@chakra-ui/react';
import { Form, Formik } from 'formik';
import { useRouter } from 'next/router';
import React from 'react';
import { SearchInputField } from '../molecules/SearchInputField';

interface BigSearchProps {
	onSubmit?: (search: string) => void;
}

export const BigSearch: React.FC<BigSearchProps> = ({ onSubmit }) => {
	const router = useRouter();

	return (
		<Box maxW="75%" mx="auto" p={4}>
			<Formik
				initialValues={{
					name: '',
				}}
				onSubmit={(values) => {
					if (onSubmit) {
						return onSubmit(values.name);
					}

					router.push(`/character/${values.name}`);
				}}
			>
				{() => (
					<Form>
						<SearchInputField
							name="name"
							placeholder="Character name"
						/>
					</Form>
				)}
			</Formik>
		</Box>
	);
};
