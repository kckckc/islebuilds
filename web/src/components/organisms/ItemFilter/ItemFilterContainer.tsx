import { Flex, useColorModeValue as mode, useToken } from '@chakra-ui/react';
import { SelectInstance } from 'chakra-react-select';
import React, { MutableRefObject, ReactNode, useContext } from 'react';
import { InventoryViewerContext } from '../../pages/InventoryPage/InventoryViewerContext';

interface ItemFilterContainerProps {
	children?: ReactNode;
	forwardFocus?: MutableRefObject<SelectInstance<any> | null>;
}

export const ItemFilterContainer: React.FC<ItemFilterContainerProps> = ({
	children,
	forwardFocus,
}) => {
	const { filterError } = useContext(InventoryViewerContext);

	const [red300, red400, red500, blue300, blue500] = useToken('colors', [
		'red.300',
		'red.400',
		'red.500',
		'blue.300',
		'blue.500',
	]);

	const onFocus = () => {
		if (forwardFocus?.current) {
			forwardFocus.current.focus();
		}
	};

	const focusBorderColor = filterError
		? mode(red500, red300)
		: mode(blue500, blue300);

	return (
		<Flex
			direction="row"
			gap={2}
			alignItems="center"
			justifyContent="flex-start"
			flexWrap="wrap"
			tabIndex={0}
			onFocus={onFocus}
			width="100%"
			minWidth={0}
			outline={0}
			position="relative"
			appearance="none"
			transitionProperty="common"
			transitionDuration="normal"
			fontSize="md"
			px={2}
			h="10"
			borderRadius="md"
			border="1px solid"
			borderColor={filterError ? 'red.400' : 'inherit'}
			boxShadow={filterError ? `0 0 8px 0 ${red400}` : 'none'}
			_hover={{
				borderColor: filterError
					? mode('red.500', 'red.300')
					: mode('gray.300', 'whiteAlpha.400'),
			}}
			_focusWithin={{
				zIndex: 1,
				borderColor: focusBorderColor,
				boxShadow: `0 0 8px 0 ${focusBorderColor}`,
			}}
		>
			{children}
		</Flex>
	);
};
