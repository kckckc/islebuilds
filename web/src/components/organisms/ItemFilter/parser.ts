/*

Comments
```
# full line comment
1 + 1 # comment after line
1 + ## comment inside line ## 1
```

*/

class InputStream {
	private input: string;
	private pos = 0;

	private line = 1;
	private col = 0;

	constructor(input: string) {
		this.input = input;
	}

	public next() {
		const ch = this.input.charAt(this.pos++);
		// Track position
		if (ch === '\n') {
			this.line++;
			this.col = 0;
		} else {
			this.col++;
		}
		return ch;
	}

	public peek() {
		return this.input.charAt(this.pos);
	}

	public eof() {
		return this.peek() === '';
	}

	public die(msg: string) {
		throw new Error(msg + `(${this.line}:${this.col})`);
	}
}

type Token =
	| {
			type: 'punc' | 'str' | 'kw' | 'var' | 'op';
			value: string;
	  }
	| {
			type: 'num';
			value: number;
	  };

class TokenStream {
	private input: InputStream;
	private current: Token | null;
	private keywords = ' true false ';

	constructor(input: InputStream) {
		this.input = input;
		this.current = null;
	}

	public next() {
		const token = this.current;
		this.current = null;
		return token || this.readNext();
	}

	public peek() {
		return this.current || (this.current = this.readNext());
	}

	public eof() {
		return this.peek() === null;
	}

	public die(msg: string) {
		this.input.die(msg);
	}

	private isKeyword(x: string) {
		return this.keywords.indexOf(' ' + x + ' ') >= 0;
	}
	private isDigit(ch: string) {
		return /[0-9]/i.test(ch);
	}
	private isIdStart(ch: string) {
		return /[a-z_]/i.test(ch);
	}
	private isId(ch: string) {
		return this.isIdStart(ch) || '0123456789:'.indexOf(ch) >= 0;
	}
	private isOpChar(ch: string) {
		return '+-*/%=&|<>!'.indexOf(ch) >= 0;
	}
	private isPunc(ch: string) {
		return ',;(){}[]'.indexOf(ch) >= 0;
	}
	private isWhitespace(ch: string) {
		return ' \n\t'.indexOf(ch) >= 0;
	}
	private readWhile(predicate: (x: string) => boolean) {
		let str = '';
		while (!this.input.eof() && predicate(this.input.peek())) {
			str += this.input.next();
		}
		return str;
	}
	private readNumber(): Token {
		let hasDot = false;
		const number = this.readWhile((ch) => {
			if (ch === '.') {
				// Only allow one dot
				if (hasDot) return false;
				hasDot = true;
				return true;
			}
			return this.isDigit(ch);
		});
		return {
			type: 'num',
			value: parseFloat(number),
		};
	}
	private readIdent(): Token {
		const id = this.readWhile(this.isId.bind(this));
		return {
			type: this.isKeyword(id) ? 'kw' : 'var',
			value: id,
		};
	}
	private readEscaped(end: string) {
		let escaped = false;
		let str = '';
		this.input.next();
		while (!this.input.eof()) {
			const ch = this.input.next();
			if (escaped) {
				str += ch;
				escaped = false;
			} else if (ch == '\\') {
				escaped = true;
			} else if (ch == end) {
				break;
			} else {
				str += ch;
			}
		}
		return str;
	}
	private readString(end: string): Token {
		return {
			type: 'str',
			value: this.readEscaped(end),
		};
	}
	private skipComment() {
		this.readWhile((ch) => ch != '\n');
		this.input.next();
	}

	private readNext(): Token | null {
		this.readWhile(this.isWhitespace);
		if (this.input.eof()) {
			return null;
		}

		const ch = this.input.peek();
		if (ch === '#') {
			//  TODO inline comment
			this.skipComment();
			return this.readNext();
		}

		if (ch === '"' || ch === "'") {
			return this.readString(ch);
		}
		if (this.isDigit(ch)) {
			return this.readNumber();
		}
		if (this.isIdStart(ch)) {
			return this.readIdent();
		}
		if (this.isPunc(ch)) {
			return {
				type: 'punc',
				value: this.input.next(),
			};
		}
		if (this.isOpChar(ch)) {
			return {
				type: 'op',
				value: this.readWhile(this.isOpChar),
			};
		}

		this.input.die('Unexpected character: ' + ch);
		return null;
	}
}

export type ASTNode =
	| { type: 'num'; value: number }
	| { type: 'str'; value: string }
	| { type: 'var'; value: string }
	| { type: 'bool'; value: boolean }
	| { type: 'call'; func: ASTNode; args: ASTNode[] }
	// | { type: 'ternary'
	| { type: 'binary'; operator: string; left: ASTNode; right: ASTNode }
	| { type: 'not'; body: ASTNode };

const PRECEDENCE: Record<string, number> = {
	'||': 2,
	'&&': 3,
	'<': 7,
	'>': 7,
	'<=': 7,
	'>=': 7,
	'=': 7,
	'!=': 7,
	'+': 10,
	'-': 10,
	'*': 20,
	'/': 20,
	'%': 20,
};

class Parser {
	private input: TokenStream;

	constructor(input: TokenStream) {
		this.input = input;
	}

	public parse() {
		return this.parseExpression();
	}

	private delimited<T>(
		start: string,
		stop: string,
		separator: string,
		parser: () => T
	): T[] {
		const list: T[] = [];
		let first = true;
		this.skipPunc(start);
		while (!this.input.eof()) {
			if (this.isPunc(stop)) break;
			if (first) {
				first = false;
			} else {
				this.skipPunc(separator);
			}
			if (this.isPunc(stop)) break;
			list.push(parser());
		}
		this.skipPunc(stop);
		return list;
	}

	private parseAtom(): ASTNode {
		return this.maybeCall(() => {
			if (this.isPunc('(')) {
				this.input.next();
				const exp = this.parseExpression();
				this.skipPunc(')');
				return exp;
			}

			if (this.isOp('!')) {
				this.input.next();
				const node: ASTNode = {
					type: 'not',
					body: this.parseAtom(),
				};
				return node;
			}

			if (this.isKw('true') || this.isKw('false')) {
				return this.parseBool();
			}

			const token = this.input.next();
			if (
				token &&
				(token.type === 'var' ||
					token.type === 'num' ||
					token.type === 'str')
			) {
				return token as ASTNode;
			}

			throw this.unexpected();
		});
	}

	private parseExpression(): ASTNode {
		return this.maybeCall(() => {
			return this.maybeBinary(this.parseAtom(), 0);
		});
	}

	private maybeCall(expr: () => ASTNode): ASTNode {
		const result = expr();
		return this.isPunc('(') ? this.parseCall(result) : result;
	}

	private parseCall(func: ASTNode): ASTNode {
		return {
			type: 'call',
			func: func,
			args: this.delimited('(', ')', ',', this.parseExpression),
		};
	}

	private maybeBinary(left: ASTNode, myPrec: number): ASTNode {
		const token = this.isOp();
		if (token) {
			const otherPrec = PRECEDENCE[token.value];
			if (otherPrec > myPrec) {
				this.input.next();
				const right = this.maybeBinary(this.parseAtom(), otherPrec);
				const node: ASTNode = {
					type: 'binary',
					operator: token.value,
					left: left,
					right: right,
				};
				return this.maybeBinary(node, myPrec);
			}
		}
		return left;
	}

	private parseBool(): ASTNode {
		const token = this.input.next();
		if (!token) {
			throw this.input.die('Expecting boolean');
		}
		return {
			type: 'bool',
			value: token.value === 'true',
		};
	}

	private isPunc(ch?: string) {
		const token = this.input.peek();
		return (
			token &&
			token.type === 'punc' &&
			(!ch || token.value === ch) &&
			token
		);
	}
	private isKw(kw?: string) {
		const token = this.input.peek();
		return (
			token && token.type === 'kw' && (!kw || token.value === kw) && token
		);
	}
	private isOp(op?: string) {
		const token = this.input.peek();
		return (
			token && token.type === 'op' && (!op || token.value === op) && token
		);
	}
	private skipPunc(ch: string) {
		if (this.isPunc(ch)) {
			this.input.next();
		} else {
			this.input.die(`Expecting punctuation: "${ch}"`);
		}
	}
	private skipKw(kw: string) {
		if (this.isKw(kw)) {
			this.input.next();
		} else {
			this.input.die(`Expecting keyword: "${kw}"`);
		}
	}
	private skipOp(op: string) {
		if (this.isOp(op)) {
			this.input.next();
		} else {
			this.input.die(`Expecting operator: "${op}"`);
		}
	}
	private unexpected() {
		this.input.die(
			`Unexpected token: ` + JSON.stringify(this.input.peek())
		);
	}
}

export const parseFilter = (input: string) => {
	// const inputStream = new InputStream(input);
	// const tokenStream = new TokenStream(inputStream);
	// while (!tokenStream.eof()) console.log(tokenStream.next());

	const parser = new Parser(new TokenStream(new InputStream(input)));
	return parser.parse();
};

export type ItemFilterRoot = ASTNode;
