import { Input } from '@chakra-ui/react';
import React, { useEffect } from 'react';

interface ItemFilterPlainProps {
	value: string;
	setValue: React.Dispatch<React.SetStateAction<string>>;
	restoreValue: string;
}

export const ItemFilterPlain: React.FC<ItemFilterPlainProps> = ({
	value,
	setValue,
	restoreValue,
}) => {
	useEffect(() => {
		setValue(restoreValue);
	}, [setValue, restoreValue]);

	return (
		<Input
			variant="unstyled"
			rounded="none"
			flex={1}
			value={value}
			placeholder="Advanced search..."
			onChange={(e) => setValue(e.currentTarget.value)}
			onKeyDown={(ev) => {
				if (ev.key === 'Escape') {
					setValue('');

					ev.preventDefault();
					ev.stopPropagation();
				}
			}}
		/>
	);
};
