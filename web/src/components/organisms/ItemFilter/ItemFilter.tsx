import { Checkbox } from '@chakra-ui/react';
import { SelectInstance } from 'chakra-react-select';
import React, { useContext, useState } from 'react';
import { useDebouncedEffect } from '../../../hooks/useDebouncedEffect';
import { InventoryViewerContext } from '../../pages/InventoryPage/InventoryViewerContext';
import { ItemFilterContainer } from './ItemFilterContainer';
import { ItemFilterFancy } from './ItemFilterFancy';
import { ItemFilterPlain } from './ItemFilterPlain';
import { parseFilter } from './parser';

interface ItemFilterProps {}

export const ItemFilter: React.FC<ItemFilterProps> = () => {
	const { setFilter, setFilterError } = useContext(InventoryViewerContext);

	const [value, setValue] = useState('');
	const [restoreValue, setRestoreValue] = useState('');

	const [plainMode, setPlainMode] = useState(false);

	useDebouncedEffect(
		() => {
			if (!value.length) {
				console.log('(empty)');
				setFilter(null);
				setFilterError(false);

				return;
			}

			try {
				const filter = parseFilter(value);

				console.log(filter);
				setFilter(filter);
				setFilterError(false);
			} catch (e) {
				console.error(e);
				setFilter(null);
				setFilterError(true);
			}
		},
		[value, setFilter, setFilterError],
		200
	);

	const [focusRef, setFocusRef] = useState<
		React.MutableRefObject<SelectInstance<any> | null> | undefined
	>(undefined);

	return (
		<ItemFilterContainer forwardFocus={focusRef}>
			<Checkbox
				isChecked={plainMode}
				onChange={() => {
					setRestoreValue(value);
					setPlainMode((x) => !x);
				}}
			/>
			{plainMode ? (
				// Plain filter just uses value directly
				<ItemFilterPlain
					restoreValue={restoreValue}
					value={value}
					setValue={setValue}
				/>
			) : (
				// Fancy filter maintains its own state and sends up the final computed value
				<ItemFilterFancy
					restoreValue={restoreValue}
					setValue={setValue}
					setFocusRef={setFocusRef}
				/>
			)}
		</ItemFilterContainer>
	);
};
