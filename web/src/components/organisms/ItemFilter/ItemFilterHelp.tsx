import {
	Box,
	Button,
	Code,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Stack,
	Text,
} from '@chakra-ui/react';
import React from 'react';

interface ItemFilterHelpModalProps {
	isOpen: boolean;
	onClose: () => void;
}

export const ItemFilterHelpModal: React.FC<ItemFilterHelpModalProps> = ({
	isOpen,
	onClose,
}) => {
	return (
		<Modal isOpen={isOpen} onClose={onClose} size="2xl">
			<ModalOverlay />
			<ModalContent>
				<ModalHeader>Inventory Filter</ModalHeader>
				<ModalCloseButton />

				<ModalBody>
					<Stack gap={4}>
						<Box>
							<Text>
								You can filter the inventory in various ways:
							</Text>
							<Code>is:common</Code>
							<Code ml={3}>is:legendary</Code>
							<Code ml={3}>quality &gt;= epic</Code>
							<br />
							<Code>level &gt;= 15</Code>
							<Code ml={3}>level &lt; 10</Code>
							<Code ml={3}>is:maxlevel</Code>
							<br />
							<Code>is:equipped</Code>
							<Code ml={3}>!is:equipped</Code>
							<br />
							<Code>is:material</Code>
							<Code ml={3}>is:weapon</Code>
							<Code ml={3}>is:rune</Code>
							<Code ml={3}>slot:head</Code>
							<Code ml={3}>slot:finger</Code>
							<br />
							<Code>damage &gt; 10</Code>
							<br />
							<Code>is:noaugs</Code>
							<Code ml={3}>is:maxaugs</Code>
							<Code ml={3}>augments &gt;= 2</Code>
							<br />
							<Code>type = &quot;silk cowl&quot;</Code>
						</Box>

						<Box>
							<Text>
								You can also enable advanced search using the
								checkbox to the left of the filter box. Advanced
								search lets you write more complicated filter
								expressions:
							</Text>
							<Code>
								(quality = legendary && level &gt; 15) ||
								(quality = epic && is:maxlevel)
							</Code>
						</Box>
					</Stack>
				</ModalBody>

				<ModalFooter>
					<Button variant="outline" onClick={onClose}>
						Close
					</Button>
				</ModalFooter>
			</ModalContent>
		</Modal>
	);
};
