import { Box, useColorModeValue as mode } from '@chakra-ui/react';
import React from 'react';

export interface ItemFilterToken {
	value: string;
}

interface ItemFilterTokenProps extends ItemFilterToken {}

export const ItemFilterToken: React.FC<ItemFilterTokenProps> = ({ value }) => {
	return (
		<Box
			px="8px"
			py="2px"
			rounded="md"
			whiteSpace="nowrap"
			bg={mode('gray.200', 'gray.700')}
		>
			{value}
		</Box>
	);
};
