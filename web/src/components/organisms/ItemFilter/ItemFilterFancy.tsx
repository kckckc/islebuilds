import {
	CreatableSelect,
	GroupBase,
	SelectInstance,
} from 'chakra-react-select';
import React, { useEffect, useRef, useState } from 'react';
import { ItemFilterToken } from './ItemFilterToken';

const SEARCH_SUGGESTIONS: OptionType[] = [
	'quality = legendary',
	'quality = epic',
	'quality = rare',
	'quality = magic',
	'quality = common',
	'is:legendary',
	'is:epic',
	'is:rare',
	'is:magic',
	'is:common',
	'is:maxlevel',
	'is:equipped',
	'is:material',
	'is:weapon',
	'is:rune',
	'slot:head',
	'slot:neck',
	'slot:chest',
	'slot:hands',
	'slot:finger',
	'slot:waist',
	'slot:legs',
	'slot:feet',
	'slot:trinket',
	'slot:onehanded',
	'slot:twohanded',
	'slot:offhand',
	'slot:tool',
	'slot:rune',
	'is:augmented',
	'is:maxaugs',
	'is:noaugs',
].map((s) => ({ label: s, value: s }));

export interface OptionType {
	label: string;
	value: string;
}

interface ItemFilterFancyProps {
	restoreValue: string;
	setValue: React.Dispatch<React.SetStateAction<string>>;
	setFocusRef: React.Dispatch<
		React.SetStateAction<
			| React.MutableRefObject<SelectInstance<
					any,
					false,
					GroupBase<any>
			  > | null>
			| undefined
		>
	>;
}

const stripParentheses = (s: string) => {
	s = s.trim();
	while (s.startsWith('(') && s.endsWith(')')) {
		s = s.slice(1, s.length - 1);
	}
	return s;
};

const wrapParentheses = (s: string) => {
	s = s.trim();
	if (s.startsWith('(') && s.endsWith(')')) {
		return s;
	} else {
		return '(' + s + ')';
	}
};

export const ItemFilterFancy: React.FC<ItemFilterFancyProps> = ({
	restoreValue,
	setValue,
	setFocusRef,
}) => {
	const [inputValue, setInputValue] = useState('');

	const [tokens, setTokens] = useState<ItemFilterToken[]>([]);

	// Restore tokens when switching out of plain mode
	useEffect(() => {
		setTokens(
			restoreValue
				.split('&&')
				.map((s) => s.trim())
				.map((s) => stripParentheses(s))
				.filter((s) => s.length > 0)
				.map((s) => ({ value: s }))
		);
	}, [restoreValue]);

	// Build filter terms from tokens
	useEffect(() => {
		const filter = [
			tokens.map((t) => wrapParentheses(t.value.trim())).join(' && '),
			inputValue ? wrapParentheses(inputValue) : '',
		]
			.filter((x) => x.length > 0)
			.join(' && ');

		setValue(filter);
	}, [inputValue, tokens, setValue]);

	const selectRef = useRef<SelectInstance<OptionType> | null>(null);
	useEffect(() => {
		setFocusRef(selectRef);
	}, [setFocusRef, selectRef]);

	return (
		<>
			{tokens.map((t, i) => (
				<ItemFilterToken key={i} {...t} />
			))}

			<CreatableSelect
				// For passing focus from fake input container and checking input selection
				ref={selectRef}
				// The outer container is styled to look like an input already
				variant="unstyled"
				// We don't want to actually select things with CreatableSelect,
				// it's mostly used for the autocomplete suggestions.
				// Value should always be null to fix hideSelectedOptions not working
				value={null}
				controlShouldRenderValue={false}
				hideSelectedOptions={false}
				escapeClearsValue={false}
				isClearable={false}
				// Only show placeholder when nothing at all has been entered
				placeholder={tokens.length === 0 ? 'Search...' : ''}
				options={SEARCH_SUGGESTIONS}
				inputValue={inputValue}
				formatCreateLabel={(input) => {
					return input;
				}}
				// Hide menu until we type something
				menuIsOpen={!!inputValue.length}
				// We need to portal out of the component because the parent container
				// has overflow:hidden for vertical scrolling and
				// we are unable to use overflow-x:hidden + overflow-y:visible
				// We need to check if window is defined because document.body breaks in SSR
				menuPortalTarget={
					typeof window !== 'undefined' ? document.body : null
				}
				onInputChange={(value, meta) => {
					if (meta.action === 'menu-close') {
						// Do nothing
					} else if (meta.action === 'input-blur') {
						// Clicking outside should turn the remaining input into a token
						// They can still backspace and edit it if they weren't done
						setTokens((prev) => {
							if (inputValue.length) {
								return [...prev, { value: inputValue }];
							} else {
								return prev;
							}
						});
						setInputValue('');
					} else {
						setInputValue(value);
					}
				}}
				// Handle backspace to remove previous tags
				onKeyDown={(ev) => {
					if (ev.key === 'Backspace') {
						const inputRef = selectRef?.current?.inputRef;
						if (!inputRef) return;

						// Check last position of cursor before backspace was pressed
						const startPos = inputRef.selectionStart;
						const endPos = inputRef.selectionEnd;

						if (startPos === 0 && startPos === endPos) {
							setTokens((prev) => {
								if (!prev.length) {
									return prev;
								}

								// If ctrl was not held, copy the contents of the last token into the current input
								if (!ev.ctrlKey) {
									const last = prev[prev.length - 1].value;
									setInputValue((prev) => {
										return last + prev;
									});
									setTimeout(() => {
										inputRef.selectionStart = last.length;
										inputRef.selectionEnd = last.length;
									}, 0);
								}

								// Remove the last token
								return prev.slice(0, -1);
							});

							ev.preventDefault();
						}
					} else if (ev.key === 'Escape') {
						// Clear everything
						setTokens([]);
						setInputValue('');

						ev.preventDefault();
						ev.stopPropagation();
					}
				}}
				// Handle selections
				onChange={(option, meta) => {
					if (
						meta.action === 'select-option' ||
						meta.action === 'create-option'
					) {
						if (option) {
							setTokens((prev) => {
								return [...prev, option];
							});
						}
					}
				}}
				// Hide dropdown indicator
				components={{
					DropdownIndicator: null,

					// Option: (props) => (
					// 	<chakraComponents.Option
					// 		{...props}
					// 	></chakraComponents.Option>
					// ),
				}}
				chakraStyles={{
					container: (base) => ({
						...base,
						minWidth: '200px',
					}),

					input: (base) => ({
						...base,
						// Fix for invisible text sometimes
						opacity: '1 !important',
					}),

					// Set a fixed width for the menu list
					menu: (base) => ({
						...base,
						width: '200px',
					}),

					// Add ellipsis to overflowing menu options
					option: (base) => ({
						...base,
						display: 'block',
						overflowX: 'hidden',
						whiteSpace: 'nowrap',
						textOverflow: 'ellipsis',
					}),
				}}
			/>
		</>
	);
};
