import { IWDItem } from '@kckckc/isleward-util/isleward';
import { ASTNode, ItemFilterRoot } from './parser';

export const evaluateFilterRoot = (
	node: ItemFilterRoot,
	item: IWDItem
): boolean => {
	try {
		const value = evaluate(node, item);
		// console.log('RV: ', value);
		if (value.type !== 'bool') {
			throw new Error('filter didnt return boolean');
		}
		return value.value;
	} catch (e) {
		console.error(e);
		return false;
	}
};

type NumValue = {
	type: 'num';
	value: number;
};
type BoolValue = {
	type: 'bool';
	value: boolean;
};
type StringValue = {
	type: 'str';
	value: string;
};
type AnyValue = NumValue | BoolValue | StringValue;

const numValue = (value: number): NumValue => ({
	type: 'num',
	value,
});

const boolValue = (value: boolean): BoolValue => ({
	type: 'bool',
	value,
});

const stringValue = (value: string): StringValue => ({
	type: 'str',
	value,
});

function assertNum(x: AnyValue): asserts x is NumValue {
	if (x.type !== 'num') {
		throw new Error('type error');
	}
}
function assertBool(x: AnyValue): asserts x is BoolValue {
	if (x.type !== 'bool') {
		throw new Error('type error');
	}
}

// Returns the result of evaluating the given node
const evaluate = (node: ASTNode, item: IWDItem): AnyValue => {
	if (node.type === 'num' || node.type === 'bool' || node.type === 'str') {
		return node;
	} else if (node.type === 'var') {
		return evaluateVar(node, item);
	} else if (node.type === 'binary') {
		if (node.operator === '&&' || node.operator === '||') {
			return evaluateOperatorShortCircuit(
				node.left,
				node.right,
				node.operator,
				item
			);
		}

		const left = evaluate(node.left, item);
		const right = evaluate(node.right, item);

		return evaluateOperator(left, right, node.operator);
	} else if (node.type === 'not') {
		const body = evaluate(node.body, item);
		assertBool(body);

		return boolValue(!body.value);
	}

	throw new Error('unhandled node: ' + JSON.stringify(node));
};

const varDefinitions: { [key: string]: (item: IWDItem) => AnyValue } = {
	quality: (i) => numValue(i.quality ?? 0),
	common: () => numValue(0),
	magic: () => numValue(1),
	rare: () => numValue(2),
	epic: () => numValue(3),
	legendary: () => numValue(4),
	'is:common': (i) => boolValue(i.quality === 0),
	'is:magic': (i) => boolValue(i.quality === 1),
	'is:rare': (i) => boolValue(i.quality === 2),
	'is:epic': (i) => boolValue(i.quality === 3),
	'is:legendary': (i) => boolValue(i.quality === 4),

	level: (i) => numValue(i.level ?? 0),
	'is:maxlevel': (i) => boolValue(i.level === 20),

	'is:material': (i) => boolValue(i.material ?? false),

	'is:eq': (i) => boolValue(i.eq ?? false),
	'is:equipped': (i) => boolValue(i.eq ?? false),

	'is:weapon': (i) => boolValue(!!i.spell && !i.ability),
	damage: (i) => numValue(i.spell?.values?.['damage'] ?? 0),

	'is:rune': (i) => boolValue(!!i.spell && !!i.ability),

	'slot:head': (i) => boolValue(i.slot === 'head'),
	'slot:neck': (i) => boolValue(i.slot === 'neck'),
	'slot:chest': (i) => boolValue(i.slot === 'chest'),
	'slot:hands': (i) => boolValue(i.slot === 'hands'),
	'slot:finger': (i) => boolValue(i.slot === 'finger'),
	'slot:waist': (i) => boolValue(i.slot === 'waist'),
	'slot:legs': (i) => boolValue(i.slot === 'legs'),
	'slot:feet': (i) => boolValue(i.slot === 'feet'),
	'slot:trinket': (i) => boolValue(i.slot === 'trinket'),
	'slot:onehanded': (i) => boolValue(i.slot === 'oneHanded'),
	'slot:twohanded': (i) => boolValue(i.slot === 'twoHanded'),
	'slot:offhand': (i) => boolValue(i.slot === 'offHand'),
	'slot:tool': (i) => boolValue(i.slot === 'tool'),
	'slot:rune': (i) => boolValue(!!i.spell && !!i.ability),

	augments: (i) => numValue(i.power ?? 0),
	'is:augmented': (i) => boolValue((i.power ?? 0) > 0),
	'is:maxaugs': (i) => boolValue((i.power ?? 0) === 3),
	'is:noaugs': (i) => boolValue((i.power ?? 0) === 0),

	type: (i) => stringValue(i.type ?? ''),
};

const evaluateVar = (
	node: ASTNode & { type: 'var' },
	item: IWDItem
): AnyValue => {
	if (varDefinitions[node.value]) {
		return varDefinitions[node.value](item);
	}

	throw new Error('unhandled var: ' + JSON.stringify(node));
};

const evaluateOperator = (
	left: AnyValue,
	right: AnyValue,
	operator: string
): NumValue | BoolValue => {
	if (operator === '=') {
		if (left.type !== right.type) {
			return boolValue(false);
		}

		if (left.type === 'str' && right.type === 'str') {
			return boolValue(
				left.value.toLowerCase() === right.value.toLowerCase()
			);
		}

		return boolValue(left.value === right.value);
	}

	assertNum(left);
	assertNum(right);

	if (operator === '<') {
		return boolValue(left.value < right.value);
	} else if (operator === '<=') {
		return boolValue(left.value <= right.value);
	} else if (operator === '>') {
		return boolValue(left.value > right.value);
	} else if (operator === '>=') {
		return boolValue(left.value >= right.value);
	} else if (operator === '+') {
		return numValue(left.value + right.value);
	} else if (operator === '-') {
		return numValue(left.value - right.value);
	} else if (operator === '*') {
		return numValue(left.value * right.value);
	} else if (operator === '/') {
		return numValue(left.value / right.value);
	} else if (operator === '%') {
		return numValue(left.value % right.value);
	}

	throw new Error('unhandled operator: ' + operator);
};

const evaluateOperatorShortCircuit = (
	leftNode: ASTNode,
	rightNode: ASTNode,
	operator: string,
	item: IWDItem
): BoolValue => {
	const left = evaluate(leftNode, item);

	// TOOD: shortcircuit other types with `||` or `??` ?
	assertBool(left);

	if (operator === '||') {
		// Short-circuit if left was true
		if (left.value) {
			return boolValue(true);
		}

		// Otherwise evaluate right
		const right = evaluate(rightNode, item);
		assertBool(right);

		return right;
	} else if (operator === '&&') {
		// Short-circuit if left was false
		if (!left.value) {
			return boolValue(false);
		}

		// Otherwise evaluate right
		const right = evaluate(rightNode, item);
		assertBool(right);

		return right;
	}

	throw new Error('unhandled operator: ' + operator);
};
