import { Flex, Heading } from '@chakra-ui/react';
import React from 'react';
import { SectionHeading } from '../templates/SectionHeading';
import { TwoColumnSection } from '../templates/TwoColumnSection';

interface ToolsListProps {}

export const ToolsList: React.FC<ToolsListProps> = ({}) => (
	<TwoColumnSection
		rowGap={8}
		left={
			<Flex direction="column" gap={8}>
				<Flex direction="column" gap={1}>
					<Heading size="sm" mb={1}>
						Sprites
					</Heading>
					<SectionHeading href={`/sprite-editor`}>
						Sprite Editor
					</SectionHeading>
					<SectionHeading href={`/sprite-editor/gallery`}>
						Sprite Gallery
					</SectionHeading>
					<SectionHeading href={`/tools/palette`}>
						Color Palette
					</SectionHeading>
				</Flex>

				<Flex direction="column" gap={1}>
					<Heading size="sm" mb={1}>
						Misc
					</Heading>

					<SectionHeading href={`/inventory`}>
						Inventory Viewer
					</SectionHeading>
					<SectionHeading href={`/character`}>
						Character Search
					</SectionHeading>
				</Flex>
			</Flex>
		}
		right={
			<Flex direction="column" gap={8}>
				<Flex direction="column" gap={1}>
					<Heading size="sm" mb={1}>
						Stats
					</Heading>
					<SectionHeading href={`/rune-loadouts`}>
						Top Rune Loadouts
					</SectionHeading>
					<SectionHeading href={`/passive-trees`}>
						Top Passive Trees
					</SectionHeading>
					<SectionHeading href={`/skins`}>Top Skins</SectionHeading>
				</Flex>

				<Flex direction="column" gap={1}>
					<Heading size="sm" mb={1}>
						Calculators
					</Heading>

					<SectionHeading href={`/tools/qq`}>
						Item Quantity/Quality
					</SectionHeading>
					<SectionHeading href={`/tools/reputation-calculator`}>
						Reputation
					</SectionHeading>
					<SectionHeading href={`/tools/idol-calculators`}>
						Idols
					</SectionHeading>
					<SectionHeading href={`/tools/cast-time`}>
						Cast Time
					</SectionHeading>
					<SectionHeading href={`/tools/slot-chances`}>
						Equipment Slots
					</SectionHeading>
				</Flex>
			</Flex>
		}
	/>
);
