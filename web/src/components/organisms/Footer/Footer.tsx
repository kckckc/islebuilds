import {
	Box,
	useColorModeValue,
	Container,
	Stack,
	Text,
	Link,
} from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';
import { REPORT_ISSUE_URL } from '../../molecules/HelpButton';

interface FooterProps {}

export const Footer: React.FC<FooterProps> = ({}) => {
	return (
		<Box
			bg={useColorModeValue('gray.50', 'gray.900')}
			color={useColorModeValue('gray.700', 'gray.200')}
			mt={16}
			style={{ position: 'sticky', top: '100vh' }}
			borderTopWidth={1}
			borderStyle={'solid'}
			borderColor={useColorModeValue('gray.200', 'gray.700')}
			px={4}
		>
			<Container
				as={Stack}
				maxW={'6xl'}
				py={4}
				direction={{ base: 'column', md: 'row' }}
				spacing={4}
				justify={{ base: 'center', md: 'space-between' }}
				align={{ base: 'center', md: 'center' }}
			>
				<Stack direction={'row'} spacing={6}>
					<NextLink href="/" passHref legacyBehavior>
						<Link>Home</Link>
					</NextLink>
					<NextLink href="/news" passHref legacyBehavior>
						<Link>News</Link>
					</NextLink>
					<Link as="a" href={REPORT_ISSUE_URL} target="_blank">
						Report Issues
					</Link>
				</Stack>
				<NextLink href="/about" passHref legacyBehavior>
					<Link>
						<Text>Islebuilds</Text>
					</Link>
				</NextLink>
			</Container>
		</Box>
	);
};
