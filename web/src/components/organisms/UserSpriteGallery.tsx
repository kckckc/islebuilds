import { Box, Button, Flex } from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';
import { useUserSpritesQuery } from '../../generated/graphql';
import { CenterLoading } from '../atoms/CenterLoading';
import { NothingText } from '../atoms/NothingText';
import { SpriteGallery } from './SpriteGallery';

interface UserSpriteGalleryProps {
	userId: string;
	limit?: number;
	noMore?: boolean;
	seeMore?: boolean;
}

export const UserSpriteGallery: React.FC<UserSpriteGalleryProps> = ({
	userId,
	limit = 25,
	noMore = false,
	seeMore = false,
}) => {
	const { data, loading, error, fetchMore } = useUserSpritesQuery({
		variables: {
			user: userId,
			limit: limit,
		},
	});

	if (loading) {
		return <CenterLoading />;
	} else if (error) {
		return <Box>error</Box>;
	}

	if (!data?.sprites) return null;

	return data.sprites.sprites.length ? (
		<Flex direction="column">
			<SpriteGallery
				data={data.sprites.sprites}
				hasMore={data.sprites.hasMore}
				onFetchMore={() => {
					fetchMore({
						variables: {
							user: userId,
							limit: limit,
							offset: data.sprites.sprites.length,
						},
					});
				}}
				showFetchMore={!noMore}
				loading={loading}
				cardMinW="120px"
				hideName
			/>
			{seeMore && (
				<NextLink href={'/user/' + userId + '/sprites'} legacyBehavior>
					<Button as="a" mx="auto" px={16} mt={4} cursor="pointer">
						see all sprites
					</Button>
				</NextLink>
			)}
		</Flex>
	) : (
		<NothingText>No sprites yet</NothingText>
	);
};
