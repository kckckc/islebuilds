import { Box, Divider, Flex, Text } from '@chakra-ui/react';
import { Form, Formik } from 'formik';
import React, { ReactNode } from 'react';
import { humanizeUntil } from '../../util/humanize';
import { InputField } from '../atoms/InputField';
import { LabeledBox } from '../molecules/LabeledBox';
import { SelectField } from '../molecules/SelectField';
import { TwoColumnLayout } from '../templates/TwoColumnLayout';

interface ReputationCalculatorProps {}

const TIERS = [
	{
		tier: 0,
		name: 'Hated',
		rep: -25000,
	},
	{
		tier: 1,
		name: 'Hostile',
		rep: -10000,
	},
	{
		tier: 2,
		name: 'Unfriendly',
		rep: -1000,
	},
	{
		tier: 3,
		name: 'Neutral',
		rep: 0,
	},
	{
		tier: 4,
		name: 'Friendly',
		rep: 1000,
	},
	{
		tier: 5,
		name: 'Honored',
		rep: 10000,
	},
	{
		tier: 6,
		name: 'Revered',
		rep: 25000,
	},
	{
		tier: 7,
		name: 'Exalted',
		rep: 50000,
	},
];
const TierOptions = () => {
	return (
		<>
			{TIERS.slice(3).map((t) => (
				<option key={t.tier} value={t.tier + ''}>
					{t.name}
				</option>
			))}
		</>
	);
};

const tierPercentToRep = (tierId: number, percent: number) => {
	const tier = TIERS.find((t) => t.tier === tierId);
	if (!tier) return 0;
	if (tier.name === 'Exalted') {
		return tier.rep;
	}

	const realPercent = percent / 100;

	const nextTier = TIERS.find((t) => t.tier === tierId + 1);
	if (!nextTier) return 0;
	const needed = nextTier.rep - tier.rep;
	const haveInTier = needed * realPercent;

	return haveInTier + tier.rep;
};

export const ReputationCalculator: React.FC<
	ReputationCalculatorProps
> = ({}) => (
	<>
		<Text mb={4}>
			Calculates how long it will take to reach a certain reputation tier.
			Enter your current tier and percent and desired tier and percent.
			Then enter how often you get reputation and how much reputation
			you&apos;re getting. This calculator is mostly intended for farming
			bosses with mostly predictable reputation rates.
		</Text>
		<Formik
			initialValues={{
				currentTier: '4',
				currentPercent: '0',
				targetTier: '7',
				targetPercent: '0',
				repAmount: '120',
				repInterval: '2.5',
			}}
			onSubmit={() => {}}
		>
			{({ values }) => {
				let resultPart: ReactNode;

				let targetReached = true;

				const currentTier = parseFloat(values.currentTier);
				const currentPercent = parseFloat(values.currentPercent);
				const targetTier = parseFloat(values.targetTier);
				const targetPercent = parseFloat(values.targetPercent);
				const repAmount = parseFloat(values.repAmount);
				const repInterval = parseFloat(values.repInterval);

				if (
					[
						currentTier,
						currentPercent,
						targetTier,
						targetPercent,
					].some((x) => isNaN(x))
				) {
					resultPart = (
						<Box borderWidth={1} p={2} textAlign="center">
							Invalid input
						</Box>
					);
				} else {
					const currentRep = tierPercentToRep(
						currentTier,
						currentPercent
					);
					const targetRep = tierPercentToRep(
						targetTier,
						targetPercent
					);

					targetReached = currentRep >= targetRep;

					const perMinute = repAmount / repInterval;
					const repRemaining = targetRep - currentRep;
					const minsRemaining = repRemaining / perMinute;
					const msRemaining = minsRemaining * 60 * 1000;

					if (!targetReached) {
						resultPart = (
							<Flex
								flexDir="column"
								gap={2}
								borderWidth={1}
								p={2}
								textAlign="center"
							>
								<Text>
									{currentRep} / {targetRep} rep
								</Text>
								<Text>
									{humanizeUntil(msRemaining)} remaining
								</Text>
							</Flex>
						);
					} else {
						resultPart = (
							<Box borderWidth={1} p={2} textAlign="center">
								Goal Reached!
							</Box>
						);
					}
				}

				return (
					<>
						<TwoColumnLayout
							left={
								<Form>
									<Flex flexDir="column" gap={2}>
										<SelectField
											label="Current Tier"
											name="currentTier"
										>
											<TierOptions />
										</SelectField>
										<InputField
											label="Current Percent"
											name="currentPercent"
										>
											<TierOptions />
										</InputField>
									</Flex>
								</Form>
							}
							right={
								<Form>
									<Flex flexDir="column" gap={2}>
										<SelectField
											label="Target Tier"
											name="targetTier"
										>
											<TierOptions />
										</SelectField>
										<InputField
											label="Target Percent"
											name="targetPercent"
										>
											<TierOptions />
										</InputField>
									</Flex>
								</Form>
							}
						/>

						<Divider my={4} />

						<TwoColumnLayout
							left={
								<Form>
									<Flex flexDir="column" gap={2}>
										<InputField
											label="Reputation chunk amount?"
											name="repAmount"
											disabled={!!targetReached}
										>
											<TierOptions />
										</InputField>
										<InputField
											label="Reputation chunk frequency (minutes)?"
											name="repInterval"
											disabled={!!targetReached}
										>
											<TierOptions />
										</InputField>
									</Flex>
								</Form>
							}
							right={
								<Flex
									flex={1}
									flexDir="column"
									justifyContent="center"
									alignItems="center"
								>
									{resultPart}
								</Flex>
							}
						/>
					</>
				);
			}}
		</Formik>
		<Divider my={4} />
		<Box>
			<LabeledBox label="Reference">
				<Flex flexDir="column" gap={2}>
					<Text>
						To get a more realistic estimate, remember to add some
						extra time for time fighting, deaths, map resets, etc.
					</Text>
					<Text>
						M&apos;ogresh spawns every 2.333 minutes and gives 120
						Gaekatla reputation.
					</Text>
				</Flex>
			</LabeledBox>
		</Box>
	</>
);
