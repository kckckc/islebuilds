import {
	Button,
	Heading,
	Input,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Textarea,
	ToastId,
	useDisclosure,
	useToast,
} from '@chakra-ui/react';
import React, { useState } from 'react';

interface ReportModalProps {
	disclosure: ReturnType<typeof useDisclosure>;
	typeLabel: string;
	callback: (title: string, desc: string, id?: ToastId) => Promise<void>;
}

export const ReportModal: React.FC<ReportModalProps> = ({
	disclosure,
	typeLabel,
	callback,
}) => {
	const toast = useToast();

	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');

	return (
		<Modal
			preserveScrollBarGap
			size="xl"
			isOpen={disclosure.isOpen}
			onClose={disclosure.onClose}
		>
			<ModalOverlay />
			<ModalContent>
				<ModalHeader>Report {typeLabel}</ModalHeader>
				<ModalCloseButton />
				<ModalBody>
					<Heading size="sm" mb={2}>
						Reason
					</Heading>
					<Input
						mb={4}
						placeholder="Reason"
						maxLength={500}
						value={title}
						onChange={(e) => setTitle(e.target.value)}
					/>

					<Heading size="sm" mb={2}>
						Description
					</Heading>
					<Textarea
						placeholder="Any additional information"
						maxLength={1500}
						value={description}
						onChange={(e) => setDescription(e.target.value)}
					/>
				</ModalBody>

				<ModalFooter>
					<Button
						colorScheme="red"
						mr={3}
						// disabled={!title.length}
						onClick={() => {
							const id = toast({
								description: 'Submitting report...',
								status: 'info',
								position: 'top-right',
							});
							callback(title, description, id);
							setTitle('');
							setDescription('');
							disclosure.onClose();
						}}
					>
						Submit
					</Button>
					<Button variant="ghost" onClick={disclosure.onClose}>
						Cancel
					</Button>
				</ModalFooter>
			</ModalContent>
		</Modal>
	);
};
