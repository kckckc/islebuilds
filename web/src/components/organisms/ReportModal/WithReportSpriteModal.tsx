import { ToastId, useDisclosure, useToast } from '@chakra-ui/react';
import React, { createContext, ReactNode, useCallback, useState } from 'react';
import { useReportSpriteMutation } from '../../../generated/graphql';
import { mockDisclosure } from '../../../util/mockDisclosure';
import { ReportModal } from './ReportModal';

export const ReportSpriteDisclosure = createContext<{
	disclosure: ReturnType<typeof useDisclosure>;
	submitted: boolean;
}>({ disclosure: mockDisclosure(), submitted: false });

interface WithReportSpriteModalProps {
	spriteId: string;
	children?: ReactNode;
}

export const WithReportSpriteModal: React.FC<WithReportSpriteModalProps> = ({
	spriteId,
	children,
}) => {
	const [reportSprite] = useReportSpriteMutation();

	const [submitted, setSubmitted] = useState(false);

	const toast = useToast();
	const disclosure = useDisclosure();

	const handleSubmit = useCallback(
		async (title: string, description: string, toastId?: ToastId) => {
			const res = await reportSprite({
				variables: {
					details: {
						title,
						description,
					},
					target: spriteId,
				},
			});

			await new Promise((x) => setTimeout(x, 1000));

			if (toastId) {
				if (res.data?.reportSprite) {
					toast.update(toastId, {
						description: 'Report submitted!',
						status: 'success',
					});

					setSubmitted(true);
				} else {
					toast.update(toastId, {
						description: 'Failed to submit',
						status: 'error',
					});
				}
			}
		},
		[reportSprite, spriteId, toast]
	);

	return (
		<ReportSpriteDisclosure.Provider value={{ disclosure, submitted }}>
			{children}
			<ReportModal
				disclosure={disclosure}
				typeLabel="Sprite"
				callback={handleSubmit}
			/>
		</ReportSpriteDisclosure.Provider>
	);
};
