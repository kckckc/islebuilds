import { ToastId, useDisclosure, useToast } from '@chakra-ui/react';
import React, { createContext, ReactNode, useCallback, useState } from 'react';
import { useReportCharacterMutation } from '../../../generated/graphql';
import { mockDisclosure } from '../../../util/mockDisclosure';
import { ReportModal } from './ReportModal';

export const ReportCharacterDisclosure = createContext<{
	disclosure: ReturnType<typeof useDisclosure>;
	submitted: boolean;
}>({ disclosure: mockDisclosure(), submitted: false });

interface WithReportCharacterModalProps {
	characterId: string;
	children?: ReactNode;
}

export const WithReportCharacterModal: React.FC<
	WithReportCharacterModalProps
> = ({ characterId, children }) => {
	const [reportCharacter] = useReportCharacterMutation();

	const [submitted, setSubmitted] = useState(false);

	const toast = useToast();
	const disclosure = useDisclosure();

	const handleSubmit = useCallback(
		async (title: string, description: string, toastId?: ToastId) => {
			const res = await reportCharacter({
				variables: {
					details: {
						title,
						description,
					},
					target: characterId,
				},
			});

			await new Promise((x) => setTimeout(x, 1000));

			if (toastId) {
				if (res.data?.reportCharacter) {
					toast.update(toastId, {
						description: 'Report submitted!',
						status: 'success',
					});

					setSubmitted(true);
				} else {
					toast.update(toastId, {
						description: 'Failed to submit',
						status: 'error',
					});
				}
			}
		},
		[reportCharacter, characterId, toast]
	);

	return (
		<ReportCharacterDisclosure.Provider value={{ disclosure, submitted }}>
			{children}
			<ReportModal
				disclosure={disclosure}
				typeLabel="Character"
				callback={handleSubmit}
			/>
		</ReportCharacterDisclosure.Provider>
	);
};
