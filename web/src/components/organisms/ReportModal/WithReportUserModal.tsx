import { ToastId, useDisclosure, useToast } from '@chakra-ui/react';
import React, { createContext, ReactNode, useCallback, useState } from 'react';
import { useReportUserMutation } from '../../../generated/graphql';
import { mockDisclosure } from '../../../util/mockDisclosure';
import { ReportModal } from './ReportModal';

export const ReportUserDisclosure = createContext<{
	disclosure: ReturnType<typeof useDisclosure>;
	submitted: boolean;
}>({ disclosure: mockDisclosure(), submitted: false });

interface WithReportUserModalProps {
	userId: string;
	children?: ReactNode;
}

export const WithReportUserModal: React.FC<WithReportUserModalProps> = ({
	userId,
	children,
}) => {
	const [reportUser] = useReportUserMutation();

	const [submitted, setSubmitted] = useState(false);

	const toast = useToast();
	const disclosure = useDisclosure();

	const handleSubmit = useCallback(
		async (title: string, description: string, toastId?: ToastId) => {
			const res = await reportUser({
				variables: {
					details: {
						title,
						description,
					},
					target: userId,
				},
			});

			await new Promise((x) => setTimeout(x, 1000));

			if (toastId) {
				if (res.data?.reportUser) {
					toast.update(toastId, {
						description: 'Report submitted!',
						status: 'success',
					});

					setSubmitted(true);
				} else {
					toast.update(toastId, {
						description: 'Failed to submit',
						status: 'error',
					});
				}
			}
		},
		[reportUser, userId, toast]
	);

	return (
		<ReportUserDisclosure.Provider value={{ disclosure, submitted }}>
			{children}
			<ReportModal
				disclosure={disclosure}
				typeLabel="User"
				callback={handleSubmit}
			/>
		</ReportUserDisclosure.Provider>
	);
};
