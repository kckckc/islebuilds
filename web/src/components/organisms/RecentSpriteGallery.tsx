import { Box } from '@chakra-ui/react';
import React from 'react';
import { useRecentSpritesQuery } from '../../generated/graphql';
import { CenterLoading } from '../atoms/CenterLoading';
import { SpriteGallery } from './SpriteGallery';

interface RecentSpriteGalleryProps {}

export const RecentSpriteGallery: React.FC<RecentSpriteGalleryProps> = ({}) => {
	const { data, loading, error, fetchMore } = useRecentSpritesQuery({
		variables: {
			limit: 40,
		},
	});

	if (loading) {
		return <CenterLoading />;
	} else if (error) {
		return <Box>error</Box>;
	}

	if (!data?.recentSprites) return null;

	return (
		<SpriteGallery
			data={data.recentSprites.sprites}
			hasMore={data.recentSprites.hasMore}
			onFetchMore={() => {
				fetchMore({
					variables: {
						cursor: data.recentSprites.cursor,
					},
				});
			}}
			showFetchMore={true}
			loading={loading}
		/>
	);
};
