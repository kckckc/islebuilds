import { EditIcon } from '@chakra-ui/icons';
import { Box, Divider, Flex, Heading, IconButton } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import NextLink from 'next/link';
import React from 'react';
import {
	CharactersSortBy,
	useMeQuery,
	UserQuery,
} from '../../generated/graphql';
import { humanize } from '../../util/humanize';
import { DiscordTag } from '../atoms/DiscordTag';
import { ReportUserButton } from '../atoms/ReportButton/ReportUserButton';
import { Section } from '../templates/Section';
import { ProfileRoles } from './ProfileRoles';
import { WithReportUserModal } from './ReportModal/WithReportUserModal';
import { UserCharacters } from './UserCharacters';
import { UserSpriteGallery } from './UserSpriteGallery';

interface UserProfileProps {
	user: Exclude<UserQuery['user'], null | undefined>;
}

export const UserProfile: React.FC<UserProfileProps> = ({ user }) => {
	const createdAt = parseInt(user.createdAt);
	const { data: meData } = useMeQuery();

	const discord = user?.connections?.find(
		(c) => c.provider === 'https://discord.com'
	);

	return (
		<WithReportUserModal userId={user.id}>
			<NextSeo title={user.displayName + ' | User'} />
			<Flex direction="row" justifyContent="space-between">
				<Box>
					<Heading mb={0} size="lg">
						{user.displayName}
						{meData && meData?.me?.id === user.id && (
							<NextLink
								href="/profile/edit"
								passHref
								legacyBehavior
							>
								<IconButton
									size="sm"
									ml={4}
									aria-label="Edit profile"
									icon={<EditIcon />}
								/>
							</NextLink>
						)}
					</Heading>
					<Flex flexDirection="row" columnGap={1} my={2}>
						{discord && (
							<DiscordTag
								displayName={discord.displayName!}
								visible={discord.visible}
							/>
						)}
						{/* <SelfTag userId={user.id} /> */}
					</Flex>
				</Box>

				<Flex flexDirection="row" gap={2}>
					<ReportUserButton />
				</Flex>
			</Flex>

			<Section>
				<span suppressHydrationWarning={true}>
					Joined {humanize(createdAt)}.
				</span>
			</Section>

			<Divider my={8} />

			<NextLink href={'/user/' + user.id + '/characters'}>
				<Heading size="md">Characters</Heading>
			</NextLink>

			<Box mt={4}>
				<UserCharacters
					userId={user.id}
					sortBy={CharactersSortBy.Level}
					limit={10}
					noMore
					seeMore
				/>
			</Box>

			<Divider my={8} />

			<NextLink href={'/user/' + user.id + '/sprites'}>
				<Heading size="md">Sprites</Heading>
			</NextLink>

			<Box mt={4}>
				<UserSpriteGallery userId={user.id} limit={10} noMore seeMore />
			</Box>

			<ProfileRoles user={user} />
		</WithReportUserModal>
	);
};
