import {
	Flex,
	FormControl,
	FormLabel,
	Heading,
	Link,
	Switch,
} from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';
import {
	MyConnectionsDocument,
	MyConnectionsQuery,
	useMyConnectionsQuery,
	useSetConnectionVisibleMutation,
} from '../../generated/graphql';
import { CenterLoading } from '../atoms/CenterLoading';
import { DiscordButton } from '../molecules/DiscordButton';
import { GoogleButton } from '../molecules/GoogleButton';

interface ConnectedAccountsProps {}

export const ConnectedAccounts: React.FC<ConnectedAccountsProps> = ({}) => {
	const { data, loading } = useMyConnectionsQuery();

	const [setVisible] = useSetConnectionVisibleMutation();

	if (loading) return <CenterLoading />;

	const google = data?.me?.connections?.find(
		(c) => c.provider === 'https://accounts.google.com'
	);
	const discord = data?.me?.connections?.find(
		(c) => c.provider === 'https://discord.com'
	);

	// @ts-ignore
	const apiUrl = globalThis.apiUrl ?? '';

	return (
		<Flex direction="column" gap={2}>
			<Heading size="sm">Google</Heading>
			{google ? (
				<>Connected</>
			) : (
				<>
					<GoogleButton next="/account" />
				</>
			)}

			<Heading size="sm" mt={4}>
				Discord
				{discord ? ': ' + discord.displayName : ''}
			</Heading>
			{discord ? (
				<>
					<FormControl display="flex" alignItems="center">
						<FormLabel
							htmlFor="discord-visible"
							mb="0"
							fontWeight="normal"
							cursor="pointer"
							flex={1}
						>
							Show on profile
						</FormLabel>
						<Switch
							id="discord-visible"
							isChecked={discord.visible}
							onChange={() => {
								setVisible({
									variables: {
										provider: 'https://discord.com',
										visible: !discord.visible,
									},
									update: (cache) => {
										const medata =
											cache.readQuery<MyConnectionsQuery>(
												{
													query: MyConnectionsDocument,
												}
											);
										const data =
											cache.readQuery<MyConnectionsQuery>(
												{
													query: MyConnectionsDocument,
												}
											);
										const newConnections = (
											data?.me?.connections ?? []
										).map((c) => {
											const newC = { ...c };
											if (
												newC.provider ===
												'https://discord.com'
											) {
												newC.visible = !newC.visible;
											}
											return newC;
										});
										cache.writeQuery({
											query: MyConnectionsDocument,
											data: {
												me: {
													...medata?.me,
													connections: newConnections,
												},
											},
										});
									},
								});
							}}
						/>
					</FormControl>
					<NextLink
						href={{
							pathname: `${apiUrl}/api/login/federated/discord`,
							query: { next: '/account' },
						}}
						passHref
						legacyBehavior
					>
						<Link color="teal" textDecoration="underline">
							Update Discord username
						</Link>
					</NextLink>
				</>
			) : (
				<>
					<DiscordButton next="/account" />
				</>
			)}
		</Flex>
	);
};
