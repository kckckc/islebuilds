import { Box } from '@chakra-ui/react';
import { Formik, Form } from 'formik';
import router from 'next/router';
import React from 'react';
import { SearchInputField } from '../../molecules/SearchInputField';

interface NavSearchProps {}

export const NavSearch: React.FC<NavSearchProps> = ({}) => {
	return (
		<Box display={{ base: 'none', lg: 'block' }} maxW="75%" mx="auto" p={4}>
			<Formik
				initialValues={{
					name: '',
				}}
				onSubmit={(values, { setSubmitting, setFieldValue }) => {
					router.push(`/character/${values.name}`);
					setSubmitting(false);
					setFieldValue('name', '');
				}}
			>
				{() => (
					<Form>
						<SearchInputField
							name="name"
							placeholder="Character"
							smallIcon
						/>
					</Form>
				)}
			</Formik>
		</Box>
	);
};
