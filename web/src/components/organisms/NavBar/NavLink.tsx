import React, { ReactNode } from 'react';
import NextLink from 'next/link';
import { Link, useColorModeValue } from '@chakra-ui/react';

interface NavLinkProps {
	children?: ReactNode;
	href: string;
}

export const NavLink: React.FC<NavLinkProps> = ({ children, href }) => {
	return (
		<NextLink href={href} passHref legacyBehavior>
			<Link
				px={2}
				py={1}
				rounded={'md'}
				_hover={{
					textDecoration: 'none',
					bg: useColorModeValue('gray.200', 'gray.700'),
				}}
				href={'#'}
			>
				{children}
			</Link>
		</NextLink>
	);
};
