import { CloseIcon, HamburgerIcon } from '@chakra-ui/icons';
import {
	Box,
	Button,
	Divider,
	Flex,
	Heading,
	HStack,
	IconButton,
	Link,
	Menu,
	MenuButton,
	MenuDivider,
	MenuItem,
	MenuList,
	Stack,
	useColorModeValue,
	useDisclosure,
} from '@chakra-ui/react';
import Image from 'next/legacy/image';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { useLogOutMutation, useMeQuery } from '../../../generated/graphql';
import { NavButtons } from './NavButtons';
import { NavLink } from './NavLink';
import { NavSearch } from './NavSearch';

interface NavBarProps {
	noSearch?: boolean;
}

const Links = [
	{
		name: 'Leaderboards',
		href: '/leaderboard',
	},
	{
		name: 'Inventory',
		href: '/inventory',
	},
	{
		name: 'Timers',
		href: '/timers',
	},
	{
		name: 'Tools',
		href: '/tools',
	},
	{
		name: 'Waddon',
		href: '/waddon',
	},
];

export const NavBar: React.FC<NavBarProps> = ({ noSearch = false }) => {
	const { isOpen, onOpen, onClose } = useDisclosure();

	const router = useRouter();

	const [logOut] = useLogOutMutation({
		update(cache) {
			cache.modify({
				fields: {
					me() {
						return null;
					},
				},
			});
			router.reload();
		},
	});
	const { data, loading } = useMeQuery();

	let rightContent = null;

	if (loading) {
		// loading
	} else if (!data?.me) {
		// not logged in
		rightContent = (
			<NextLink href="/login" passHref legacyBehavior>
				<Link mr={4}>log in</Link>
			</NextLink>
		);
	} else {
		// logged in
		rightContent = (
			<Menu>
				<MenuButton
					as={Button}
					variant={'link'}
					cursor={'pointer'}
					minW={0}
				>
					{data.me.displayName}
				</MenuButton>
				<MenuList>
					<NextLink href="/profile" passHref legacyBehavior>
						<MenuItem as="a" cursor="pointer">
							Profile
						</MenuItem>
					</NextLink>
					<NextLink href="/account" passHref legacyBehavior>
						<MenuItem as="a" cursor="pointer">
							Account settings
						</MenuItem>
					</NextLink>
					<NextLink href="/account/keys" passHref legacyBehavior>
						<MenuItem as="a" cursor="pointer">
							Manage API keys
						</MenuItem>
					</NextLink>
					<MenuDivider />
					<MenuItem onClick={() => logOut()} cursor="pointer">
						Log out
					</MenuItem>
				</MenuList>
			</Menu>
		);
	}

	return (
		<Box
			bg={useColorModeValue('gray.100', 'gray.900')}
			px={{ base: 4, md: 8 }}
			shadow="md"
			borderBottomWidth="1px"
		>
			<Flex h={16} alignItems={'center'}>
				<IconButton
					size={'md'}
					icon={isOpen ? <CloseIcon /> : <HamburgerIcon />}
					aria-label={'Open Menu'}
					display={{ md: 'none' }}
					mr={4}
					onClick={isOpen ? onClose : onOpen}
				/>

				<HStack spacing={8} alignItems={'center'}>
					<Flex flexDirection="row" alignItems="center">
						<Image
							src="/icon.png"
							alt="Logo"
							width={32}
							height={32}
							priority
						/>
						<Heading size="md" ml={2}>
							<NextLink href="/" passHref legacyBehavior>
								<Link as="a" color="teal.500">
									Islebuilds
								</Link>
							</NextLink>
						</Heading>
					</Flex>
					<HStack
						as={'nav'}
						spacing={4}
						display={{ base: 'none', md: 'flex' }}
					>
						{Links.map((link) => (
							<NavLink key={link.href} href={link.href}>
								{link.name}
							</NavLink>
						))}
					</HStack>
				</HStack>

				<Box flex={1} />

				{!noSearch && <NavSearch />}

				<Flex
					alignItems={'center'}
					columnGap={2}
					display={{ base: 'none', md: 'flex' }}
				>
					<NavButtons />
					{rightContent}
				</Flex>
				<Flex
					alignItems="center"
					display={{ base: 'flex', md: 'none' }}
				>
					{rightContent}
				</Flex>
			</Flex>

			{isOpen ? (
				<Box pb={4} display={{ md: 'none' }}>
					<Stack as={'nav'} spacing={4}>
						{Links.map((link) => (
							<NavLink key={link.href} href={link.href}>
								{link.name}
							</NavLink>
						))}
						<Divider />
						<Flex direction="row" justifyContent="space-around">
							<NavButtons />
						</Flex>
					</Stack>
				</Box>
			) : null}
		</Box>
	);
};
