import React from 'react';
import NextLink from 'next/link';
import { SettingsIcon } from '@chakra-ui/icons';
import { IconButton } from '@chakra-ui/react';
import { ColorModeButton } from '../../molecules/ColorModeButton';
import { HelpButton } from '../../molecules/HelpButton';

interface NavButtonsProps {}

export const NavButtons: React.FC<NavButtonsProps> = ({}) => {
	return (
		<>
			<NextLink href="/settings" legacyBehavior>
				<IconButton
					aria-label={'Go to Settings'}
					icon={<SettingsIcon />}
				/>
			</NextLink>
			<ColorModeButton />
			<HelpButton />
		</>
	);
};
