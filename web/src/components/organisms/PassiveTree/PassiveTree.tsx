import { As, Box, Flex } from '@chakra-ui/react';
import { translateStatName, translateStatValue } from '@kckckc/isleward-util';
import useMouse from '@react-hook/mouse-position';
import NextLink from 'next/link';
import React, {
	ReactNode,
	useCallback,
	useEffect,
	useRef,
	useState,
} from 'react';
import { SimpleTooltip } from '../../atoms/SimpleTooltip';
import styles from './PassiveTree.module.css';
import { passiveTreeData } from './passiveTreeData';

const { nodes } = passiveTreeData;
const links = passiveTreeData.links.map((l) => ({
	from: {
		id: l.from,
	},
	to: {
		id: l.to,
	},
}));

interface TooltipState {
	left: number;
	top: number;
	active: boolean;
	text: ReactNode;
}

interface PassiveTreeProps {
	selected: number[];
	drawBackground?: boolean;
	divBackground?: boolean;
	crop?: boolean;
	link?: boolean;
}

export const PassiveTree: React.FC<PassiveTreeProps> = ({
	selected,
	drawBackground = true,
	divBackground = true,
	crop = true,
	link = true,
}) => {
	const canvasRef = useRef<HTMLCanvasElement>(null);

	const [size, setSize] = useState({
		w: 0,
		h: 0,
	});

	// Calc width
	let forceNoCrop = false;
	if (selected.length < 2) {
		forceNoCrop = true;
	}
	const firstSelectedId = selected[0];
	const firstNode = nodes.find((n) => n.id === firstSelectedId) ?? nodes[0];
	let minX = firstNode.pos.x;
	let minY = firstNode.pos.y;
	let maxX = firstNode.pos.x;
	let maxY = firstNode.pos.y;

	// Crop to selected nodes
	for (let node of nodes) {
		if (!forceNoCrop) {
			// If cropping is enabled and this node is not selected, don't check its position
			if (crop && !selected.includes(node.id)) continue;
		}

		if (node.pos.x < minX) {
			minX = node.pos.x;
		}
		if (node.pos.x > maxX) {
			maxX = node.pos.x;
		}
		if (node.pos.y < minY) {
			minY = node.pos.y;
		}
		if (node.pos.y > maxY) {
			maxY = node.pos.y;
		}
	}

	const lineWidth = 5;
	const blockSize = 20;
	const gridSize = 30;

	// const addX = -1 * minX * gridSize;
	// const addY = -1 * minY * gridSize;
	const addX = -1 * minX * gridSize + 1 * gridSize;
	const addY = -1 * minY * gridSize + 1 * gridSize;
	const showW = (maxX - minX + 3) * gridSize;
	const showH = (maxY - minY + 3) * gridSize;

	const onResize = useCallback(() => {
		if (canvasRef?.current) {
			canvasRef.current.width = showW;
			canvasRef.current.height = showH;

			setSize({
				w: canvasRef.current.width,
				h: canvasRef.current.height,
			});
		}
	}, [showW, showH]);

	const mouse = useMouse(canvasRef);

	const [tooltip, setTooltip] = useState<TooltipState>({
		top: 0,
		left: 0,
		active: false,
		text: null,
	});

	// Process mouse moves
	useEffect(() => {
		const cell = { x: 0, y: 0 };
		if (!canvasRef.current || !mouse?.clientX || !mouse?.clientY) {
			return;
		}

		// TODO: Logic for hovering near small nodes / over edges of large nodes
		const rect = canvasRef.current.getBoundingClientRect();
		cell.x = ~~(
			(((mouse.clientX - rect.left) / (rect.right - rect.left)) *
				canvasRef.current.width -
				addX) /
			gridSize
		);
		cell.y = ~~(
			(((mouse.clientY - rect.top) / (rect.bottom - rect.top)) *
				canvasRef.current.height -
				addY) /
			gridSize
		);

		const node = nodes.find(
			(n) => n.pos.x === cell.x && n.pos.y === cell.y
		);

		if (node) {
			let text: ReactNode = Object.keys(node.stats).map((s, idx) => {
				const statName = translateStatName(s);
				const statValue = translateStatValue(
					s,
					node.stats[s as keyof typeof node.stats]
				);

				const negative = (statValue + '')[0] === '-';

				return (
					<span key={`${node.id}-${idx}`}>
						{negative ? '' : '+'}
						{statValue} {statName}
					</span>
				);
			});

			if (node.spiritStart) {
				text = <>Starting node for {node.spiritStart} spirits</>;
			}

			setTooltip({
				left: mouse.clientX + 15,
				top: mouse.clientY,
				active: true,
				text,
			});
		} else {
			setTooltip({
				left: 0,
				top: 0,
				active: false,
				text: '',
			});
		}
	}, [mouse, addX, addY]);

	// Draw
	useEffect(() => {
		const canvas = canvasRef.current;

		if (!canvas) return;

		const drawClear = (ctx: CanvasRenderingContext2D) => {
			if (drawBackground) {
				ctx.fillStyle = '#373041';
				ctx.fillRect(0, 0, size.w, size.h);
			} else {
				ctx.clearRect(0, 0, size.w, size.h);
			}
		};

		const drawLine = (
			ctx: CanvasRenderingContext2D,
			fromId: number,
			toId: number,
			linked: boolean
		) => {
			const halfSize = blockSize / 2;

			const fromNode = nodes.find((n) => n.id === fromId);
			const toNode = nodes.find((n) => n.id === toId);

			if (!fromNode || !toNode) return;

			const fromX = fromNode?.pos.x * gridSize + halfSize + addX;
			const fromY = fromNode?.pos.y * gridSize + halfSize + addY;
			const toX = toNode?.pos.x * gridSize + halfSize + addX;
			const toY = toNode?.pos.y * gridSize + halfSize + addY;

			if (
				!linked &&
				!selected.includes(fromId) &&
				!selected.includes(toId)
			) {
				ctx.globalAlpha = 0.25;
			}

			ctx.strokeStyle = linked ? '#fafcfc' : '#69696e';
			ctx.beginPath();
			ctx.moveTo(fromX, fromY);
			ctx.lineTo(toX, toY);
			ctx.closePath();
			ctx.stroke();

			if (
				!linked &&
				!selected.includes(fromId) &&
				!selected.includes(toId)
			) {
				ctx.globalAlpha = 1;
			}
		};

		const drawNode = (ctx: CanvasRenderingContext2D, nodeId: number) => {
			const node = nodes.find((n) => n.id === nodeId);
			if (!node) return;

			let color = node.color >= 0 ? node.color + 1 : -1;

			if (node.spiritStart) {
				color = 8;
				node.size = 1;
			}

			ctx.fillStyle = [
				'#69696e',
				'#c0c3cf',
				'#3fa7dd',
				'#4ac441',
				'#d43346',
				'#a24eff',
				'#faac45',
				'#44cb95',
				'#fafcfc',
			][color];
			const size = [blockSize, blockSize * 2, blockSize * 3][node.size];
			const x = node.pos.x * gridSize - (size - blockSize) / 2 + addX;
			const y = node.pos.y * gridSize - (size - blockSize) / 2 + addY;

			const linked = links.some((l) => {
				if (l.from.id !== nodeId && l.to.id !== node.id) {
					return false;
				}

				return nodes.some(
					(n) =>
						(n.id === l.from.id && selected.includes(n.id)) ||
						(n.id === l.to.id && selected.includes(n.id))
				);
			});

			if (!linked) {
				ctx.globalAlpha = 0.25;
			}

			ctx.fillRect(x, y, size, size);

			if (linked) {
				ctx.strokeStyle = [
					'#69696e',
					'#69696e',
					'#42548d',
					'#386646',
					'#763b3b',
					'#533399',
					'#d07840',
					'#3f8d6d',
					'#fafcfc',
				][color];
				ctx.strokeRect(x, y, size, size);

				if (selected.includes(node.id)) {
					ctx.strokeStyle = '#fafcfc';
					ctx.strokeRect(x, y, size, size);
				}
			}

			if (!linked) {
				ctx.globalAlpha = 1;
			}
		};

		const renderNodes = (ctx: CanvasRenderingContext2D) => {
			drawClear(ctx);

			links.forEach((l) => {
				const linked =
					selected.includes(
						nodes.find((n) => n.id === l.from.id)?.id ?? -1
					) &&
					selected.includes(
						nodes.find((n) => n.id === l.to.id)?.id ?? -1
					);
				drawLine(ctx, l.from.id, l.to.id, linked);
			});

			nodes.forEach((n) => {
				drawNode(ctx, n.id);
			});
		};

		const ctx = canvas.getContext('2d');
		if (!ctx) return;
		ctx.lineWidth = lineWidth;
		renderNodes(ctx);
	}, [drawBackground, size, selected, addX, addY]);

	// Initial resize
	useEffect(() => {
		const canvas = canvasRef.current;
		if (canvas) {
			onResize();
		}
	}, [onResize]);

	// Resize listener
	useEffect(() => {
		const resizeHandler = onResize;

		window.addEventListener('resize', resizeHandler);

		return () => {
			window.removeEventListener('resize', resizeHandler);
		};
	});

	const el = (
		<Box position="relative" borderWidth="1px" rounded="xl">
			<canvas
				ref={canvasRef}
				className={styles.canvas}
				style={{
					backgroundColor: divBackground ? '#373041' : undefined,
					width: '100%',
					height: '100%',
					borderRadius: 'var(--chakra-radii-xl)',
				}}
			/>
		</Box>
	);

	return (
		<>
			{tooltip.active && (
				<SimpleTooltip left={tooltip.left} top={tooltip.top}>
					<Flex flexDirection="column">{tooltip.text}</Flex>
				</SimpleTooltip>
			)}

			{link ? (
				<NextLink
					href={`/passive-trees/${selected.join(',')}`}
					passHref
					legacyBehavior
				>
					<Box as="a" cursor="pointer">
						{el}
					</Box>
				</NextLink>
			) : (
				el
			)}
		</>
	);
};
