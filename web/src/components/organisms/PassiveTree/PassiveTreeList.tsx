import { Box, Button, Center, Divider, Flex, Text } from '@chakra-ui/react';
import React, { ReactNode } from 'react';
import { PaginatedPassiveTrees } from '../../../generated/graphql';
import { humanize } from '../../../util/humanize';
import { PassiveTree } from './PassiveTree';
import NextLink from 'next/link';

interface PassiveTreeListProps {
	spirit?: string;
	data: PaginatedPassiveTrees;
	fetchMore: () => void;
}

export const PassiveTreeList: React.FC<PassiveTreeListProps> = ({
	spirit,
	data,
	fetchMore,
}) => {
	const { lastUpdated, hasMore, passiveTrees } = data;

	const passiveElements: ReactNode[] | undefined = passiveTrees?.map(
		({ passives, count }, i): ReactNode => {
			return (
				<Flex key={i} flexDir="column" gap={2}>
					<PassiveTree selected={passives} />
					<Text textAlign="center">
						Used by {count} character{count === 1 ? '' : 's'}
					</Text>
				</Flex>
			);
		}
	);

	let body: ReactNode;
	if (!passiveElements || !passiveElements.length) {
		body = <Center>No data found</Center>;
	} else {
		body = passiveElements.reduce((a, b, i) => [
			a,
			<Divider key={Math.random()} my={8} />,
			b,
		]);
	}

	return (
		<>
			{lastUpdated && (
				<Box mb={4} suppressHydrationWarning={true}>
					Updated {humanize(lastUpdated)}
				</Box>
			)}

			{body}

			{hasMore && (
				<Center mt={8}>
					<Button onClick={fetchMore}>load more</Button>
				</Center>
			)}
		</>
	);
};
