import { Box, Center } from '@chakra-ui/react';
import {
	calcPoints,
	translateStatName,
	translateStatValue,
} from '@kckckc/isleward-util';
import React, { ReactNode } from 'react';
import { Section } from '../../templates/Section';
import { TwoColumnLayout } from '../../templates/TwoColumnLayout';
import { passiveTreeData } from './passiveTreeData';

interface PassiveStatsListProps {
	level: number;
	selected: number[];
}

export const PassiveStatsList: React.FC<PassiveStatsListProps> = ({
	level,
	selected,
}) => {
	const pointsRem = calcPoints(level, selected);

	const elements = selected
		.map((id) => {
			const node = passiveTreeData.nodes.find((n) => n.id === id);

			if (node) {
				if (node.spiritStart) {
					return null;
				}

				let text: ReactNode[] = Object.keys(node.stats).map((s, i) => {
					const statName = translateStatName(s);
					const statValue = translateStatValue(
						s,
						node.stats[s as keyof typeof node.stats]
					);

					const negative = (statValue + '')[0] === '-';

					return (
						<span key={i}>
							{negative ? '' : '+'}
							{statValue} {statName}
						</span>
					);
				});

				return (
					<Box key={node.id}>
						{text.reduce((a, b) => [a, ', ', b])}
					</Box>
				);
			}
		})
		.filter((x) => !!x);

	const halfLen = Math.ceil(elements.length / 2);
	const leftHalf = elements.slice(0, halfLen);
	const rightHalf = elements.slice(halfLen);

	return (
		<>
			{pointsRem > 0 && (
				<Center mb={4}>Points Available: {pointsRem}</Center>
			)}
			<Section>
				<TwoColumnLayout left={leftHalf} right={rightHalf} />
			</Section>
		</>
	);
};
