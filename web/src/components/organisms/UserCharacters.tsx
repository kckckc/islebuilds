import { Box, Button, Flex } from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';
import {
	CharactersSortBy,
	usePreviewCharactersQuery,
} from '../../generated/graphql';
import { CenterLoading } from '../atoms/CenterLoading';
import { NothingText } from '../atoms/NothingText';
import { CharacterList } from '../molecules/CharacterList';

interface UserCharactersProps {
	userId: string;
	sortBy: CharactersSortBy;
	noMore?: boolean;
	seeMore?: boolean;
	limit?: number;
}

export const UserCharacters: React.FC<UserCharactersProps> = ({
	userId,
	sortBy,
	noMore = false,
	seeMore = false,
	limit = 10,
}) => {
	const { data, loading, error, fetchMore } = usePreviewCharactersQuery({
		variables: {
			input: {
				limit: limit,
				offset: 0,
				filter: {
					sortBy,
					userId,
				},
			},
		},
	});

	if (loading) {
		return <CenterLoading />;
	} else if (error) {
		return <Box>error</Box>;
	}

	if (!data?.characters) return null;

	return data.characters.characters.length ? (
		<Flex direction="column">
			<CharacterList
				data={
					noMore
						? data.characters.characters.slice(0, limit)
						: data.characters.characters
				}
				hasMore={data.characters.hasMore}
				onFetchMore={() => {
					fetchMore({
						variables: {
							input: {
								limit: limit,
								offset: data.characters.characters.length,
								filter: {
									sortBy,
									userId,
								},
							},
						},
					});
				}}
				showFetchMore={!noMore}
				loading={loading}
			/>
			{seeMore && (
				<NextLink
					href={'/user/' + userId + '/characters'}
					legacyBehavior
				>
					<Button as="a" mx="auto" px={16} mt={4} cursor="pointer">
						see all characters
					</Button>
				</NextLink>
			)}
		</Flex>
	) : (
		<NothingText>No characters yet</NothingText>
	);
};
