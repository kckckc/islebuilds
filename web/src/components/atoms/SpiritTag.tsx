import { Tag } from '@chakra-ui/react';
import React from 'react';
import { capitalize } from '../../util/capitalize';

interface SpiritTagProps {
	spirit: string;
}

const colorSchemeMap = {
	owl: 'blue',
	lynx: 'green',
	bear: 'red',
};

export const SpiritTag: React.FC<SpiritTagProps> = ({ spirit }) => {
	return (
		// Use TS assert, don't really care about typechecking here
		<Tag
			colorScheme={colorSchemeMap[spirit as keyof typeof colorSchemeMap]}
		>
			{capitalize(spirit)}
		</Tag>
	);
};
