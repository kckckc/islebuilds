import { Tag, Tooltip } from '@chakra-ui/react';
import React from 'react';
import { useMeQuery } from '../../generated/graphql';

interface SelfTagProps {
	userId: string;
}

export const SelfTag: React.FC<SelfTagProps> = ({ userId }) => {
	const { data: me } = useMeQuery();

	return me?.me?.id === userId ? (
		<Tooltip label="This character was uploaded by you">
			<Tag colorScheme="teal">You</Tag>
		</Tooltip>
	) : null;
};
