import { Flex, Heading } from '@chakra-ui/react';
import React from 'react';

interface WaddonHeroProps {}

export const WaddonHero: React.FC<WaddonHeroProps> = ({}) => {
	return (
		<Flex
			justifyContent="center"
			alignItems="center"
			// height="100vh"
			bgGradient="linear(to-l, heroGradientStart, heroGradientEnd)"
			bgClip="text"
		>
			<Heading fontSize="6vw">Waddon</Heading>
		</Flex>
	);
};
