import { Tag } from '@chakra-ui/react';
import React from 'react';
import { Span } from './Span';

interface DiscordTagProps {
	displayName: string;
	visible: boolean;
}

export const DiscordTag: React.FC<DiscordTagProps> = ({
	displayName,
	visible,
}) => {
	return (
		<Tag bg="#5865F2" textColor="whiteAlpha.900">
			<Span>
				<Span fontWeight={'normal'}>Discord:</Span>{' '}
				<Span>{displayName}</Span>
				{!visible && <Span fontWeight={'normal'}> (hidden)</Span>}
			</Span>
		</Tag>
	);
};
