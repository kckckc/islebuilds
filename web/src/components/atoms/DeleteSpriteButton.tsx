import {
	Button,
	IconButton,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Tooltip,
	useDisclosure,
} from '@chakra-ui/react';
import { Trash } from 'lucide-react';
import React from 'react';
import { useSetSpriteDeletedMutation } from '../../generated/graphql';

interface DeleteSpriteButtonProps {
	spriteId: string;
	isDeleted: boolean;
}

export const DeleteSpriteButton: React.FC<DeleteSpriteButtonProps> = ({
	spriteId,
	isDeleted,
}) => {
	const { isOpen, onOpen, onClose } = useDisclosure();

	const [setSpriteDeleted] = useSetSpriteDeletedMutation();

	const handleDelete = () => {
		onClose();

		setSpriteDeleted({
			variables: {
				sprite: spriteId,
				deleted: !isDeleted,
			},
			refetchQueries: ['SpriteDetails'],
		});
	};

	return (
		<>
			<Tooltip label={isDeleted ? 'Undo delete sprite' : 'Delete sprite'}>
				<IconButton
					aria-label={
						isDeleted
							? 'Undo delete sprite button'
							: 'Delete sprite button'
					}
					icon={<Trash size={20} />}
					colorScheme={isDeleted ? 'green' : 'red'}
					onClick={isDeleted ? handleDelete : onOpen}
				/>
			</Tooltip>

			<Modal
				isOpen={isOpen}
				onClose={onClose}
				preserveScrollBarGap
				isCentered
			>
				<ModalOverlay />
				<ModalContent>
					<ModalHeader>Are you sure?</ModalHeader>
					<ModalCloseButton />
					<ModalBody>Deleting a sprite is permanent.</ModalBody>

					<ModalFooter>
						<Button colorScheme="red" mr={2} onClick={handleDelete}>
							Delete
						</Button>
						<Button onClick={onClose}>Cancel</Button>
					</ModalFooter>
				</ModalContent>
			</Modal>
		</>
	);
};
