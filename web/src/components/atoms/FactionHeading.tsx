import { Flex } from '@chakra-ui/react';
import React from 'react';
import {
	SectionHeading,
	SectionHeadingProps,
} from '../templates/SectionHeading';
import { FactionIcon } from './FactionIcon';
import { FactionName } from './FactionName';

interface FactionHeadingProps extends SectionHeadingProps {
	factionId: string;
}

export const FactionHeading: React.FC<FactionHeadingProps> = ({
	factionId,
	...rest
}) => {
	return (
		<Flex flexDirection="row" alignItems="center" columnGap={2}>
			<FactionIcon
				href={`/leaderboard/faction/${factionId}`}
				factionId={factionId}
				small
			/>
			<SectionHeading
				{...rest}
				href={`/leaderboard/faction/${factionId}`}
			>
				<FactionName factionId={factionId} />
			</SectionHeading>
		</Flex>
	);
};
