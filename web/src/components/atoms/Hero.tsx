import { Flex, Heading } from '@chakra-ui/react';
import React from 'react';

interface HeroProps {}

export const Hero: React.FC<HeroProps> = ({}) => {
	return (
		<Flex
			justifyContent="center"
			alignItems="center"
			// height="100vh"
			bgGradient="linear(to-l, heroGradientStart, heroGradientEnd)"
			bgClip="text"
		>
			<Heading fontSize="6vw">islebuilds</Heading>
		</Flex>
	);
};
