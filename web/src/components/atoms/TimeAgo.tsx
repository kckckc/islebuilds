import { Tooltip, chakra } from '@chakra-ui/react';
import React from 'react';
import { humanizeAgo, toDateTime } from '../../util/humanize';

interface TimeAgoProps {
	date: Date;
}

export const TimeAgo: React.FC<TimeAgoProps> = ({ date }) => (
	<Tooltip label={toDateTime(date.getTime())}>
		<chakra.span
			textDecoration="dotted underline"
			textUnderlineOffset={1}
			suppressHydrationWarning={true}
		>
			{humanizeAgo(date.getTime())} ago
		</chakra.span>
	</Tooltip>
);
