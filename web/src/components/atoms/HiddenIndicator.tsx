import { ViewOffIcon } from '@chakra-ui/icons';
import { Tooltip } from '@chakra-ui/react';
import React from 'react';

interface HiddenIndicatorProps {}

export const HiddenIndicator: React.FC<HiddenIndicatorProps> = ({}) => {
	return (
		<Tooltip label="Only you can see this">
			<ViewOffIcon />
		</Tooltip>
	);
};
