import { Tag } from '@chakra-ui/react';
import React from 'react';

interface LevelTagProps {
	level: number;
}

export const LevelTag: React.FC<LevelTagProps> = ({ level }) => {
	return <Tag>Level {level}</Tag>;
};
