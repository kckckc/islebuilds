import {
	ArrowForwardIcon,
	ArrowRightIcon,
	ChevronRightIcon,
} from '@chakra-ui/icons';
import { Button } from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';
import { capitalize } from '../../util/capitalize';

interface SpiritLinkButtonProps {
	spirit: string;
	href: string;
}

const colorSchemeMap = {
	owl: 'blue',
	lynx: 'green',
	bear: 'red',
};

export const SpiritLinkButton: React.FC<SpiritLinkButtonProps> = ({
	spirit,
	href,
}) => {
	return (
		<NextLink href={href} passHref legacyBehavior>
			<Button
				as="a"
				colorScheme={
					colorSchemeMap[spirit as keyof typeof colorSchemeMap]
				}
				variant="solid"
				rightIcon={<ArrowForwardIcon />}
			>
				{capitalize(spirit)}
			</Button>
		</NextLink>
	);
};
