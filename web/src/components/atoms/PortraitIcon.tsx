import { Box, Tag } from '@chakra-ui/react';
import React from 'react';

interface PortraitIconProps {
	portraitX: number;
	portraitY: number;
}

export const PortraitIcon: React.FC<PortraitIconProps> = ({
	portraitX,
	portraitY,
}) => {
	const url = new URL(
		'/images/portraitIcons.png',
		'https://play.isleward.com/'
	).href;

	const px = portraitX * -32;
	const py = portraitY * -32;

	return (
		<Box
			width="32px"
			height="32px"
			margin={1}
			style={{
				background: `url("${url}") ${px}px ${py}px / 800% no-repeat scroll`,
			}}
		/>
	);
};
