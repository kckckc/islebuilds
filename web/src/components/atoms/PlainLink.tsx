import { Link } from '@chakra-ui/react';
import NextLink from 'next/link';
import React, { ReactNode } from 'react';

interface PlainLinkProps {
	href?: string;
	children?: ReactNode;
	external?: boolean;
	onClick?: React.MouseEventHandler<HTMLAnchorElement>;
	newTab?: boolean;
}

export const PlainLink: React.FC<PlainLinkProps> = ({
	href,
	children,
	external = false,
	onClick,
	newTab = false,
}) => {
	let inner = (
		<Link
			as="a"
			href={external ? href : undefined}
			color="blue.500"
			onClick={onClick}
			target={newTab ? '_blank' : undefined}
		>
			{children}
		</Link>
	);
	if (external || !href) {
		return inner;
	}

	return (
		<NextLink href={href} passHref legacyBehavior>
			{inner}
		</NextLink>
	);
};
