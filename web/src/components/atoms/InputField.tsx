import React from 'react';
import { useField, FieldHookConfig } from 'formik';
import {
	FormControl,
	FormErrorMessage,
	FormLabel,
	Input,
} from '@chakra-ui/react';

type InputFieldProps = FieldHookConfig<any> & {
	label: string;
	placeholder?: string;
	disabled?: boolean;
};

export const InputField: React.FC<InputFieldProps> = (props) => {
	const [field, { error }] = useField(props);

	return (
		<FormControl isInvalid={!!error}>
			<FormLabel htmlFor={field.name}>{props.label}</FormLabel>
			<Input
				{...field}
				id={field.name}
				placeholder={props.placeholder}
				disabled={props.disabled}
			/>
			{error ? <FormErrorMessage>{error}</FormErrorMessage> : null}
		</FormControl>
	);
};
