import { Box } from '@chakra-ui/react';
import { Portal } from '@kckckc/isleward-util';
import React, { ReactNode } from 'react';

interface SimpleTooltipProps {
	left: number;
	top: number;
	children?: ReactNode;
	maxWidth?: number;
}

export const SimpleTooltip: React.FC<SimpleTooltipProps> = ({
	left,
	top,
	children,
	maxWidth = 300,
}) => {
	return (
		<Portal>
			<Box
				position="fixed"
				backgroundColor="rgba(60,63,76,.95)"
				padding="8px"
				textAlign="center"
				color="whiteAlpha.900"
				borderWidth="1px"
				borderColor="whiteAlpha.300"
				style={{
					left: left + 'px',
					top: top + 'px',
					maxWidth: maxWidth + 'px',
				}}
			>
				{children}
			</Box>
		</Portal>
	);
};
