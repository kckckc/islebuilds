import { Link } from '@chakra-ui/react';
import React from 'react';
import NextLink from 'next/link';

interface UserLinkProps {
	id: string;
	displayName: string;
}

export const UserLink: React.FC<UserLinkProps> = ({ id, displayName }) => {
	return (
		<NextLink href={`/user/${id}`} passHref legacyBehavior>
			<Link color="teal.500">{displayName}</Link>
		</NextLink>
	);
};
