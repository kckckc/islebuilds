import { Center, Spinner } from '@chakra-ui/react';
import React from 'react';

interface CenterLoadingProps {}

export const CenterLoading: React.FC<CenterLoadingProps> = ({}) => {
	return (
		<Center>
			<Spinner />
		</Center>
	);
};
