import React from 'react';

interface FactionNameProps {
	factionId: string;
}

const headings = {
	gaekatla: 'Gaekatla',
	fjolgard: 'Fjolgard',
	akarei: 'The Akarei',
	peopleOfTheSun: 'The Sceibians',
	pumpkinSailor: 'The Pumpkin Sailor',
	theWinterMan: 'The Winter Man',
};

export const getFactionName = (factionId: string) => {
	return headings[factionId as keyof typeof headings] ?? factionId;
};

export const FactionName: React.FC<FactionNameProps> = ({ factionId }) => {
	return <>{getFactionName(factionId)}</>;
};
