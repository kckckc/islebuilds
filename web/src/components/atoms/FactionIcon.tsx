import { Box, Flex } from '@chakra-ui/react';
import { css } from '@emotion/react';
import Image from "next/legacy/image";
import React from 'react';
import { getFactionName } from './FactionName';
import NextLink from 'next/link';

import styles from './FactionIcon.module.css';

interface FactionIconProps {
	factionId: string;
	href?: string;
	small?: boolean;
}

export const FactionIcon: React.FC<FactionIconProps> = ({
	factionId,
	href,
	small = false,
}) => {
	const icon = (
		<Flex
			as={href ? 'a' : undefined}
			width={!small ? '64px' : '40px'}
			height={!small ? '64px' : '40px'}
			backgroundColor="#312136"
			alignItems="center"
			justifyContent="center"
			rounded={!small ? 'xl' : 'md'}
			borderWidth="1px"
		>
			<Image
				src={`/images/factions/${factionId}.png`}
				alt={`${getFactionName(factionId)} faction icon`}
				width={!small ? 48 : 32}
				height={!small ? 48 : 32}
				unoptimized={true}
				objectFit="contain"
				className={styles.pixelated}
			/>
		</Flex>
	);

	if (href) {
		return (
            <NextLink href={href} passHref legacyBehavior>
				{icon}
			</NextLink>
        );
	} else {
		return icon;
	}
};
