import { Box, Tooltip } from '@chakra-ui/react';
import React from 'react';
import { SpriteDisplay } from './SpriteDisplay';

const abilityIconSprites: Record<
	string,
	{ cell: number; spritesheet: string }
> = {
	Smite: {
		cell: 0,
		spritesheet: '/images/abilityIcons.png',
	},
	'Magic Missile': {
		cell: 1,
		spritesheet: '/images/abilityIcons.png',
	},
	Slash: {
		cell: 3,
		spritesheet: '/images/abilityIcons.png',
	},
	'Harvest Life': {
		cell: 4,
		spritesheet: '/images/abilityIcons.png',
	},
	Whirlwind: {
		cell: 5,
		spritesheet: '/images/abilityIcons.png',
	},
	Consecrate: {
		cell: 8,
		spritesheet: '/images/abilityIcons.png',
	},
	'Ice Spear': {
		cell: 9,
		spritesheet: '/images/abilityIcons.png',
	},
	Smokebomb: {
		cell: 10,
		spritesheet: '/images/abilityIcons.png',
	},
	Charge: {
		cell: 11,
		spritesheet: '/images/abilityIcons.png',
	},
	'Summon Skeleton': {
		cell: 12,
		spritesheet: '/images/abilityIcons.png',
	},
	Fireblast: {
		cell: 17,
		spritesheet: '/images/abilityIcons.png',
	},
	'Blood Barrier': {
		cell: 20,
		spritesheet: '/images/abilityIcons.png',
	},
	'Healing Touch': {
		cell: 24,
		spritesheet: '/images/abilityIcons.png',
	},
	Flurry: {
		cell: 26,
		spritesheet: '/images/abilityIcons.png',
	},
	Innervation: {
		cell: 27,
		spritesheet: '/images/abilityIcons.png',
	},
	Ambush: {
		cell: 34,
		spritesheet: '/images/abilityIcons.png',
	},
	Tranquility: {
		cell: 35,
		spritesheet: '/images/abilityIcons.png',
	},
	Swiftness: {
		cell: 43,
		spritesheet: '/images/abilityIcons.png',
	},
	'Crystal Spikes': {
		cell: 56,
		spritesheet: '/images/abilityIcons.png',
	},
	Roll: {
		cell: 0,
		spritesheet: '/server/mods/iwd-ranger/images/abilityIcons.png',
	},
	Taunt: {
		cell: 1,
		spritesheet: '/server/mods/iwd-gaekatla-temple/images/items.png',
	},
	'Barbed Chain': {
		cell: 2,
		spritesheet: '/server/mods/iwd-gaekatla-temple/images/items.png',
	},
};

interface AbilityIconDisplayProps {
	name: string;
	size?: 'sm' | 'md' | 'lg';
}

export const AbilityIconDisplay: React.FC<AbilityIconDisplayProps> = ({
	name,
	size = 'lg',
}) => {
	const nameShort = name.replace('Rune of ', name);

	const sizeIndex = ['sm', 'md', 'lg'].indexOf(size);

	return (
		<Tooltip label={nameShort}>
			<Box>
				<SpriteDisplay
					spritesheet={abilityIconSprites[nameShort]?.spritesheet}
					cell={abilityIconSprites[nameShort]?.cell}
					size={[32, 48, 64][sizeIndex]}
					margin={[4, 8, 12][sizeIndex]}
					rounded={['md', 'xl', 'xl'][sizeIndex]}
				/>
			</Box>
		</Tooltip>
	);
};
