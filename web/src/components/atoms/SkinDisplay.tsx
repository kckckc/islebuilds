import { Box, Tooltip } from '@chakra-ui/react';
import React from 'react';
import { skinNames } from '../../util/skinNames';
import { SpriteDisplay } from './SpriteDisplay';

interface SkinDisplayProps {
	skinSheetName?: string | null;
	skinCell?: number | null;
	skinId?: string;
	size?: 'sm' | 'md' | 'lg';
}

export const SkinDisplay: React.FC<SkinDisplayProps> = ({
	skinSheetName,
	skinCell,
	skinId,
	size = 'lg',
}) => {
	const sizeIndex = ['sm', 'md', 'lg'].indexOf(size);

	const el = (
		<SpriteDisplay
			spritesheet={skinSheetName ?? undefined}
			cell={skinCell ?? undefined}
			size={[32, 48, 64][sizeIndex]}
			margin={[4, 8, 12][sizeIndex]}
			rounded={['md', 'xl', 'xl'][sizeIndex]}
		/>
	);

	if (skinId) {
		return (
			<Tooltip label={'Skin: ' + skinNames[skinId] || skinId}>
				<Box>{el}</Box>
			</Tooltip>
		);
	} else {
		return el;
	}
};
