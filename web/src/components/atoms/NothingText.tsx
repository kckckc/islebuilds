import { chakra, useColorModeValue } from '@chakra-ui/react';
import React, { ReactNode } from 'react';

interface NothingTextProps {
	children?: ReactNode;
}

export const NothingText: React.FC<NothingTextProps> = ({ children }) => {
	return (
		<chakra.em textColor={useColorModeValue('gray.500', 'gray.400')}>
			({children})
		</chakra.em>
	);
};
