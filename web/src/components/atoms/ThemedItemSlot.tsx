import { Box, Flex, FlexProps, useColorMode } from '@chakra-ui/react';
import { SlotType, ItemSlot } from '@kckckc/isleward-util';
import { IWDItem } from '@kckckc/isleward-util/isleward';
import React from 'react';
import { useQualityIndicators } from '../../hooks/settingsHooks';
import theme from '../../theme';

interface ThemedItemSlotProps {
	item?: IWDItem;
	itemJson?: string | null;
	slot?: SlotType;
	darken?: boolean;
	size?: 'sm' | 'md';
}

export const ThemedItemSlot: React.FC<ThemedItemSlotProps> = ({
	item,
	itemJson,
	slot,
	darken = false,
	size = 'md',
}) => {
	const { colorMode } = useColorMode();

	let [borderType] = useQualityIndicators();

	const parsedItem = item ?? JSON.parse(itemJson ?? '{}');

	const parentProps: FlexProps = {};
	const overlayProps: FlexProps = {
		zIndex: 5,
	};

	const qualityColor =
		typeof parsedItem.quality === 'number'
			? theme.semanticTokens.colors[`q${parsedItem.quality}`]
			: undefined;

	// Undefined or 0 (common) should not be styled
	if (!parsedItem.quality) {
		borderType = 'off';
	}

	if (borderType === 'border') {
		overlayProps.borderWidth = { sm: '2px', md: '4px' }[size];
		overlayProps.borderColor = qualityColor;
	} else if (borderType === 'bottom') {
		parentProps.borderWidth = '1px';
		overlayProps.sx = {
			backgroundColor: `${qualityColor}`,
			top: { sm: '56px', md: '78px' }[size],
			roundedTop: 'none',
		};
	} else if (borderType === 'background') {
		overlayProps.backgroundColor = qualityColor;
		overlayProps.zIndex = 1;
	} else {
		// Including 'off'
		parentProps.borderWidth = '1px';
	}

	return (
		<Flex
			position="relative"
			alignItems="center"
			justifyContent="center"
			width={{ sm: '64px', md: '88px' }[size]}
			height={{ sm: '64px', md: '88px' }[size]}
			rounded="xl"
			{...parentProps}
			boxSizing="border-box"
			backgroundColor={colorMode === 'light' ? '#312136' : undefined}
			zIndex={0}
			filter={darken ? 'brightness(50%)' : 'none'}
			transition="0.2s filter ease-out"
		>
			<Box
				position="absolute"
				left={0}
				right={0}
				top={0}
				bottom={0}
				rounded="xl"
				pointerEvents="none"
				{...overlayProps}
			/>

			<Box
				height="80px"
				zIndex={3}
				transform={{ sm: 'scale(0.75)', md: '' }[size]}
			>
				<ItemSlot
					item={item || itemJson ? parsedItem : undefined}
					slot={slot}
					noBackground={true}
					dropShadow={borderType === 'background'}
				/>
			</Box>
		</Flex>
	);
};
