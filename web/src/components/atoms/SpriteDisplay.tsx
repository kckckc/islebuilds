import { Box, Flex, FlexProps } from '@chakra-ui/react';
import { css } from '@emotion/react';
import React from 'react';

interface SpriteDisplayProps {
	spritesheet?: string;
	cell?: number;

	size: number;
	margin: number;
	rounded: FlexProps['rounded'];
}

export const SpriteDisplay: React.FC<SpriteDisplayProps> = ({
	spritesheet,
	cell,
	size,
	margin,
	rounded,
}) => {
	let bg = 'none';

	if (typeof spritesheet === 'string' && typeof cell === 'number') {
		// TODO: might be a problem to hardcode 8 wide
		let spriteY = ~~(cell / 8);
		let spriteX = cell - spriteY * 8;

		spriteX = spriteX * size * -1;
		spriteY = spriteY * size * -1;

		// TODO: ?
		if (spritesheet === 'characters') {
			spritesheet = '/images/characters.png';
		} else if (spritesheet === 'objects') {
			spritesheet = '/images/objects.png';
		}

		const url = new URL(spritesheet, 'https://play.isleward.com').href;
		bg = `url("${url}") ${spriteX}px ${spriteY}px / 800% no-repeat scroll`;
	}

	return (
		<Flex
			position="relative"
			alignItems="center"
			width={size + margin * 2 + 'px'}
			height={size + margin * 2 + 'px'}
			borderWidth="1px"
			rounded={rounded}
			backgroundColor="#312136"
		>
			<Box
				css={css`
					image-rendering: pixelated;
					image-rendering: optimizeSpeed;
					image-rendering: crisp-edges;
				`}
				style={{
					position: 'absolute',
					width: size + 'px',
					height: size + 'px',
					marginLeft: 'auto',
					marginRight: 'auto',
					left: margin + 'px',
					top: margin + 'px',
					background: bg,
				}}
			/>
		</Flex>
	);
};
