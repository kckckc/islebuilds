import React, { ReactNode } from 'react';
import { chakra, TypographyProps } from '@chakra-ui/react';

interface WeightProps {
	weight?: TypographyProps['fontWeight'];
	children?: ReactNode;
}

export const Weight: React.FC<WeightProps> = ({
	weight = 'bold',
	children,
}) => {
	return <chakra.span fontWeight={weight}>{children}</chakra.span>;
};
