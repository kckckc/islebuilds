import React, { useContext } from 'react';
import { useMeQuery } from '../../../generated/graphql';
import { ReportCharacterDisclosure } from '../../organisms/ReportModal/WithReportCharacterModal';
import { ReportButton } from './ReportButton';

interface ReportCharacterButtonProps {}

export const ReportCharacterButton: React.FC<
	ReportCharacterButtonProps
> = ({}) => {
	const { disclosure, submitted } = useContext(ReportCharacterDisclosure);

	// Must be logged in
	const { data, loading } = useMeQuery();
	if (!loading && !data?.me) return null;

	return (
		<ReportButton
			subject="character"
			callback={disclosure.onOpen}
			submitted={submitted}
		/>
	);
};
