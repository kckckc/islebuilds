import { IconButton, Tooltip } from '@chakra-ui/react';
import { Flag } from 'lucide-react';
import React from 'react';

interface ReportButtonProps {
	subject: string;
	callback: () => void;
	submitted: boolean;
}

export const ReportButton: React.FC<ReportButtonProps> = ({
	subject,
	callback,
	submitted,
}) => {
	const button = (
		<IconButton
			aria-label={`Report ${subject} button`}
			icon={<Flag size={20} />}
			onClick={callback}
			disabled={submitted}
		/>
	);

	if (!submitted) {
		return <Tooltip label={`Report ${subject}`}>{button}</Tooltip>;
	} else {
		return button;
	}
};
