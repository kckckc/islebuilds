import React, { useContext } from 'react';
import { useMeQuery } from '../../../generated/graphql';
import { ReportUserDisclosure } from '../../organisms/ReportModal/WithReportUserModal';
import { ReportButton } from './ReportButton';

interface ReportUserButtonProps {}

export const ReportUserButton: React.FC<ReportUserButtonProps> = ({}) => {
	const { disclosure, submitted } = useContext(ReportUserDisclosure);

	// Must be logged in
	const { data, loading } = useMeQuery();
	if (!loading && !data?.me) return null;

	return (
		<ReportButton
			subject="user"
			callback={disclosure.onOpen}
			submitted={submitted}
		/>
	);
};
