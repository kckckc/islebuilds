import React, { useContext } from 'react';
import { useMeQuery } from '../../../generated/graphql';
import { ReportSpriteDisclosure } from '../../organisms/ReportModal/WithReportSpriteModal';
import { ReportButton } from './ReportButton';

interface ReportSpriteButtonProps {}

export const ReportSpriteButton: React.FC<ReportSpriteButtonProps> = ({}) => {
	const { disclosure, submitted } = useContext(ReportSpriteDisclosure);

	// Must be logged in
	const { data, loading } = useMeQuery();
	if (!loading && !data?.me) return null;

	return (
		<ReportButton
			subject="sprite"
			callback={disclosure.onOpen}
			submitted={submitted}
		/>
	);
};
