import { Heading } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import React, { ReactNode } from 'react';

interface PageHeadingProps {
	children?: string;
	nowrap?: boolean;
	overwriteTitle?: string;
}

export const PageHeading: React.FC<PageHeadingProps> = ({
	children,
	nowrap,
	overwriteTitle,
}) => {
	return (
		<>
			<NextSeo title={overwriteTitle ?? children} />
			<Heading
				size="lg"
				mb={2}
				style={{ whiteSpace: nowrap ? 'nowrap' : undefined }}
			>
				{children}
			</Heading>
		</>
	);
};
