import React from 'react';
import { useCurrentTime } from '../../hooks/useCurrentTime';
import { hhmmUTC } from '../../util/humanize';

interface CurrentTimeProps {
	serverTime?: boolean;
}

export const CurrentTime: React.FC<CurrentTimeProps> = ({ serverTime }) => {
	const { timestamp } = useCurrentTime(1000);

	let fmt;
	let date = new Date(timestamp);

	if (serverTime) {
		fmt = hhmmUTC(date);
	} else {
		let ampm = 'AM';
		let hr = date.getHours();
		if (hr > 12) {
			hr -= 12;
			ampm = 'PM';
		}
		const min = ('' + date.getMinutes()).padStart(2, '0');

		fmt = `${hr}:${min} ${ampm}`;
	}

	return <>{fmt}</>;
};
