import { Tag, Tooltip } from '@chakra-ui/react';
import React from 'react';

interface LegacyPropheciesProps {
	prophecy_austere?: boolean;
	prophecy_butcher?: boolean;
	prophecy_hardcore?: boolean;
	prophecy_crushable?: boolean;
}

export const LegacyProphecies: React.FC<LegacyPropheciesProps> = ({
	prophecy_austere,
	prophecy_butcher,
	prophecy_hardcore,
	prophecy_crushable,
}) => {
	const prophecies = [
		prophecy_austere ? 'Austere' : null,
		prophecy_butcher ? 'Butcher' : null,
		prophecy_hardcore ? 'Hardcore' : null,
		prophecy_crushable ? 'Crushable' : null,
	].filter((s) => s !== null);

	if (!prophecies.length) {
		return null;
	}

	let label: string;
	if (prophecies.length === 1) {
		label = `Previously was: ${prophecies[0]}`;
	} else {
		label = `Previously was: ${prophecies.join(', ')}`;
	}

	return (
		<Tooltip label={label}>
			<Tag colorScheme="purple">Prophecies</Tag>
		</Tooltip>
	);
};
