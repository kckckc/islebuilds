import { Tag, Tooltip } from '@chakra-ui/react';
import React from 'react';
import { capitalize } from '../../util/capitalize';

interface LeagueTagProps {
	league: string;
}

const getColorScheme = (league: string) => {
	if (league === 'standard') {
		return 'orange';
	} else if (league === 'hardcore') {
		return 'red';
	} else {
		return 'pink';
	}
};

export const LeagueTag: React.FC<LeagueTagProps> = ({ league }) => {
	return (
		<Tooltip
			label={`This character is in the ${capitalize(league)} league`}
		>
			<Tag colorScheme={getColorScheme(league)}>{capitalize(league)}</Tag>
		</Tooltip>
	);
};
