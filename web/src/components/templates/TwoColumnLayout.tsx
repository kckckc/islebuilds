import { Flex, Grid, GridItem } from '@chakra-ui/react';
import React from 'react';

interface TwoColumnLayoutProps {
	left: React.ReactNode;
	right: React.ReactNode;
	rowGap?: number;
}

export const TwoColumnLayout: React.FC<TwoColumnLayoutProps> = ({
	left,
	right,
	rowGap = 2,
}) => {
	return (
		<Grid
			w="100%"
			templateColumns="repeat(auto-fill, minmax(250px, 1fr))"
			columnGap={6}
			rowGap={rowGap}
		>
			<GridItem display="flex">
				<Flex flexDirection="column" rowGap={2} flex={1}>
					{left}
				</Flex>
			</GridItem>
			<GridItem display="flex">
				<Flex flexDirection="column" rowGap={2} flex={1}>
					{right}
				</Flex>
			</GridItem>
		</Grid>
	);
};
