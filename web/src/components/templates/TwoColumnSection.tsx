import { Flex, Grid, GridItem } from '@chakra-ui/react';
import React from 'react';
import { Section } from './Section';
import { TwoColumnLayout } from './TwoColumnLayout';

interface TwoColumnSectionProps {
	left: React.ReactNode;
	right: React.ReactNode;
	rowGap?: number;
}

export const TwoColumnSection: React.FC<TwoColumnSectionProps> = ({
	left,
	right,
	rowGap,
}) => {
	return (
		<Section>
			<TwoColumnLayout left={left} right={right} rowGap={rowGap} />
		</Section>
	);
};
