import { Flex, forwardRef } from '@chakra-ui/react';
import React from 'react';

interface SectionProps {
	children?: React.ReactNode;
}

export const Section = React.forwardRef<HTMLDivElement, SectionProps>(
	({ children }, ref) => {
		return (
			<Flex ref={ref} p={5} shadow="md" borderWidth="1px" rounded="md">
				<Flex flex={1} flexDirection="column" rowGap={2}>
					{children}
				</Flex>
			</Flex>
		);
	}
);
Section.displayName = 'Section';
