import { Box } from '@chakra-ui/react';
import React, { ReactNode } from 'react';
import { Footer } from '../organisms/Footer';
import { NavBar } from '../organisms/NavBar';

interface DefaultLayoutProps {
	children?: ReactNode;
	noSearch?: boolean;
}

export const DefaultLayout: React.FC<DefaultLayoutProps> = ({
	children,
	noSearch = false,
}) => {
	return (
		<>
			<NavBar noSearch={noSearch} />
			<Box mt={8} mx="auto" maxW="800px" w="100%" px={4}>
				{children}
			</Box>
			<Footer />
		</>
	);
};
