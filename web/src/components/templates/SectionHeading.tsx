import { Heading, HeadingProps, Link } from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';

export interface SectionHeadingProps {
	href?: string;
	children?: React.ReactNode;
	noColor?: boolean;
}

export const SectionHeading: React.FC<SectionHeadingProps & HeadingProps> = ({
	href,
	children,
	noColor = false,
	...props
}) => {
	if (!props.size) {
		props.size = 'md';
	}

	return (
		<>
			<Heading {...props}>
				{href ? (
					<NextLink href={href} passHref legacyBehavior>
						<Link color={noColor ? undefined : 'teal.500'}>
							{children}
						</Link>
					</NextLink>
				) : (
					children
				)}
			</Heading>
		</>
	);
};
