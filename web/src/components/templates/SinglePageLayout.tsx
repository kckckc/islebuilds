import { Box } from '@chakra-ui/react';
import React, { ReactNode } from 'react';
import { Footer } from '../organisms/Footer';
import { NavBar } from '../organisms/NavBar';

interface SinglePageLayoutProps {
	children?: ReactNode;
	noSearch?: boolean;
}

export const SinglePageLayout: React.FC<SinglePageLayoutProps> = ({
	children,
	noSearch = false,
}) => {
	return (
		<>
			<NavBar noSearch={noSearch} />
			<Box mt={8} mx="auto" w="100%" px={8}>
				{children}
			</Box>
			<Footer />
		</>
	);
};
