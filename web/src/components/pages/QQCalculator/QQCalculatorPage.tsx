import { Box, Flex, Text } from '@chakra-ui/react';
import React from 'react';
import { PageHeading } from '../../atoms/PageHeading';
import { PlainLink } from '../../atoms/PlainLink';
import { BackLink } from '../../molecules/BackLink';
import { Section } from '../../templates/Section';
import { SectionHeading } from '../../templates/SectionHeading';
import { QQCalculator } from './QQCalculator';

interface QQCalculatorPageProps {}

export const QQCalculatorPage: React.FC<QQCalculatorPageProps> = ({}) => {
	return (
		<>
			<PageHeading>Item Quantity/Quality</PageHeading>
			<Box my={2}>
				<BackLink href="/tools">Tools</BackLink>
			</Box>

			<Flex direction="column" gap={4}>
				<Section>
					<Text mb={8}>
						Calculates expected item drops, accounting for player
						quality/quantity and mob magic find.
					</Text>

					<QQCalculator />
				</Section>

				<Section>
					<SectionHeading>QQ Explanation</SectionHeading>

					<Text>
						Each mob has a &lsquo;rolls&rsquo; property and
						&lsquo;chance&rsquo; property. Each &lsquo;roll&rsquo;
						has an individual &lsquo;chance&rsquo; at dropping an
						item.
					</Text>
					<Text>
						1 item quantity gives a 1% chance for another roll.
						Every 100 quantity guarantees another roll and any
						remaining quantity becomes a percentage chance to add
						another roll. For example, a mob with 2 rolls killed by
						a player with 135 item quantity will have at least 3
						rolls and a 35% chance for 4 rolls. The item quantity
						stat of the character who dealt the killing damage to an
						enemy is used. Item quantity is also hard capped at
						200%.
					</Text>
					<Text>
						Each roll has a chance to drop an item, which is the
						mob&apos;s chance property. This defaults to 35% but it
						can be changed per zone and per mob. Bosses usually have
						a 100% chance per roll. Once an item drop is earned from
						a roll, there are more chances that change the loot.
						There is a base 3.5% chance for a rune to drop, which is
						checked first. There is also a base 3.5% chance for a
						currency item to drop (idols), but only the kill&apos;s
						first roll is able to drop a currency item. Currency
						drops are also{' '}
						<PlainLink href="/tools/idol-calculators">
							affected by mob level and level difference, and
							weighted to different idols
						</PlainLink>
						. Items drop at the level of the mob killed. The slot
						that an item drops in is also{' '}
						<PlainLink href="/tools/slot-chances">
							randomly generated and weighted
						</PlainLink>
						.
					</Text>
					<Text>
						The rarity of the generated item is chosen from a pool
						of possible rarities. There are 2000 chances for a
						common item, 350 chances for a magic item, 80 for a rare
						item, 17 for an epic item, and 1 chance for a legendary
						item. Player item quality and per-mob magic find reduces
						the number of chances for a certain rarity to be picked.
					</Text>
					<Text>
						Each point of player item quality removes 7 commons from
						the rarity pool. Therefore, 286 quality removes 2002
						common items from the pool, leaving only magic and
						higher rarity items. Some mobs and zones have magic find
						(item quality) on common or magic items already, so
						sometimes less quality is needed to reach the effective
						cap. For bosses like Stinktooth and M&apos;ogresh, which
						already have -2000 chances at common items, item quality
						is not needed.
					</Text>
				</Section>
			</Flex>
		</>
	);
};
