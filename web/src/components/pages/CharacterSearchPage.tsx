import { Box, Center } from '@chakra-ui/react';
import React, { ReactNode, useState } from 'react';
import { useSearchCharactersQuery } from '../../generated/graphql';
import { PageHeading } from '../atoms/PageHeading';
import { CharacterList } from '../molecules/CharacterList';
import { BigSearch } from '../organisms/BigSearch';

interface CharacterSearchPageProps {}

export const CharacterSearchPage: React.FC<CharacterSearchPageProps> = ({}) => {
	const [search, setSearch] = useState<string | null>(null);
	const [cb, setCb] = useState<(() => void) | null>(null);

	const { data } = useSearchCharactersQuery({
		variables: {
			name: search ?? '',
		},
		skip: !search,
	});

	let el: ReactNode = null;
	if (data?.searchCharacters) {
		// Clear submitting state
		if (cb) {
			cb();
			setCb(null);
		}

		const chars = data.searchCharacters;

		el = (
			<>
				<Center mb={4}>
					{chars.length > 0 ? (
						<>
							Found {chars.length} character
							{chars.length === 1 ? '' : 's'}:
						</>
					) : (
						<>No characters found.</>
					)}
				</Center>

				<CharacterList data={chars} showFetchMore={false} />
			</>
		);
	}

	return (
		<>
			<PageHeading>Character Search</PageHeading>
			<BigSearch
				onSubmit={(newSearch) => {
					setSearch(newSearch);

					return new Promise((resolve) => {
						setCb(() => resolve);
					});
				}}
			/>

			{el}
		</>
	);
};
