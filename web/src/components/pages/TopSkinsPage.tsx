import { Box, Center, Divider, Flex, Select, Text } from '@chakra-ui/react';
import React, { useState } from 'react';
import { useTopSkinsQuery } from '../../generated/graphql';
import { skinNames } from '../../util/skinNames';
import { CenterLoading } from '../atoms/CenterLoading';
import { PageHeading } from '../atoms/PageHeading';
import { SkinDisplay } from '../atoms/SkinDisplay';
import { SectionHeading } from '../templates/SectionHeading';

interface TopSkinsPageProps {}

export const TopSkinsPage: React.FC<TopSkinsPageProps> = ({}) => {
	const [sortOrder, setSortOrder] = useState('desc');

	const { data, loading, error } = useTopSkinsQuery();

	if (loading) {
		return <CenterLoading />;
	} else if (error || !data?.topSkins) {
		return <Center>Error</Center>;
	}

	const { skins } = data.topSkins;

	const sorted: ((typeof skins)[0] & { trueCount: number })[] = [];

	// We did a group by + count on id, sheetname, and cell
	// Sometimes sheetname/cell don't match because of mounts
	// We need to total the counts by id and use the most common sprite
	skins.forEach((skin) => {
		const existing = sorted.find((x) => x.id === skin.id);
		if (existing) {
			// Add the count of this item to total
			existing.trueCount += skin.count;

			// If this item has more than the previous item,
			// replace the sheetname/cell with the more common version
			if (skin.count > existing.count) {
				existing.sheetName = skin.sheetName;
				existing.cell = skin.cell;
				existing.count = skin.count;
			}
		} else {
			// Add a new group
			sorted.push({
				...skin,
				trueCount: skin.count,
			});
		}
	});

	if (sortOrder === 'asc') {
		sorted.sort((a, b) => a.count - b.count);
	} else if (sortOrder === 'desc') {
		sorted.sort((a, b) => b.count - a.count);
	} else if (sortOrder === 'packs') {
		const keyList = Object.keys(skinNames);
		sorted.sort((a, b) => keyList.indexOf(a.id) - keyList.indexOf(b.id));
	} else if (sortOrder === 'alphabetized') {
		sorted.sort((a, b) => skinNames[a.id].localeCompare(skinNames[b.id]));
	}

	return (
		<>
			<PageHeading>Top Skins</PageHeading>

			<Flex mt={2} direction="row" gap={2} alignItems="center">
				<Box>Showing:</Box>
				<Box>
					<Select
						value={sortOrder}
						onChange={(e) => setSortOrder(e.target.value)}
					>
						<option value="desc">Most common</option>
						<option value="asc">Least common</option>
						<option value="packs">Packs</option>
						<option value="alphabetized">Alphabetical</option>
					</Select>
				</Box>
			</Flex>

			<Divider my={4} />

			<Flex flexDir="column" gap={2}>
				{sorted.map(({ id, sheetName, cell, count }) => (
					<Flex
						flex={1}
						key={id}
						p={2}
						shadow="md"
						borderWidth="1px"
						rounded="md"
					>
						<Box flex={1}>
							<Flex flexDirection={'row'} alignItems="center">
								<Box mr={2}>
									<SkinDisplay
										skinCell={cell}
										skinSheetName={sheetName}
										size="sm"
									/>
								</Box>
								<Box>
									<SectionHeading
										fontSize={{
											base: 'md',
											sm: 'lg',
											md: 'xl',
										}}
									>
										{skinNames[id] ?? id}
									</SectionHeading>
								</Box>
								<Box flex={1} />
								<Box>
									<Text
										fontSize="md"
										display={{
											base: 'none',
											md: 'block',
										}}
									>
										Used by {count} character
										{count === 1 ? '' : 's'}
									</Text>
									<Text
										fontSize="md"
										display={{
											base: 'block',
											md: 'none',
										}}
									>
										{count}x
									</Text>
								</Box>
							</Flex>
						</Box>
					</Flex>
				))}
			</Flex>
		</>
	);
};
