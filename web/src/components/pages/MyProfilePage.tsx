import { Box } from '@chakra-ui/react';
import React from 'react';
import { useMeQuery } from '../../generated/graphql';
import { CenterLoading } from '../atoms/CenterLoading';
import { UserProfile } from '../organisms/UserProfile';

interface MyProfilePageProps {}

export const MyProfilePage: React.FC<MyProfilePageProps> = ({}) => {
	const { data, loading, error } = useMeQuery();

	if (loading) {
		return <CenterLoading />;
	} else if (error || !data?.me) {
		return <Box>error</Box>;
	}

	return <UserProfile user={data.me} />;
};
