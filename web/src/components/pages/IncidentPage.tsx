import { Box } from '@chakra-ui/react';
import React from 'react';
import { useIncidentByIdQuery } from '../../generated/graphql';
import { PageHeading } from '../atoms/PageHeading';
import { BackLink } from '../molecules/BackLink';
import { IncidentCard } from './IncidentListPage/IncidentCard';

interface IncidentPageProps {
	incidentId: string;
}

export const IncidentPage: React.FC<IncidentPageProps> = ({ incidentId }) => {
	const { data, loading } = useIncidentByIdQuery({
		variables: {
			id: incidentId,
		},
	});

	return (
		<>
			<PageHeading>Incident</PageHeading>
			<Box my={2} mb={2}>
				<BackLink href="/incident">Incidents List</BackLink>
			</Box>

			{data?.incident ? (
				<IncidentCard incident={data?.incident} />
			) : loading ? (
				'Loading...'
			) : (
				'Not found'
			)}
		</>
	);
};
