import { Box } from '@chakra-ui/react';
import React from 'react';
import { PageHeading } from '../atoms/PageHeading';
import { BackLink } from '../molecules/BackLink';
import { RecentSpriteGallery } from '../organisms/RecentSpriteGallery';

interface SpriteGalleryPageProps {}

export const SpriteGalleryPage: React.FC<SpriteGalleryPageProps> = ({}) => {
	return (
		<>
			<PageHeading>Sprite Gallery</PageHeading>
			<Box mt={2} mb={6}>
				<BackLink href="/sprite-editor">Sprite Editor</BackLink>
			</Box>

			<RecentSpriteGallery />
		</>
	);
};
