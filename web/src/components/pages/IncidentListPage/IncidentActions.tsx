import { Box, Button, Flex, Heading } from '@chakra-ui/react';
import React from 'react';
import {
	SearchIncidentsQuery,
	useSetIncidentResolvedMutation,
} from '../../../generated/graphql';

interface IncidentActionsProps {
	incident: SearchIncidentsQuery['searchIncidents'][0];
}

export const IncidentActions: React.FC<IncidentActionsProps> = ({
	incident,
}) => {
	const [setIncidentResolved] = useSetIncidentResolvedMutation();

	const handleClick = () => {
		setIncidentResolved({
			variables: {
				incident: incident.id,
				resolved: !incident.resolved,
			},
			refetchQueries: ['SearchIncidents', 'IncidentById'],
		});
	};

	return (
		<Box>
			<Heading size="sm">Actions</Heading>
			<Flex mt={2} direction="row" gap={2}>
				{!incident.resolved ? (
					<Button colorScheme="teal" onClick={handleClick}>
						Mark resolved
					</Button>
				) : (
					<Button onClick={handleClick}>Undo mark resolved</Button>
				)}
			</Flex>
		</Box>
	);
};
