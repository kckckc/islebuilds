import { Box, Flex, IconButton, Switch } from '@chakra-ui/react';
import React, { useState } from 'react';
import { FiArrowLeft, FiArrowRight } from 'react-icons/fi';
import { PageHeading } from '../../atoms/PageHeading';
import { IncidentList } from './IncidentList';

interface IncidentListPageProps {}

export const IncidentListPage: React.FC<IncidentListPageProps> = ({}) => {
	const [showResolved, setShowResolved] = useState(false);
	const [showUnresolved, setShowUnresolved] = useState(true);
	const [page, setPage] = useState(0);

	return (
		<>
			<PageHeading>Incidents</PageHeading>

			<Flex gap={2} alignItems="center">
				<Switch
					isChecked={showResolved}
					onChange={() => setShowResolved((x) => !x)}
				/>
				<Box>Show resolved</Box>
			</Flex>
			<Flex gap={2} alignItems="center">
				<Switch
					isChecked={showUnresolved}
					onChange={() => setShowUnresolved((x) => !x)}
				/>
				<Box>Show unresolved</Box>
			</Flex>

			<Flex my={4} gap={2} alignItems="center">
				<IconButton
					aria-label="Previous page"
					onClick={() => {
						setPage((x) => x - 1);
					}}
					disabled={page <= 0}
					icon={<FiArrowLeft />}
				/>
				<Box>Page {page + 1}</Box>
				<IconButton
					aria-label="Next page"
					onClick={() => {
						setPage((x) => x + 1);
					}}
					icon={<FiArrowRight />}
				/>
			</Flex>

			<IncidentList
				showResolved={showResolved}
				showUnresolved={showUnresolved}
				page={page}
			/>
		</>
	);
};
