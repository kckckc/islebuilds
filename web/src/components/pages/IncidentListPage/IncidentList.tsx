import { Flex } from '@chakra-ui/react';
import React from 'react';
import { useSearchIncidentsQuery } from '../../../generated/graphql';
import { IncidentCard } from './IncidentCard';

interface IncidentListProps {
	showResolved: boolean;
	showUnresolved: boolean;
	page: number;
}

export const IncidentList: React.FC<IncidentListProps> = ({
	showResolved,
	showUnresolved,
	page,
}) => {
	const { data } = useSearchIncidentsQuery({
		variables: {
			filter: {
				showResolved,
				showUnresolved,
				page,
			},
		},
	});

	return (
		<Flex direction="column" gap={2}>
			{data?.searchIncidents?.map((incident) => (
				<IncidentCard key={incident.id} incident={incident} />
			))}
		</Flex>
	);
};
