import { Box, Flex, Heading } from '@chakra-ui/react';
import React from 'react';
import { SearchIncidentsQuery } from '../../../generated/graphql';
import { humanize } from '../../../util/humanize';
import { NothingText } from '../../atoms/NothingText';
import { PlainLink } from '../../atoms/PlainLink';
import { SpriteGalleryCard } from '../../molecules/SpriteGalleryCard';
import { SectionHeading } from '../../templates/SectionHeading';
import { IncidentActions } from './IncidentActions';

interface IncidentCardProps {
	incident: SearchIncidentsQuery['searchIncidents'][0];
}

export const IncidentCard: React.FC<IncidentCardProps> = ({ incident }) => {
	return (
		<Flex
			direction="column"
			gap={2}
			p={4}
			shadow="md"
			borderWidth="1px"
			rounded="md"
		>
			<SectionHeading size="md" href={`/incident/${incident.id}`}>
				{incident.title || '(No title)'}
			</SectionHeading>

			{incident.reportedUser && (
				<Box>
					Entity is owned by{' '}
					<PlainLink href={`/user/${incident.reportedUser?.id}`}>
						{incident.reportedUser.displayName}
					</PlainLink>
				</Box>
			)}

			{incident.reportedBy && (
				<Box>
					Report created by{' '}
					<PlainLink href={`/user/${incident.reportedBy?.id}`}>
						{incident.reportedBy.displayName}
					</PlainLink>
				</Box>
			)}

			<Box suppressHydrationWarning={true}>
				Reported {humanize(parseInt(incident.createdAt))}
			</Box>

			{incident.description && (
				<Box>
					<Heading size="sm">Report Description</Heading>
					<Box mt={2}>{incident.description}</Box>
				</Box>
			)}

			{incident.reportedProfile && (
				<Box>
					<Heading size="sm">Is Profile Report</Heading>
					<Box mt={2}>Yes</Box>
				</Box>
			)}

			{incident.reportedCharacter && (
				<Box>
					<Heading size="sm">Character</Heading>
					<Box mt={2}>
						<PlainLink
							href={`/character/${incident.reportedCharacter.name}`}
						>
							{incident.reportedCharacter.name}
						</PlainLink>
					</Box>
				</Box>
			)}

			{incident.reportedSprite && (
				<Box>
					<Heading size="sm">Sprite</Heading>
					<Flex mt={2} direction="row" justifyContent="space-between">
						<Flex ml={8} direction="column" gap={2}>
							<Box>
								<Heading size="sm">Uploaded</Heading>
								<Box suppressHydrationWarning={true}>
									{humanize(
										parseInt(
											incident.reportedSprite.createdAt
										)
									)}
								</Box>
							</Box>
							<Box>
								<Heading size="sm">Visible</Heading>
								{incident.reportedSprite.visible ? 'Yes' : 'No'}
							</Box>
							<Box>
								<Heading size="sm">Name</Heading>
								{incident.reportedSprite.name || (
									<NothingText>No name</NothingText>
								)}
							</Box>
							<Box>
								<Heading size="sm">Description</Heading>
								{incident.reportedSprite.description || (
									<NothingText>No description</NothingText>
								)}
							</Box>
						</Flex>

						<Box maxW="300px">
							<SpriteGalleryCard
								sprite={incident.reportedSprite}
								hideName
							/>
						</Box>
					</Flex>
				</Box>
			)}

			<IncidentActions incident={incident} />
		</Flex>
	);
};
