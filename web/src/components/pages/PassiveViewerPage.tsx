import { Box, useColorMode } from '@chakra-ui/react';
import React from 'react';
import { PageHeading } from '../atoms/PageHeading';
import { PassiveTree } from '../organisms/PassiveTree';
import { PassiveStatsList } from '../organisms/PassiveTree/PassiveStatsList';

interface PassiveViewerPageProps {
	selected: number[];
}

export const PassiveViewerPage: React.FC<PassiveViewerPageProps> = ({
	selected,
}) => {
	const { colorMode } = useColorMode();

	return (
		<>
			<PageHeading>Passive Tree Viewer</PageHeading>

			<PassiveTree
				selected={selected}
				divBackground={colorMode === 'light'}
				drawBackground={false}
				link={false}
			/>

			<Box mt={4}>
				<PassiveStatsList level={20} selected={selected} />
			</Box>
		</>
	);
};
