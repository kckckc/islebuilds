import React, { useEffect } from 'react';
import { useCharacterQuery } from '../../generated/graphql';
import { CenterLoading } from '../atoms/CenterLoading';
import { CharacterNotFound } from '../organisms/CharacterNotFound';
import { CharacterProfile } from '../organisms/CharacterProfile';

interface CharacterPageProps {
	name: string;
}

export const CharacterPage: React.FC<CharacterPageProps> = ({ name }) => {
	const { data, loading, error } = useCharacterQuery({
		variables: {
			name,
		},
	});

	// Correct capitalization after loading
	useEffect(() => {
		const parts = window.location.pathname.split('/');
		if (
			typeof window !== 'undefined' &&
			data?.character?.name &&
			parts[parts.length - 1] !== data.character.name
		) {
			window.history.replaceState(
				null,
				'',
				`/character/${data.character.name}`
			);
		}
	}, [data?.character?.name]);

	if (loading) {
		return <CenterLoading />;
	} else if (error || !data?.character) {
		return <CharacterNotFound />;
	}

	return <CharacterProfile character={data.character} />;
};
