import { Box } from '@chakra-ui/react';
import React from 'react';
import { usePassiveTreesQuery } from '../../generated/graphql';
import { CenterLoading } from '../atoms/CenterLoading';
import { PassiveTreeList } from '../organisms/PassiveTree/PassiveTreeList';

interface PassiveTreesPageProps {
	spirit?: string;
}

const LIMIT = 25;

export const PassiveTreesPage: React.FC<PassiveTreesPageProps> = ({
	spirit,
}) => {
	// Only fetch if valid
	const { data, loading, error, fetchMore } = usePassiveTreesQuery({
		variables: {
			input: {
				limit: LIMIT,
				offset: 0,
				spirit,
			},
		},
		skip: !!spirit && !['owl', 'bear', 'lynx'].includes(spirit),
	});

	if (loading) {
		return <CenterLoading />;
	} else if (error || !data?.passiveTrees) {
		return <Box>error</Box>;
	}

	const boundFetchMore = () => {
		if (!data?.passiveTrees?.passiveTrees) return;

		const newOffset = data.passiveTrees.passiveTrees.length;

		fetchMore({
			variables: {
				input: {
					limit: LIMIT,
					offset: newOffset,
					spirit,
				},
			},
		});
	};

	return (
		<PassiveTreeList
			spirit={spirit}
			data={data.passiveTrees}
			fetchMore={boundFetchMore}
		/>
	);
};
