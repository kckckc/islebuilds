import { Box, Heading, Text } from '@chakra-ui/react';
import React from 'react';
import { PageHeading } from '../atoms/PageHeading';
import { BackLink } from '../molecules/BackLink';
import { ChangeDisplayNameForm } from '../molecules/ChangeDisplayNameForm';
import { TwoColumnSection } from '../templates/TwoColumnSection';

interface EditProfilePageProps {}

export const EditProfilePage: React.FC<EditProfilePageProps> = ({}) => {
	return (
		<>
			<PageHeading>Edit Profile</PageHeading>
			<Box mt={2} mb={2}>
				<BackLink href="/profile">Profile</BackLink>
			</Box>

			<TwoColumnSection
				left={
					<>
						<Heading size="md">Visibility</Heading>
						<Text>
							Your profile details will be visible to others on
							all characters you upload.
						</Text>
					</>
				}
				right={<ChangeDisplayNameForm />}
			/>
		</>
	);
};
