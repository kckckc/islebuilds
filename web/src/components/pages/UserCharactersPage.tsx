import { Box, Heading } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import React from 'react';
import { CharactersSortBy, UserQuery } from '../../generated/graphql';
import { BackLink } from '../molecules/BackLink';
import { UserCharacters } from '../organisms/UserCharacters';
import { UserSpriteGallery } from '../organisms/UserSpriteGallery';

interface UserCharactersPageProps {
	user: Exclude<UserQuery['user'], null | undefined>;
}

export const UserCharactersPage: React.FC<UserCharactersPageProps> = ({
	user,
}) => {
	return (
		<>
			<NextSeo title={user.displayName + "'s Characters | User"} />
			<Heading size="lg">{user.displayName}&apos;s Characters</Heading>
			<Box mt={2}>
				<BackLink href={'/user/' + user.id}>
					{user.displayName}
				</BackLink>
			</Box>

			<Box mt={4}>
				<UserCharacters
					userId={user.id}
					sortBy={CharactersSortBy.Level}
				/>
			</Box>
		</>
	);
};
