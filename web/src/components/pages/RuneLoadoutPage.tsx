import { Box, Center, Flex } from '@chakra-ui/react';
import React from 'react';
import {
	CharactersSortBy,
	usePreviewCharactersQuery,
} from '../../generated/graphql';
import { runeOrder } from '../../util/runeOrder';
import { AbilityIconDisplay } from '../atoms/AbilityIconDisplay';
import { CenterLoading } from '../atoms/CenterLoading';
import { NothingText } from '../atoms/NothingText';
import { PageHeading } from '../atoms/PageHeading';
import { CharacterList } from '../molecules/CharacterList';
import { Section } from '../templates/Section';
import { BackLink } from '../molecules/BackLink';
import { useIsMobile } from '../../hooks/useIsMobile';

interface RuneLoadoutPageProps {
	runes: string[];
}

const LIMIT = 25;

export const RuneLoadoutPage: React.FC<RuneLoadoutPageProps> = ({ runes }) => {
	const shortRunes = runes.map((r) => r.replace('Rune of ', ''));

	const filter = {
		runes: shortRunes.map((r) => 'Rune of ' + r),
		sortBy: CharactersSortBy.Level,
		level: 20,
	};

	const { data, loading, error, fetchMore } = usePreviewCharactersQuery({
		variables: {
			input: {
				limit: LIMIT,
				offset: 0,
				filter,
			},
		},
	});

	const isMobile = useIsMobile();

	if (loading) {
		return <CenterLoading />;
	} else if (error || !data?.characters) {
		return <Box>error</Box>;
	}

	const boundFetchMore = () => {
		if (!data?.characters?.characters) return;

		const newOffset = data.characters.characters.length;

		fetchMore({
			variables: {
				input: {
					limit: LIMIT,
					offset: newOffset,
					filter,
				},
			},
		});
	};

	const heading = Object.entries(
		shortRunes.reduce((a, v) => {
			a[v] = (a[v] ?? 0) + 1;
			return a;
		}, {} as Record<string, number>)
	)
		.toSorted((a, b) => runeOrder.indexOf(a[0]) - runeOrder.indexOf(b[0]))
		.map((r) => (r[1] > 1 ? r[1] + 'x ' : '') + r[0])
		.join(', ');

	return (
		<>
			<PageHeading>{heading}</PageHeading>

			<Box mb={2}>
				<BackLink href="/rune-loadouts">Rune Loadouts</BackLink>
			</Box>

			<Section>
				<Flex flexDir="row" justifyContent="center" gap={4}>
					{shortRunes
						.toSorted(
							(a, b) =>
								runeOrder.indexOf(a) - runeOrder.indexOf(b)
						)
						.map((rune, i) => (
							<AbilityIconDisplay
								key={rune + i}
								name={rune}
								size={isMobile ? 'sm' : 'lg'}
							/>
						))}
				</Flex>
			</Section>

			{data.characters.characters.length ? (
				<Flex direction="column" mt={4}>
					<CharacterList
						data={data.characters.characters}
						hasMore={data.characters.hasMore}
						onFetchMore={boundFetchMore}
						showFetchMore={true}
						loading={loading}
					/>
				</Flex>
			) : (
				<Center mt={4}>
					<NothingText>No characters yet</NothingText>
				</Center>
			)}
		</>
	);
};
