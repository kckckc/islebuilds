import {
	Center,
	Flex,
	Stat,
	StatLabel,
	StatNumber,
	Text,
} from '@chakra-ui/react';
import React from 'react';
import { CurrentTime } from '../atoms/CurrentTime';
import { PageHeading } from '../atoms/PageHeading';
import { Timer } from '../molecules/Timer';
import { Section } from '../templates/Section';
import { SectionHeading } from '../templates/SectionHeading';
import { TwoColumnLayout } from '../templates/TwoColumnLayout';

interface TimersPageProps {}

export const TimersPage: React.FC<TimersPageProps> = ({}) => {
	return (
		<>
			<PageHeading>Timers</PageHeading>

			<Flex direction="column" gap={4}>
				<Section>
					<TwoColumnLayout
						left={
							<Flex direction="column" gap={2}>
								<Timer
									name="Fishing Tournament"
									cron="0 7,19 * * *"
								/>
								<Timer
									name="Plague of Rats"
									cron="0 */3 * * *"
								/>
								<Timer
									name="Finn Elderbow"
									cron="0 */4 * * *"
								/>
							</Flex>
						}
						right={
							<Flex direction="column" gap={2}>
								<Timer name="Thumper" cron="0 * * * *" />
								<Timer name="Sundfehr" cron="0 */2 * * *" />
							</Flex>
						}
					/>
				</Section>

				<Section>
					<TwoColumnLayout
						left={
							<Stat>
								<StatNumber>
									<CurrentTime />
								</StatNumber>
								<StatLabel>Local Time</StatLabel>
							</Stat>
						}
						right={
							<Stat>
								<StatNumber>
									<CurrentTime serverTime />
								</StatNumber>
								<StatLabel>Server Time (UTC)</StatLabel>
							</Stat>
						}
					/>
				</Section>

				{/* <Section>
					<SectionHeading>Bosses</SectionHeading>

					<Center>TBA</Center>
				</Section> */}

				<Section>
					<SectionHeading>Seasonal Events</SectionHeading>
					<Text>
						{/* The Festival of the Sun started on July 20 at 18:00
						server time. The event map resets every hour after
						server restart instead of at a set time, so
						unfortunately there is no timer available for now. If
						you ask in Discord or ingame someone can probably tell
						you the timing, or you can look at the &quot;Maintaining
						the Portal&quot; event text in the Pyramid map for an
						estimate. */}
						{/* Soul&apos;s Moor started on October 27. Lord Squash
							spawns in the Squash Patch every hour at XX:10.*/}
						There is currently no seasonal event active.
					</Text>

					{/*<Center>
						<Timer name="Lord Squash" cron="10 * * * *" />
						</Center>*/}
				</Section>
			</Flex>
		</>
	);
};
