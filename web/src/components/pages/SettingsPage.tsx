import {
	FormControl,
	FormLabel,
	Select,
	Text,
	useColorMode,
} from '@chakra-ui/react';
import React from 'react';
import { useQualityIndicators } from '../../hooks/settingsHooks';
import { PageHeading } from '../atoms/PageHeading';
import { TwoColumnSection } from '../templates/TwoColumnSection';

interface SettingsPageProps {}

export const SettingsPage: React.FC<SettingsPageProps> = ({}) => {
	const { colorMode, toggleColorMode } = useColorMode();
	const [qualityIndicators, setQualityIndicators] = useQualityIndicators();

	return (
		<>
			<PageHeading>Settings</PageHeading>
			<TwoColumnSection
				left={
					<>
						<Text>
							These settings affect the way some parts of the
							website look. Site settings are saved as cookies and
							can be used without an account.
						</Text>
					</>
				}
				right={
					<>
						<FormControl>
							<FormLabel htmlFor="selectTheme">Theme</FormLabel>
							<Select
								id="selectTheme"
								value={colorMode}
								onChange={toggleColorMode}
							>
								<option value="light">Light</option>
								<option value="dark">Dark</option>
							</Select>
						</FormControl>
						<FormControl>
							<FormLabel htmlFor="selectQualityIndicators">
								Item Quality Indicators
							</FormLabel>
							<Select
								id="selectQualityIndicators"
								value={qualityIndicators}
								onChange={(e) =>
									setQualityIndicators(e.target.value)
								}
							>
								<option value="off">Off</option>
								<option value="border">Border</option>
								<option value="bottom">Bottom</option>
								<option value="background">Background</option>
							</Select>
						</FormControl>
					</>
				}
			/>
		</>
	);
};
