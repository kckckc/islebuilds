import { Box, Flex, Stack } from '@chakra-ui/react';
import React from 'react';
import { useRuneLoadoutsQuery } from '../../generated/graphql';
import { runeOrder } from '../../util/runeOrder';
import { AbilityIconDisplay } from '../atoms/AbilityIconDisplay';
import { CenterLoading } from '../atoms/CenterLoading';
import { PageHeading } from '../atoms/PageHeading';
import { Section } from '../templates/Section';
import { SectionHeading } from '../templates/SectionHeading';
import { useIsMobile } from '../../hooks/useIsMobile';

interface TopRuneLoadoutsPageProps {}

export const TopRuneLoadoutsPage: React.FC<TopRuneLoadoutsPageProps> = ({}) => {
	const { data, loading, error } = useRuneLoadoutsQuery();

	const isMobile = useIsMobile();

	if (loading) {
		return <CenterLoading />;
	} else if (error || !data?.runeLoadouts) {
		return <Box>error</Box>;
	}

	return (
		<>
			<PageHeading>Top Rune Loadouts</PageHeading>

			<Stack gap={4}>
				{data.runeLoadouts.loadouts.map((loadout) => {
					const shortRunes = loadout.runes.map((r) =>
						r.replace('Rune of ', '')
					);

					const href = '/rune-loadouts/' + shortRunes.join(',');

					const heading = Object.entries(
						shortRunes.reduce((a, v) => {
							a[v] = (a[v] ?? 0) + 1;
							return a;
						}, {} as Record<string, number>)
					)
						.toSorted(
							(a, b) =>
								runeOrder.indexOf(a[0]) -
								runeOrder.indexOf(b[0])
						)
						.map((r) => (r[1] > 1 ? r[1] + 'x ' : '') + r[0])
						.join(', ');

					return (
						<Section key={JSON.stringify(loadout)}>
							<SectionHeading href={href}>
								{heading}
							</SectionHeading>
							<SectionHeading size="sm">
								{loadout.count} character
								{loadout.count != 1 ? 's' : ''}
							</SectionHeading>

							<Flex flexDir="row" justifyContent="center" gap={4}>
								{shortRunes
									.toSorted(
										(a, b) =>
											runeOrder.indexOf(a) -
											runeOrder.indexOf(b)
									)
									.map((rune, i) => (
										<AbilityIconDisplay
											key={rune + i}
											name={rune}
											size={isMobile ? 'sm' : 'lg'}
										/>
									))}
							</Flex>
						</Section>
					);
				})}
			</Stack>
		</>
	);
};
