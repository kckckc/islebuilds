import { Box, Divider, Flex, Heading, Tag } from '@chakra-ui/react';
import React from 'react';
import { FactionHeading } from '../atoms/FactionHeading';
import { Hero } from '../atoms/Hero';
import { BigSearch } from '../organisms/BigSearch';
import { NewsPreview } from '../organisms/NewsPreview';
import { RecentCharacters } from '../organisms/RecentCharacters';
import { ToolsList } from '../organisms/ToolsList';
import { Section } from '../templates/Section';
import { SectionHeading } from '../templates/SectionHeading';
import { TwoColumnLayout } from '../templates/TwoColumnLayout';

interface HomePageProps {}

export const HomePage: React.FC<HomePageProps> = ({}) => {
	return (
		<>
			<Hero />

			<Box my={8}>
				<BigSearch />
			</Box>

			<Divider my={8} />

			<TwoColumnLayout
				left={
					<>
						<Heading size="md">Recently Uploaded</Heading>
						<RecentCharacters />
					</>
				}
				right={
					<>
						<Heading size="md">Reputation Leaderboards</Heading>
						<Section>
							<Flex flexDirection="column" rowGap={2}>
								{[
									'gaekatla',
									'fjolgard',
									'akarei',
									'peopleOfTheSun',
									'pumpkinSailor',
									'theWinterMan',
								].map((f) => (
									<FactionHeading factionId={f} key={f} />
								))}
							</Flex>
						</Section>
					</>
				}
			/>

			<Divider my={8} />

			<Box>
				<NewsPreview />
			</Box>

			<Divider my={8} />

			<Box>
				<Box mb={2}>
					<SectionHeading href="/tools" noColor>
						Tools
					</SectionHeading>
				</Box>
				<ToolsList />
			</Box>
		</>
	);
};
