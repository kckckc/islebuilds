import { Flex, Heading, Text } from '@chakra-ui/react';
import React from 'react';
import { PageHeading } from '../atoms/PageHeading';
import { AccountRoles } from '../organisms/AccountRoles';
import { ConnectedAccounts } from '../organisms/ConnectedAccounts';
import { Section } from '../templates/Section';
import { SectionHeading } from '../templates/SectionHeading';
import { TwoColumnSection } from '../templates/TwoColumnSection';

interface AccountPageProps {}

export const AccountPage: React.FC<AccountPageProps> = ({}) => {
	return (
		<>
			<PageHeading>Account Settings</PageHeading>

			<Flex direction="column" gap={4}>
				<Section>
					<SectionHeading href="/account/keys">
						Manage API Keys
					</SectionHeading>

					<Text>
						You can generate an API key to give an application (such
						as Waddon2) access to your profile on Islebuilds.
					</Text>
				</Section>

				<TwoColumnSection
					left={
						<>
							<Heading size="md">Connections</Heading>
							<Text>
								You can connect additional accounts to log in to
								Islebuilds or display on your profile.
							</Text>
						</>
					}
					right={<ConnectedAccounts />}
				/>

				<AccountRoles />
			</Flex>

			{/* <Divider my={8} /> */}
		</>
	);
};
