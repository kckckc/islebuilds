import { Flex } from '@chakra-ui/react';
import { IWDItem } from '@kckckc/isleward-util/isleward';
import React, { useContext, useMemo } from 'react';
import { ThemedItemSlot } from '../../atoms/ThemedItemSlot';
import { evaluateFilterRoot } from '../../organisms/ItemFilter/filter';
import { getItemSlotName, slotOrder } from './InventoryBody';
import {
	InventoryGroupBy,
	InventoryViewerContext,
} from './InventoryViewerContext';

const INVENTORY_WIDTH = 10;

interface ItemInfo {
	item: IWDItem | null;
	filtered: boolean;
}

interface InventoryItemListProps {
	items: IWDItem[];
	isStash: boolean;
}

export const InventoryItemList: React.FC<InventoryItemListProps> = ({
	items,
	isStash,
}) => {
	const {
		groupBy,
		filter,
		compact,
		limitWidth,
		hideFiltered,
		hideEquipped,
		smallItems,
	} = useContext(InventoryViewerContext);

	const rows = useMemo(() => {
		const filteredItems = items.map((item) => {
			const filterDidNotMatch = filter
				? !evaluateFilterRoot(filter, item)
				: false;

			return {
				item,
				filtered: filterDidNotMatch,
			};
		});

		// Alternative method for non-compact character grouping
		if (groupBy === InventoryGroupBy.Character && !compact && !isStash) {
			const rows: ItemInfo[][] = [];

			const remainingItems = filteredItems;
			for (let y = 0; y < 5; y++) {
				const row: ItemInfo[] = [];

				for (let x = 0; x < 10; x++) {
					const index = y * INVENTORY_WIDTH + x;

					const itemIndex = remainingItems.findIndex(
						(i) => i.item.pos === index
					);
					if (
						itemIndex === -1 ||
						(hideFiltered && remainingItems[itemIndex].filtered) ||
						(hideEquipped && remainingItems[itemIndex].item.eq)
					) {
						row[x] = { item: null, filtered: false };
					} else {
						row[x] = remainingItems[itemIndex];

						remainingItems.splice(itemIndex, 1);
					}
				}

				rows.push(row);
			}

			let remRow: ItemInfo[] = [];
			remainingItems
				.sort(
					(a, b) =>
						slotOrder.indexOf(getItemSlotName(a.item)) -
						slotOrder.indexOf(getItemSlotName(b.item))
				)
				.forEach((item) => {
					if (
						(hideFiltered && item.filtered) ||
						(hideEquipped && item.item.eq)
					)
						return;

					if (remRow.length >= INVENTORY_WIDTH) {
						rows.push(remRow);
						remRow = [];
					}
					remRow.push(item);
				});
			if (remRow.length > 0) {
				rows.push(remRow);
			}

			return rows;
		}

		const chunkWidth =
			(groupBy === InventoryGroupBy.Character && !compact) || limitWidth
				? INVENTORY_WIDTH
				: Infinity;

		const chunks: ItemInfo[][] = [];
		let currentChunk: ItemInfo[] = [];

		filteredItems.forEach((item) => {
			// Skip if hideFiltered is enabled
			if (hideFiltered && item.filtered) return;

			// Skip based on hideEquipped
			if (hideEquipped && item.item.eq) return;

			// Split into chunks of 10 items
			if (currentChunk.length >= chunkWidth) {
				chunks.push(currentChunk);
				currentChunk = [];
			}
			currentChunk.push(item);
		});

		if (currentChunk.length > 0) {
			chunks.push(currentChunk);
		}

		return chunks;
	}, [
		items,
		isStash,
		groupBy,
		filter,
		compact,
		limitWidth,
		hideFiltered,
		hideEquipped,
	]);

	return (
		<Flex direction="column" gap={2}>
			{rows.map((rowItems, rowIdx) => (
				<Flex key={rowIdx} direction="row" gap={2} flexWrap="wrap">
					{rowItems.map((item, idx) => {
						return (
							<ThemedItemSlot
								key={`${rowIdx}-${idx}`}
								item={item.item ?? undefined}
								darken={item.filtered}
								size={smallItems ? 'sm' : 'md'}
							/>
						);
					})}
				</Flex>
			))}
		</Flex>
	);
};
