import { Center, Flex, Heading, Spinner } from '@chakra-ui/react';
import React, { ReactNode } from 'react';
import { useMeQuery } from '../../../generated/graphql';
import { LogInButton } from '../../molecules/LogInButton';

interface InventoryGatekeeperProps {
	children?: ReactNode;
}

export const InventoryGatekeeper: React.FC<InventoryGatekeeperProps> = ({
	children,
}) => {
	const { data, loading } = useMeQuery();

	if (loading) {
		return (
			<Center flex={1}>
				<Spinner />
			</Center>
		);
	} else if (data?.me) {
		return <>{children}</>;
	} else {
		return (
			<Flex
				flex={1}
				gap={4}
				direction="column"
				justifyContent="center"
				alignItems="center"
			>
				<Heading size="md">
					You need an account to use the Inventory Viewer.
				</Heading>
				<LogInButton />
			</Flex>
		);
	}
};
