import {
	Flex,
	FormControl,
	FormLabel,
	IconButton,
	useDisclosure,
} from '@chakra-ui/react';
import { Select } from 'chakra-react-select';
import { BookOpenIcon } from 'lucide-react';
import React, { useContext } from 'react';
import { ItemFilter } from '../../organisms/ItemFilter/ItemFilter';
import { ItemFilterHelpModal } from '../../organisms/ItemFilter/ItemFilterHelp';
import { InventoryOptions } from './InventoryOptions';
import {
	InventoryGroupBy,
	InventoryViewerContext,
} from './InventoryViewerContext';

interface InventoryControlsProps {}

export const InventoryControls: React.FC<InventoryControlsProps> = ({}) => {
	const { groupBy, setGroupBy } = useContext(InventoryViewerContext);

	const helpModal = useDisclosure();

	return (
		<>
			<ItemFilterHelpModal
				isOpen={helpModal.isOpen}
				onClose={helpModal.onClose}
			/>

			<InventoryOptions />

			<Flex direction="row" gap={2} alignItems="flex-end">
				<FormControl flexBasis={1}>
					<FormLabel>Group by</FormLabel>
					<Select
						isClearable={false}
						value={{
							label: groupBy,
							value: groupBy,
						}}
						onChange={(v) =>
							setGroupBy(v?.value ?? InventoryGroupBy.Character)
						}
						chakraStyles={{
							container: (base, state) => ({
								...base,
								w: '160px',
							}),
							dropdownIndicator: (base, state) => ({
								...base,
								w: '32px',
							}),
						}}
						options={Object.values(InventoryGroupBy).map((v) => ({
							label: v,
							value: v,
						}))}
					/>
				</FormControl>
				<ItemFilter />
				<IconButton
					aria-label="Help"
					onClick={() => helpModal.onOpen()}
				>
					<BookOpenIcon size={20} />
				</IconButton>
			</Flex>
		</>
	);
};
