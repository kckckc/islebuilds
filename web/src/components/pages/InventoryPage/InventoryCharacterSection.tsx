import { Box, Flex } from '@chakra-ui/react';
import React from 'react';
import { SkinDisplay } from '../../atoms/SkinDisplay';
import { TimeAgo } from '../../atoms/TimeAgo';
import { SectionHeading } from '../../templates/SectionHeading';
import { InventoryItemList } from './InventoryItemList';
import { InventoryData } from './InventoryViewerContext';

interface InventoryCharacterSectionProps {
	inventory: InventoryData;
}

export const InventoryCharacterSection: React.FC<
	InventoryCharacterSectionProps
> = ({ inventory }) => {
	const { isStash, character, items } = inventory;
	const { skinCell, skinSheetName } = character ?? {
		skinCell: 122,
		skinSheetName: 'objects',
	};

	const title = (
		<SectionHeading
			size="md"
			href={isStash ? undefined : `/character/${inventory.name}`}
		>
			{inventory.name}
		</SectionHeading>
	);

	return (
		<Flex direction="column" gap={2}>
			{/* Heading row */}
			<Flex direction="row" gap={2} alignItems="center">
				<SkinDisplay
					skinCell={skinCell}
					skinSheetName={skinSheetName}
					size="sm"
				/>

				<Flex direction="row" gap={2} alignItems="baseline">
					{title}
					<Box>
						<TimeAgo date={inventory.uploadedAt} />
					</Box>
				</Flex>
			</Flex>

			{/* Items */}
			<InventoryItemList items={items} isStash={isStash} />
		</Flex>
	);
};
