import { Checkbox, Flex } from '@chakra-ui/react';
import React, { useContext } from 'react';
import { InventoryViewerContext } from './InventoryViewerContext';

interface InventoryOptionsProps {}

export const InventoryOptions: React.FC<InventoryOptionsProps> = ({}) => {
	const {
		compact,
		setCompact,
		limitWidth,
		setLimitWidth,
		hideEquipped,
		setHideEquipped,
		hideFiltered,
		setHideFiltered,
		smallItems,
		setSmallItems,
	} = useContext(InventoryViewerContext);

	return (
		<Flex direction="column" mb={4}>
			<Checkbox
				isChecked={compact}
				onChange={() => setCompact((x) => !x)}
			>
				Compact characters
			</Checkbox>
			<Checkbox
				isChecked={limitWidth}
				onChange={() => setLimitWidth((x) => !x)}
			>
				Limit width
			</Checkbox>
			<Checkbox
				isChecked={hideEquipped}
				onChange={() => setHideEquipped((x) => !x)}
			>
				Hide equipped items
			</Checkbox>
			<Checkbox
				isChecked={hideFiltered}
				onChange={() => {
					console.log('aaa');
					setHideFiltered((x) => !x);
				}}
			>
				Hide filtered items
			</Checkbox>
			<Checkbox
				isChecked={smallItems}
				onChange={() => setSmallItems((x) => !x)}
			>
				Small item icons
			</Checkbox>
		</Flex>
	);
};
