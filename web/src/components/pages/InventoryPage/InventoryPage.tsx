import {
	Center,
	Divider,
	Flex,
	Heading,
	Spinner,
	Text,
} from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import React, { useMemo, useState } from 'react';
import { useMyInventoriesQuery } from '../../../generated/graphql';
import { useInventorySettings } from '../../../hooks/settingsHooks';
import { ItemFilterRoot } from '../../organisms/ItemFilter/parser';
import { InventoryBody } from './InventoryBody';
import { InventoryControls } from './InventoryControls';
import { InventoryGatekeeper } from './InventoryGatekeeper';
import {
	InventoryData,
	InventoryGroupBy,
	InventoryGroupByType,
	InventoryViewerContext,
	InventoryViewerContextProps,
} from './InventoryViewerContext';

interface InventoryPageProps {}

export const InventoryPage: React.FC<InventoryPageProps> = ({}) => {
	const { data: queryData, error: queryError } = useMyInventoriesQuery();

	const [groupBy, setGroupBy] = useState<InventoryGroupByType>(
		InventoryGroupBy.Character
	);

	const [filter, setFilter] = useState<ItemFilterRoot | null>(null);
	const [filterError, setFilterError] = useState<boolean>(false);

	const inventorySettings = useInventorySettings();

	const inventoryList: InventoryData[] = useMemo(() => {
		const rawInventories = queryData?.myInventories ?? [];

		return rawInventories.map((i) => {
			let items = [];
			try {
				items = JSON.parse(i.items ?? '[]');
			} catch (e) {
				console.error('Failed to parse inventory items:', i.items);
				items = [];
			}

			return {
				name: i.name,
				isStash: i.stash,
				items: items,
				uploadedAt: new Date(parseInt(i.uploadedAt)),
				character: i.character ?? undefined,
			};
		});
	}, [queryData?.myInventories]);

	// Loading
	let body = (
		<Center flex={1}>
			<Spinner />
		</Center>
	);
	if (queryError) {
		body = (
			<Flex
				flex={1}
				direction="column"
				justifyContent="center"
				alignItems="center"
			>
				<Heading size="md">An error occurred</Heading>
				<Text>Unable to load inventories.</Text>
			</Flex>
		);
	} else if (queryData?.myInventories) {
		console.log(inventoryList);

		body = <InventoryBody />;
	}

	const context = {
		inventories: inventoryList,
		groupBy,
		setGroupBy,
		filter,
		setFilter,
		filterError,
		setFilterError,
		...inventorySettings,
	} satisfies InventoryViewerContextProps;

	return (
		<>
			<NextSeo title="Inventory Viewer" />
			<InventoryGatekeeper>
				<InventoryViewerContext.Provider value={context}>
					<InventoryControls />
					<Divider my={4} />
					{body}
				</InventoryViewerContext.Provider>
			</InventoryGatekeeper>
		</>
	);
};
