import { Flex } from '@chakra-ui/react';
import { IWDItem } from '@kckckc/isleward-util/isleward';
import React from 'react';
import { SectionHeading } from '../../templates/SectionHeading';
import { InventoryItemList } from './InventoryItemList';

interface InventorySectionProps {
	name?: string;
	items: IWDItem[];
}

export const InventorySection: React.FC<InventorySectionProps> = ({
	name,
	items,
}) => {
	return (
		<Flex direction="column" gap={2}>
			{name && <SectionHeading size="md">{name}</SectionHeading>}
			<InventoryItemList items={items} isStash={false} />
		</Flex>
	);
};
