import { Box, Center, Flex, Heading, Text } from '@chakra-ui/react';
import { IWDItem } from '@kckckc/isleward-util/isleward';
import React, { useContext, useMemo } from 'react';
import { PlainLink } from '../../atoms/PlainLink';
import { InventoryCharacterSection } from './InventoryCharacterSection';
import { InventorySection } from './InventorySection';
import {
	InventoryData,
	InventoryGroupBy,
	InventoryViewerContext,
} from './InventoryViewerContext';

const slotNames: Record<string, string> = {
	head: 'Head',
	neck: 'Neck',
	chest: 'Chest',
	hands: 'Hands',
	finger: 'Finger',
	waist: 'Waist',
	legs: 'Legs',
	feet: 'Feet',
	trinket: 'Trinket',
	oneHanded: 'Onehanded weapons',
	twoHanded: 'Twohanded weapons',
	offHand: 'Offhand',
	rune: 'Rune',
	tool: 'Tool',
	other: 'Other',
};
export const slotOrder = Object.values(slotNames);

export const getItemSlotName = (item: IWDItem) => {
	let slot = item.slot ?? 'Other';
	if (item.name?.startsWith('Rune of')) {
		slot = 'Rune';
	}
	return slotNames[slot] ?? slot;
};

const typeNames: Record<string, string> = {
	toy: 'Toy',
	mount: 'Mount',
	recipe: 'Recipe',
};

const sectionItemsByPredicate = (
	inventories: InventoryData[],
	group: (item: IWDItem) => string,
	order?: string[]
) => {
	const groups: Record<string, IWDItem[]> = {};

	inventories.forEach((inventory) => {
		inventory.items.forEach((item) => {
			const val = group(item);
			if (!groups[val]) {
				groups[val] = [];
			}
			groups[val].push(item);
		});
	});

	const entries = Object.entries(groups);

	if (order) {
		entries.sort(
			(a, b) => slotOrder.indexOf(a[0]) - slotOrder.indexOf(b[0])
		);
	}

	return entries.map(([slot, items]) => (
		<InventorySection key={slot} name={slot} items={items} />
	));
};

interface InventoryBodyProps {}

export const InventoryBody: React.FC<InventoryBodyProps> = ({}) => {
	const { groupBy, inventories } = useContext(InventoryViewerContext);

	const inventorySections = useMemo(() => {
		if (groupBy === InventoryGroupBy.Character) {
			return inventories.map((i, idx) => (
				<InventoryCharacterSection key={idx} inventory={i} />
			));
		} else if (groupBy === InventoryGroupBy.Slot) {
			return sectionItemsByPredicate(
				inventories,
				getItemSlotName,
				slotOrder
			);
		} else if (groupBy === InventoryGroupBy.Type) {
			return sectionItemsByPredicate(inventories, (item) => {
				let type = item.type ?? 'Other';
				if (item.name?.startsWith('Rune of')) {
					type = 'Rune';
				} else if (item.material) {
					type = 'Crafting Material';
				}
				return typeNames[type] ?? type;
			});
		} else if (groupBy === InventoryGroupBy.None) {
			const items = inventories.flatMap((i) => i.items);
			return [<InventorySection key="none" items={items} />];
		} else {
			return [];
		}
	}, [groupBy, inventories]);

	if (!inventorySections.length) {
		return (
			<Flex direction="row" justifyContent="center">
				<Flex direction="column" gap={2}>
					<Heading size="md">Nothing uploaded yet</Heading>
					<Text>
						To use the Inventory Viewer, install{' '}
						<PlainLink href="/waddon">Waddon</PlainLink> and{' '}
						<PlainLink href="/waddon/connect">
							connect your account
						</PlainLink>
						.
						<br />
						Then enable &quot;sync inventory&quot; under the
						Islebuilds section of the Waddon menu.
					</Text>
				</Flex>
			</Flex>
		);
	}

	return (
		<Flex direction="column" gap={8}>
			{inventorySections}
		</Flex>
	);
};
