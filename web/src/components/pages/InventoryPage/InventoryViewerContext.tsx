import { IWDItem } from '@kckckc/isleward-util/isleward';
import { createContext, Dispatch, SetStateAction } from 'react';
import { PreviewCharacterFragment } from '../../../generated/graphql';
import { ItemFilterRoot } from '../../organisms/ItemFilter/parser';

type ValueOf<T> = T[keyof T];

export const InventoryGroupBy = {
	Character: 'Character',
	Slot: 'Slot',
	Type: 'Type',
	None: 'None',
} as const;

export type InventoryGroupByType = ValueOf<typeof InventoryGroupBy>;

export interface InventoryData {
	name: string;
	isStash: boolean;
	items: IWDItem[];
	uploadedAt: Date;
	character?: PreviewCharacterFragment;
}

type SetState<T> = Dispatch<SetStateAction<T>>;

export interface InventoryViewerContextProps {
	inventories: InventoryData[];

	groupBy: InventoryGroupByType;
	setGroupBy: SetState<InventoryGroupByType>;

	compact: boolean;
	setCompact: SetState<boolean>;

	limitWidth: boolean;
	setLimitWidth: SetState<boolean>;

	hideEquipped: boolean;
	setHideEquipped: SetState<boolean>;

	hideFiltered: boolean;
	setHideFiltered: SetState<boolean>;

	smallItems: boolean;
	setSmallItems: SetState<boolean>;

	filter: ItemFilterRoot | null;
	setFilter: SetState<ItemFilterRoot | null>;
	filterError: boolean;
	setFilterError: SetState<boolean>;
}

export const mockInventoryViewerContext = () => {
	return {
		inventories: [],
		groupBy: InventoryGroupBy.Character,
		setGroupBy: () => {},
		compact: true,
		setCompact: () => {},
		limitWidth: true,
		setLimitWidth: () => {},
		hideEquipped: false,
		setHideEquipped: () => {},
		hideFiltered: false,
		setHideFiltered: () => {},
		smallItems: false,
		setSmallItems: () => {},
		filter: null,
		setFilter: () => {},
		filterError: false,
		setFilterError: () => {},
	};
};

export const InventoryViewerContext =
	createContext<InventoryViewerContextProps>(mockInventoryViewerContext());
