import { Box, Heading } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import React from 'react';
import { UserQuery } from '../../generated/graphql';
import { BackLink } from '../molecules/BackLink';
import { UserSpriteGallery } from '../organisms/UserSpriteGallery';

interface UserSpritesPageProps {
	user: Exclude<UserQuery['user'], null | undefined>;
}

export const UserSpritesPage: React.FC<UserSpritesPageProps> = ({ user }) => {
	return (
		<>
			<NextSeo title={user.displayName + "'s Sprites | User"} />
			<Heading size="lg">{user.displayName}&apos;s Sprites</Heading>
			<Box mt={2}>
				<BackLink href={'/user/' + user.id}>
					{user.displayName}
				</BackLink>
			</Box>

			<Box mt={4}>
				<UserSpriteGallery userId={user.id} />
			</Box>
		</>
	);
};
