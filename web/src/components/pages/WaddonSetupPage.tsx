import { Box, Button, Flex, Heading, Text } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import React, { useState } from 'react';
import { useMeQuery } from '../../generated/graphql';
import { BackLink } from '../molecules/BackLink';
import { WaddonSetupWizard } from '../organisms/WaddonSetupWizard';
import NextLink from 'next/link';

interface WaddonSetupPageProps {}

export const WaddonSetupPage: React.FC<WaddonSetupPageProps> = ({}) => {
	const [wizardActive, setWizardActive] = useState(false);

	const { data: meData } = useMeQuery();

	return (
		<>
			<NextSeo title="Connect Waddon" />

			<Flex
				mt={2}
				mb={8}
				flexDirection="column"
				alignItems="center"
				textAlign="center"
			>
				<Heading>Connect Waddon</Heading>

				<Text mt={4} color={'gray.500'} maxW={'3xl'}>
					Click to
					{meData?.me ? '' : ' log in, then'} connect Waddon with
					Islebuilds to finish setting up uploads.
				</Text>

				{meData?.me ? (
					<Button
						my={8}
						rounded={'full'}
						px={6}
						colorScheme={'teal'}
						onClick={() => {
							if (!wizardActive) {
								setWizardActive(true);
							}
						}}
					>
						Connect
					</Button>
				) : (
					<NextLink
						href="/login?next=/waddon/connect"
						passHref
						legacyBehavior
					>
						<Button
							as="a"
							my={8}
							rounded={'full'}
							px={6}
							colorScheme={'teal'}
						>
							Log in
						</Button>
					</NextLink>
				)}

				<Box mb={8}>
					<WaddonSetupWizard active={!!meData?.me && wizardActive} />
				</Box>

				<BackLink href="/waddon">Back to Waddon info</BackLink>
			</Flex>
		</>
	);
};
