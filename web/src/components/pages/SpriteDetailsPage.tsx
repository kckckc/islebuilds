import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import { Box, Button, chakra, Flex, Heading } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import React, { useRef } from 'react';
import {
	useMeQuery,
	useSetSpriteVisibleMutation,
	useSpriteDetailsQuery,
} from '../../generated/graphql';
import { useDrawSprite } from '../../hooks/useDrawSprite';
import { useRoleCheck } from '../../hooks/useRoleCheck';
import { humanize } from '../../util/humanize';
import { CenterLoading } from '../atoms/CenterLoading';
import { DeleteSpriteButton } from '../atoms/DeleteSpriteButton';
import { HiddenIndicator } from '../atoms/HiddenIndicator';
import { NothingText } from '../atoms/NothingText';
import { ReportSpriteButton } from '../atoms/ReportButton/ReportSpriteButton';
import { UserLink } from '../atoms/UserLink';
import { BackLink } from '../molecules/BackLink';
import { SpriteDownload } from '../molecules/SpriteDownload';
import { SpriteNotFound } from '../molecules/SpriteNotFound';
import { WithReportSpriteModal } from '../organisms/ReportModal/WithReportSpriteModal';
import { Section } from '../templates/Section';

interface SpriteDetailsPageProps {
	id: string;
}

export const SpriteDetailsPage: React.FC<SpriteDetailsPageProps> = ({ id }) => {
	const { data, loading, error } = useSpriteDetailsQuery({
		variables: {
			id,
		},
	});

	const [setVisible, { loading: setVisibleLoading }] =
		useSetSpriteVisibleMutation();

	const canvasRef = useRef<HTMLCanvasElement | null>(null);
	useDrawSprite(canvasRef, data?.spriteById ?? undefined);

	const { data: meData } = useMeQuery();

	const isModerator = useRoleCheck('MODERATION');

	if (loading) {
		return <CenterLoading />;
	} else if (error || !data?.spriteById) {
		return <SpriteNotFound />;
	}

	const sprite = data.spriteById;
	const createdAt = parseInt(sprite.createdAt);
	const isAuthor = meData?.me?.id === sprite.user.id;

	return (
		<WithReportSpriteModal spriteId={sprite.id}>
			<Flex direction="column" gap={4}>
				<Box>
					<NextSeo title={sprite.name + ' | Sprite Gallery'} />
					<Flex direction="row">
						<Box>
							<Heading size="md">
								{sprite.name}{' '}
								{!sprite.visible && (
									<chakra.span ml={2}>
										<HiddenIndicator />
									</chakra.span>
								)}
							</Heading>
							<Box mt={2}>
								<BackLink href="/sprite-editor/gallery">
									Sprite Gallery
								</BackLink>
							</Box>
						</Box>

						<Box flex={1} />

						<Flex flexDirection="row" gap={2}>
							{(isModerator || isAuthor) && (
								<DeleteSpriteButton
									spriteId={sprite.id}
									isDeleted={sprite.deleted}
								/>
							)}
							<ReportSpriteButton />
						</Flex>
					</Flex>
				</Box>

				<Flex direction="row" justifyContent="center">
					<Box
						flex={1}
						shadow="md"
						borderWidth="1px"
						rounded="md"
						maxW="400px"
					>
						<chakra.canvas
							w="100%"
							bg="#2d2136"
							p={4}
							rounded="md"
							style={{
								imageRendering: 'pixelated',
							}}
							ref={canvasRef}
						></chakra.canvas>
					</Box>
				</Flex>

				<Section>
					{sprite.deleted && (
						<Box>
							<Heading size="lg" color="red.500">
								Deleted
							</Heading>
							<Box>
								This sprite has been deleted by its owner or by
								a moderator.
							</Box>
							<Box>
								Deleted sprites are inaccessible to users, but
								moderators can still see them.
							</Box>
							<Flex direction="row" alignItems="center" gap={2}>
								<Box>Undo delete:</Box>
								<DeleteSpriteButton
									spriteId={sprite.id}
									isDeleted={sprite.deleted}
								/>
							</Flex>
						</Box>
					)}

					<Heading size="md">Name</Heading>
					<Box>{sprite.name}</Box>

					<Heading mt={4} size="md">
						Description
					</Heading>
					<Box>
						{sprite.description.length ? (
							sprite.description
						) : (
							<NothingText>No description provided</NothingText>
						)}
					</Box>

					<Heading mt={4} size="md">
						Details
					</Heading>
					<span suppressHydrationWarning={true}>
						Created by{' '}
						<UserLink
							id={sprite.user.id}
							displayName={sprite.user.displayName}
						/>{' '}
						on {humanize(createdAt)}.
					</span>

					{isAuthor && (
						<>
							<Heading size="md" mt={4}>
								Visibility
							</Heading>
							<Box>
								{sprite.visible
									? 'Anyone can see this sprite.'
									: 'Only you can see this sprite.'}
							</Box>
							<Box>
								<Button
									leftIcon={
										sprite.visible ? (
											<ViewIcon />
										) : (
											<ViewOffIcon />
										)
									}
									isLoading={setVisibleLoading}
									onClick={() => {
										setVisible({
											variables: {
												sprite: sprite.id,
												visible: !sprite.visible,
											},
											refetchQueries: ['SpriteDetails'],
										});
									}}
								>
									Toggle visibility
								</Button>
							</Box>
						</>
					)}

					<Heading size="md" mt={4}>
						Download
					</Heading>
					<Box>
						<SpriteDownload sprite={sprite} />
					</Box>
				</Section>
			</Flex>
		</WithReportSpriteModal>
	);
};
