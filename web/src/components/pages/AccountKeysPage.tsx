import { Box, Divider, Heading, Text } from '@chakra-ui/react';
import React from 'react';
import { PageHeading } from '../atoms/PageHeading';
import { BackLink } from '../molecules/BackLink';
import { CreateTokenForm } from '../molecules/CreateTokenForm';
import { TokenList } from '../molecules/TokenList';
import { TwoColumnSection } from '../templates/TwoColumnSection';

interface AccountKeysPageProps {}

export const AccountKeysPage: React.FC<AccountKeysPageProps> = ({}) => {
	return (
		<>
			<PageHeading>Manage API Keys</PageHeading>
			<Box mt={2} mb={2}>
				<BackLink href="/account">Account</BackLink>
			</Box>

			<TwoColumnSection
				left={
					<>
						<Heading size="md">API Keys</Heading>
						<Text>
							You can generate an API key to give an application
							(such as Waddon) access to your profile on
							Islebuilds.
						</Text>
						<Text>
							To get started, create a key. Next, press
							&quot;Use&quot; to have Waddon automatically import
							the key. Otherwise, you can copy and paste the key
							manually into Waddon. You can delete keys at any
							time to revoke access.
						</Text>
						<Text>
							You should <strong>never</strong> share your API key
							with anyone else!
						</Text>
					</>
				}
				right={<CreateTokenForm />}
			/>

			<Divider my={8} />

			<TokenList />
		</>
	);
};
