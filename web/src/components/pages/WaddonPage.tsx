import {
	Box,
	Button,
	Center,
	Code,
	Divider,
	Flex,
	Heading,
	HStack,
	Stack,
	Text,
} from '@chakra-ui/react';
import React, { useRef } from 'react';
import { PlainLink } from '../atoms/PlainLink';
import { WaddonHero } from '../atoms/WaddonHero';
import { Section } from '../templates/Section';
import { SectionHeading } from '../templates/SectionHeading';
import NextLink from 'next/link';
import { NextSeo } from 'next-seo';

interface WaddonPageProps {}

const USERSCRIPT_URL =
	'https://gitlab.com/Isleward-Modding/addons/waddon/-/raw/master/dist/waddon.user.js';
const VM_URL = 'https://violentmonkey.github.io/get-it/';
const SOURCE_URL = 'https://gitlab.com/Isleward-Modding/addons/waddon';
const ISSUES_URL = 'https://gitlab.com/Isleward-Modding/addons/waddon/-/issues';

export const WaddonPage: React.FC<WaddonPageProps> = ({}) => {
	const instructionsRef = useRef<HTMLDivElement>(null);

	const scrollToInstructions = () =>
		instructionsRef.current?.scrollIntoView({
			behavior: 'smooth',
			block: 'start',
		});

	const heroSection = (
		<>
			<WaddonHero />

			<Flex
				mt={2}
				mb={8}
				flexDirection="column"
				alignItems="center"
				textAlign="center"
			>
				<Text color={'gray.500'} maxW={'3xl'}>
					Minimap, boss timers, Islebuilds integration, item stat
					ranges, XP/loot tracker, and more.
				</Text>

				<HStack mt={8} spacing={4}>
					<NextLink href={USERSCRIPT_URL} passHref legacyBehavior>
						<Button
							as="a"
							rounded={'full'}
							px={6}
							colorScheme={'teal'}
							target="_blank"
						>
							Latest Userscript
						</Button>
					</NextLink>
					<NextLink href={ISSUES_URL} passHref legacyBehavior>
						<Button as="a" rounded={'full'} px={6} target="_blank">
							Report Issues
						</Button>
					</NextLink>
				</HStack>
				<HStack mt={4} spacing={4} height="30px">
					<PlainLink href="/waddon/connect">
						Already installed Waddon?
					</PlainLink>
					<Divider orientation="vertical" />
					<PlainLink onClick={scrollToInstructions}>
						Installation Guide
					</PlainLink>
					<Divider orientation="vertical" />
					<PlainLink href={VM_URL} external newTab>
						Get ViolentMonkey
					</PlainLink>
					<Divider orientation="vertical" />
					<PlainLink href={SOURCE_URL} external newTab>
						Source Code
					</PlainLink>
				</HStack>
			</Flex>
		</>
	);

	const introSection = (
		<Section>
			<Text>
				Waddon is a general purpose client-side addon for Isleward. It
				features a minimap, boss timers, item stat ranges, XP tracker,
				other QoL changes, and more.
			</Text>
			<Text>
				Waddon is also currently the only way to upload characters to
				Islebuilds.
			</Text>
			<Text>
				More information might also be available in the README at the{' '}
				<PlainLink href={SOURCE_URL} external newTab>
					GitLab repository
				</PlainLink>
				.
			</Text>
		</Section>
	);

	const installSections = (
		<>
			<Section ref={instructionsRef}>
				<SectionHeading>Installation (Browser)</SectionHeading>
				<Text>
					To install Waddon in a browser, you will need a userscript
					manager.{' '}
					<PlainLink href={VM_URL} external newTab>
						ViolentMonkey
					</PlainLink>{' '}
					is recommended and tested.
				</Text>
				<Text>
					After installing ViolentMonkey, click{' '}
					<PlainLink href={USERSCRIPT_URL} external newTab>
						here
					</PlainLink>{' '}
					to install the addon. Clicking the link should open your
					userscript manager automatically. If it didn&apos;t open,
					you can also manually create a new userscript and copy and
					paste the contents of <Code>waddon.user.js</Code> into it.
				</Text>
				<Text>
					Refresh Isleward and the addon should appear. See below for
					usage instructions. Please report issues with Waddon{' '}
					<PlainLink href={ISSUES_URL} external newTab>
						here
					</PlainLink>
					.
				</Text>
				<Box textAlign="center">
					{/* TODO: embed video */}
					<PlainLink
						href={'https://youtu.be/-JW8TD6y8W4'}
						external
						newTab
					>
						Video Instructions
					</PlainLink>
				</Box>
			</Section>
			<Section>
				<SectionHeading>Installation (Desktop)</SectionHeading>
				<Text>
					To install Waddon for the Isleward desktop client, download{' '}
					<Code>waddon.user.js</Code> and place the file in the
					client&apos;s addons folder. See{' '}
					<PlainLink
						href="https://gitlab.com/Isleward/desktop-client/-/tree/master#how-do-i-load-addons"
						external
					>
						here
					</PlainLink>{' '}
					for instructions for each operating system. Restart the
					client afterwards.
				</Text>
				<Text>
					Not all Waddon features are tested in the desktop client,
					but most should work the same as in a browser.
				</Text>
				<Text>
					It&apos;s not possible to install Waddon on mobile clients
					at this time.
				</Text>
			</Section>
		</>
	);

	return (
		<>
			<NextSeo title="Waddon" />

			{heroSection}

			<Flex flexDirection="column" rowGap={4}>
				{introSection}

				<Section>
					<SectionHeading>Usage</SectionHeading>
					<Text>
						Press <Code>N</Code> to open the main addon options
						menu. You can enable other features from here. Some
						panels have more options that can be accessed by
						right-clicking the panel&apos;s title bar.
					</Text>
					<Text>
						You can press <Code>N+M</Code> to toggle the minimap,{' '}
						<Code>N+T</Code> to toggle timers, and <Code>N+L</Code>{' '}
						to toggle the XP tracker.
					</Text>
					<Text>
						For convenience, you can also press <Code>/</Code> to
						open the Slash command menu and search for a command to
						run.
					</Text>
				</Section>
				<Section>
					<SectionHeading>Islebuilds Integration</SectionHeading>
					<Text>
						Waddon can upload your characters to Islebuilds, either
						manually or automatically. To start uploading, you will
						need to create an Islebuilds account and connect it with
						Waddon here:
					</Text>

					<Center>
						<NextLink
							href="/waddon/connect"
							passHref
							legacyBehavior
						>
							<Button
								as="a"
								rounded={'full'}
								my={4}
								px={6}
								colorScheme={'teal'}
							>
								Connect Waddon
							</Button>
						</NextLink>
					</Center>

					<Text>
						Once Waddon has been connected, you can type{' '}
						<Code>/upload</Code> in the game chat to update and view
						your character on the site. You can also use the Slash
						action by pressing <Code>/</Code> and searching for{' '}
						<Code>upload</Code>.
					</Text>
					<Text>
						From the main options menu (<Code>N</Code>), you can
						change some Islebuilds options. You can set the
						auto-upload interval to keep your character up-to-date
						for others to view.
					</Text>
					<Text>
						When uploading your character, Waddon will read your
						level, spirit, portrait, skin, equipment, prophecies,
						reputation, and passives. This information will be
						visible publicly.
					</Text>
					<Text>
						You can also manually connect Waddon. First, log in with
						Google <PlainLink href="/login">here</PlainLink>, then
						navigate to{' '}
						<PlainLink href="/account/keys">
							API key settings
						</PlainLink>{' '}
						and generate a key. Pressing the Use button will open
						Isleward, and Waddon will automatically fetch and store
						the key. If you&apos;re playing on the desktop client,
						you will need to manually copy and paste the key into
						Waddon instead.
					</Text>
				</Section>
				{installSections}
				<Section>
					<SectionHeading>Troubleshooting</SectionHeading>
					<Text>
						If you&apos;re having problems with Waddon, you can try
						refreshing Isleward, restarting your browser/client,
						reinstalling the addon, or restarting your computer.
					</Text>
					<Text>
						If you have found a problem with Waddon or have a
						feature request or other suggestion, please{' '}
						<PlainLink href={ISSUES_URL} external newTab>
							open an issue
						</PlainLink>
						. Check your browser console for errors and include them
						in your report, along with what platform you&apos;re
						playing on and how you&apos;re loading Waddon
						(ViolentMonkey? TamperMonkey? Desktop? Other?).
					</Text>
					<Text>
						Waddon should display a message in the browser console
						when it loads. If there&apos;s no message from Waddon,
						the addon didn&apos;t load properly.
					</Text>
				</Section>
				<Section>
					<SectionHeading>Thanks</SectionHeading>
					<Text>
						Waddon started out as a rewrite of Polfy&apos;s original
						IsleWaddon, for Isleward v0.3.3.
					</Text>
					<Text>
						Special thanks to Polfy and Vildravn for their previous
						addon work. Some of the features in Waddon are rewrites
						and improvements of their addons. Also thanks to Googz,
						Tribrid, Carnagion, Qndel, and others for their
						contributions to the Isleward addon ecosystem. More
						addons can be found at the unofficial{' '}
						<PlainLink
							href="https://gitlab.com/Isleward-Modding/addons"
							external
							newTab
						>
							Isleward Addons group
						</PlainLink>
						.
					</Text>
					<Text>
						Thanks to everyone in the Isleward community and
						everyone who has tested, reported issues, and given
						feedback for Waddon.
					</Text>
					<Text>
						And finally, thanks to You, for playing Isleward and
						using Waddon!
					</Text>
				</Section>
			</Flex>
		</>
	);
};
