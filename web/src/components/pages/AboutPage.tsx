import { Divider, Flex, Heading, HStack, Text } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import React from 'react';
import { PlainLink } from '../atoms/PlainLink';
import { REPORT_ISSUE_URL } from '../molecules/HelpButton';
import { Section } from '../templates/Section';
import { SectionHeading } from '../templates/SectionHeading';

interface AboutPageProps {}

export const AboutPage: React.FC<AboutPageProps> = ({}) => {
	return (
		<>
			<NextSeo title="About" />
			<Heading mb={2} size="lg">
				Islebuilds
			</Heading>
			<HStack mb={2} spacing={4} height="30px">
				<PlainLink href="/news">News</PlainLink>
				<Divider orientation="vertical" />
				<PlainLink href={REPORT_ISSUE_URL} external newTab>
					Report an issue
				</PlainLink>
			</HStack>
			<Flex flexDir="column" rowGap={4}>
				<Section>
					<Text>
						Islebuilds is a companion site for{' '}
						<PlainLink
							external
							newTab
							href="https://play.isleward.com"
						>
							Isleward
						</PlainLink>{' '}
						(
						<PlainLink
							external
							newTab
							href="https://gitlab.com/Isleward/Isleward"
						>
							GitLab
						</PlainLink>
						).
					</Text>
				</Section>
				<Section>
					<SectionHeading>Help</SectionHeading>
					<Text>
						To upload characters to Islebuilds, you will need to
						install and set up Waddon, a client-side addon for
						Isleward. Detailed instructions can be found{' '}
						<PlainLink href="/waddon">here</PlainLink>.
					</Text>
					<Text>
						To report a problem, make a feature request, or give a
						suggestion, please{' '}
						<PlainLink external newTab href={REPORT_ISSUE_URL}>
							open an issue
						</PlainLink>{' '}
						on the Islebuilds issue tracker.
					</Text>
				</Section>
				<Section>
					<SectionHeading>Credits</SectionHeading>
					<Text>
						Isleward, and some parts of Islebuilds, use a pixel font
						called{' '}
						<PlainLink
							external
							newTab
							href="https://masharcade.itch.io/bitty"
						>
							Bitty
						</PlainLink>
						.
					</Text>
				</Section>
			</Flex>
		</>
	);
};
