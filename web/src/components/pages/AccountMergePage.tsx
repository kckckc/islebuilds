import { Button, Center, Flex, Text } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React from 'react';
import {
	useGetMergeInfoQuery,
	useMeQuery,
	usePerformMergeMutation,
} from '../../generated/graphql';
import { CenterLoading } from '../atoms/CenterLoading';
import { PageHeading } from '../atoms/PageHeading';
import { Span } from '../atoms/Span';
import { Section } from '../templates/Section';
import { SectionHeading } from '../templates/SectionHeading';
import { TwoColumnLayout } from '../templates/TwoColumnLayout';

interface AccountMergePageProps {
	mergeId: string;
}

export const AccountMergePage: React.FC<AccountMergePageProps> = ({
	mergeId,
}) => {
	const router = useRouter();

	const { data: meData } = useMeQuery();

	const { data: mergeData } = useGetMergeInfoQuery({
		variables: {
			mergeId,
		},
	});

	const [performMerge] = usePerformMergeMutation();

	if (!mergeData) return <CenterLoading />;

	const left = mergeData?.getMergeInfo?.current;
	const right = mergeData?.getMergeInfo?.other;

	return (
		<>
			<PageHeading>Merge Accounts</PageHeading>
			<Section>
				<Text>
					The connection you tried to link to{' '}
					<Span fontWeight="bold">{meData?.me?.displayName}</Span>{' '}
					already belongs to another account.
				</Text>
				<Text mb={4}>
					You can merge the accounts now if desired. If so, select
					which account will be the main account after merging.
				</Text>

				<TwoColumnLayout
					left={
						<Flex direction="column" gap={2}>
							<Center>
								<SectionHeading href={`/user/${left.id}`}>
									{left.displayName}
								</SectionHeading>
							</Center>
							<Center>
								<Button
									onClick={async () => {
										await performMerge({
											variables: {
												mergeId,
												primaryId: left.id,
											},
										});
										router.push('/account');
									}}
									size="sm"
									colorScheme="teal"
								>
									Merge into {left.displayName}
								</Button>
							</Center>
						</Flex>
					}
					right={
						<Flex direction="column" gap={2}>
							<Center>
								<SectionHeading href={`/user/${right.id}`}>
									{right.displayName}
								</SectionHeading>
							</Center>
							<Center>
								<Button
									onClick={async () => {
										await performMerge({
											variables: {
												mergeId,
												primaryId: right.id,
											},
										});
										router.push('/account');
									}}
									size="sm"
									colorScheme="teal"
								>
									Merge into {right.displayName}
								</Button>
							</Center>
						</Flex>
					}
				/>
			</Section>
		</>
	);
};
