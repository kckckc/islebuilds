import { Box, Center, Spinner } from '@chakra-ui/react';
import React from 'react';
import { useFactionLeaderboardQuery } from '../../../../generated/graphql';
import { humanize } from '../../../../util/humanize';
import { getFactionName } from '../../../atoms/FactionName';
import { PageHeading } from '../../../atoms/PageHeading';
import { BackLink } from '../../../molecules/BackLink';
import { FactionCharacterList } from './FactionCharacterList';

interface FactionLeaderboardPageProps {
	factionId: string;
}

export const FactionLeaderboardPage: React.FC<FactionLeaderboardPageProps> = ({
	factionId,
}) => {
	const { data, loading, error } = useFactionLeaderboardQuery({
		variables: {
			faction: factionId as string,
		},
	});

	if (loading) {
		return (
			<Center mt={8}>
				<Spinner />
			</Center>
		);
	} else if (error || !data?.factionLeaderboard) {
		return <Box>error</Box>;
	}

	const { lastUpdated, characters } = data.factionLeaderboard;

	return (
		<>
			<PageHeading>
				{getFactionName(factionId as string) +
					' Reputation Leaderboard'}
			</PageHeading>
			<Box my={2}>
				<BackLink href="/leaderboard">All Leaderboards</BackLink>
			</Box>

			<Box suppressHydrationWarning={true}>
				Updated {humanize(lastUpdated)}
			</Box>
			<Box mt={4}>
				<FactionCharacterList
					factionId={factionId}
					characters={characters}
				/>
			</Box>
		</>
	);
};
