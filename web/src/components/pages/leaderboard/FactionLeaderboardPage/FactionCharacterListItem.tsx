import { Box, Flex, Heading, Progress } from '@chakra-ui/react';
import React from 'react';
import { FactionLeaderboardQuery } from '../../../../generated/graphql';
import { useIsMobile } from '../../../../hooks/useIsMobile';
import { LevelTag } from '../../../atoms/LevelTag';
import { SelfTag } from '../../../atoms/SelfTag';
import { SkinDisplay } from '../../../atoms/SkinDisplay';
import { SpiritTag } from '../../../atoms/SpiritTag';
import { SectionHeading } from '../../../templates/SectionHeading';

type CharType = FactionLeaderboardQuery['factionLeaderboard']['characters'][0];

interface FactionCharacterListItemProps {
	character: CharType;
	factionId: string;
	rank: number;
	topRep: number;
}

export const FactionCharacterListItem: React.FC<
	FactionCharacterListItemProps
> = ({ character, factionId, rank, topRep }) => {
	const isMobile = useIsMobile();

	const right = (
		<Flex flexDirection="row" columnGap={1}>
			<SelfTag userId={character.userId} />
			<LevelTag {...character} />
			<SpiritTag {...character} />
		</Flex>
	);

	const rep = character[('rep_' + factionId) as keyof CharType] as number;

	let headingSize = 'md';

	if (isMobile) {
		headingSize = 'sm';
		if (rank <= 3) {
			headingSize = 'lg';
		} else if (rank <= 10) {
			headingSize = 'md';
		}
	} else {
		if (rank <= 3) {
			headingSize = 'xl';
		} else if (rank <= 10) {
			headingSize = 'lg';
		}
	}

	return (
		<Flex flexDirection="row" alignItems="center">
			<Heading
				size={headingSize}
				mr={4}
				textAlign="right"
				whiteSpace="nowrap"
				w={isMobile ? '30px' : '50px'}
			>
				#{rank}
			</Heading>
			<Flex
				flex={1}
				key={character.id}
				p={2}
				shadow="md"
				borderWidth="1px"
				rounded="md"
			>
				<Box flex={1}>
					<Flex direction="row" gap={2}>
						<SkinDisplay
							skinCell={character.skinCell}
							skinSheetName={character.skinSheetName}
							size="sm"
						/>
						<Flex
							flex={1}
							direction={{ base: 'column', md: 'row' }}
							alignItems={{ base: 'flex-start', md: 'center' }}
							gap={1}
						>
							<Box minW={{ base: 0, md: '220px' }}>
								<SectionHeading
									href={`/character/${character.name}`}
									fontSize={{
										base: 'md',
										sm: 'lg',
										md: 'xl',
									}}
								>
									{character.name}
								</SectionHeading>
							</Box>
							{/* <Box flex={{ base: 1, md: 'none' }} /> */}
							<Box flex={{ base: 'none', md: 1 }}>
								<SectionHeading
									fontSize={{
										base: 'md',
										sm: 'lg',
										md: 'xl',
									}}
								>
									{rep} rep
								</SectionHeading>
							</Box>
							{right}
						</Flex>
					</Flex>
					<Progress
						mt={2}
						value={(rep / topRep) * 100}
						size="xs"
						colorScheme="purple"
					/>
				</Box>
			</Flex>
		</Flex>
	);
};
