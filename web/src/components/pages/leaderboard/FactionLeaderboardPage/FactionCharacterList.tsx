import { Flex } from '@chakra-ui/react';
import React from 'react';
import { FactionLeaderboardQuery } from '../../../../generated/graphql';
import { FactionCharacterListItem } from './FactionCharacterListItem';

interface FactionCharacterListProps {
	characters: FactionLeaderboardQuery['factionLeaderboard']['characters'];
	factionId: string;
}

export const FactionCharacterList: React.FC<FactionCharacterListProps> = ({
	characters,
	factionId,
}) => {
	const topChar = characters[0];
	const prop = `rep_${factionId}`;
	let topRep = 0;
	if (topChar) {
		topRep = topChar[prop as keyof typeof topChar] as number;
	}

	return (
		<Flex flexDirection="column" rowGap={2}>
			{characters.map((c, i) => (
				<FactionCharacterListItem
					key={c.id}
					factionId={factionId}
					character={c}
					rank={i + 1}
					topRep={topRep}
				/>
			))}
		</Flex>
	);
};
