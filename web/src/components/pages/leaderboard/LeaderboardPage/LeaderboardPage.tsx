import { Flex, Text } from '@chakra-ui/react';
import React from 'react';
import { FactionHeading } from '../../../atoms/FactionHeading';
import { PageHeading } from '../../../atoms/PageHeading';
import { Section } from '../../../templates/Section';
import { SectionHeading } from '../../../templates/SectionHeading';
import { TwoColumnLayout } from '../../../templates/TwoColumnLayout';

interface LeaderboardPageProps {}

export const LeaderboardPage: React.FC<LeaderboardPageProps> = ({}) => {
	return (
		<>
			<PageHeading>Leaderboards</PageHeading>
			<Section>
				<SectionHeading>Faction Reputation</SectionHeading>
				<Text>
					Top 100 characters with the most reputation in each faction.
				</Text>
				<TwoColumnLayout
					left={
						<Flex flexDirection="column" rowGap={2}>
							{['gaekatla', 'fjolgard', 'akarei'].map((f) => (
								<FactionHeading factionId={f} key={f} />
							))}
						</Flex>
					}
					right={
						<Flex flexDirection="column" rowGap={2}>
							{[
								'peopleOfTheSun',
								'pumpkinSailor',
								'theWinterMan',
							].map((f) => (
								<FactionHeading factionId={f} key={f} />
							))}
						</Flex>
					}
				/>
			</Section>
		</>
	);
};
