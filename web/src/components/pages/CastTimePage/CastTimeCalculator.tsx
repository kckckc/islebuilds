import { chakra } from '@chakra-ui/react';
import React from 'react';

const COLORS = [
	'#57BB8A',
	'#8FC47E',
	'#C7CD72',
	'#FFD666',
	'#F7B96A',
	'#EF9B6E',
	'#E67C73',
];

interface CastTimeCalculatorProps {}

export const CastTimeCalculator: React.FC<CastTimeCalculatorProps> = ({}) => {
	const maxCastTime = 7;

	const Row = ({ n }: { n: number }) => (
		<chakra.tr>
			<chakra.th textAlign="center" width="calc(100% / 52)">
				{n}
			</chakra.th>
			{new Array(51).fill(null).map((_, i) => {
				let value = Math.ceil(n * (1 - 0.01 * i));
				return (
					<chakra.td
						key={i}
						textAlign="center"
						width="calc(100% / 52)"
						backgroundColor={COLORS[value]}
					>
						{value}
					</chakra.td>
				);
			})}
		</chakra.tr>
	);

	return (
		<chakra.table
			style={{
				tableLayout: 'fixed',
			}}
		>
			<thead>
				<tr>
					<chakra.th
						textAlign="center"
						width="calc(100% / 52)"
					></chakra.th>
					{new Array(51).fill(null).map((_, i) => (
						<chakra.th
							textAlign="center"
							width="calc(100% / 52)"
							key={i}
						>
							{i}
						</chakra.th>
					))}
				</tr>

				{new Array(maxCastTime).fill(null).map((_, i) => (
					<Row key={i} n={i}></Row>
				))}
			</thead>
		</chakra.table>
	);
};
