import { Box, Text } from '@chakra-ui/react';
import React from 'react';
import { PageHeading } from '../../atoms/PageHeading';
import { BackLink } from '../../molecules/BackLink';
import { Section } from '../../templates/Section';
import { CastTimeCalculator } from './CastTimeCalculator';

interface CastTimePageProps {}

export const CastTimePage: React.FC<CastTimePageProps> = ({}) => {
	return (
		<>
			<PageHeading>Cast Time</PageHeading>
			<Box my={2}>
				<BackLink href="/tools">Tools</BackLink>
			</Box>

			<Section>
				<Text mb={8}>
					Each row is a cast time in ticks. The columns correspond to
					the percent bonus cast speed or attack speed that a
					character has. The cells are the adjusted cast times.
				</Text>

				<CastTimeCalculator />
			</Section>
		</>
	);
};
