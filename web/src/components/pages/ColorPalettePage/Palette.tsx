import { Box, Divider, Flex } from '@chakra-ui/react';
import React from 'react';
import { PlainLink } from '../../atoms/PlainLink';
import { TwoColumnLayout } from '../../templates/TwoColumnLayout';
import { ColorDisplay } from './ColorDisplay';

interface PaletteProps {}

export const Palette: React.FC<PaletteProps> = ({}) => {
	return (
		<>
			<Box>Click a color tile to copy its hex color.</Box>
			<Box>
				You can also see the original palette image{' '}
				<PlainLink
					href="https://gitlab.com/Isleward/isleward/-/blob/master/src/client/images/palette.png"
					external
					newTab
				>
					here
				</PlainLink>
				.
			</Box>
			<Divider my={4} />
			<TwoColumnLayout
				left={
					<Flex flexDirection="column" rowGap={2}>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay name="redA" color="#ff4252" />
							<ColorDisplay name="redB" color="#d43346" />
							<ColorDisplay name="redC" color="#a82841" />
							<ColorDisplay name="redD" color="#802343" />
						</Flex>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay
								name="orangeA"
								extra="legendary"
								color="#ff6942"
							/>
							<ColorDisplay name="orangeB" color="#db5538" />
							<ColorDisplay name="orangeC" color="#b34b3a" />
							<ColorDisplay name="orangeD" color="#953f36" />
						</Flex>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay name="brownA" color="#e39a30" />
							<ColorDisplay name="brownB" color="#ca752f" />
							<ColorDisplay name="brownC" color="#b15a30" />
							<ColorDisplay name="brownD" color="#763b3b" />
						</Flex>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay name="yellowA" color="#ffeb38" />
							<ColorDisplay name="yellowB" color="#faac45" />
							<ColorDisplay name="yellowC" color="#d07840" />
							<ColorDisplay name="yellowD" color="#9a5a3c" />
						</Flex>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay name="greenA" color="#80f643" />
							<ColorDisplay
								name="greenB"
								extra="magic"
								color="#4ac441"
							/>
							<ColorDisplay name="greenC" color="#386646" />
							<ColorDisplay name="greenD" color="#2b4b3e" />
						</Flex>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay name="tealA" color="#baffd7" />
							<ColorDisplay name="tealB" color="#51fc9a" />
							<ColorDisplay name="tealC" color="#44cb95" />
							<ColorDisplay name="tealD" color="#3f8d6d" />
						</Flex>
					</Flex>
				}
				right={
					<Flex flexDirection="column" rowGap={2}>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay name="blueA" color="#48edff" />
							<ColorDisplay
								name="blueB"
								extra="rare"
								color="#3fa7dd"
							/>
							<ColorDisplay name="blueC" color="#3a71ba" />
							<ColorDisplay name="blueD" color="#42548d" />
						</Flex>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay
								name="purpleA"
								extra="epic"
								color="#a24eff"
							/>
							<ColorDisplay name="purpleB" color="#7a3ad3" />
							<ColorDisplay name="purpleC" color="#533399" />
							<ColorDisplay name="purpleD" color="#393268" />
						</Flex>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay name="pinkA" color="#fc66f7" />
							<ColorDisplay name="pinkB" color="#de43ae" />
							<ColorDisplay name="pinkC" color="#b4347a" />
							<ColorDisplay name="pinkD" color="#933159" />
						</Flex>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay
								name="grayA"
								extra="common"
								color="#fcfcfc"
							/>
							<ColorDisplay name="grayB" color="#c0c3cf" />
							<ColorDisplay name="grayC" color="#929398" />
							<ColorDisplay name="grayD" color="#69696e" />
						</Flex>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay name="blackA" color="#505360" />
							<ColorDisplay name="blackB" color="#3c3f4c" />
							<ColorDisplay name="blackC" color="#373041" />
							<ColorDisplay name="blackD" color="#312136" />
						</Flex>
						<Flex maxW="450px" flexDirection="row" columnGap={2}>
							<ColorDisplay name="black" color="#2d2136" />
							<ColorDisplay name="white" color="#fcfcfc" />
							<Box flex={1} />
							<Box flex={1} />
						</Flex>
					</Flex>
				}
			/>
		</>
	);
};
