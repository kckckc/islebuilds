import { Box } from '@chakra-ui/react';
import React from 'react';
import { PageHeading } from '../../atoms/PageHeading';
import { BackLink } from '../../molecules/BackLink';
import { Palette } from './Palette';
import { Section } from '../../templates/Section';

interface ColorPalettePageProps {}

export const ColorPalettePage: React.FC<ColorPalettePageProps> = ({}) => {
	return (
		<>
			<PageHeading>Color Palette</PageHeading>
			<Box my={2}>
				<BackLink href="/tools">Tools</BackLink>
			</Box>

			<Section>
				<Palette />
			</Section>
		</>
	);
};
