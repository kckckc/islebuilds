import { Center, Tag, useClipboard, VStack } from '@chakra-ui/react';
import React, { useState } from 'react';

interface ColorDisplayProps {
	name: string;
	extra?: string;
	color: string;
}

export const ColorDisplay: React.FC<ColorDisplayProps> = ({
	name,
	extra,
	color,
}) => {
	const [hover, setHover] = useState(false);
	const { hasCopied, onCopy } = useClipboard(color);

	return (
		<Center
			onMouseEnter={() => setHover(true)}
			onMouseLeave={() => setHover(false)}
			onClick={onCopy}
			sx={{ aspectRatio: '1/1' }}
			flex={1}
			backgroundColor={color}
			cursor="pointer"
		>
			<VStack>
				<Tag size="sm" backgroundColor="gray.200" textColor="black">
					{hasCopied ? 'Copied' : hover ? color : name}
				</Tag>
				{extra && (
					<Tag size="sm" backgroundColor="gray.200" textColor="black">
						{extra}
					</Tag>
				)}
			</VStack>
		</Center>
	);
};
