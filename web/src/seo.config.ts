const SEO = {
	titleTemplate: '%s | Islebuilds',
	defaultTitle: 'Islebuilds',
	description: 'Companion site for Isleward.',
};

export default SEO;
