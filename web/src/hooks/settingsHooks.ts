import { useBooleanCookie, useCookie } from './useCookie';

export const useQualityIndicators = () => {
	return useCookie('item-quality-indicators', 'bottom');
};

export const useInventorySettings = () => {
	const [compact, setCompact] = useBooleanCookie('inventory-compact', false);
	const [limitWidth, setLimitWidth] = useBooleanCookie(
		'inventory-limit-width',
		false
	);
	const [hideEquipped, setHideEquipped] = useBooleanCookie(
		'inventory-hide-equipped',
		false
	);
	const [hideFiltered, setHideFiltered] = useBooleanCookie(
		'inventory-hide-filtered',
		true
	);
	const [smallItems, setSmallItems] = useBooleanCookie(
		'inventory-small-items',
		true
	);

	return {
		compact,
		setCompact,
		limitWidth,
		setLimitWidth,
		hideEquipped,
		setHideEquipped,
		hideFiltered,
		setHideFiltered,
		smallItems,
		setSmallItems,
	};
};
