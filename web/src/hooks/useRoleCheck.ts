import { useMeQuery } from '../generated/graphql';

export const checkRoles = (roles: string[], required: string[]) => {
	if (roles.includes('*')) {
		return true;
	}

	if (roles.includes('ADMIN') && !roles.includes('MODERATION')) {
		roles.push('MODERATION');
	}

	if (required.some((r) => !roles.includes(r))) {
		return false;
	}

	return true;
};

export const useRoleCheck = (required: string | string[]) => {
	const { data } = useMeQuery();

	const roles = data?.me?.roles ?? [];

	const requiredList = Array.isArray(required) ? required : [required];

	return checkRoles(roles, requiredList);
};
