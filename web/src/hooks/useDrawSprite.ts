import { DeepPartial } from '@chakra-ui/react';
import { MutableRefObject, Ref, useEffect } from 'react';
import { Sprite } from '../generated/graphql';
import { getPaletteColor } from '../util/getPaletteColor';

export const useDrawSprite = (
	canvasRef: MutableRefObject<HTMLCanvasElement | null>,
	sprite?: DeepPartial<Sprite>
) => {
	useEffect(() => {
		const canvas = canvasRef.current;
		if (!canvas || !sprite) return;

		if (!sprite.size || !sprite.pixels) return;
		const { size } = sprite;
		const pixels = JSON.parse(sprite.pixels);

		const mult = 4;

		canvas.width = size * mult;
		canvas.height = size * mult;

		const context = canvas.getContext('2d');
		if (!context) return;

		context?.clearRect(0, 0, canvas.width, canvas.height);

		for (let y = 0; y < size; y++) {
			for (let x = 0; x < size; x++) {
				const color = getPaletteColor(pixels[y][x]);
				if (!color) continue;
				context.fillStyle = color;
				context.fillRect(x * mult, y * mult, mult, mult);
			}
		}
	}, [canvasRef, sprite]);
};
