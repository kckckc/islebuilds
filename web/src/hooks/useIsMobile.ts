import { useBreakpointValue } from '@chakra-ui/react';

export const useIsMobile = () => {
	return (
		// Default to false during SSR
		useBreakpointValue(
			{ base: true, md: false },

			// Fix flickering
			{ ssr: typeof document === 'undefined' }
		) ?? false
	);
};
