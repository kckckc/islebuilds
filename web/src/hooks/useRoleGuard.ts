import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useMeQuery } from '../generated/graphql';
import { checkRoles } from './useRoleCheck';

export const useRoleGuard = (required: string | string[]) => {
	const { data, loading } = useMeQuery();
	const router = useRouter();

	const [allowed, setAllowed] = useState(false);

	useEffect(() => {
		if (loading) return;

		const roles = data?.me?.roles ?? [];

		const requiredList = Array.isArray(required) ? required : [required];

		if (!checkRoles(roles, requiredList)) {
			router.replace('/');
		} else {
			setAllowed(true);
		}
	}, [data, loading, required, router]);

	return allowed;
};
