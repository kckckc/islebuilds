import { useEffect, useState } from 'react';

export const useCurrentTime = (interval = 5000) => {
	const [time, setTime] = useState(Date.now());

	useEffect(() => {
		const timer = setInterval(() => {
			setTime(Date.now());
		}, interval);

		return () => {
			clearInterval(timer);
		};
	}, [interval]);

	return {
		timestamp: time,
	};
};
