import { getCookie, setCookie } from 'cookies-next';
import React, { createContext, useContext, useEffect, useState } from 'react';

export const CookiesContext = createContext<Record<string, string>>({});
export const CookiesProvider = CookiesContext.Provider;

const cookieListeners: Record<string, ((value: string) => void)[]> = {};

export const useCookie = (key: string, defaultValue: string) => {
	const ssrCookies = useContext(CookiesContext);

	const [state, setState] = useState<string>(() => {
		// We need this to prevent hydration mismatches when SSRing
		if (typeof window === 'undefined') {
			return ssrCookies[key] ?? defaultValue;
		}

		return getCookie(key)?.toString() ?? defaultValue;
	});

	// Listen for other instances of this hook setting its value
	useEffect(() => {
		const listener = (value: string) => {
			setState(value);
		};

		if (!cookieListeners[key]) {
			cookieListeners[key] = [];
		}

		cookieListeners[key].push(listener);

		return () => {
			cookieListeners[key] = cookieListeners[key].filter(
				(l) => l !== listener
			);
		};
	});

	const setStateAndPublish = (action: React.SetStateAction<string>) => {
		const newValue = typeof action === 'function' ? action(state) : action;

		setCookie(key, newValue);

		cookieListeners[key].forEach((listener) => listener(newValue));
	};

	return [state, setStateAndPublish] as const;
};

export const useBooleanCookie = (key: string, defaultValue: boolean) => {
	const [cookie, updateCookie] = useCookie(key, defaultValue.toString());

	const value =
		cookie === 'true' ? true : cookie === 'false' ? false : defaultValue;

	const setValue = (action: React.SetStateAction<boolean>) => {
		const newValue = typeof action === 'function' ? action(value) : action;

		updateCookie(newValue.toString());
	};

	return [value, setValue] as const;
};
