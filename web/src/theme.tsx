import { extendTheme } from '@chakra-ui/react';

const fonts = { mono: `'Menlo', monospace` };

const breakpoints = {
	// sm: '40em',
	// md: '52em',
	// lg: '64em',
	// xl: '80em',
	sm: '320px', // mobile
	md: '768px', // tablet
	lg: '960px', // small screen
	xl: '1200px', // large screen
	'2xl': '1536px', // extra large screen
};

const config = {
	semanticTokens: {
		colors: {
			heroGradientStart: {
				default: '#3fa7dd',
				_dark: '#48edff',
			},
			heroGradientEnd: {
				default: '#4ac441',
				_dark: '#fc66f7',
			},
			q0: '#fcfcfc', //@white;
			q1: '#4ac441', //@greenB;
			q2: '#3fa7dd', //@blueB;
			q3: '#a24eff', //@purpleA;
			q4: '#ff6942', //@orangeA;
		},
		radii: {
			button: '12px',
		},
	},
	colors: {
		black: '#16161D',
	},
	fonts,
	breakpoints,
};

const theme = extendTheme(config);

export default theme;
