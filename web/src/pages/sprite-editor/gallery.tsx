import { NextPage } from 'next';
import { SpriteGalleryPage } from '../../components/pages/SpriteGalleryPage';
import { SinglePageLayout } from '../../components/templates/SinglePageLayout';
import { withLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return (
		<>
			<SpriteGalleryPage />
		</>
	);
};

export default withLayout(SinglePageLayout)(Page);
