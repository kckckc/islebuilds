import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { SpriteDetailsPage } from '../../components/pages/SpriteDetailsPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	const router = useRouter();
	const { id } = router.query;

	return id ? <SpriteDetailsPage id={id as string} /> : null;
};

export default withDefaultLayout(Page);
