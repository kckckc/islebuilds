import { NextPage } from 'next';
import { SpriteEditor } from '../../components/organisms/SpriteEditor';
import { SinglePageLayout } from '../../components/templates/SinglePageLayout';
import { withLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return (
		<>
			<SpriteEditor />
		</>
	);
};

export default withLayout(SinglePageLayout)(Page);
