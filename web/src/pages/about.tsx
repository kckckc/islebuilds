import { NextPage } from 'next';
import { AboutPage } from '../components/pages/AboutPage';
import { withDefaultLayout } from '../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <AboutPage />;
};

export default withDefaultLayout(Page);
