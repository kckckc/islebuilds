import { NextPage } from 'next';
import { MyProfilePage } from '../../components/pages/MyProfilePage';
import { useAuth } from '../../hooks/useAuth';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	useAuth();

	return <MyProfilePage />;
};

export default withDefaultLayout(Page);
