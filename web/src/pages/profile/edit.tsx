import { NextPage } from 'next';
import { EditProfilePage } from '../../components/pages/EditProfilePage';
import { useAuth } from '../../hooks/useAuth';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	useAuth();

	return <EditProfilePage />;
};

export default withDefaultLayout(Page);
