import { NextPage } from 'next';
import { AccountPage } from '../../components/pages/AccountPage';
import { useAuth } from '../../hooks/useAuth';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	useAuth();

	return <AccountPage />;
};

export default withDefaultLayout(Page);
