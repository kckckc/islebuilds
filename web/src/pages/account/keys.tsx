import { NextPage } from 'next';
import { AccountKeysPage } from '../../components/pages/AccountKeysPage';
import { useAuth } from '../../hooks/useAuth';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	useAuth();

	return <AccountKeysPage />;
};

export default withDefaultLayout(Page);
