import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { AccountMergePage } from '../../../components/pages/AccountMergePage';
import { withDefaultLayout } from '../../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	const router = useRouter();
	const { mergeId } = router.query;

	return <AccountMergePage mergeId={mergeId as string} />;
};

export default withDefaultLayout(Page);
