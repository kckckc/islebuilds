import { NextPage } from 'next';
import { LeaderboardPage } from '../../components/pages/leaderboard/LeaderboardPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <LeaderboardPage />;
};

export default withDefaultLayout(Page);
