import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { FactionLeaderboardPage } from '../../../components/pages/leaderboard/FactionLeaderboardPage';
import { withDefaultLayout } from '../../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	const router = useRouter();
	const { factionId } = router.query;

	// Redirect if invalid
	if (
		typeof window !== 'undefined' &&
		factionId &&
		![
			'gaekatla',
			'fjolgard',
			'akarei',
			'peopleOfTheSun',
			'pumpkinSailor',
			'theWinterMan',
		].includes(factionId as string)
	) {
		router.push('/leaderboard');
	}

	return <FactionLeaderboardPage factionId={factionId as string} />;
};

export default withDefaultLayout(Page);
