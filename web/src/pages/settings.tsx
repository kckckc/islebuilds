import { NextPage } from 'next';
import { SettingsPage } from '../components/pages/SettingsPage';
import { withDefaultLayout } from '../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <SettingsPage />;
};

export default withDefaultLayout(Page);
