import { Flex } from '@chakra-ui/react';
import { NextPage } from 'next';
import { ReactNode } from 'react';
import { Footer } from '../components/organisms/Footer';
import { NavBar } from '../components/organisms/NavBar';
import { InventoryPage } from '../components/pages/InventoryPage';
import { withLayout } from '../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <InventoryPage />;
};

const Layout: React.FC<{ children?: ReactNode }> = ({ children }) => (
	<Flex direction="column" h="100%">
		<NavBar />
		<Flex direction="column" flex={1} p={8}>
			{children}
		</Flex>
		<Footer />
	</Flex>
);

export default withLayout(Layout)(Page);
