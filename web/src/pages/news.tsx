import { Flex } from '@chakra-ui/react';
import { NextPage } from 'next';
import { PageHeading } from '../components/atoms/PageHeading';
import { NewsPost } from '../components/molecules/NewsPost';
import { newsData } from '../static/newsData';
import { withDefaultLayout } from '../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return (
		<>
			<PageHeading>News</PageHeading>
			<Flex mb={32} flexDirection="column" rowGap={8}>
				{newsData.map((p, idx) => (
					<NewsPost key={idx} post={p} />
				))}
			</Flex>
		</>
	);
};

export default withDefaultLayout(Page);
