import { parse as parseCookies } from 'cookie';
import NextDocument, {
	DocumentContext,
	DocumentInitialProps,
	Head,
	Html,
	Main,
	NextScript,
} from 'next/document';
import theme from '../theme';

const isProd = process.env.NODE_ENV === 'production';

type DocumentProps = DocumentInitialProps & {
	colorMode: string;
	measurementId: string;
	apiUrl: string;
};

export default class MyDocument extends NextDocument<DocumentProps> {
	// Adding getInitialProps to the base document will disable Next.js's Automatic Static Optimization
	// so we will be doing SSR on every request, but this is probably necessary to fix dark mode flash
	// and render the username in the navbar and so on
	static async getInitialProps(ctx: DocumentContext): Promise<DocumentProps> {
		const initialProps = await NextDocument.getInitialProps(ctx);

		// We needed this in addition to the colorModeManager in _app to fix color mode flashing
		let colorMode: string = 'dark';
		if (ctx.req && ctx.req.headers.cookie) {
			colorMode =
				parseCookies(ctx.req.headers.cookie)['chakra-ui-color-mode'] ??
				theme.config.initialColorMode;
		}

		const measurementId = process.env.APP_MEASUREMENT_ID ?? '';
		const apiUrl = process.env.APP_API_URL ?? '';

		return { ...initialProps, colorMode, measurementId, apiUrl };
	}

	render() {
		const { colorMode, measurementId, apiUrl } = this.props;

		return (
			<Html data-theme={colorMode}>
				<Head>
					{/* Google Analytics */}
					{isProd && (
						<>
							<script
								async
								src={`https://www.googletagmanager.com/gtag/js?id=${measurementId}`}
							/>
							<script
								// eslint-disable-next-line react/no-danger
								dangerouslySetInnerHTML={{
									__html: `
										window.dataLayer = window.dataLayer || [];
										function gtag(){dataLayer.push(arguments);}
										gtag('js', new Date());
										gtag('config', '${measurementId}', {
											page_path: window.location.pathname,
										});
									`,
								}}
							/>
						</>
					)}

					{/* hack */}
					<script
						// eslint-disable-next-line react/no-danger
						dangerouslySetInnerHTML={{
							__html: `/* oh? publicRuntimeConfig is deprecated? */ window.apiUrl = '${apiUrl}';`,
						}}
					/>

					{/* Icons */}
					<link rel="icon" type="image/x-icon" href="/favicon.ico" />
					<link
						rel="apple-touch-icon"
						sizes="180x180"
						href="/apple-touch-icon.png"
					/>
					<link
						rel="icon"
						type="image/png"
						sizes="32x32"
						href="/favicon-32x32.png"
					/>
					<link
						rel="icon"
						type="image/png"
						sizes="16x16"
						href="/favicon-16x16.png"
					/>
					<link rel="manifest" href="/site.webmanifest" />
					<link
						rel="mask-icon"
						href="/safari-pinned-tab.svg"
						color="#5bbad5"
					/>
					<meta name="msapplication-TileColor" content="#2d2136" />
					<meta name="theme-color" content="#a24eff" />
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}
