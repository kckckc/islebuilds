import { NextPage } from 'next';
import { TopRuneLoadoutsPage } from '../../components/pages/TopRuneLoadoutsPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <TopRuneLoadoutsPage />;
};

export default withDefaultLayout(Page);
