import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { RuneLoadoutPage } from '../../components/pages/RuneLoadoutPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	const router = useRouter();
	const { rawRunes } = router.query;

	const runes = (rawRunes as string).split(',');

	return <RuneLoadoutPage runes={runes} />;
};

export default withDefaultLayout(Page);
