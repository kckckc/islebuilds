import { Box } from '@chakra-ui/react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { UserSpritesPage } from '../../../components/pages/UserSpritesPage';
import { useUserQuery } from '../../../generated/graphql';
import { withDefaultLayout } from '../../../util/withLayout';
import { CenterLoading } from '../../../components/atoms/CenterLoading';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	const router = useRouter();
	const { userId } = router.query;

	const { data, loading, error } = useUserQuery({
		variables: {
			id: userId as string,
		},
	});

	if (loading) {
		return <CenterLoading />;
	} else if (error || !data?.user) {
		return <Box>error</Box>;
	}

	return <UserSpritesPage user={data.user} />;
};

export default withDefaultLayout(Page);
