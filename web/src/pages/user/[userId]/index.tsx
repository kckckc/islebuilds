import { Box } from '@chakra-ui/react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { CenterLoading } from '../../../components/atoms/CenterLoading';
import { UserProfile } from '../../../components/organisms/UserProfile';
import { useUserQuery } from '../../../generated/graphql';
import { withDefaultLayout } from '../../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	const router = useRouter();
	const { userId } = router.query;

	const { data, loading, error } = useUserQuery({
		variables: {
			id: userId as string,
		},
	});

	if (loading) {
		return <CenterLoading />;
	} else if (error || !data?.user) {
		return <Box>error</Box>;
	}

	return <UserProfile user={data.user} />;
};

export default withDefaultLayout(Page);
