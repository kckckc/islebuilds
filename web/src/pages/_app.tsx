import { ApolloClient, ApolloProvider } from '@apollo/client';
import {
	ChakraProvider,
	cookieStorageManager,
	cookieStorageManagerSSR,
	localStorageManager,
} from '@chakra-ui/react';
import { parse as parseCookies } from 'cookie';
import { DefaultSeo } from 'next-seo';
import App, { AppProps } from 'next/app';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import '../global.css';
import { CookiesProvider } from '../hooks/useCookie';
import { useScrollRestoration } from '../hooks/useScrollRestoration';
import SEO from '../seo.config';
import theme from '../theme';
import { withApollo } from '../util/apollo';
import * as gtag from '../util/gtag';

const isProd = process.env.NODE_ENV === 'production';

type MyAppProps = AppProps & {
	apollo: ApolloClient<any>;
	measurementId: string;
};

type AppType = (({ Component, pageProps }: MyAppProps) => JSX.Element) & {
	getInitialProps?: (ctx: any) => any;
};

const MyApp: AppType = ({
	Component,
	pageProps,
	apollo,
	measurementId,
}: MyAppProps) => {
	const router = useRouter();

	// Restore scroll position
	useScrollRestoration(router);

	// For SEO
	const canonicalUrl = (
		`https://islebuilds.com` + (router.asPath === '/' ? '' : router.asPath)
	).split('?')[0];

	// Analytics
	useEffect(() => {
		const handleRouteChange = (url: URL) => {
			if (isProd) {
				gtag.pageview(measurementId, url);
			}
		};

		router.events.on('routeChangeComplete', handleRouteChange);
		return () => {
			router.events.off('routeChangeComplete', handleRouteChange);
		};
	}, [router.events, measurementId]);

	// Fix flash with SSR + dark mode
	const colorModeManager =
		typeof pageProps.cookieString === 'string'
			? cookieStorageManagerSSR(pageProps.cookieString)
			: cookieStorageManager;

	return (
		<>
			<DefaultSeo {...SEO} canonical={canonicalUrl} />
			<ApolloProvider client={apollo}>
				<ChakraProvider
					resetCSS
					theme={theme}
					colorModeManager={colorModeManager}
				>
					<CookiesProvider value={pageProps.cookies ?? {}}>
						<Component {...pageProps} />
					</CookiesProvider>
				</ChakraProvider>
			</ApolloProvider>
		</>
	);
};

MyApp.getInitialProps = async (appContext: any) => {
	const appProps = await App.getInitialProps(appContext);

	const ctx = appContext.ctx;

	const cookieString: string = ctx?.req?.headers?.cookie ?? '';
	const cookies = parseCookies(cookieString);

	const measurementId = process.env.APP_MEASUREMENT_ID ?? '';

	return {
		...appProps,

		pageProps: {
			...appProps.pageProps,
			cookieString,
			cookies,
		},

		measurementId,
	};
};

export function reportWebVitals({
	id,
	name,
	label,
	value,
}: {
	id: string;
	name: string;
	label: string;
	value: number;
}) {
	if (!isProd) return;

	window.gtag('event', name, {
		event_category:
			label === 'web-vital' ? 'Web Vitals' : 'Next.js custom metric',
		value: Math.round(name === 'CLS' ? value * 1000 : value),
		event_label: id,
		non_interaction: true,
	});
}

export default withApollo(MyApp);
