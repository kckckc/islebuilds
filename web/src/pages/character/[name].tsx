import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { CharacterPage } from '../../components/pages/CharacterPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	const router = useRouter();
	const { name } = router.query;

	return name ? (
		<CharacterPage name={(name as string).toLowerCase()} />
	) : null;
};

export default withDefaultLayout(Page);
