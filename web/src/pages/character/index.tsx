import { NextPage } from 'next';
import React from 'react';
import { CharacterSearchPage } from '../../components/pages/CharacterSearchPage';
import { DefaultLayout } from '../../components/templates/DefaultLayout';
import { LayoutProps, withLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <CharacterSearchPage />;
};

const PageLayout: React.FC<LayoutProps> = ({ children }) => (
	<DefaultLayout noSearch>{children}</DefaultLayout>
);

export default withLayout(PageLayout)(Page);
