import { Box } from '@chakra-ui/react';
import { NextPage } from 'next';
import { PageHeading } from '../../components/atoms/PageHeading';
import { BackLink } from '../../components/molecules/BackLink';
import { ReputationCalculator } from '../../components/organisms/ReputationCalculator';
import { Section } from '../../components/templates/Section';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return (
		<>
			<PageHeading>Reputation Calculator</PageHeading>
			<Box my={2}>
				<BackLink href="/tools">Tools</BackLink>
			</Box>

			<Section>
				<ReputationCalculator />
			</Section>
		</>
	);
};

export default withDefaultLayout(Page);
