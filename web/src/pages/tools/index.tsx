import { NextPage } from 'next';
import { PageHeading } from '../../components/atoms/PageHeading';
import { ToolsList } from '../../components/organisms/ToolsList';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return (
		<>
			<PageHeading>Tools</PageHeading>

			<ToolsList />
		</>
	);
};

export default withDefaultLayout(Page);
