import { NextPage } from 'next';
import { ColorPalettePage } from '../../components/pages/ColorPalettePage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <ColorPalettePage />;
};

export default withDefaultLayout(Page);
