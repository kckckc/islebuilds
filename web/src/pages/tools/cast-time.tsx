import { NextPage } from 'next';
import { CastTimePage } from '../../components/pages/CastTimePage';
import { SinglePageLayout } from '../../components/templates/SinglePageLayout';
import { withLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <CastTimePage />;
};

export default withLayout(SinglePageLayout)(Page);
