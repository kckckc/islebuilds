import { NextPage } from 'next';
import { QQCalculatorPage } from '../../components/pages/QQCalculator/QQCalculatorPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <QQCalculatorPage />;
};

export default withDefaultLayout(Page);
