import { Box, Flex } from '@chakra-ui/react';
import { NextPage } from 'next';
import { PageHeading } from '../../components/atoms/PageHeading';
import { BackLink } from '../../components/molecules/BackLink';
import { IdolChanceCalculator } from '../../components/organisms/IdolCalculators/IdolChanceCalculator';
import { IdolTypeCalculator } from '../../components/organisms/IdolCalculators/IdolTypeCalculator';
import { Section } from '../../components/templates/Section';
import { SectionHeading } from '../../components/templates/SectionHeading';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return (
		<>
			<PageHeading>Idol Calculators</PageHeading>
			<Box my={2}>
				<BackLink href="/tools">Tools</BackLink>
			</Box>

			<Flex flexDir="column" gap={8}>
				<Section>
					<SectionHeading>Type Chances</SectionHeading>
					<IdolTypeCalculator />
				</Section>
				<Section>
					<SectionHeading>Drop Chances</SectionHeading>
					<IdolChanceCalculator />
				</Section>
			</Flex>
		</>
	);
};

export default withDefaultLayout(Page);
