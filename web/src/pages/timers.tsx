import { NextPage } from 'next';
import { TimersPage } from '../components/pages/TimersPage';
import { withDefaultLayout } from '../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <TimersPage />;
};

export default withDefaultLayout(Page);
