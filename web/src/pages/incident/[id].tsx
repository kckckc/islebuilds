import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { IncidentPage } from '../../components/pages/IncidentPage';
import { useRoleGuard } from '../../hooks/useRoleGuard';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	const isGuarded = useRoleGuard(['MODERATION']);

	const router = useRouter();
	const { id } = router.query;

	// Don't show anything until checked
	if (!isGuarded) return null;

	return id ? <IncidentPage incidentId={id as string} /> : null;
};

export default withDefaultLayout(Page);
