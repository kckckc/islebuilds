import { NextPage } from 'next';
import { IncidentListPage } from '../../components/pages/IncidentListPage';
import { useRoleGuard } from '../../hooks/useRoleGuard';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	const isGuarded = useRoleGuard(['MODERATION']);

	// Don't show anything until checked
	if (!isGuarded) return null;

	return <IncidentListPage />;
};

export default withDefaultLayout(Page);
