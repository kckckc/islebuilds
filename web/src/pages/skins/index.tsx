import { NextPage } from 'next';
import { TopSkinsPage } from '../../components/pages/TopSkinsPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <TopSkinsPage />;
};

export default withDefaultLayout(Page);
