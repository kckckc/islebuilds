import { NextPage } from 'next';
import { WaddonPage } from '../../components/pages/WaddonPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <WaddonPage />;
};

export default withDefaultLayout(Page);
