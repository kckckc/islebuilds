import { NextPage } from 'next';
import { WaddonSetupPage } from '../../components/pages/WaddonSetupPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <WaddonSetupPage />;
};

export default withDefaultLayout(Page);
