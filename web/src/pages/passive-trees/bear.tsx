import { Box } from '@chakra-ui/react';
import { NextPage } from 'next';
import { PageHeading } from '../../components/atoms/PageHeading';
import { BackLink } from '../../components/molecules/BackLink';
import { PassiveTreesPage } from '../../components/pages/PassiveTreesPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return (
		<>
			<PageHeading>Top Bear Passive Trees</PageHeading>
			<Box my={2}>
				<BackLink href="/passive-trees">Passive Trees</BackLink>
			</Box>
			<PassiveTreesPage spirit={'bear'} />
		</>
	);
};

export default withDefaultLayout(Page);
