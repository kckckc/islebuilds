import { Flex, Text } from '@chakra-ui/react';
import { NextPage } from 'next';
import { PageHeading } from '../../components/atoms/PageHeading';
import { SpiritLinkButton } from '../../components/atoms/SpiritLinkButton';
import { PassiveTreesPage } from '../../components/pages/PassiveTreesPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return (
		<>
			<PageHeading>Top Passive Trees</PageHeading>
			<Flex my={2} gap={4} alignItems="center">
				<Text>Filter:</Text>
				<SpiritLinkButton spirit={'owl'} href={'/passive-trees/owl'} />
				<SpiritLinkButton
					spirit={'bear'}
					href={'/passive-trees/bear'}
				/>
				<SpiritLinkButton
					spirit={'lynx'}
					href={'/passive-trees/lynx'}
				/>
			</Flex>
			<PassiveTreesPage />
		</>
	);
};

export default withDefaultLayout(Page);
