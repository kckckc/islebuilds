import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { PassiveViewerPage } from '../../components/pages/PassiveViewerPage';
import { withDefaultLayout } from '../../util/withLayout';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	const router = useRouter();
	const { rawPassives } = router.query;

	const split = (rawPassives as string).split(',');
	const selected: number[] = [];
	split.forEach((node) => {
		const parsed = parseInt(node);
		if (!isNaN(parsed)) {
			selected.push(parsed);
		}
	});

	return <PassiveViewerPage selected={selected} />;
};

export default withDefaultLayout(Page);
