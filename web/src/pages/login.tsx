import { NextPage } from 'next';
import React from 'react';
import { LogInCard } from '../components/organisms/LogInCard';

interface PageProps {}

const Page: NextPage<PageProps> = ({}) => {
	return <LogInCard />;
};

export default Page;
