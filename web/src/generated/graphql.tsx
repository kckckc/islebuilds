import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
import { FieldPolicy, FieldReadFunction, TypePolicies, TypePolicy } from '@apollo/client/cache';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
};

export type AuthToken = {
  __typename?: 'AuthToken';
  createdAt: Scalars['String']['output'];
  id: Scalars['String']['output'];
  label: Scalars['String']['output'];
  token: Scalars['String']['output'];
  updatedAt: Scalars['String']['output'];
  user: User;
  userId: Scalars['String']['output'];
};

export type ChangeDisplayNameResponse = {
  __typename?: 'ChangeDisplayNameResponse';
  errors?: Maybe<Array<FieldError>>;
  success?: Maybe<Scalars['Boolean']['output']>;
};

export type Character = {
  __typename?: 'Character';
  createdAt: Scalars['String']['output'];
  eq_chest?: Maybe<Scalars['String']['output']>;
  eq_feet?: Maybe<Scalars['String']['output']>;
  eq_finger1?: Maybe<Scalars['String']['output']>;
  eq_finger2?: Maybe<Scalars['String']['output']>;
  eq_hands?: Maybe<Scalars['String']['output']>;
  eq_head?: Maybe<Scalars['String']['output']>;
  eq_legs?: Maybe<Scalars['String']['output']>;
  eq_neck?: Maybe<Scalars['String']['output']>;
  eq_offHand?: Maybe<Scalars['String']['output']>;
  eq_oneHanded?: Maybe<Scalars['String']['output']>;
  eq_quickslot?: Maybe<Scalars['String']['output']>;
  eq_rune1?: Maybe<Scalars['String']['output']>;
  eq_rune2?: Maybe<Scalars['String']['output']>;
  eq_rune3?: Maybe<Scalars['String']['output']>;
  eq_rune4?: Maybe<Scalars['String']['output']>;
  eq_tool?: Maybe<Scalars['String']['output']>;
  eq_trinket?: Maybe<Scalars['String']['output']>;
  eq_waist?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  inventory?: Maybe<Inventory>;
  isMine: Scalars['Boolean']['output'];
  league: Scalars['String']['output'];
  level: Scalars['Float']['output'];
  name: Scalars['String']['output'];
  passives: Array<Scalars['Int']['output']>;
  portraitX: Scalars['Float']['output'];
  portraitY: Scalars['Float']['output'];
  prophecy_austere: Scalars['Boolean']['output'];
  prophecy_butcher: Scalars['Boolean']['output'];
  prophecy_crushable: Scalars['Boolean']['output'];
  prophecy_hardcore: Scalars['Boolean']['output'];
  rankings: Array<CharacterRanking>;
  rep_akarei?: Maybe<Scalars['Float']['output']>;
  rep_fjolgard?: Maybe<Scalars['Float']['output']>;
  rep_gaekatla?: Maybe<Scalars['Float']['output']>;
  rep_peopleOfTheSun?: Maybe<Scalars['Float']['output']>;
  rep_pumpkinSailor?: Maybe<Scalars['Float']['output']>;
  rep_theWinterMan?: Maybe<Scalars['Float']['output']>;
  skinCell?: Maybe<Scalars['Float']['output']>;
  skinId: Scalars['String']['output'];
  skinSheetName?: Maybe<Scalars['String']['output']>;
  spirit: Scalars['String']['output'];
  updatedAt: Scalars['String']['output'];
  uploadedAt: Scalars['String']['output'];
  user: User;
  userId: Scalars['String']['output'];
};

export type CharacterRanking = {
  __typename?: 'CharacterRanking';
  leaderboard: Scalars['String']['output'];
  rank: Scalars['Float']['output'];
};

export type CharactersFilters = {
  level?: InputMaybe<Scalars['Float']['input']>;
  runes?: InputMaybe<Array<Scalars['String']['input']>>;
  sortBy?: InputMaybe<CharactersSortBy>;
  userId?: InputMaybe<Scalars['String']['input']>;
};

/** Sorting orders for paginated character lists. */
export enum CharactersSortBy {
  Level = 'Level',
  Name = 'Name'
}

export type CursorPaginatedCharacters = {
  __typename?: 'CursorPaginatedCharacters';
  characters: Array<Character>;
  cursor?: Maybe<Scalars['String']['output']>;
  hasMore: Scalars['Boolean']['output'];
};

export type CursorPaginatedCharactersInput = {
  cursor?: InputMaybe<Scalars['String']['input']>;
  filter?: InputMaybe<CharactersFilters>;
  limit?: InputMaybe<Scalars['Int']['input']>;
};

export type CursorPaginatedSprites = {
  __typename?: 'CursorPaginatedSprites';
  cursor?: Maybe<Scalars['String']['output']>;
  hasMore: Scalars['Boolean']['output'];
  sprites: Array<Sprite>;
};

export type CursorPaginatedSpritesInput = {
  cursor?: InputMaybe<Scalars['String']['input']>;
  filter?: InputMaybe<SpritesFilters>;
  limit?: InputMaybe<Scalars['Int']['input']>;
};

export type FieldError = {
  __typename?: 'FieldError';
  field: Scalars['String']['output'];
  message: Scalars['String']['output'];
};

export type Incident = {
  __typename?: 'Incident';
  createdAt: Scalars['String']['output'];
  description?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  reportedBy?: Maybe<User>;
  reportedById?: Maybe<Scalars['String']['output']>;
  reportedCharacter?: Maybe<Character>;
  reportedCharacterId?: Maybe<Scalars['String']['output']>;
  reportedProfile: Scalars['Boolean']['output'];
  reportedSprite?: Maybe<Sprite>;
  reportedSpriteId?: Maybe<Scalars['String']['output']>;
  reportedUser?: Maybe<User>;
  reportedUserId?: Maybe<Scalars['String']['output']>;
  resolved: Scalars['Boolean']['output'];
  title?: Maybe<Scalars['String']['output']>;
  updatedAt: Scalars['String']['output'];
};

export type Inventory = {
  __typename?: 'Inventory';
  character?: Maybe<Character>;
  createdAt: Scalars['String']['output'];
  id: Scalars['String']['output'];
  items?: Maybe<Scalars['String']['output']>;
  name: Scalars['String']['output'];
  stash: Scalars['Boolean']['output'];
  updatedAt: Scalars['String']['output'];
  uploadedAt: Scalars['String']['output'];
  user: User;
  userId: Scalars['String']['output'];
  visible: Scalars['Boolean']['output'];
};

export type LeaderboardResult = {
  __typename?: 'LeaderboardResult';
  characters: Array<Character>;
  error?: Maybe<Scalars['Boolean']['output']>;
  lastUpdated: Scalars['Float']['output'];
};

export type Mutation = {
  __typename?: 'Mutation';
  changeDisplayName: ChangeDisplayNameResponse;
  createAuthToken: AuthToken;
  deleteCharacter: Scalars['Boolean']['output'];
  generateOTP?: Maybe<Scalars['String']['output']>;
  logout?: Maybe<Scalars['Boolean']['output']>;
  performMerge: Scalars['Boolean']['output'];
  postSprite: Scalars['Boolean']['output'];
  reportCharacter: Scalars['Boolean']['output'];
  reportSprite: Scalars['Boolean']['output'];
  reportUser: Scalars['Boolean']['output'];
  revokeAuthToken: Scalars['Boolean']['output'];
  setConnectionVisible: Scalars['Boolean']['output'];
  setIncidentResolved: Scalars['Boolean']['output'];
  setInventoryVisible: Scalars['Boolean']['output'];
  setSpriteDeleted: Scalars['Boolean']['output'];
  setSpriteVisible: Scalars['Boolean']['output'];
  setUserRole: Scalars['Boolean']['output'];
};


export type MutationChangeDisplayNameArgs = {
  displayName: Scalars['String']['input'];
};


export type MutationCreateAuthTokenArgs = {
  label: Scalars['String']['input'];
};


export type MutationDeleteCharacterArgs = {
  character: Scalars['String']['input'];
};


export type MutationGenerateOtpArgs = {
  authTokenId: Scalars['String']['input'];
};


export type MutationPerformMergeArgs = {
  mergeId: Scalars['String']['input'];
  primaryId: Scalars['String']['input'];
};


export type MutationPostSpriteArgs = {
  input: PostSpriteInput;
};


export type MutationReportCharacterArgs = {
  details: ReportDetailsInput;
  target: Scalars['String']['input'];
};


export type MutationReportSpriteArgs = {
  details: ReportDetailsInput;
  target: Scalars['String']['input'];
};


export type MutationReportUserArgs = {
  details: ReportDetailsInput;
  target: Scalars['String']['input'];
};


export type MutationRevokeAuthTokenArgs = {
  id: Scalars['String']['input'];
};


export type MutationSetConnectionVisibleArgs = {
  provider: Scalars['String']['input'];
  visible: Scalars['Boolean']['input'];
};


export type MutationSetIncidentResolvedArgs = {
  incident: Scalars['String']['input'];
  resolved: Scalars['Boolean']['input'];
};


export type MutationSetInventoryVisibleArgs = {
  inventory: Scalars['String']['input'];
  visible: Scalars['Boolean']['input'];
};


export type MutationSetSpriteDeletedArgs = {
  deleted: Scalars['Boolean']['input'];
  sprite: Scalars['String']['input'];
};


export type MutationSetSpriteVisibleArgs = {
  sprite: Scalars['String']['input'];
  visible: Scalars['Boolean']['input'];
};


export type MutationSetUserRoleArgs = {
  role: Scalars['String']['input'];
  user: Scalars['String']['input'];
  value: Scalars['Boolean']['input'];
};

export type OffsetPaginatedCharacters = {
  __typename?: 'OffsetPaginatedCharacters';
  characters: Array<Character>;
  cursor?: Maybe<Scalars['Float']['output']>;
  hasMore: Scalars['Boolean']['output'];
};

export type OffsetPaginatedCharactersInput = {
  filter?: InputMaybe<CharactersFilters>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
};

export type OffsetPaginatedSprites = {
  __typename?: 'OffsetPaginatedSprites';
  cursor?: Maybe<Scalars['Float']['output']>;
  hasMore: Scalars['Boolean']['output'];
  sprites: Array<Sprite>;
};

export type OffsetPaginatedSpritesInput = {
  filter?: InputMaybe<SpritesFilters>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
};

export type PaginatedPassiveTrees = {
  __typename?: 'PaginatedPassiveTrees';
  error?: Maybe<Scalars['Boolean']['output']>;
  hasMore?: Maybe<Scalars['Boolean']['output']>;
  lastUpdated?: Maybe<Scalars['Float']['output']>;
  passiveTrees?: Maybe<Array<PassiveTreeWithCount>>;
};

export type PaginatedPassiveTreesInput = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  spirit?: InputMaybe<Scalars['String']['input']>;
};

export type PassiveTreeWithCount = {
  __typename?: 'PassiveTreeWithCount';
  count: Scalars['Int']['output'];
  passives: Array<Scalars['Int']['output']>;
};

export type PostSpriteInput = {
  description: Scalars['String']['input'];
  name: Scalars['String']['input'];
  pixels: Scalars['String']['input'];
  size: Scalars['Float']['input'];
  visible: Scalars['Boolean']['input'];
};

export type Query = {
  __typename?: 'Query';
  authTokens: Array<AuthToken>;
  character?: Maybe<Character>;
  characterById?: Maybe<Character>;
  characters: OffsetPaginatedCharacters;
  factionLeaderboard: LeaderboardResult;
  getMergeInfo: UserMergeInfo;
  incident?: Maybe<Incident>;
  me?: Maybe<User>;
  myInventories: Array<Inventory>;
  passiveTrees: PaginatedPassiveTrees;
  recentCharacters: CursorPaginatedCharacters;
  recentSprites: CursorPaginatedSprites;
  runeLoadouts: RuneLoadoutsResult;
  searchCharacters: Array<Character>;
  searchIncidents: Array<Incident>;
  searchSprites: Array<Sprite>;
  spriteById?: Maybe<Sprite>;
  sprites: OffsetPaginatedSprites;
  topSkins: TopSkinsResult;
  user?: Maybe<User>;
};


export type QueryCharacterArgs = {
  name: Scalars['String']['input'];
};


export type QueryCharacterByIdArgs = {
  id: Scalars['String']['input'];
};


export type QueryCharactersArgs = {
  input: OffsetPaginatedCharactersInput;
};


export type QueryFactionLeaderboardArgs = {
  faction: Scalars['String']['input'];
};


export type QueryGetMergeInfoArgs = {
  mergeId: Scalars['String']['input'];
};


export type QueryIncidentArgs = {
  id: Scalars['String']['input'];
};


export type QueryPassiveTreesArgs = {
  input: PaginatedPassiveTreesInput;
};


export type QueryRecentCharactersArgs = {
  input: CursorPaginatedCharactersInput;
};


export type QueryRecentSpritesArgs = {
  input: CursorPaginatedSpritesInput;
};


export type QuerySearchCharactersArgs = {
  name: Scalars['String']['input'];
};


export type QuerySearchIncidentsArgs = {
  filter: SearchIncidentsInput;
};


export type QuerySearchSpritesArgs = {
  search: Scalars['String']['input'];
};


export type QuerySpriteByIdArgs = {
  id: Scalars['String']['input'];
};


export type QuerySpritesArgs = {
  input: OffsetPaginatedSpritesInput;
};


export type QueryUserArgs = {
  id: Scalars['String']['input'];
};

export type ReportDetailsInput = {
  description: Scalars['String']['input'];
  title: Scalars['String']['input'];
};

export type RuneLoadout = {
  __typename?: 'RuneLoadout';
  count: Scalars['Float']['output'];
  runes: Array<Scalars['String']['output']>;
};

export type RuneLoadoutsResult = {
  __typename?: 'RuneLoadoutsResult';
  lastUpdated: Scalars['Float']['output'];
  loadouts: Array<RuneLoadout>;
};

export type SearchIncidentsInput = {
  page: Scalars['Float']['input'];
  showResolved: Scalars['Boolean']['input'];
  showUnresolved: Scalars['Boolean']['input'];
};

export type SkinRanking = {
  __typename?: 'SkinRanking';
  cell?: Maybe<Scalars['Float']['output']>;
  count: Scalars['Float']['output'];
  id: Scalars['String']['output'];
  sheetName?: Maybe<Scalars['String']['output']>;
};

export type Sprite = {
  __typename?: 'Sprite';
  createdAt: Scalars['String']['output'];
  deleted: Scalars['Boolean']['output'];
  description: Scalars['String']['output'];
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  pixels: Scalars['String']['output'];
  size: Scalars['Float']['output'];
  updatedAt: Scalars['String']['output'];
  user: User;
  userId: Scalars['String']['output'];
  visible: Scalars['Boolean']['output'];
};

export type SpritesFilters = {
  userId?: InputMaybe<Scalars['String']['input']>;
};

export type TopSkinsResult = {
  __typename?: 'TopSkinsResult';
  lastUpdated: Scalars['Float']['output'];
  skins: Array<SkinRanking>;
};

export type User = {
  __typename?: 'User';
  connections?: Maybe<Array<UserConnection>>;
  createdAt: Scalars['String']['output'];
  displayName: Scalars['String']['output'];
  id: Scalars['String']['output'];
  lastCharacter?: Maybe<Character>;
  roles?: Maybe<Array<Scalars['String']['output']>>;
  updatedAt: Scalars['String']['output'];
};

export type UserConnection = {
  __typename?: 'UserConnection';
  displayName?: Maybe<Scalars['String']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  provider: Scalars['String']['output'];
  visible: Scalars['Boolean']['output'];
};

export type UserMergeInfo = {
  __typename?: 'UserMergeInfo';
  current: User;
  other: User;
};

export type CharacterEquipmentFragment = { __typename?: 'Character', eq_head?: string | null, eq_neck?: string | null, eq_chest?: string | null, eq_hands?: string | null, eq_finger1?: string | null, eq_finger2?: string | null, eq_waist?: string | null, eq_legs?: string | null, eq_feet?: string | null, eq_trinket?: string | null, eq_oneHanded?: string | null, eq_offHand?: string | null, eq_tool?: string | null, eq_quickslot?: string | null, eq_rune1?: string | null, eq_rune2?: string | null, eq_rune3?: string | null, eq_rune4?: string | null };

export type CharacterPropheciesFragment = { __typename?: 'Character', prophecy_austere: boolean, prophecy_butcher: boolean, prophecy_crushable: boolean, prophecy_hardcore: boolean };

export type CharacterReputationFragment = { __typename?: 'Character', rep_akarei?: number | null, rep_fjolgard?: number | null, rep_gaekatla?: number | null, rep_peopleOfTheSun?: number | null, rep_pumpkinSailor?: number | null, rep_theWinterMan?: number | null };

export type CharacterVisualFragment = { __typename?: 'Character', portraitX: number, portraitY: number, skinSheetName?: string | null, skinId: string, skinCell?: number | null };

export type FullCharacterFragment = { __typename?: 'Character', id: string, userId: string, isMine: boolean, name: string, level: number, spirit: string, passives: Array<number>, league: string, uploadedAt: string, rep_akarei?: number | null, rep_fjolgard?: number | null, rep_gaekatla?: number | null, rep_peopleOfTheSun?: number | null, rep_pumpkinSailor?: number | null, rep_theWinterMan?: number | null, prophecy_austere: boolean, prophecy_butcher: boolean, prophecy_crushable: boolean, prophecy_hardcore: boolean, eq_head?: string | null, eq_neck?: string | null, eq_chest?: string | null, eq_hands?: string | null, eq_finger1?: string | null, eq_finger2?: string | null, eq_waist?: string | null, eq_legs?: string | null, eq_feet?: string | null, eq_trinket?: string | null, eq_oneHanded?: string | null, eq_offHand?: string | null, eq_tool?: string | null, eq_quickslot?: string | null, eq_rune1?: string | null, eq_rune2?: string | null, eq_rune3?: string | null, eq_rune4?: string | null, portraitX: number, portraitY: number, skinSheetName?: string | null, skinId: string, skinCell?: number | null, inventory?: { __typename?: 'Inventory', id: string, name: string, items?: string | null, visible: boolean, uploadedAt: string } | null };

export type PreviewCharacterFragment = { __typename?: 'Character', id: string, userId: string, name: string, level: number, spirit: string, skinSheetName?: string | null, skinCell?: number | null, league: string };

export type RegularUserFragment = { __typename?: 'User', id: string, displayName: string, createdAt: string };

export type ChangeDisplayNameMutationVariables = Exact<{
  displayName: Scalars['String']['input'];
}>;


export type ChangeDisplayNameMutation = { __typename?: 'Mutation', changeDisplayName: { __typename?: 'ChangeDisplayNameResponse', success?: boolean | null, errors?: Array<{ __typename?: 'FieldError', field: string, message: string }> | null } };

export type CreateAuthTokenMutationVariables = Exact<{
  label: Scalars['String']['input'];
}>;


export type CreateAuthTokenMutation = { __typename?: 'Mutation', createAuthToken: { __typename?: 'AuthToken', id: string, label: string, token: string, createdAt: string } };

export type DeleteCharacterMutationVariables = Exact<{
  character: Scalars['String']['input'];
}>;


export type DeleteCharacterMutation = { __typename?: 'Mutation', deleteCharacter: boolean };

export type GenerateOtpMutationVariables = Exact<{
  authTokenId: Scalars['String']['input'];
}>;


export type GenerateOtpMutation = { __typename?: 'Mutation', generateOTP?: string | null };

export type LogOutMutationVariables = Exact<{ [key: string]: never; }>;


export type LogOutMutation = { __typename?: 'Mutation', logout?: boolean | null };

export type PerformMergeMutationVariables = Exact<{
  mergeId: Scalars['String']['input'];
  primaryId: Scalars['String']['input'];
}>;


export type PerformMergeMutation = { __typename?: 'Mutation', performMerge: boolean };

export type PostSpriteMutationVariables = Exact<{
  input: PostSpriteInput;
}>;


export type PostSpriteMutation = { __typename?: 'Mutation', postSprite: boolean };

export type ReportCharacterMutationVariables = Exact<{
  target: Scalars['String']['input'];
  details: ReportDetailsInput;
}>;


export type ReportCharacterMutation = { __typename?: 'Mutation', reportCharacter: boolean };

export type ReportSpriteMutationVariables = Exact<{
  target: Scalars['String']['input'];
  details: ReportDetailsInput;
}>;


export type ReportSpriteMutation = { __typename?: 'Mutation', reportSprite: boolean };

export type ReportUserMutationVariables = Exact<{
  target: Scalars['String']['input'];
  details: ReportDetailsInput;
}>;


export type ReportUserMutation = { __typename?: 'Mutation', reportUser: boolean };

export type RevokeAuthTokenMutationVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type RevokeAuthTokenMutation = { __typename?: 'Mutation', revokeAuthToken: boolean };

export type SetConnectionVisibleMutationVariables = Exact<{
  provider: Scalars['String']['input'];
  visible: Scalars['Boolean']['input'];
}>;


export type SetConnectionVisibleMutation = { __typename?: 'Mutation', setConnectionVisible: boolean };

export type SetIncidentResolvedMutationVariables = Exact<{
  incident: Scalars['String']['input'];
  resolved: Scalars['Boolean']['input'];
}>;


export type SetIncidentResolvedMutation = { __typename?: 'Mutation', setIncidentResolved: boolean };

export type SetInventoryVisibleMutationVariables = Exact<{
  inventory: Scalars['String']['input'];
  visible: Scalars['Boolean']['input'];
}>;


export type SetInventoryVisibleMutation = { __typename?: 'Mutation', setInventoryVisible: boolean };

export type SetSpriteDeletedMutationVariables = Exact<{
  sprite: Scalars['String']['input'];
  deleted: Scalars['Boolean']['input'];
}>;


export type SetSpriteDeletedMutation = { __typename?: 'Mutation', setSpriteDeleted: boolean };

export type SetSpriteVisibleMutationVariables = Exact<{
  sprite: Scalars['String']['input'];
  visible: Scalars['Boolean']['input'];
}>;


export type SetSpriteVisibleMutation = { __typename?: 'Mutation', setSpriteVisible: boolean };

export type SetUserRoleMutationVariables = Exact<{
  user: Scalars['String']['input'];
  role: Scalars['String']['input'];
  value: Scalars['Boolean']['input'];
}>;


export type SetUserRoleMutation = { __typename?: 'Mutation', setUserRole: boolean };

export type AuthTokensQueryVariables = Exact<{ [key: string]: never; }>;


export type AuthTokensQuery = { __typename?: 'Query', authTokens: Array<{ __typename?: 'AuthToken', id: string, label: string, token: string, createdAt: string }> };

export type CharacterQueryVariables = Exact<{
  name: Scalars['String']['input'];
}>;


export type CharacterQuery = { __typename?: 'Query', character?: { __typename?: 'Character', id: string, userId: string, isMine: boolean, name: string, level: number, spirit: string, passives: Array<number>, league: string, uploadedAt: string, rep_akarei?: number | null, rep_fjolgard?: number | null, rep_gaekatla?: number | null, rep_peopleOfTheSun?: number | null, rep_pumpkinSailor?: number | null, rep_theWinterMan?: number | null, prophecy_austere: boolean, prophecy_butcher: boolean, prophecy_crushable: boolean, prophecy_hardcore: boolean, eq_head?: string | null, eq_neck?: string | null, eq_chest?: string | null, eq_hands?: string | null, eq_finger1?: string | null, eq_finger2?: string | null, eq_waist?: string | null, eq_legs?: string | null, eq_feet?: string | null, eq_trinket?: string | null, eq_oneHanded?: string | null, eq_offHand?: string | null, eq_tool?: string | null, eq_quickslot?: string | null, eq_rune1?: string | null, eq_rune2?: string | null, eq_rune3?: string | null, eq_rune4?: string | null, portraitX: number, portraitY: number, skinSheetName?: string | null, skinId: string, skinCell?: number | null, user: { __typename?: 'User', id: string, displayName: string, createdAt: string }, rankings: Array<{ __typename?: 'CharacterRanking', leaderboard: string, rank: number }>, inventory?: { __typename?: 'Inventory', id: string, name: string, items?: string | null, visible: boolean, uploadedAt: string } | null } | null };

export type FactionLeaderboardQueryVariables = Exact<{
  faction: Scalars['String']['input'];
}>;


export type FactionLeaderboardQuery = { __typename?: 'Query', factionLeaderboard: { __typename?: 'LeaderboardResult', lastUpdated: number, characters: Array<{ __typename?: 'Character', rep_gaekatla?: number | null, rep_fjolgard?: number | null, rep_akarei?: number | null, rep_peopleOfTheSun?: number | null, rep_pumpkinSailor?: number | null, rep_theWinterMan?: number | null, id: string, userId: string, name: string, level: number, spirit: string, skinSheetName?: string | null, skinCell?: number | null, league: string }> } };

export type GetMergeInfoQueryVariables = Exact<{
  mergeId: Scalars['String']['input'];
}>;


export type GetMergeInfoQuery = { __typename?: 'Query', getMergeInfo: { __typename?: 'UserMergeInfo', current: { __typename?: 'User', id: string, displayName: string, createdAt: string }, other: { __typename?: 'User', id: string, displayName: string, createdAt: string } } };

export type IncidentByIdQueryVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type IncidentByIdQuery = { __typename?: 'Query', incident?: { __typename?: 'Incident', id: string, resolved: boolean, title?: string | null, description?: string | null, reportedProfile: boolean, createdAt: string, reportedBy?: { __typename?: 'User', id: string, displayName: string } | null, reportedUser?: { __typename?: 'User', id: string, displayName: string } | null, reportedCharacter?: { __typename?: 'Character', id: string, name: string } | null, reportedSprite?: { __typename?: 'Sprite', id: string, name: string, description: string, visible: boolean, deleted: boolean, size: number, pixels: string, createdAt: string, user: { __typename?: 'User', id: string, displayName: string } } | null } | null };

export type MeQueryVariables = Exact<{ [key: string]: never; }>;


export type MeQuery = { __typename?: 'Query', me?: { __typename?: 'User', roles?: Array<string> | null, id: string, displayName: string, createdAt: string, connections?: Array<{ __typename?: 'UserConnection', provider: string, displayName?: string | null, visible: boolean }> | null } | null };

export type MyConnectionsQueryVariables = Exact<{ [key: string]: never; }>;


export type MyConnectionsQuery = { __typename?: 'Query', me?: { __typename?: 'User', connections?: Array<{ __typename?: 'UserConnection', provider: string, displayName?: string | null, email?: string | null, visible: boolean }> | null } | null };

export type MyInventoriesQueryVariables = Exact<{ [key: string]: never; }>;


export type MyInventoriesQuery = { __typename?: 'Query', myInventories: Array<{ __typename?: 'Inventory', name: string, stash: boolean, items?: string | null, visible: boolean, uploadedAt: string, character?: { __typename?: 'Character', id: string, userId: string, name: string, level: number, spirit: string, skinSheetName?: string | null, skinCell?: number | null, league: string } | null }> };

export type PassiveTreesQueryVariables = Exact<{
  input: PaginatedPassiveTreesInput;
}>;


export type PassiveTreesQuery = { __typename?: 'Query', passiveTrees: { __typename?: 'PaginatedPassiveTrees', error?: boolean | null, lastUpdated?: number | null, hasMore?: boolean | null, passiveTrees?: Array<{ __typename?: 'PassiveTreeWithCount', passives: Array<number>, count: number }> | null } };

export type PreviewCharactersQueryVariables = Exact<{
  input: OffsetPaginatedCharactersInput;
}>;


export type PreviewCharactersQuery = { __typename?: 'Query', characters: { __typename?: 'OffsetPaginatedCharacters', cursor?: number | null, hasMore: boolean, characters: Array<{ __typename?: 'Character', id: string, userId: string, name: string, level: number, spirit: string, skinSheetName?: string | null, skinCell?: number | null, league: string }> } };

export type RecentCharactersQueryVariables = Exact<{
  cursor?: InputMaybe<Scalars['String']['input']>;
  limit?: InputMaybe<Scalars['Int']['input']>;
}>;


export type RecentCharactersQuery = { __typename?: 'Query', recentCharacters: { __typename?: 'CursorPaginatedCharacters', cursor?: string | null, hasMore: boolean, characters: Array<{ __typename?: 'Character', id: string, userId: string, name: string, level: number, spirit: string, skinSheetName?: string | null, skinCell?: number | null, league: string }> } };

export type RecentSpritesQueryVariables = Exact<{
  cursor?: InputMaybe<Scalars['String']['input']>;
  limit?: InputMaybe<Scalars['Int']['input']>;
}>;


export type RecentSpritesQuery = { __typename?: 'Query', recentSprites: { __typename?: 'CursorPaginatedSprites', cursor?: string | null, hasMore: boolean, sprites: Array<{ __typename?: 'Sprite', id: string, name: string, description: string, visible: boolean, deleted: boolean, size: number, pixels: string, user: { __typename?: 'User', id: string, displayName: string } }> } };

export type RuneLoadoutsQueryVariables = Exact<{ [key: string]: never; }>;


export type RuneLoadoutsQuery = { __typename?: 'Query', runeLoadouts: { __typename?: 'RuneLoadoutsResult', loadouts: Array<{ __typename?: 'RuneLoadout', runes: Array<string>, count: number }> } };

export type SearchCharactersQueryVariables = Exact<{
  name: Scalars['String']['input'];
}>;


export type SearchCharactersQuery = { __typename?: 'Query', searchCharacters: Array<{ __typename?: 'Character', id: string, userId: string, name: string, level: number, spirit: string, skinSheetName?: string | null, skinCell?: number | null, league: string }> };

export type SearchIncidentsQueryVariables = Exact<{
  filter: SearchIncidentsInput;
}>;


export type SearchIncidentsQuery = { __typename?: 'Query', searchIncidents: Array<{ __typename?: 'Incident', id: string, resolved: boolean, title?: string | null, description?: string | null, reportedProfile: boolean, createdAt: string, reportedBy?: { __typename?: 'User', id: string, displayName: string } | null, reportedUser?: { __typename?: 'User', id: string, displayName: string } | null, reportedCharacter?: { __typename?: 'Character', id: string, name: string } | null, reportedSprite?: { __typename?: 'Sprite', id: string, name: string, description: string, visible: boolean, deleted: boolean, size: number, pixels: string, createdAt: string, user: { __typename?: 'User', id: string, displayName: string } } | null }> };

export type SpriteDetailsQueryVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type SpriteDetailsQuery = { __typename?: 'Query', spriteById?: { __typename?: 'Sprite', id: string, name: string, description: string, visible: boolean, deleted: boolean, size: number, pixels: string, createdAt: string, user: { __typename?: 'User', id: string, displayName: string } } | null };

export type TopSkinsQueryVariables = Exact<{ [key: string]: never; }>;


export type TopSkinsQuery = { __typename?: 'Query', topSkins: { __typename?: 'TopSkinsResult', lastUpdated: number, skins: Array<{ __typename?: 'SkinRanking', id: string, cell?: number | null, sheetName?: string | null, count: number }> } };

export type UserQueryVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type UserQuery = { __typename?: 'Query', user?: { __typename?: 'User', roles?: Array<string> | null, id: string, displayName: string, createdAt: string, connections?: Array<{ __typename?: 'UserConnection', provider: string, displayName?: string | null, visible: boolean }> | null } | null };

export type UserSpritesQueryVariables = Exact<{
  user: Scalars['String']['input'];
  offset?: InputMaybe<Scalars['Int']['input']>;
  limit?: InputMaybe<Scalars['Int']['input']>;
}>;


export type UserSpritesQuery = { __typename?: 'Query', sprites: { __typename?: 'OffsetPaginatedSprites', cursor?: number | null, hasMore: boolean, sprites: Array<{ __typename?: 'Sprite', id: string, name: string, description: string, visible: boolean, size: number, pixels: string, user: { __typename?: 'User', id: string, displayName: string } }> } };

export const namedOperations = {
  Query: {
    AuthTokens: 'AuthTokens',
    Character: 'Character',
    FactionLeaderboard: 'FactionLeaderboard',
    GetMergeInfo: 'GetMergeInfo',
    IncidentById: 'IncidentById',
    Me: 'Me',
    MyConnections: 'MyConnections',
    MyInventories: 'MyInventories',
    PassiveTrees: 'PassiveTrees',
    PreviewCharacters: 'PreviewCharacters',
    RecentCharacters: 'RecentCharacters',
    RecentSprites: 'RecentSprites',
    RuneLoadouts: 'RuneLoadouts',
    SearchCharacters: 'SearchCharacters',
    SearchIncidents: 'SearchIncidents',
    SpriteDetails: 'SpriteDetails',
    TopSkins: 'TopSkins',
    User: 'User',
    UserSprites: 'UserSprites'
  },
  Mutation: {
    ChangeDisplayName: 'ChangeDisplayName',
    CreateAuthToken: 'CreateAuthToken',
    DeleteCharacter: 'DeleteCharacter',
    GenerateOTP: 'GenerateOTP',
    LogOut: 'LogOut',
    PerformMerge: 'PerformMerge',
    PostSprite: 'PostSprite',
    ReportCharacter: 'ReportCharacter',
    ReportSprite: 'ReportSprite',
    ReportUser: 'ReportUser',
    RevokeAuthToken: 'RevokeAuthToken',
    SetConnectionVisible: 'SetConnectionVisible',
    SetIncidentResolved: 'SetIncidentResolved',
    SetInventoryVisible: 'SetInventoryVisible',
    SetSpriteDeleted: 'SetSpriteDeleted',
    SetSpriteVisible: 'SetSpriteVisible',
    SetUserRole: 'SetUserRole'
  },
  Fragment: {
    CharacterEquipment: 'CharacterEquipment',
    CharacterProphecies: 'CharacterProphecies',
    CharacterReputation: 'CharacterReputation',
    CharacterVisual: 'CharacterVisual',
    FullCharacter: 'FullCharacter',
    PreviewCharacter: 'PreviewCharacter',
    RegularUser: 'RegularUser'
  }
}
export const CharacterReputationFragmentDoc = gql`
    fragment CharacterReputation on Character {
  rep_akarei
  rep_fjolgard
  rep_gaekatla
  rep_peopleOfTheSun
  rep_pumpkinSailor
  rep_theWinterMan
}
    `;
export const CharacterPropheciesFragmentDoc = gql`
    fragment CharacterProphecies on Character {
  prophecy_austere
  prophecy_butcher
  prophecy_crushable
  prophecy_hardcore
}
    `;
export const CharacterEquipmentFragmentDoc = gql`
    fragment CharacterEquipment on Character {
  eq_head
  eq_neck
  eq_chest
  eq_hands
  eq_finger1
  eq_finger2
  eq_waist
  eq_legs
  eq_feet
  eq_trinket
  eq_oneHanded
  eq_offHand
  eq_tool
  eq_quickslot
  eq_rune1
  eq_rune2
  eq_rune3
  eq_rune4
}
    `;
export const CharacterVisualFragmentDoc = gql`
    fragment CharacterVisual on Character {
  portraitX
  portraitY
  skinSheetName
  skinId
  skinCell
}
    `;
export const FullCharacterFragmentDoc = gql`
    fragment FullCharacter on Character {
  id
  userId
  isMine
  name
  level
  spirit
  passives
  league
  inventory {
    id
    name
    items
    visible
    uploadedAt
  }
  uploadedAt
  ...CharacterReputation
  ...CharacterProphecies
  ...CharacterEquipment
  ...CharacterVisual
}
    ${CharacterReputationFragmentDoc}
${CharacterPropheciesFragmentDoc}
${CharacterEquipmentFragmentDoc}
${CharacterVisualFragmentDoc}`;
export const PreviewCharacterFragmentDoc = gql`
    fragment PreviewCharacter on Character {
  id
  userId
  name
  level
  spirit
  skinSheetName
  skinCell
  league
}
    `;
export const RegularUserFragmentDoc = gql`
    fragment RegularUser on User {
  id
  displayName
  createdAt
}
    `;
export const ChangeDisplayNameDocument = gql`
    mutation ChangeDisplayName($displayName: String!) {
  changeDisplayName(displayName: $displayName) {
    errors {
      field
      message
    }
    success
  }
}
    `;
export type ChangeDisplayNameMutationFn = Apollo.MutationFunction<ChangeDisplayNameMutation, ChangeDisplayNameMutationVariables>;

/**
 * __useChangeDisplayNameMutation__
 *
 * To run a mutation, you first call `useChangeDisplayNameMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeDisplayNameMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeDisplayNameMutation, { data, loading, error }] = useChangeDisplayNameMutation({
 *   variables: {
 *      displayName: // value for 'displayName'
 *   },
 * });
 */
export function useChangeDisplayNameMutation(baseOptions?: Apollo.MutationHookOptions<ChangeDisplayNameMutation, ChangeDisplayNameMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeDisplayNameMutation, ChangeDisplayNameMutationVariables>(ChangeDisplayNameDocument, options);
      }
export type ChangeDisplayNameMutationHookResult = ReturnType<typeof useChangeDisplayNameMutation>;
export type ChangeDisplayNameMutationResult = Apollo.MutationResult<ChangeDisplayNameMutation>;
export type ChangeDisplayNameMutationOptions = Apollo.BaseMutationOptions<ChangeDisplayNameMutation, ChangeDisplayNameMutationVariables>;
export const CreateAuthTokenDocument = gql`
    mutation CreateAuthToken($label: String!) {
  createAuthToken(label: $label) {
    id
    label
    token
    createdAt
  }
}
    `;
export type CreateAuthTokenMutationFn = Apollo.MutationFunction<CreateAuthTokenMutation, CreateAuthTokenMutationVariables>;

/**
 * __useCreateAuthTokenMutation__
 *
 * To run a mutation, you first call `useCreateAuthTokenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateAuthTokenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createAuthTokenMutation, { data, loading, error }] = useCreateAuthTokenMutation({
 *   variables: {
 *      label: // value for 'label'
 *   },
 * });
 */
export function useCreateAuthTokenMutation(baseOptions?: Apollo.MutationHookOptions<CreateAuthTokenMutation, CreateAuthTokenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateAuthTokenMutation, CreateAuthTokenMutationVariables>(CreateAuthTokenDocument, options);
      }
export type CreateAuthTokenMutationHookResult = ReturnType<typeof useCreateAuthTokenMutation>;
export type CreateAuthTokenMutationResult = Apollo.MutationResult<CreateAuthTokenMutation>;
export type CreateAuthTokenMutationOptions = Apollo.BaseMutationOptions<CreateAuthTokenMutation, CreateAuthTokenMutationVariables>;
export const DeleteCharacterDocument = gql`
    mutation DeleteCharacter($character: String!) {
  deleteCharacter(character: $character)
}
    `;
export type DeleteCharacterMutationFn = Apollo.MutationFunction<DeleteCharacterMutation, DeleteCharacterMutationVariables>;

/**
 * __useDeleteCharacterMutation__
 *
 * To run a mutation, you first call `useDeleteCharacterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCharacterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCharacterMutation, { data, loading, error }] = useDeleteCharacterMutation({
 *   variables: {
 *      character: // value for 'character'
 *   },
 * });
 */
export function useDeleteCharacterMutation(baseOptions?: Apollo.MutationHookOptions<DeleteCharacterMutation, DeleteCharacterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteCharacterMutation, DeleteCharacterMutationVariables>(DeleteCharacterDocument, options);
      }
export type DeleteCharacterMutationHookResult = ReturnType<typeof useDeleteCharacterMutation>;
export type DeleteCharacterMutationResult = Apollo.MutationResult<DeleteCharacterMutation>;
export type DeleteCharacterMutationOptions = Apollo.BaseMutationOptions<DeleteCharacterMutation, DeleteCharacterMutationVariables>;
export const GenerateOtpDocument = gql`
    mutation GenerateOTP($authTokenId: String!) {
  generateOTP(authTokenId: $authTokenId)
}
    `;
export type GenerateOtpMutationFn = Apollo.MutationFunction<GenerateOtpMutation, GenerateOtpMutationVariables>;

/**
 * __useGenerateOtpMutation__
 *
 * To run a mutation, you first call `useGenerateOtpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useGenerateOtpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [generateOtpMutation, { data, loading, error }] = useGenerateOtpMutation({
 *   variables: {
 *      authTokenId: // value for 'authTokenId'
 *   },
 * });
 */
export function useGenerateOtpMutation(baseOptions?: Apollo.MutationHookOptions<GenerateOtpMutation, GenerateOtpMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<GenerateOtpMutation, GenerateOtpMutationVariables>(GenerateOtpDocument, options);
      }
export type GenerateOtpMutationHookResult = ReturnType<typeof useGenerateOtpMutation>;
export type GenerateOtpMutationResult = Apollo.MutationResult<GenerateOtpMutation>;
export type GenerateOtpMutationOptions = Apollo.BaseMutationOptions<GenerateOtpMutation, GenerateOtpMutationVariables>;
export const LogOutDocument = gql`
    mutation LogOut {
  logout
}
    `;
export type LogOutMutationFn = Apollo.MutationFunction<LogOutMutation, LogOutMutationVariables>;

/**
 * __useLogOutMutation__
 *
 * To run a mutation, you first call `useLogOutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogOutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logOutMutation, { data, loading, error }] = useLogOutMutation({
 *   variables: {
 *   },
 * });
 */
export function useLogOutMutation(baseOptions?: Apollo.MutationHookOptions<LogOutMutation, LogOutMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LogOutMutation, LogOutMutationVariables>(LogOutDocument, options);
      }
export type LogOutMutationHookResult = ReturnType<typeof useLogOutMutation>;
export type LogOutMutationResult = Apollo.MutationResult<LogOutMutation>;
export type LogOutMutationOptions = Apollo.BaseMutationOptions<LogOutMutation, LogOutMutationVariables>;
export const PerformMergeDocument = gql`
    mutation PerformMerge($mergeId: String!, $primaryId: String!) {
  performMerge(mergeId: $mergeId, primaryId: $primaryId)
}
    `;
export type PerformMergeMutationFn = Apollo.MutationFunction<PerformMergeMutation, PerformMergeMutationVariables>;

/**
 * __usePerformMergeMutation__
 *
 * To run a mutation, you first call `usePerformMergeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePerformMergeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [performMergeMutation, { data, loading, error }] = usePerformMergeMutation({
 *   variables: {
 *      mergeId: // value for 'mergeId'
 *      primaryId: // value for 'primaryId'
 *   },
 * });
 */
export function usePerformMergeMutation(baseOptions?: Apollo.MutationHookOptions<PerformMergeMutation, PerformMergeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<PerformMergeMutation, PerformMergeMutationVariables>(PerformMergeDocument, options);
      }
export type PerformMergeMutationHookResult = ReturnType<typeof usePerformMergeMutation>;
export type PerformMergeMutationResult = Apollo.MutationResult<PerformMergeMutation>;
export type PerformMergeMutationOptions = Apollo.BaseMutationOptions<PerformMergeMutation, PerformMergeMutationVariables>;
export const PostSpriteDocument = gql`
    mutation PostSprite($input: PostSpriteInput!) {
  postSprite(input: $input)
}
    `;
export type PostSpriteMutationFn = Apollo.MutationFunction<PostSpriteMutation, PostSpriteMutationVariables>;

/**
 * __usePostSpriteMutation__
 *
 * To run a mutation, you first call `usePostSpriteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePostSpriteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [postSpriteMutation, { data, loading, error }] = usePostSpriteMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function usePostSpriteMutation(baseOptions?: Apollo.MutationHookOptions<PostSpriteMutation, PostSpriteMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<PostSpriteMutation, PostSpriteMutationVariables>(PostSpriteDocument, options);
      }
export type PostSpriteMutationHookResult = ReturnType<typeof usePostSpriteMutation>;
export type PostSpriteMutationResult = Apollo.MutationResult<PostSpriteMutation>;
export type PostSpriteMutationOptions = Apollo.BaseMutationOptions<PostSpriteMutation, PostSpriteMutationVariables>;
export const ReportCharacterDocument = gql`
    mutation ReportCharacter($target: String!, $details: ReportDetailsInput!) {
  reportCharacter(details: $details, target: $target)
}
    `;
export type ReportCharacterMutationFn = Apollo.MutationFunction<ReportCharacterMutation, ReportCharacterMutationVariables>;

/**
 * __useReportCharacterMutation__
 *
 * To run a mutation, you first call `useReportCharacterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useReportCharacterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [reportCharacterMutation, { data, loading, error }] = useReportCharacterMutation({
 *   variables: {
 *      target: // value for 'target'
 *      details: // value for 'details'
 *   },
 * });
 */
export function useReportCharacterMutation(baseOptions?: Apollo.MutationHookOptions<ReportCharacterMutation, ReportCharacterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ReportCharacterMutation, ReportCharacterMutationVariables>(ReportCharacterDocument, options);
      }
export type ReportCharacterMutationHookResult = ReturnType<typeof useReportCharacterMutation>;
export type ReportCharacterMutationResult = Apollo.MutationResult<ReportCharacterMutation>;
export type ReportCharacterMutationOptions = Apollo.BaseMutationOptions<ReportCharacterMutation, ReportCharacterMutationVariables>;
export const ReportSpriteDocument = gql`
    mutation ReportSprite($target: String!, $details: ReportDetailsInput!) {
  reportSprite(details: $details, target: $target)
}
    `;
export type ReportSpriteMutationFn = Apollo.MutationFunction<ReportSpriteMutation, ReportSpriteMutationVariables>;

/**
 * __useReportSpriteMutation__
 *
 * To run a mutation, you first call `useReportSpriteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useReportSpriteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [reportSpriteMutation, { data, loading, error }] = useReportSpriteMutation({
 *   variables: {
 *      target: // value for 'target'
 *      details: // value for 'details'
 *   },
 * });
 */
export function useReportSpriteMutation(baseOptions?: Apollo.MutationHookOptions<ReportSpriteMutation, ReportSpriteMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ReportSpriteMutation, ReportSpriteMutationVariables>(ReportSpriteDocument, options);
      }
export type ReportSpriteMutationHookResult = ReturnType<typeof useReportSpriteMutation>;
export type ReportSpriteMutationResult = Apollo.MutationResult<ReportSpriteMutation>;
export type ReportSpriteMutationOptions = Apollo.BaseMutationOptions<ReportSpriteMutation, ReportSpriteMutationVariables>;
export const ReportUserDocument = gql`
    mutation ReportUser($target: String!, $details: ReportDetailsInput!) {
  reportUser(details: $details, target: $target)
}
    `;
export type ReportUserMutationFn = Apollo.MutationFunction<ReportUserMutation, ReportUserMutationVariables>;

/**
 * __useReportUserMutation__
 *
 * To run a mutation, you first call `useReportUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useReportUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [reportUserMutation, { data, loading, error }] = useReportUserMutation({
 *   variables: {
 *      target: // value for 'target'
 *      details: // value for 'details'
 *   },
 * });
 */
export function useReportUserMutation(baseOptions?: Apollo.MutationHookOptions<ReportUserMutation, ReportUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ReportUserMutation, ReportUserMutationVariables>(ReportUserDocument, options);
      }
export type ReportUserMutationHookResult = ReturnType<typeof useReportUserMutation>;
export type ReportUserMutationResult = Apollo.MutationResult<ReportUserMutation>;
export type ReportUserMutationOptions = Apollo.BaseMutationOptions<ReportUserMutation, ReportUserMutationVariables>;
export const RevokeAuthTokenDocument = gql`
    mutation RevokeAuthToken($id: String!) {
  revokeAuthToken(id: $id)
}
    `;
export type RevokeAuthTokenMutationFn = Apollo.MutationFunction<RevokeAuthTokenMutation, RevokeAuthTokenMutationVariables>;

/**
 * __useRevokeAuthTokenMutation__
 *
 * To run a mutation, you first call `useRevokeAuthTokenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRevokeAuthTokenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [revokeAuthTokenMutation, { data, loading, error }] = useRevokeAuthTokenMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useRevokeAuthTokenMutation(baseOptions?: Apollo.MutationHookOptions<RevokeAuthTokenMutation, RevokeAuthTokenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RevokeAuthTokenMutation, RevokeAuthTokenMutationVariables>(RevokeAuthTokenDocument, options);
      }
export type RevokeAuthTokenMutationHookResult = ReturnType<typeof useRevokeAuthTokenMutation>;
export type RevokeAuthTokenMutationResult = Apollo.MutationResult<RevokeAuthTokenMutation>;
export type RevokeAuthTokenMutationOptions = Apollo.BaseMutationOptions<RevokeAuthTokenMutation, RevokeAuthTokenMutationVariables>;
export const SetConnectionVisibleDocument = gql`
    mutation SetConnectionVisible($provider: String!, $visible: Boolean!) {
  setConnectionVisible(provider: $provider, visible: $visible)
}
    `;
export type SetConnectionVisibleMutationFn = Apollo.MutationFunction<SetConnectionVisibleMutation, SetConnectionVisibleMutationVariables>;

/**
 * __useSetConnectionVisibleMutation__
 *
 * To run a mutation, you first call `useSetConnectionVisibleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetConnectionVisibleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setConnectionVisibleMutation, { data, loading, error }] = useSetConnectionVisibleMutation({
 *   variables: {
 *      provider: // value for 'provider'
 *      visible: // value for 'visible'
 *   },
 * });
 */
export function useSetConnectionVisibleMutation(baseOptions?: Apollo.MutationHookOptions<SetConnectionVisibleMutation, SetConnectionVisibleMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SetConnectionVisibleMutation, SetConnectionVisibleMutationVariables>(SetConnectionVisibleDocument, options);
      }
export type SetConnectionVisibleMutationHookResult = ReturnType<typeof useSetConnectionVisibleMutation>;
export type SetConnectionVisibleMutationResult = Apollo.MutationResult<SetConnectionVisibleMutation>;
export type SetConnectionVisibleMutationOptions = Apollo.BaseMutationOptions<SetConnectionVisibleMutation, SetConnectionVisibleMutationVariables>;
export const SetIncidentResolvedDocument = gql`
    mutation SetIncidentResolved($incident: String!, $resolved: Boolean!) {
  setIncidentResolved(incident: $incident, resolved: $resolved)
}
    `;
export type SetIncidentResolvedMutationFn = Apollo.MutationFunction<SetIncidentResolvedMutation, SetIncidentResolvedMutationVariables>;

/**
 * __useSetIncidentResolvedMutation__
 *
 * To run a mutation, you first call `useSetIncidentResolvedMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetIncidentResolvedMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setIncidentResolvedMutation, { data, loading, error }] = useSetIncidentResolvedMutation({
 *   variables: {
 *      incident: // value for 'incident'
 *      resolved: // value for 'resolved'
 *   },
 * });
 */
export function useSetIncidentResolvedMutation(baseOptions?: Apollo.MutationHookOptions<SetIncidentResolvedMutation, SetIncidentResolvedMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SetIncidentResolvedMutation, SetIncidentResolvedMutationVariables>(SetIncidentResolvedDocument, options);
      }
export type SetIncidentResolvedMutationHookResult = ReturnType<typeof useSetIncidentResolvedMutation>;
export type SetIncidentResolvedMutationResult = Apollo.MutationResult<SetIncidentResolvedMutation>;
export type SetIncidentResolvedMutationOptions = Apollo.BaseMutationOptions<SetIncidentResolvedMutation, SetIncidentResolvedMutationVariables>;
export const SetInventoryVisibleDocument = gql`
    mutation SetInventoryVisible($inventory: String!, $visible: Boolean!) {
  setInventoryVisible(inventory: $inventory, visible: $visible)
}
    `;
export type SetInventoryVisibleMutationFn = Apollo.MutationFunction<SetInventoryVisibleMutation, SetInventoryVisibleMutationVariables>;

/**
 * __useSetInventoryVisibleMutation__
 *
 * To run a mutation, you first call `useSetInventoryVisibleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetInventoryVisibleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setInventoryVisibleMutation, { data, loading, error }] = useSetInventoryVisibleMutation({
 *   variables: {
 *      inventory: // value for 'inventory'
 *      visible: // value for 'visible'
 *   },
 * });
 */
export function useSetInventoryVisibleMutation(baseOptions?: Apollo.MutationHookOptions<SetInventoryVisibleMutation, SetInventoryVisibleMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SetInventoryVisibleMutation, SetInventoryVisibleMutationVariables>(SetInventoryVisibleDocument, options);
      }
export type SetInventoryVisibleMutationHookResult = ReturnType<typeof useSetInventoryVisibleMutation>;
export type SetInventoryVisibleMutationResult = Apollo.MutationResult<SetInventoryVisibleMutation>;
export type SetInventoryVisibleMutationOptions = Apollo.BaseMutationOptions<SetInventoryVisibleMutation, SetInventoryVisibleMutationVariables>;
export const SetSpriteDeletedDocument = gql`
    mutation SetSpriteDeleted($sprite: String!, $deleted: Boolean!) {
  setSpriteDeleted(sprite: $sprite, deleted: $deleted)
}
    `;
export type SetSpriteDeletedMutationFn = Apollo.MutationFunction<SetSpriteDeletedMutation, SetSpriteDeletedMutationVariables>;

/**
 * __useSetSpriteDeletedMutation__
 *
 * To run a mutation, you first call `useSetSpriteDeletedMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetSpriteDeletedMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setSpriteDeletedMutation, { data, loading, error }] = useSetSpriteDeletedMutation({
 *   variables: {
 *      sprite: // value for 'sprite'
 *      deleted: // value for 'deleted'
 *   },
 * });
 */
export function useSetSpriteDeletedMutation(baseOptions?: Apollo.MutationHookOptions<SetSpriteDeletedMutation, SetSpriteDeletedMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SetSpriteDeletedMutation, SetSpriteDeletedMutationVariables>(SetSpriteDeletedDocument, options);
      }
export type SetSpriteDeletedMutationHookResult = ReturnType<typeof useSetSpriteDeletedMutation>;
export type SetSpriteDeletedMutationResult = Apollo.MutationResult<SetSpriteDeletedMutation>;
export type SetSpriteDeletedMutationOptions = Apollo.BaseMutationOptions<SetSpriteDeletedMutation, SetSpriteDeletedMutationVariables>;
export const SetSpriteVisibleDocument = gql`
    mutation SetSpriteVisible($sprite: String!, $visible: Boolean!) {
  setSpriteVisible(sprite: $sprite, visible: $visible)
}
    `;
export type SetSpriteVisibleMutationFn = Apollo.MutationFunction<SetSpriteVisibleMutation, SetSpriteVisibleMutationVariables>;

/**
 * __useSetSpriteVisibleMutation__
 *
 * To run a mutation, you first call `useSetSpriteVisibleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetSpriteVisibleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setSpriteVisibleMutation, { data, loading, error }] = useSetSpriteVisibleMutation({
 *   variables: {
 *      sprite: // value for 'sprite'
 *      visible: // value for 'visible'
 *   },
 * });
 */
export function useSetSpriteVisibleMutation(baseOptions?: Apollo.MutationHookOptions<SetSpriteVisibleMutation, SetSpriteVisibleMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SetSpriteVisibleMutation, SetSpriteVisibleMutationVariables>(SetSpriteVisibleDocument, options);
      }
export type SetSpriteVisibleMutationHookResult = ReturnType<typeof useSetSpriteVisibleMutation>;
export type SetSpriteVisibleMutationResult = Apollo.MutationResult<SetSpriteVisibleMutation>;
export type SetSpriteVisibleMutationOptions = Apollo.BaseMutationOptions<SetSpriteVisibleMutation, SetSpriteVisibleMutationVariables>;
export const SetUserRoleDocument = gql`
    mutation SetUserRole($user: String!, $role: String!, $value: Boolean!) {
  setUserRole(user: $user, role: $role, value: $value)
}
    `;
export type SetUserRoleMutationFn = Apollo.MutationFunction<SetUserRoleMutation, SetUserRoleMutationVariables>;

/**
 * __useSetUserRoleMutation__
 *
 * To run a mutation, you first call `useSetUserRoleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetUserRoleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setUserRoleMutation, { data, loading, error }] = useSetUserRoleMutation({
 *   variables: {
 *      user: // value for 'user'
 *      role: // value for 'role'
 *      value: // value for 'value'
 *   },
 * });
 */
export function useSetUserRoleMutation(baseOptions?: Apollo.MutationHookOptions<SetUserRoleMutation, SetUserRoleMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SetUserRoleMutation, SetUserRoleMutationVariables>(SetUserRoleDocument, options);
      }
export type SetUserRoleMutationHookResult = ReturnType<typeof useSetUserRoleMutation>;
export type SetUserRoleMutationResult = Apollo.MutationResult<SetUserRoleMutation>;
export type SetUserRoleMutationOptions = Apollo.BaseMutationOptions<SetUserRoleMutation, SetUserRoleMutationVariables>;
export const AuthTokensDocument = gql`
    query AuthTokens {
  authTokens {
    id
    label
    token
    createdAt
  }
}
    `;

/**
 * __useAuthTokensQuery__
 *
 * To run a query within a React component, call `useAuthTokensQuery` and pass it any options that fit your needs.
 * When your component renders, `useAuthTokensQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAuthTokensQuery({
 *   variables: {
 *   },
 * });
 */
export function useAuthTokensQuery(baseOptions?: Apollo.QueryHookOptions<AuthTokensQuery, AuthTokensQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<AuthTokensQuery, AuthTokensQueryVariables>(AuthTokensDocument, options);
      }
export function useAuthTokensLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<AuthTokensQuery, AuthTokensQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<AuthTokensQuery, AuthTokensQueryVariables>(AuthTokensDocument, options);
        }
export function useAuthTokensSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<AuthTokensQuery, AuthTokensQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<AuthTokensQuery, AuthTokensQueryVariables>(AuthTokensDocument, options);
        }
export type AuthTokensQueryHookResult = ReturnType<typeof useAuthTokensQuery>;
export type AuthTokensLazyQueryHookResult = ReturnType<typeof useAuthTokensLazyQuery>;
export type AuthTokensSuspenseQueryHookResult = ReturnType<typeof useAuthTokensSuspenseQuery>;
export type AuthTokensQueryResult = Apollo.QueryResult<AuthTokensQuery, AuthTokensQueryVariables>;
export const CharacterDocument = gql`
    query Character($name: String!) {
  character(name: $name) {
    ...FullCharacter
    user {
      ...RegularUser
    }
    rankings {
      leaderboard
      rank
    }
  }
}
    ${FullCharacterFragmentDoc}
${RegularUserFragmentDoc}`;

/**
 * __useCharacterQuery__
 *
 * To run a query within a React component, call `useCharacterQuery` and pass it any options that fit your needs.
 * When your component renders, `useCharacterQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCharacterQuery({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function useCharacterQuery(baseOptions: Apollo.QueryHookOptions<CharacterQuery, CharacterQueryVariables> & ({ variables: CharacterQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CharacterQuery, CharacterQueryVariables>(CharacterDocument, options);
      }
export function useCharacterLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CharacterQuery, CharacterQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CharacterQuery, CharacterQueryVariables>(CharacterDocument, options);
        }
export function useCharacterSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<CharacterQuery, CharacterQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<CharacterQuery, CharacterQueryVariables>(CharacterDocument, options);
        }
export type CharacterQueryHookResult = ReturnType<typeof useCharacterQuery>;
export type CharacterLazyQueryHookResult = ReturnType<typeof useCharacterLazyQuery>;
export type CharacterSuspenseQueryHookResult = ReturnType<typeof useCharacterSuspenseQuery>;
export type CharacterQueryResult = Apollo.QueryResult<CharacterQuery, CharacterQueryVariables>;
export const FactionLeaderboardDocument = gql`
    query FactionLeaderboard($faction: String!) {
  factionLeaderboard(faction: $faction) {
    characters {
      ...PreviewCharacter
      rep_gaekatla
      rep_fjolgard
      rep_akarei
      rep_peopleOfTheSun
      rep_pumpkinSailor
      rep_theWinterMan
    }
    lastUpdated
  }
}
    ${PreviewCharacterFragmentDoc}`;

/**
 * __useFactionLeaderboardQuery__
 *
 * To run a query within a React component, call `useFactionLeaderboardQuery` and pass it any options that fit your needs.
 * When your component renders, `useFactionLeaderboardQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFactionLeaderboardQuery({
 *   variables: {
 *      faction: // value for 'faction'
 *   },
 * });
 */
export function useFactionLeaderboardQuery(baseOptions: Apollo.QueryHookOptions<FactionLeaderboardQuery, FactionLeaderboardQueryVariables> & ({ variables: FactionLeaderboardQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FactionLeaderboardQuery, FactionLeaderboardQueryVariables>(FactionLeaderboardDocument, options);
      }
export function useFactionLeaderboardLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FactionLeaderboardQuery, FactionLeaderboardQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FactionLeaderboardQuery, FactionLeaderboardQueryVariables>(FactionLeaderboardDocument, options);
        }
export function useFactionLeaderboardSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<FactionLeaderboardQuery, FactionLeaderboardQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<FactionLeaderboardQuery, FactionLeaderboardQueryVariables>(FactionLeaderboardDocument, options);
        }
export type FactionLeaderboardQueryHookResult = ReturnType<typeof useFactionLeaderboardQuery>;
export type FactionLeaderboardLazyQueryHookResult = ReturnType<typeof useFactionLeaderboardLazyQuery>;
export type FactionLeaderboardSuspenseQueryHookResult = ReturnType<typeof useFactionLeaderboardSuspenseQuery>;
export type FactionLeaderboardQueryResult = Apollo.QueryResult<FactionLeaderboardQuery, FactionLeaderboardQueryVariables>;
export const GetMergeInfoDocument = gql`
    query GetMergeInfo($mergeId: String!) {
  getMergeInfo(mergeId: $mergeId) {
    current {
      id
      displayName
      createdAt
    }
    other {
      id
      displayName
      createdAt
    }
  }
}
    `;

/**
 * __useGetMergeInfoQuery__
 *
 * To run a query within a React component, call `useGetMergeInfoQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMergeInfoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetMergeInfoQuery({
 *   variables: {
 *      mergeId: // value for 'mergeId'
 *   },
 * });
 */
export function useGetMergeInfoQuery(baseOptions: Apollo.QueryHookOptions<GetMergeInfoQuery, GetMergeInfoQueryVariables> & ({ variables: GetMergeInfoQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetMergeInfoQuery, GetMergeInfoQueryVariables>(GetMergeInfoDocument, options);
      }
export function useGetMergeInfoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetMergeInfoQuery, GetMergeInfoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetMergeInfoQuery, GetMergeInfoQueryVariables>(GetMergeInfoDocument, options);
        }
export function useGetMergeInfoSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<GetMergeInfoQuery, GetMergeInfoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<GetMergeInfoQuery, GetMergeInfoQueryVariables>(GetMergeInfoDocument, options);
        }
export type GetMergeInfoQueryHookResult = ReturnType<typeof useGetMergeInfoQuery>;
export type GetMergeInfoLazyQueryHookResult = ReturnType<typeof useGetMergeInfoLazyQuery>;
export type GetMergeInfoSuspenseQueryHookResult = ReturnType<typeof useGetMergeInfoSuspenseQuery>;
export type GetMergeInfoQueryResult = Apollo.QueryResult<GetMergeInfoQuery, GetMergeInfoQueryVariables>;
export const IncidentByIdDocument = gql`
    query IncidentById($id: String!) {
  incident(id: $id) {
    id
    resolved
    title
    description
    reportedBy {
      id
      displayName
    }
    reportedUser {
      id
      displayName
    }
    reportedProfile
    reportedCharacter {
      id
      name
    }
    reportedSprite {
      id
      name
      description
      visible
      deleted
      size
      pixels
      user {
        id
        displayName
      }
      createdAt
    }
    createdAt
  }
}
    `;

/**
 * __useIncidentByIdQuery__
 *
 * To run a query within a React component, call `useIncidentByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useIncidentByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useIncidentByIdQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useIncidentByIdQuery(baseOptions: Apollo.QueryHookOptions<IncidentByIdQuery, IncidentByIdQueryVariables> & ({ variables: IncidentByIdQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<IncidentByIdQuery, IncidentByIdQueryVariables>(IncidentByIdDocument, options);
      }
export function useIncidentByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<IncidentByIdQuery, IncidentByIdQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<IncidentByIdQuery, IncidentByIdQueryVariables>(IncidentByIdDocument, options);
        }
export function useIncidentByIdSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<IncidentByIdQuery, IncidentByIdQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<IncidentByIdQuery, IncidentByIdQueryVariables>(IncidentByIdDocument, options);
        }
export type IncidentByIdQueryHookResult = ReturnType<typeof useIncidentByIdQuery>;
export type IncidentByIdLazyQueryHookResult = ReturnType<typeof useIncidentByIdLazyQuery>;
export type IncidentByIdSuspenseQueryHookResult = ReturnType<typeof useIncidentByIdSuspenseQuery>;
export type IncidentByIdQueryResult = Apollo.QueryResult<IncidentByIdQuery, IncidentByIdQueryVariables>;
export const MeDocument = gql`
    query Me {
  me {
    ...RegularUser
    connections {
      provider
      displayName
      visible
    }
    roles
  }
}
    ${RegularUserFragmentDoc}`;

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
      }
export function useMeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
        }
export function useMeSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<MeQuery, MeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<MeQuery, MeQueryVariables>(MeDocument, options);
        }
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeSuspenseQueryHookResult = ReturnType<typeof useMeSuspenseQuery>;
export type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;
export const MyConnectionsDocument = gql`
    query MyConnections {
  me {
    connections {
      provider
      displayName
      email
      visible
    }
  }
}
    `;

/**
 * __useMyConnectionsQuery__
 *
 * To run a query within a React component, call `useMyConnectionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyConnectionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyConnectionsQuery({
 *   variables: {
 *   },
 * });
 */
export function useMyConnectionsQuery(baseOptions?: Apollo.QueryHookOptions<MyConnectionsQuery, MyConnectionsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MyConnectionsQuery, MyConnectionsQueryVariables>(MyConnectionsDocument, options);
      }
export function useMyConnectionsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MyConnectionsQuery, MyConnectionsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MyConnectionsQuery, MyConnectionsQueryVariables>(MyConnectionsDocument, options);
        }
export function useMyConnectionsSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<MyConnectionsQuery, MyConnectionsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<MyConnectionsQuery, MyConnectionsQueryVariables>(MyConnectionsDocument, options);
        }
export type MyConnectionsQueryHookResult = ReturnType<typeof useMyConnectionsQuery>;
export type MyConnectionsLazyQueryHookResult = ReturnType<typeof useMyConnectionsLazyQuery>;
export type MyConnectionsSuspenseQueryHookResult = ReturnType<typeof useMyConnectionsSuspenseQuery>;
export type MyConnectionsQueryResult = Apollo.QueryResult<MyConnectionsQuery, MyConnectionsQueryVariables>;
export const MyInventoriesDocument = gql`
    query MyInventories {
  myInventories {
    name
    stash
    items
    visible
    uploadedAt
    character {
      ...PreviewCharacter
    }
  }
}
    ${PreviewCharacterFragmentDoc}`;

/**
 * __useMyInventoriesQuery__
 *
 * To run a query within a React component, call `useMyInventoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyInventoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyInventoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useMyInventoriesQuery(baseOptions?: Apollo.QueryHookOptions<MyInventoriesQuery, MyInventoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MyInventoriesQuery, MyInventoriesQueryVariables>(MyInventoriesDocument, options);
      }
export function useMyInventoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MyInventoriesQuery, MyInventoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MyInventoriesQuery, MyInventoriesQueryVariables>(MyInventoriesDocument, options);
        }
export function useMyInventoriesSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<MyInventoriesQuery, MyInventoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<MyInventoriesQuery, MyInventoriesQueryVariables>(MyInventoriesDocument, options);
        }
export type MyInventoriesQueryHookResult = ReturnType<typeof useMyInventoriesQuery>;
export type MyInventoriesLazyQueryHookResult = ReturnType<typeof useMyInventoriesLazyQuery>;
export type MyInventoriesSuspenseQueryHookResult = ReturnType<typeof useMyInventoriesSuspenseQuery>;
export type MyInventoriesQueryResult = Apollo.QueryResult<MyInventoriesQuery, MyInventoriesQueryVariables>;
export const PassiveTreesDocument = gql`
    query PassiveTrees($input: PaginatedPassiveTreesInput!) {
  passiveTrees(input: $input) {
    error
    passiveTrees {
      passives
      count
    }
    lastUpdated
    hasMore
  }
}
    `;

/**
 * __usePassiveTreesQuery__
 *
 * To run a query within a React component, call `usePassiveTreesQuery` and pass it any options that fit your needs.
 * When your component renders, `usePassiveTreesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePassiveTreesQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function usePassiveTreesQuery(baseOptions: Apollo.QueryHookOptions<PassiveTreesQuery, PassiveTreesQueryVariables> & ({ variables: PassiveTreesQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<PassiveTreesQuery, PassiveTreesQueryVariables>(PassiveTreesDocument, options);
      }
export function usePassiveTreesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PassiveTreesQuery, PassiveTreesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<PassiveTreesQuery, PassiveTreesQueryVariables>(PassiveTreesDocument, options);
        }
export function usePassiveTreesSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<PassiveTreesQuery, PassiveTreesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<PassiveTreesQuery, PassiveTreesQueryVariables>(PassiveTreesDocument, options);
        }
export type PassiveTreesQueryHookResult = ReturnType<typeof usePassiveTreesQuery>;
export type PassiveTreesLazyQueryHookResult = ReturnType<typeof usePassiveTreesLazyQuery>;
export type PassiveTreesSuspenseQueryHookResult = ReturnType<typeof usePassiveTreesSuspenseQuery>;
export type PassiveTreesQueryResult = Apollo.QueryResult<PassiveTreesQuery, PassiveTreesQueryVariables>;
export const PreviewCharactersDocument = gql`
    query PreviewCharacters($input: OffsetPaginatedCharactersInput!) {
  characters(input: $input) {
    cursor
    hasMore
    characters {
      ...PreviewCharacter
    }
  }
}
    ${PreviewCharacterFragmentDoc}`;

/**
 * __usePreviewCharactersQuery__
 *
 * To run a query within a React component, call `usePreviewCharactersQuery` and pass it any options that fit your needs.
 * When your component renders, `usePreviewCharactersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePreviewCharactersQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function usePreviewCharactersQuery(baseOptions: Apollo.QueryHookOptions<PreviewCharactersQuery, PreviewCharactersQueryVariables> & ({ variables: PreviewCharactersQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<PreviewCharactersQuery, PreviewCharactersQueryVariables>(PreviewCharactersDocument, options);
      }
export function usePreviewCharactersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PreviewCharactersQuery, PreviewCharactersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<PreviewCharactersQuery, PreviewCharactersQueryVariables>(PreviewCharactersDocument, options);
        }
export function usePreviewCharactersSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<PreviewCharactersQuery, PreviewCharactersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<PreviewCharactersQuery, PreviewCharactersQueryVariables>(PreviewCharactersDocument, options);
        }
export type PreviewCharactersQueryHookResult = ReturnType<typeof usePreviewCharactersQuery>;
export type PreviewCharactersLazyQueryHookResult = ReturnType<typeof usePreviewCharactersLazyQuery>;
export type PreviewCharactersSuspenseQueryHookResult = ReturnType<typeof usePreviewCharactersSuspenseQuery>;
export type PreviewCharactersQueryResult = Apollo.QueryResult<PreviewCharactersQuery, PreviewCharactersQueryVariables>;
export const RecentCharactersDocument = gql`
    query RecentCharacters($cursor: String, $limit: Int) {
  recentCharacters(input: {cursor: $cursor, limit: $limit}) {
    cursor
    hasMore
    characters {
      ...PreviewCharacter
    }
  }
}
    ${PreviewCharacterFragmentDoc}`;

/**
 * __useRecentCharactersQuery__
 *
 * To run a query within a React component, call `useRecentCharactersQuery` and pass it any options that fit your needs.
 * When your component renders, `useRecentCharactersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRecentCharactersQuery({
 *   variables: {
 *      cursor: // value for 'cursor'
 *      limit: // value for 'limit'
 *   },
 * });
 */
export function useRecentCharactersQuery(baseOptions?: Apollo.QueryHookOptions<RecentCharactersQuery, RecentCharactersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<RecentCharactersQuery, RecentCharactersQueryVariables>(RecentCharactersDocument, options);
      }
export function useRecentCharactersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<RecentCharactersQuery, RecentCharactersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<RecentCharactersQuery, RecentCharactersQueryVariables>(RecentCharactersDocument, options);
        }
export function useRecentCharactersSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<RecentCharactersQuery, RecentCharactersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<RecentCharactersQuery, RecentCharactersQueryVariables>(RecentCharactersDocument, options);
        }
export type RecentCharactersQueryHookResult = ReturnType<typeof useRecentCharactersQuery>;
export type RecentCharactersLazyQueryHookResult = ReturnType<typeof useRecentCharactersLazyQuery>;
export type RecentCharactersSuspenseQueryHookResult = ReturnType<typeof useRecentCharactersSuspenseQuery>;
export type RecentCharactersQueryResult = Apollo.QueryResult<RecentCharactersQuery, RecentCharactersQueryVariables>;
export const RecentSpritesDocument = gql`
    query RecentSprites($cursor: String, $limit: Int) {
  recentSprites(input: {cursor: $cursor, limit: $limit}) {
    cursor
    hasMore
    sprites {
      id
      name
      description
      visible
      deleted
      size
      pixels
      user {
        id
        displayName
      }
    }
  }
}
    `;

/**
 * __useRecentSpritesQuery__
 *
 * To run a query within a React component, call `useRecentSpritesQuery` and pass it any options that fit your needs.
 * When your component renders, `useRecentSpritesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRecentSpritesQuery({
 *   variables: {
 *      cursor: // value for 'cursor'
 *      limit: // value for 'limit'
 *   },
 * });
 */
export function useRecentSpritesQuery(baseOptions?: Apollo.QueryHookOptions<RecentSpritesQuery, RecentSpritesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<RecentSpritesQuery, RecentSpritesQueryVariables>(RecentSpritesDocument, options);
      }
export function useRecentSpritesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<RecentSpritesQuery, RecentSpritesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<RecentSpritesQuery, RecentSpritesQueryVariables>(RecentSpritesDocument, options);
        }
export function useRecentSpritesSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<RecentSpritesQuery, RecentSpritesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<RecentSpritesQuery, RecentSpritesQueryVariables>(RecentSpritesDocument, options);
        }
export type RecentSpritesQueryHookResult = ReturnType<typeof useRecentSpritesQuery>;
export type RecentSpritesLazyQueryHookResult = ReturnType<typeof useRecentSpritesLazyQuery>;
export type RecentSpritesSuspenseQueryHookResult = ReturnType<typeof useRecentSpritesSuspenseQuery>;
export type RecentSpritesQueryResult = Apollo.QueryResult<RecentSpritesQuery, RecentSpritesQueryVariables>;
export const RuneLoadoutsDocument = gql`
    query RuneLoadouts {
  runeLoadouts {
    loadouts {
      runes
      count
    }
  }
}
    `;

/**
 * __useRuneLoadoutsQuery__
 *
 * To run a query within a React component, call `useRuneLoadoutsQuery` and pass it any options that fit your needs.
 * When your component renders, `useRuneLoadoutsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRuneLoadoutsQuery({
 *   variables: {
 *   },
 * });
 */
export function useRuneLoadoutsQuery(baseOptions?: Apollo.QueryHookOptions<RuneLoadoutsQuery, RuneLoadoutsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<RuneLoadoutsQuery, RuneLoadoutsQueryVariables>(RuneLoadoutsDocument, options);
      }
export function useRuneLoadoutsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<RuneLoadoutsQuery, RuneLoadoutsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<RuneLoadoutsQuery, RuneLoadoutsQueryVariables>(RuneLoadoutsDocument, options);
        }
export function useRuneLoadoutsSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<RuneLoadoutsQuery, RuneLoadoutsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<RuneLoadoutsQuery, RuneLoadoutsQueryVariables>(RuneLoadoutsDocument, options);
        }
export type RuneLoadoutsQueryHookResult = ReturnType<typeof useRuneLoadoutsQuery>;
export type RuneLoadoutsLazyQueryHookResult = ReturnType<typeof useRuneLoadoutsLazyQuery>;
export type RuneLoadoutsSuspenseQueryHookResult = ReturnType<typeof useRuneLoadoutsSuspenseQuery>;
export type RuneLoadoutsQueryResult = Apollo.QueryResult<RuneLoadoutsQuery, RuneLoadoutsQueryVariables>;
export const SearchCharactersDocument = gql`
    query SearchCharacters($name: String!) {
  searchCharacters(name: $name) {
    ...PreviewCharacter
  }
}
    ${PreviewCharacterFragmentDoc}`;

/**
 * __useSearchCharactersQuery__
 *
 * To run a query within a React component, call `useSearchCharactersQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchCharactersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchCharactersQuery({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function useSearchCharactersQuery(baseOptions: Apollo.QueryHookOptions<SearchCharactersQuery, SearchCharactersQueryVariables> & ({ variables: SearchCharactersQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchCharactersQuery, SearchCharactersQueryVariables>(SearchCharactersDocument, options);
      }
export function useSearchCharactersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchCharactersQuery, SearchCharactersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchCharactersQuery, SearchCharactersQueryVariables>(SearchCharactersDocument, options);
        }
export function useSearchCharactersSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<SearchCharactersQuery, SearchCharactersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<SearchCharactersQuery, SearchCharactersQueryVariables>(SearchCharactersDocument, options);
        }
export type SearchCharactersQueryHookResult = ReturnType<typeof useSearchCharactersQuery>;
export type SearchCharactersLazyQueryHookResult = ReturnType<typeof useSearchCharactersLazyQuery>;
export type SearchCharactersSuspenseQueryHookResult = ReturnType<typeof useSearchCharactersSuspenseQuery>;
export type SearchCharactersQueryResult = Apollo.QueryResult<SearchCharactersQuery, SearchCharactersQueryVariables>;
export const SearchIncidentsDocument = gql`
    query SearchIncidents($filter: SearchIncidentsInput!) {
  searchIncidents(filter: $filter) {
    id
    resolved
    title
    description
    reportedBy {
      id
      displayName
    }
    reportedUser {
      id
      displayName
    }
    reportedProfile
    reportedCharacter {
      id
      name
    }
    reportedSprite {
      id
      name
      description
      visible
      deleted
      size
      pixels
      user {
        id
        displayName
      }
      createdAt
    }
    createdAt
  }
}
    `;

/**
 * __useSearchIncidentsQuery__
 *
 * To run a query within a React component, call `useSearchIncidentsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchIncidentsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchIncidentsQuery({
 *   variables: {
 *      filter: // value for 'filter'
 *   },
 * });
 */
export function useSearchIncidentsQuery(baseOptions: Apollo.QueryHookOptions<SearchIncidentsQuery, SearchIncidentsQueryVariables> & ({ variables: SearchIncidentsQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchIncidentsQuery, SearchIncidentsQueryVariables>(SearchIncidentsDocument, options);
      }
export function useSearchIncidentsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchIncidentsQuery, SearchIncidentsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchIncidentsQuery, SearchIncidentsQueryVariables>(SearchIncidentsDocument, options);
        }
export function useSearchIncidentsSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<SearchIncidentsQuery, SearchIncidentsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<SearchIncidentsQuery, SearchIncidentsQueryVariables>(SearchIncidentsDocument, options);
        }
export type SearchIncidentsQueryHookResult = ReturnType<typeof useSearchIncidentsQuery>;
export type SearchIncidentsLazyQueryHookResult = ReturnType<typeof useSearchIncidentsLazyQuery>;
export type SearchIncidentsSuspenseQueryHookResult = ReturnType<typeof useSearchIncidentsSuspenseQuery>;
export type SearchIncidentsQueryResult = Apollo.QueryResult<SearchIncidentsQuery, SearchIncidentsQueryVariables>;
export const SpriteDetailsDocument = gql`
    query SpriteDetails($id: String!) {
  spriteById(id: $id) {
    id
    name
    description
    visible
    deleted
    size
    pixels
    user {
      id
      displayName
    }
    createdAt
  }
}
    `;

/**
 * __useSpriteDetailsQuery__
 *
 * To run a query within a React component, call `useSpriteDetailsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSpriteDetailsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSpriteDetailsQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useSpriteDetailsQuery(baseOptions: Apollo.QueryHookOptions<SpriteDetailsQuery, SpriteDetailsQueryVariables> & ({ variables: SpriteDetailsQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SpriteDetailsQuery, SpriteDetailsQueryVariables>(SpriteDetailsDocument, options);
      }
export function useSpriteDetailsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SpriteDetailsQuery, SpriteDetailsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SpriteDetailsQuery, SpriteDetailsQueryVariables>(SpriteDetailsDocument, options);
        }
export function useSpriteDetailsSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<SpriteDetailsQuery, SpriteDetailsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<SpriteDetailsQuery, SpriteDetailsQueryVariables>(SpriteDetailsDocument, options);
        }
export type SpriteDetailsQueryHookResult = ReturnType<typeof useSpriteDetailsQuery>;
export type SpriteDetailsLazyQueryHookResult = ReturnType<typeof useSpriteDetailsLazyQuery>;
export type SpriteDetailsSuspenseQueryHookResult = ReturnType<typeof useSpriteDetailsSuspenseQuery>;
export type SpriteDetailsQueryResult = Apollo.QueryResult<SpriteDetailsQuery, SpriteDetailsQueryVariables>;
export const TopSkinsDocument = gql`
    query TopSkins {
  topSkins {
    skins {
      id
      cell
      sheetName
      count
    }
    lastUpdated
  }
}
    `;

/**
 * __useTopSkinsQuery__
 *
 * To run a query within a React component, call `useTopSkinsQuery` and pass it any options that fit your needs.
 * When your component renders, `useTopSkinsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTopSkinsQuery({
 *   variables: {
 *   },
 * });
 */
export function useTopSkinsQuery(baseOptions?: Apollo.QueryHookOptions<TopSkinsQuery, TopSkinsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TopSkinsQuery, TopSkinsQueryVariables>(TopSkinsDocument, options);
      }
export function useTopSkinsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TopSkinsQuery, TopSkinsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TopSkinsQuery, TopSkinsQueryVariables>(TopSkinsDocument, options);
        }
export function useTopSkinsSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<TopSkinsQuery, TopSkinsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<TopSkinsQuery, TopSkinsQueryVariables>(TopSkinsDocument, options);
        }
export type TopSkinsQueryHookResult = ReturnType<typeof useTopSkinsQuery>;
export type TopSkinsLazyQueryHookResult = ReturnType<typeof useTopSkinsLazyQuery>;
export type TopSkinsSuspenseQueryHookResult = ReturnType<typeof useTopSkinsSuspenseQuery>;
export type TopSkinsQueryResult = Apollo.QueryResult<TopSkinsQuery, TopSkinsQueryVariables>;
export const UserDocument = gql`
    query User($id: String!) {
  user(id: $id) {
    ...RegularUser
    connections {
      provider
      displayName
      visible
    }
    roles
  }
}
    ${RegularUserFragmentDoc}`;

/**
 * __useUserQuery__
 *
 * To run a query within a React component, call `useUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUserQuery(baseOptions: Apollo.QueryHookOptions<UserQuery, UserQueryVariables> & ({ variables: UserQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UserQuery, UserQueryVariables>(UserDocument, options);
      }
export function useUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserQuery, UserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UserQuery, UserQueryVariables>(UserDocument, options);
        }
export function useUserSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<UserQuery, UserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<UserQuery, UserQueryVariables>(UserDocument, options);
        }
export type UserQueryHookResult = ReturnType<typeof useUserQuery>;
export type UserLazyQueryHookResult = ReturnType<typeof useUserLazyQuery>;
export type UserSuspenseQueryHookResult = ReturnType<typeof useUserSuspenseQuery>;
export type UserQueryResult = Apollo.QueryResult<UserQuery, UserQueryVariables>;
export const UserSpritesDocument = gql`
    query UserSprites($user: String!, $offset: Int, $limit: Int) {
  sprites(input: {offset: $offset, limit: $limit, filter: {userId: $user}}) {
    cursor
    hasMore
    sprites {
      id
      name
      description
      visible
      size
      pixels
      user {
        id
        displayName
      }
    }
  }
}
    `;

/**
 * __useUserSpritesQuery__
 *
 * To run a query within a React component, call `useUserSpritesQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserSpritesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserSpritesQuery({
 *   variables: {
 *      user: // value for 'user'
 *      offset: // value for 'offset'
 *      limit: // value for 'limit'
 *   },
 * });
 */
export function useUserSpritesQuery(baseOptions: Apollo.QueryHookOptions<UserSpritesQuery, UserSpritesQueryVariables> & ({ variables: UserSpritesQueryVariables; skip?: boolean; } | { skip: boolean; }) ) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UserSpritesQuery, UserSpritesQueryVariables>(UserSpritesDocument, options);
      }
export function useUserSpritesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserSpritesQuery, UserSpritesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UserSpritesQuery, UserSpritesQueryVariables>(UserSpritesDocument, options);
        }
export function useUserSpritesSuspenseQuery(baseOptions?: Apollo.SuspenseQueryHookOptions<UserSpritesQuery, UserSpritesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useSuspenseQuery<UserSpritesQuery, UserSpritesQueryVariables>(UserSpritesDocument, options);
        }
export type UserSpritesQueryHookResult = ReturnType<typeof useUserSpritesQuery>;
export type UserSpritesLazyQueryHookResult = ReturnType<typeof useUserSpritesLazyQuery>;
export type UserSpritesSuspenseQueryHookResult = ReturnType<typeof useUserSpritesSuspenseQuery>;
export type UserSpritesQueryResult = Apollo.QueryResult<UserSpritesQuery, UserSpritesQueryVariables>;
export type AuthTokenKeySpecifier = ('createdAt' | 'id' | 'label' | 'token' | 'updatedAt' | 'user' | 'userId' | AuthTokenKeySpecifier)[];
export type AuthTokenFieldPolicy = {
	createdAt?: FieldPolicy<any> | FieldReadFunction<any>,
	id?: FieldPolicy<any> | FieldReadFunction<any>,
	label?: FieldPolicy<any> | FieldReadFunction<any>,
	token?: FieldPolicy<any> | FieldReadFunction<any>,
	updatedAt?: FieldPolicy<any> | FieldReadFunction<any>,
	user?: FieldPolicy<any> | FieldReadFunction<any>,
	userId?: FieldPolicy<any> | FieldReadFunction<any>
};
export type ChangeDisplayNameResponseKeySpecifier = ('errors' | 'success' | ChangeDisplayNameResponseKeySpecifier)[];
export type ChangeDisplayNameResponseFieldPolicy = {
	errors?: FieldPolicy<any> | FieldReadFunction<any>,
	success?: FieldPolicy<any> | FieldReadFunction<any>
};
export type CharacterKeySpecifier = ('createdAt' | 'eq_chest' | 'eq_feet' | 'eq_finger1' | 'eq_finger2' | 'eq_hands' | 'eq_head' | 'eq_legs' | 'eq_neck' | 'eq_offHand' | 'eq_oneHanded' | 'eq_quickslot' | 'eq_rune1' | 'eq_rune2' | 'eq_rune3' | 'eq_rune4' | 'eq_tool' | 'eq_trinket' | 'eq_waist' | 'id' | 'inventory' | 'isMine' | 'league' | 'level' | 'name' | 'passives' | 'portraitX' | 'portraitY' | 'prophecy_austere' | 'prophecy_butcher' | 'prophecy_crushable' | 'prophecy_hardcore' | 'rankings' | 'rep_akarei' | 'rep_fjolgard' | 'rep_gaekatla' | 'rep_peopleOfTheSun' | 'rep_pumpkinSailor' | 'rep_theWinterMan' | 'skinCell' | 'skinId' | 'skinSheetName' | 'spirit' | 'updatedAt' | 'uploadedAt' | 'user' | 'userId' | CharacterKeySpecifier)[];
export type CharacterFieldPolicy = {
	createdAt?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_chest?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_feet?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_finger1?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_finger2?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_hands?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_head?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_legs?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_neck?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_offHand?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_oneHanded?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_quickslot?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_rune1?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_rune2?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_rune3?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_rune4?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_tool?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_trinket?: FieldPolicy<any> | FieldReadFunction<any>,
	eq_waist?: FieldPolicy<any> | FieldReadFunction<any>,
	id?: FieldPolicy<any> | FieldReadFunction<any>,
	inventory?: FieldPolicy<any> | FieldReadFunction<any>,
	isMine?: FieldPolicy<any> | FieldReadFunction<any>,
	league?: FieldPolicy<any> | FieldReadFunction<any>,
	level?: FieldPolicy<any> | FieldReadFunction<any>,
	name?: FieldPolicy<any> | FieldReadFunction<any>,
	passives?: FieldPolicy<any> | FieldReadFunction<any>,
	portraitX?: FieldPolicy<any> | FieldReadFunction<any>,
	portraitY?: FieldPolicy<any> | FieldReadFunction<any>,
	prophecy_austere?: FieldPolicy<any> | FieldReadFunction<any>,
	prophecy_butcher?: FieldPolicy<any> | FieldReadFunction<any>,
	prophecy_crushable?: FieldPolicy<any> | FieldReadFunction<any>,
	prophecy_hardcore?: FieldPolicy<any> | FieldReadFunction<any>,
	rankings?: FieldPolicy<any> | FieldReadFunction<any>,
	rep_akarei?: FieldPolicy<any> | FieldReadFunction<any>,
	rep_fjolgard?: FieldPolicy<any> | FieldReadFunction<any>,
	rep_gaekatla?: FieldPolicy<any> | FieldReadFunction<any>,
	rep_peopleOfTheSun?: FieldPolicy<any> | FieldReadFunction<any>,
	rep_pumpkinSailor?: FieldPolicy<any> | FieldReadFunction<any>,
	rep_theWinterMan?: FieldPolicy<any> | FieldReadFunction<any>,
	skinCell?: FieldPolicy<any> | FieldReadFunction<any>,
	skinId?: FieldPolicy<any> | FieldReadFunction<any>,
	skinSheetName?: FieldPolicy<any> | FieldReadFunction<any>,
	spirit?: FieldPolicy<any> | FieldReadFunction<any>,
	updatedAt?: FieldPolicy<any> | FieldReadFunction<any>,
	uploadedAt?: FieldPolicy<any> | FieldReadFunction<any>,
	user?: FieldPolicy<any> | FieldReadFunction<any>,
	userId?: FieldPolicy<any> | FieldReadFunction<any>
};
export type CharacterRankingKeySpecifier = ('leaderboard' | 'rank' | CharacterRankingKeySpecifier)[];
export type CharacterRankingFieldPolicy = {
	leaderboard?: FieldPolicy<any> | FieldReadFunction<any>,
	rank?: FieldPolicy<any> | FieldReadFunction<any>
};
export type CursorPaginatedCharactersKeySpecifier = ('characters' | 'cursor' | 'hasMore' | CursorPaginatedCharactersKeySpecifier)[];
export type CursorPaginatedCharactersFieldPolicy = {
	characters?: FieldPolicy<any> | FieldReadFunction<any>,
	cursor?: FieldPolicy<any> | FieldReadFunction<any>,
	hasMore?: FieldPolicy<any> | FieldReadFunction<any>
};
export type CursorPaginatedSpritesKeySpecifier = ('cursor' | 'hasMore' | 'sprites' | CursorPaginatedSpritesKeySpecifier)[];
export type CursorPaginatedSpritesFieldPolicy = {
	cursor?: FieldPolicy<any> | FieldReadFunction<any>,
	hasMore?: FieldPolicy<any> | FieldReadFunction<any>,
	sprites?: FieldPolicy<any> | FieldReadFunction<any>
};
export type FieldErrorKeySpecifier = ('field' | 'message' | FieldErrorKeySpecifier)[];
export type FieldErrorFieldPolicy = {
	field?: FieldPolicy<any> | FieldReadFunction<any>,
	message?: FieldPolicy<any> | FieldReadFunction<any>
};
export type IncidentKeySpecifier = ('createdAt' | 'description' | 'id' | 'reportedBy' | 'reportedById' | 'reportedCharacter' | 'reportedCharacterId' | 'reportedProfile' | 'reportedSprite' | 'reportedSpriteId' | 'reportedUser' | 'reportedUserId' | 'resolved' | 'title' | 'updatedAt' | IncidentKeySpecifier)[];
export type IncidentFieldPolicy = {
	createdAt?: FieldPolicy<any> | FieldReadFunction<any>,
	description?: FieldPolicy<any> | FieldReadFunction<any>,
	id?: FieldPolicy<any> | FieldReadFunction<any>,
	reportedBy?: FieldPolicy<any> | FieldReadFunction<any>,
	reportedById?: FieldPolicy<any> | FieldReadFunction<any>,
	reportedCharacter?: FieldPolicy<any> | FieldReadFunction<any>,
	reportedCharacterId?: FieldPolicy<any> | FieldReadFunction<any>,
	reportedProfile?: FieldPolicy<any> | FieldReadFunction<any>,
	reportedSprite?: FieldPolicy<any> | FieldReadFunction<any>,
	reportedSpriteId?: FieldPolicy<any> | FieldReadFunction<any>,
	reportedUser?: FieldPolicy<any> | FieldReadFunction<any>,
	reportedUserId?: FieldPolicy<any> | FieldReadFunction<any>,
	resolved?: FieldPolicy<any> | FieldReadFunction<any>,
	title?: FieldPolicy<any> | FieldReadFunction<any>,
	updatedAt?: FieldPolicy<any> | FieldReadFunction<any>
};
export type InventoryKeySpecifier = ('character' | 'createdAt' | 'id' | 'items' | 'name' | 'stash' | 'updatedAt' | 'uploadedAt' | 'user' | 'userId' | 'visible' | InventoryKeySpecifier)[];
export type InventoryFieldPolicy = {
	character?: FieldPolicy<any> | FieldReadFunction<any>,
	createdAt?: FieldPolicy<any> | FieldReadFunction<any>,
	id?: FieldPolicy<any> | FieldReadFunction<any>,
	items?: FieldPolicy<any> | FieldReadFunction<any>,
	name?: FieldPolicy<any> | FieldReadFunction<any>,
	stash?: FieldPolicy<any> | FieldReadFunction<any>,
	updatedAt?: FieldPolicy<any> | FieldReadFunction<any>,
	uploadedAt?: FieldPolicy<any> | FieldReadFunction<any>,
	user?: FieldPolicy<any> | FieldReadFunction<any>,
	userId?: FieldPolicy<any> | FieldReadFunction<any>,
	visible?: FieldPolicy<any> | FieldReadFunction<any>
};
export type LeaderboardResultKeySpecifier = ('characters' | 'error' | 'lastUpdated' | LeaderboardResultKeySpecifier)[];
export type LeaderboardResultFieldPolicy = {
	characters?: FieldPolicy<any> | FieldReadFunction<any>,
	error?: FieldPolicy<any> | FieldReadFunction<any>,
	lastUpdated?: FieldPolicy<any> | FieldReadFunction<any>
};
export type MutationKeySpecifier = ('changeDisplayName' | 'createAuthToken' | 'deleteCharacter' | 'generateOTP' | 'logout' | 'performMerge' | 'postSprite' | 'reportCharacter' | 'reportSprite' | 'reportUser' | 'revokeAuthToken' | 'setConnectionVisible' | 'setIncidentResolved' | 'setInventoryVisible' | 'setSpriteDeleted' | 'setSpriteVisible' | 'setUserRole' | MutationKeySpecifier)[];
export type MutationFieldPolicy = {
	changeDisplayName?: FieldPolicy<any> | FieldReadFunction<any>,
	createAuthToken?: FieldPolicy<any> | FieldReadFunction<any>,
	deleteCharacter?: FieldPolicy<any> | FieldReadFunction<any>,
	generateOTP?: FieldPolicy<any> | FieldReadFunction<any>,
	logout?: FieldPolicy<any> | FieldReadFunction<any>,
	performMerge?: FieldPolicy<any> | FieldReadFunction<any>,
	postSprite?: FieldPolicy<any> | FieldReadFunction<any>,
	reportCharacter?: FieldPolicy<any> | FieldReadFunction<any>,
	reportSprite?: FieldPolicy<any> | FieldReadFunction<any>,
	reportUser?: FieldPolicy<any> | FieldReadFunction<any>,
	revokeAuthToken?: FieldPolicy<any> | FieldReadFunction<any>,
	setConnectionVisible?: FieldPolicy<any> | FieldReadFunction<any>,
	setIncidentResolved?: FieldPolicy<any> | FieldReadFunction<any>,
	setInventoryVisible?: FieldPolicy<any> | FieldReadFunction<any>,
	setSpriteDeleted?: FieldPolicy<any> | FieldReadFunction<any>,
	setSpriteVisible?: FieldPolicy<any> | FieldReadFunction<any>,
	setUserRole?: FieldPolicy<any> | FieldReadFunction<any>
};
export type OffsetPaginatedCharactersKeySpecifier = ('characters' | 'cursor' | 'hasMore' | OffsetPaginatedCharactersKeySpecifier)[];
export type OffsetPaginatedCharactersFieldPolicy = {
	characters?: FieldPolicy<any> | FieldReadFunction<any>,
	cursor?: FieldPolicy<any> | FieldReadFunction<any>,
	hasMore?: FieldPolicy<any> | FieldReadFunction<any>
};
export type OffsetPaginatedSpritesKeySpecifier = ('cursor' | 'hasMore' | 'sprites' | OffsetPaginatedSpritesKeySpecifier)[];
export type OffsetPaginatedSpritesFieldPolicy = {
	cursor?: FieldPolicy<any> | FieldReadFunction<any>,
	hasMore?: FieldPolicy<any> | FieldReadFunction<any>,
	sprites?: FieldPolicy<any> | FieldReadFunction<any>
};
export type PaginatedPassiveTreesKeySpecifier = ('error' | 'hasMore' | 'lastUpdated' | 'passiveTrees' | PaginatedPassiveTreesKeySpecifier)[];
export type PaginatedPassiveTreesFieldPolicy = {
	error?: FieldPolicy<any> | FieldReadFunction<any>,
	hasMore?: FieldPolicy<any> | FieldReadFunction<any>,
	lastUpdated?: FieldPolicy<any> | FieldReadFunction<any>,
	passiveTrees?: FieldPolicy<any> | FieldReadFunction<any>
};
export type PassiveTreeWithCountKeySpecifier = ('count' | 'passives' | PassiveTreeWithCountKeySpecifier)[];
export type PassiveTreeWithCountFieldPolicy = {
	count?: FieldPolicy<any> | FieldReadFunction<any>,
	passives?: FieldPolicy<any> | FieldReadFunction<any>
};
export type QueryKeySpecifier = ('authTokens' | 'character' | 'characterById' | 'characters' | 'factionLeaderboard' | 'getMergeInfo' | 'incident' | 'me' | 'myInventories' | 'passiveTrees' | 'recentCharacters' | 'recentSprites' | 'runeLoadouts' | 'searchCharacters' | 'searchIncidents' | 'searchSprites' | 'spriteById' | 'sprites' | 'topSkins' | 'user' | QueryKeySpecifier)[];
export type QueryFieldPolicy = {
	authTokens?: FieldPolicy<any> | FieldReadFunction<any>,
	character?: FieldPolicy<any> | FieldReadFunction<any>,
	characterById?: FieldPolicy<any> | FieldReadFunction<any>,
	characters?: FieldPolicy<any> | FieldReadFunction<any>,
	factionLeaderboard?: FieldPolicy<any> | FieldReadFunction<any>,
	getMergeInfo?: FieldPolicy<any> | FieldReadFunction<any>,
	incident?: FieldPolicy<any> | FieldReadFunction<any>,
	me?: FieldPolicy<any> | FieldReadFunction<any>,
	myInventories?: FieldPolicy<any> | FieldReadFunction<any>,
	passiveTrees?: FieldPolicy<any> | FieldReadFunction<any>,
	recentCharacters?: FieldPolicy<any> | FieldReadFunction<any>,
	recentSprites?: FieldPolicy<any> | FieldReadFunction<any>,
	runeLoadouts?: FieldPolicy<any> | FieldReadFunction<any>,
	searchCharacters?: FieldPolicy<any> | FieldReadFunction<any>,
	searchIncidents?: FieldPolicy<any> | FieldReadFunction<any>,
	searchSprites?: FieldPolicy<any> | FieldReadFunction<any>,
	spriteById?: FieldPolicy<any> | FieldReadFunction<any>,
	sprites?: FieldPolicy<any> | FieldReadFunction<any>,
	topSkins?: FieldPolicy<any> | FieldReadFunction<any>,
	user?: FieldPolicy<any> | FieldReadFunction<any>
};
export type RuneLoadoutKeySpecifier = ('count' | 'runes' | RuneLoadoutKeySpecifier)[];
export type RuneLoadoutFieldPolicy = {
	count?: FieldPolicy<any> | FieldReadFunction<any>,
	runes?: FieldPolicy<any> | FieldReadFunction<any>
};
export type RuneLoadoutsResultKeySpecifier = ('lastUpdated' | 'loadouts' | RuneLoadoutsResultKeySpecifier)[];
export type RuneLoadoutsResultFieldPolicy = {
	lastUpdated?: FieldPolicy<any> | FieldReadFunction<any>,
	loadouts?: FieldPolicy<any> | FieldReadFunction<any>
};
export type SkinRankingKeySpecifier = ('cell' | 'count' | 'id' | 'sheetName' | SkinRankingKeySpecifier)[];
export type SkinRankingFieldPolicy = {
	cell?: FieldPolicy<any> | FieldReadFunction<any>,
	count?: FieldPolicy<any> | FieldReadFunction<any>,
	id?: FieldPolicy<any> | FieldReadFunction<any>,
	sheetName?: FieldPolicy<any> | FieldReadFunction<any>
};
export type SpriteKeySpecifier = ('createdAt' | 'deleted' | 'description' | 'id' | 'name' | 'pixels' | 'size' | 'updatedAt' | 'user' | 'userId' | 'visible' | SpriteKeySpecifier)[];
export type SpriteFieldPolicy = {
	createdAt?: FieldPolicy<any> | FieldReadFunction<any>,
	deleted?: FieldPolicy<any> | FieldReadFunction<any>,
	description?: FieldPolicy<any> | FieldReadFunction<any>,
	id?: FieldPolicy<any> | FieldReadFunction<any>,
	name?: FieldPolicy<any> | FieldReadFunction<any>,
	pixels?: FieldPolicy<any> | FieldReadFunction<any>,
	size?: FieldPolicy<any> | FieldReadFunction<any>,
	updatedAt?: FieldPolicy<any> | FieldReadFunction<any>,
	user?: FieldPolicy<any> | FieldReadFunction<any>,
	userId?: FieldPolicy<any> | FieldReadFunction<any>,
	visible?: FieldPolicy<any> | FieldReadFunction<any>
};
export type TopSkinsResultKeySpecifier = ('lastUpdated' | 'skins' | TopSkinsResultKeySpecifier)[];
export type TopSkinsResultFieldPolicy = {
	lastUpdated?: FieldPolicy<any> | FieldReadFunction<any>,
	skins?: FieldPolicy<any> | FieldReadFunction<any>
};
export type UserKeySpecifier = ('connections' | 'createdAt' | 'displayName' | 'id' | 'lastCharacter' | 'roles' | 'updatedAt' | UserKeySpecifier)[];
export type UserFieldPolicy = {
	connections?: FieldPolicy<any> | FieldReadFunction<any>,
	createdAt?: FieldPolicy<any> | FieldReadFunction<any>,
	displayName?: FieldPolicy<any> | FieldReadFunction<any>,
	id?: FieldPolicy<any> | FieldReadFunction<any>,
	lastCharacter?: FieldPolicy<any> | FieldReadFunction<any>,
	roles?: FieldPolicy<any> | FieldReadFunction<any>,
	updatedAt?: FieldPolicy<any> | FieldReadFunction<any>
};
export type UserConnectionKeySpecifier = ('displayName' | 'email' | 'provider' | 'visible' | UserConnectionKeySpecifier)[];
export type UserConnectionFieldPolicy = {
	displayName?: FieldPolicy<any> | FieldReadFunction<any>,
	email?: FieldPolicy<any> | FieldReadFunction<any>,
	provider?: FieldPolicy<any> | FieldReadFunction<any>,
	visible?: FieldPolicy<any> | FieldReadFunction<any>
};
export type UserMergeInfoKeySpecifier = ('current' | 'other' | UserMergeInfoKeySpecifier)[];
export type UserMergeInfoFieldPolicy = {
	current?: FieldPolicy<any> | FieldReadFunction<any>,
	other?: FieldPolicy<any> | FieldReadFunction<any>
};
export type StrictTypedTypePolicies = {
	AuthToken?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | AuthTokenKeySpecifier | (() => undefined | AuthTokenKeySpecifier),
		fields?: AuthTokenFieldPolicy,
	},
	ChangeDisplayNameResponse?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | ChangeDisplayNameResponseKeySpecifier | (() => undefined | ChangeDisplayNameResponseKeySpecifier),
		fields?: ChangeDisplayNameResponseFieldPolicy,
	},
	Character?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | CharacterKeySpecifier | (() => undefined | CharacterKeySpecifier),
		fields?: CharacterFieldPolicy,
	},
	CharacterRanking?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | CharacterRankingKeySpecifier | (() => undefined | CharacterRankingKeySpecifier),
		fields?: CharacterRankingFieldPolicy,
	},
	CursorPaginatedCharacters?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | CursorPaginatedCharactersKeySpecifier | (() => undefined | CursorPaginatedCharactersKeySpecifier),
		fields?: CursorPaginatedCharactersFieldPolicy,
	},
	CursorPaginatedSprites?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | CursorPaginatedSpritesKeySpecifier | (() => undefined | CursorPaginatedSpritesKeySpecifier),
		fields?: CursorPaginatedSpritesFieldPolicy,
	},
	FieldError?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | FieldErrorKeySpecifier | (() => undefined | FieldErrorKeySpecifier),
		fields?: FieldErrorFieldPolicy,
	},
	Incident?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | IncidentKeySpecifier | (() => undefined | IncidentKeySpecifier),
		fields?: IncidentFieldPolicy,
	},
	Inventory?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | InventoryKeySpecifier | (() => undefined | InventoryKeySpecifier),
		fields?: InventoryFieldPolicy,
	},
	LeaderboardResult?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | LeaderboardResultKeySpecifier | (() => undefined | LeaderboardResultKeySpecifier),
		fields?: LeaderboardResultFieldPolicy,
	},
	Mutation?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | MutationKeySpecifier | (() => undefined | MutationKeySpecifier),
		fields?: MutationFieldPolicy,
	},
	OffsetPaginatedCharacters?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | OffsetPaginatedCharactersKeySpecifier | (() => undefined | OffsetPaginatedCharactersKeySpecifier),
		fields?: OffsetPaginatedCharactersFieldPolicy,
	},
	OffsetPaginatedSprites?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | OffsetPaginatedSpritesKeySpecifier | (() => undefined | OffsetPaginatedSpritesKeySpecifier),
		fields?: OffsetPaginatedSpritesFieldPolicy,
	},
	PaginatedPassiveTrees?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | PaginatedPassiveTreesKeySpecifier | (() => undefined | PaginatedPassiveTreesKeySpecifier),
		fields?: PaginatedPassiveTreesFieldPolicy,
	},
	PassiveTreeWithCount?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | PassiveTreeWithCountKeySpecifier | (() => undefined | PassiveTreeWithCountKeySpecifier),
		fields?: PassiveTreeWithCountFieldPolicy,
	},
	Query?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | QueryKeySpecifier | (() => undefined | QueryKeySpecifier),
		fields?: QueryFieldPolicy,
	},
	RuneLoadout?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | RuneLoadoutKeySpecifier | (() => undefined | RuneLoadoutKeySpecifier),
		fields?: RuneLoadoutFieldPolicy,
	},
	RuneLoadoutsResult?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | RuneLoadoutsResultKeySpecifier | (() => undefined | RuneLoadoutsResultKeySpecifier),
		fields?: RuneLoadoutsResultFieldPolicy,
	},
	SkinRanking?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | SkinRankingKeySpecifier | (() => undefined | SkinRankingKeySpecifier),
		fields?: SkinRankingFieldPolicy,
	},
	Sprite?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | SpriteKeySpecifier | (() => undefined | SpriteKeySpecifier),
		fields?: SpriteFieldPolicy,
	},
	TopSkinsResult?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | TopSkinsResultKeySpecifier | (() => undefined | TopSkinsResultKeySpecifier),
		fields?: TopSkinsResultFieldPolicy,
	},
	User?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | UserKeySpecifier | (() => undefined | UserKeySpecifier),
		fields?: UserFieldPolicy,
	},
	UserConnection?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | UserConnectionKeySpecifier | (() => undefined | UserConnectionKeySpecifier),
		fields?: UserConnectionFieldPolicy,
	},
	UserMergeInfo?: Omit<TypePolicy, "fields" | "keyFields"> & {
		keyFields?: false | UserMergeInfoKeySpecifier | (() => undefined | UserMergeInfoKeySpecifier),
		fields?: UserMergeInfoFieldPolicy,
	}
};
export type TypedTypePolicies = StrictTypedTypePolicies & TypePolicies;