export const PALETTE = [
	'#ff4252',
	'#d43346',
	'#a82841',
	'#802343',
	'#ff6942',
	'#db5538',
	'#b34b3a',
	'#953f36',
	'#e39a30',
	'#ca752f',
	'#b15a30',
	'#763b3b',
	'#ffeb38',
	'#faac45',
	'#d07840',
	'#9a5a3c',
	'#80f643',
	'#4ac441',
	'#386646',
	'#2b4b3e',
	'#baffd7',
	'#51fc9a',
	'#44cb95',
	'#3f8d6d',
	'#48edff',
	'#3fa7dd',
	'#3a71ba',
	'#42548d',
	'#a24eff',
	'#7a3ad3',
	'#533399',
	'#393268',
	'#fc66f7',
	'#de43ae',
	'#b4347a',
	'#933159',
	'#fcfcfc',
	'#c0c3cf',
	'#929398',
	'#69696e',
	'#505360',
	'#3c3f4c',
	'#373041',
	'#312136',
];

// 0 -> empty
// 1+ -> from palette
export const getPaletteColor = (index: number): string | null => {
    if (index > 0) {
        return PALETTE[index - 1]
    } else {
        return null;
    }
}