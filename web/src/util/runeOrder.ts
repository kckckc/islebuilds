export const runeOrder = [
	'Magic Missile',
	'Whirlwind',
	'Harvest Life',
	'Slash',
	'Smokebomb',
	'Crystal Spikes',
	'Ambush',

	'Smite',
	'Ice Spear',
	'Fireblast',
	'Charge',
	'Summon Skeleton',
	'Barbed Chain',

	'Consecrate',
	'Healing Touch',
	'Blood Barrier',
	'Flurry',

	'Roll',
	'Taunt',

	'Tranquility',
	'Swiftness',
	'Innervation',
];
