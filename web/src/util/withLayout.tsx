import { ComponentType, ReactNode } from 'react';
import { DefaultLayout } from '../components/templates/DefaultLayout';

export type LayoutProps = {
	children?: ReactNode;
};

export const withLayout = (WrapWith: ComponentType<LayoutProps>) => {
	return (WrappedComponent: ComponentType) => {
		const cpn: React.FC = (props) => {
			return (
				<WrapWith>
					<WrappedComponent {...props} />
				</WrapWith>
			);
		};
		cpn.displayName = `withLayout(${WrapWith.displayName})(${WrappedComponent.displayName})`;
		return cpn;
	};
};

export const withDefaultLayout = withLayout(DefaultLayout);
