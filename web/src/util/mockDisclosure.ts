import { useDisclosure } from '@chakra-ui/react';

export const mockDisclosure = (): ReturnType<typeof useDisclosure> => {
	return {
		isOpen: false,
		onOpen: () => {},
		onClose: () => {},
		onToggle: () => {},
		isControlled: false,
		getButtonProps: () => {},
		getDisclosureProps: () => {},
	};
};
