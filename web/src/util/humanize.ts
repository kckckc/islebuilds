import humanizeDuration from 'humanize-duration';

export const humanizeUntil = (time: number) =>
	humanizeDuration(time, { largest: 2, round: true });

export const humanizeAgo = (time: number) =>
	humanizeDuration(Date.now() - time, { largest: 2, round: true });

export const toDateTime = (time: number) => {
	return new Date(time).toLocaleString(undefined, {
		year: 'numeric',
		month: 'long',
		day: 'numeric',
		hour: 'numeric',
		minute: 'numeric',
		timeZoneName: 'short',
	});
};

export const toDateTimeShort = (time: number) => {
	return new Date(time).toLocaleString(undefined, {
		year: 'numeric',
		month: 'long',
		day: 'numeric',
	});
};

export const humanize = (time: number) =>
	`${toDateTime(time)} (${humanizeAgo(time)} ago)`;

interface withUTCMethods {
	getUTCHours: () => number;
	getUTCMinutes: () => number;
}
export const hhmmUTC = <T extends withUTCMethods>(t: T) => {
	let h = '' + t.getUTCHours();
	let m = '' + t.getUTCMinutes();
	return `${h.padStart(2, '0')}:${m.padStart(2, '0')}`;
};
