export const pageview = (measurementId: string, url: URL): void => {
	window.gtag('config', measurementId, {
		page_path: url,
	});
};

type GTagEvent = {
	action: string;
	category: string;
	label: string;
	value: number;
};

export const event = ({ action, category, label, value }: GTagEvent): void => {
	window.gtag('event', action, {
		event_category: category,
		event_label: label,
		value,
	});
};
