export const skinNames: Record<string, string> = {
	// Base
	wizard: 'Wizard',
	warrior: 'Warrior',
	thief: 'Thief',
	// Gaekatlan
	skinGaekatlanDruid: 'Gaekatlan Druid',
	// Valentines
	skinSjofnianHuntress: 'Sjofnian Huntress',
	skinSjofnianDeadeye: 'Sjofnian Deadeye',
	// Summer
	skinDesertMage: 'Desert Mage',
	skinDesertAssassin: 'Desert Assassin',
	skinDesertWarrior: 'Desert Warrior',
	skinLunarMage: 'Lunar Mage',
	skinLunarAssassin: 'Lunar Assassin',
	skinLunarWarrior: 'Lunar Warrior',
	// Halloween
	skinSinisterRaider: 'Sinister Raider',
	skinSinisterMarauder: 'Sinister Marauder',
	skinSinisterCorsair: 'Sinister Corsair',
	// Winter
	skinBeardedPriest: 'Bearded Priest',
	skinBeardedWizard: 'Bearded Wizard',
	skinBeardedKnight: 'Bearded Knight',
	skinGlacialKnight: 'Glacial Knight',
	skinGlacialPriest: 'Glacial Priest',
	skinGlacialWizard: 'Glacial Warrior',
	skinGlacialReaper: 'Glacial Reaper',
	// Alternate Pack
	skinThiefAlternate: 'Thief Alternate',
	skinWizardAlternate: 'Wizard Alternate',
	skinWarriorAlternate: 'Warrior Alternate',
	skinClericAlternate: 'Cleric Alternate',
	skinCleric: 'Cleric',
	skinNecromancer: 'Necromancer',
	// Pack
	skinOccultist: 'Occultist',
	skinManOfWar: 'Man of War',
	skinDiviner: 'Diviner',
	skinSorcerer: 'Sorcerer',
	skinCutthroat: 'Cutthroat',
	// Crusaders
	skinGrandCrusader: 'Grand Crusader',
	skinInfernalCrusader: 'Infernal Crusader',
	skinSteelCrusader: 'Steel Crusader',
	skinSteelCrusaderAlternate: 'Steel Crusader Alternate',
	skinCobaltCrusader: 'Cobalt Crusader',
	skinCobaltCrusaderAlternate: 'Cobalt Crusader Alternate',
	// Frozen
	skinFrozenLanceKnight: 'Frozen Lance Knight',
	skinFrozenInvoker: 'Frozen Invoker',
	// Royal Fjolgardian
	skinRoyalFjolgardianMage: 'Royal Fjolgardian Mage',
	skinRoyalFjolgardianAssassin: 'Royal Fjolgardian Assassin',
	skinRoyalFjolgardianSoldier: 'Royal Fjolgardian Soldier',
	// Misc
	skinResplendentWizard: 'Resplendent Wizard',
	skinCrossoverVeilRunner: 'Veilrunner',
	skinBluewiz: 'Bluewiz',
};
