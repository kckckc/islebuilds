import { ApolloClient, InMemoryCache } from '@apollo/client';
import { getDataFromTree } from '@apollo/client/react/ssr';
import {
	InitApolloOptions,
	withApollo as createWithApollo,
} from 'next-with-apollo';
import { StrictTypedTypePolicies } from '../generated/graphql';

export const createClient = (options: InitApolloOptions<any>) => {
	const typePolicies: StrictTypedTypePolicies = {
		Query: {
			fields: {
				recentCharacters: {
					keyArgs: ['input', ['filter']],

					merge(existing, incoming, { readField }) {
						const characters = existing
							? { ...existing.characters }
							: {};
						incoming.characters.forEach((char: any) => {
							characters[readField('id', char) as string] = char;
						});
						return {
							cursor: incoming.cursor,
							hasMore: incoming.hasMore,
							characters,
						};
					},

					read(existing) {
						if (existing) {
							return {
								cursor: existing.cursor,
								hasMore: existing.hasMore,
								characters: Object.values(existing.characters),
							};
						}
					},
				},
				characters: {
					keyArgs: ['input', ['filter']],

					merge(existing, incoming, { args }) {
						const offset = args?.input?.offset ?? 0;
						const merged = existing?.characters
							? existing.characters.slice(0)
							: [];
						for (let i = 0; i < incoming.characters.length; i++) {
							merged[offset + i] = incoming.characters[i];
						}
						return {
							cursor: incoming.cursor,
							hasMore: incoming.hasMore,
							characters: merged,
						};
					},
				},
				recentSprites: {
					keyArgs: ['input', ['filter']],

					merge(existing, incoming, { readField }) {
						const sprites = existing ? { ...existing.sprites } : {};
						incoming.sprites.forEach((spr: any) => {
							sprites[readField('id', spr) as string] = spr;
						});
						return {
							cursor: incoming.cursor,
							hasMore: incoming.hasMore,
							sprites: sprites,
						};
					},

					read(existing) {
						if (existing) {
							return {
								cursor: existing.cursor,
								hasMore: existing.hasMore,
								sprites: Object.values(existing.sprites),
							};
						}
					},
				},
				sprites: {
					keyArgs: ['input', ['filter']],

					merge(
						existing,
						incoming,
						{
							args: {
								//@ts-expect-error
								input: { offset = 0 },
							},
						}
					) {
						const merged = existing?.sprites
							? existing.sprites.slice(0)
							: [];
						for (let i = 0; i < incoming.sprites.length; i++) {
							merged[offset + i] = incoming.sprites[i];
						}
						return {
							cursor: incoming.cursor,
							hasMore: incoming.hasMore,
							sprites: merged,
						};
					},
				},
				passiveTrees: {
					keyArgs: ['input', ['spirit']],

					merge(existing, incoming) {
						const merged = existing
							? [
									...existing.passiveTrees,
									...incoming.passiveTrees,
							  ]
							: incoming.passiveTrees;
						return {
							...incoming,
							passiveTrees: merged,
						};
					},
				},
			},
		},
	};

	const cache = new InMemoryCache({
		typePolicies,
	}).restore(options.initialState || {});

	// hack
	const apiUrl =
		typeof window === 'undefined'
			? process.env.APP_API_URL_SERVER
			: // @ts-ignore
			  globalThis.apiUrl;

	const client = new ApolloClient({
		uri: apiUrl + '/api/graphql',
		credentials: 'include',
		headers: {
			cookie:
				(typeof window === 'undefined'
					? options?.headers?.cookie
					: undefined) ?? '',
		},
		cache,
	});

	return client;
};

export const withApollo = createWithApollo(
	(options: InitApolloOptions<any>) => {
		return createClient(options);
	},
	{
		getDataFromTree: getDataFromTree,
	}
);
