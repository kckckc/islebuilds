import { getPaletteColor } from './getPaletteColor';

export const downloadPixels = (
	pixels: number[][],
	size: number,
	resize: number
) => {
	// Create a new canvas
	const canvas = document.createElement('canvas');
	canvas.width = size * resize;
	canvas.height = size * resize;

	const context = canvas.getContext('2d');
	if (!context) return;

	// Write the pixel data to the canvas, resized 1x/2x/4x/etc
	for (let y = 0; y < size; y++) {
		for (let x = 0; x < size; x++) {
			const pixelColor = getPaletteColor(pixels[y][x]);
			if (!pixelColor) continue;

			context.fillStyle = pixelColor;
			context.fillRect(x * resize, y * resize, resize, resize);
		}
	}

	// Convert the canvas to a data url
	const dataUrl = canvas
		.toDataURL('image/png')
		.replace(/^data:image\/[^;]+/, 'data:application/octet-stream');

	// Create a download link and click it
	const link = document.createElement('a');
	link.download = 'sprite.png';
	link.href = dataUrl;
	document.body.appendChild(link);
	link.click();
	document.body.removeChild(link);
};
