module.exports = {
	siteUrl: 'https://islebuilds.com',
	generateRobotsTxt: true,
	exclude: ['/profile*', '/account*', '/incident*'],
};
