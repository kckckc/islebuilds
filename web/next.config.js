module.exports = {
	output: 'standalone',
	i18n: {
		locales: ['en-US'],
		defaultLocale: 'en-US',
	},
	async redirects() {
		return [
			/// Shortcuts

			{
				source: '/et',
				destination: '/timers',
				permanent: true,
			},

			/// Fixes

			// Old links had plural leaderboards
			{
				source: '/leaderboards/:path*',
				destination: '/leaderboard/:path*',
				permanent: true,
			},
			// Remove 2
			{
				source: '/waddon2',
				destination: '/waddon',
				permanent: true,
			},
			// Changed
			{
				source: '/waddon/finish',
				destination: '/waddon/connect',
				permanent: true,
			},
		];
	},
};
